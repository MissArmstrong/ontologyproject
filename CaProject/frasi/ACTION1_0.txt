You remove the seat and lift the battery holder.
You unscrew the finger screw positioned at the centre of the filter and carefully slide the filter out of its case.
You wash the filter carefully, you assemble the air filter and you rescrew the finger screw.
You put_back the battery holder and assemble the seat.