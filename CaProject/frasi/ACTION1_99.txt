Remove the pressurization: close the manual valve upstream from the TMS pressurization circuit, if present; otherwise, interrupt the flow by means of the DOTMSP bit.
Open and close the shutters to release the compressed air that has accumulated downstream from the circuit.
You power_off the laser; it is advisable to unplug the connector (after powering off) to prevent accidental movements of the shutter.
Remove the four M3x40 front screws attaching the cover
Remove the shutter cap, without damaging the shutter sliding sheet. 
You clean the dirty components using alcohol: laser glass, shutter cap, shutter sliding sheet.
Assemble the shutter cap and close, making sure the O-ring is correctly positioned.
 