1	Remove	remove	remove	VB	VB	_	_	0	0	ROOT	ROOT	Y	remove.01	_	_
2	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
3	lens	lens	lens	NN	NN	_	_	4	4	NMOD	NMOD	_	_	_	A1
4	cover	cover	cover	NN	NN	_	_	1	1	OBJ	OBJ	Y	cover.02	A1	A0
5	.	.	.	.	.	_	_	1	1	P	P	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_	_
2	clean	clean	clean	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	clean.01	_	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_	_
4	lens	lens	lens	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_	_
5	with	with	with	IN	IN	_	_	2	2	MNR	MNR	_	_	AM-MNR	_	_
6	a	a	a	DT	DT	_	_	8	8	NMOD	NMOD	_	_	_	_	_
7	soft	soft	soft	JJ	JJ	_	_	8	8	NMOD	NMOD	_	_	_	_	_
8	cloth	cloth	cloth	NN	NN	_	_	5	5	PMOD	PMOD	_	_	_	_	_
9	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_	_
10	you	you	you	PRP	PRP	_	_	11	11	SBJ	SBJ	_	_	_	A0	A0
11	put	put	put	VBP	VBP	_	_	9	9	CONJ	CONJ	Y	put.01	_	_	_
12	back	back	back	RP	RP	_	_	11	11	PRT	PRT	_	_	_	AM-DIR	_
13	the	the	the	DT	DT	_	_	15	15	NMOD	NMOD	_	_	_	_	_
14	lens	lens	lens	NN	NN	_	_	15	15	NMOD	NMOD	_	_	_	_	A1
15	cover	cover	cover	NN	NN	_	_	11	11	OBJ	OBJ	Y	cover.02	_	A1	A0
16	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0
2	assemble	assemble	assemble	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	assemble.02	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_
4	shutter	shutter	shutter	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1
5	.	.	.	.	.	_	_	2	2	P	P	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_
2	tighten	tighten	tighten	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	tighten.01	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
4	screw	screw	screw	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	A0
5	that	that	that	WDT	WDT	_	_	6	6	SBJ	SBJ	_	_	_	R-A0
6	hold	hold	hold	VBP	VBP	_	_	4	4	NMOD	NMOD	Y	hold.01	_	_
7	it	it	it	PRP	PRP	_	_	6	6	OBJ	OBJ	_	_	_	A1
8	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_	A0	_
2	assemble	assemble	assemble	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	assemble.02	_	_	_	_
3	the	the	the	DT	DT	_	_	5	5	NMOD	NMOD	_	_	_	_	_	_
4	outer	outer	outer	JJ	JJ	_	_	5	5	NMOD	NMOD	_	_	_	AM-MNR	_	_
5	cover	cover	cover	NN	NN	_	_	2	2	OBJ	OBJ	Y	cover.02	A1	A0	_	_
6	,	,	,	,	,	_	_	2	2	P	P	_	_	_	_	_	_
7	making	make	make	VBG	VBG	_	_	2	2	ADV	ADV	Y	make.04	AM-ADV	_	_	_
8	sure	sure	sure	JJ	JJ	_	_	7	7	DEP	DEP	_	_	_	_	A1	_
9	the	the	the	DT	DT	_	_	10	10	NMOD	NMOD	_	_	_	_	_	_
10	O-ring	o-ring	o-ring	NN	NN	_	_	11	11	SBJ	SBJ	_	_	_	_	_	A1
11	is	be	be	VBZ	VBZ	_	_	8	8	AMOD	AMOD	_	_	_	_	_	_
12	correctly	correctly	correctly	RB	RB	_	_	13	13	MNR	MNR	_	_	_	_	_	AM-MNR
13	positioned	position	position	VBN	VBN	_	_	11	11	PRD	PRD	Y	position.01	_	_	_	_
14	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_
2	tighten	tighten	tighten	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	tighten.01	_	_
3	the	the	the	DT	DT	_	_	5	5	NMOD	NMOD	_	_	_	_
4	four	four	four	CD	CD	_	_	5	5	NMOD	NMOD	_	_	_	_
5	screw	screw	screw	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_
6	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_
7	you	you	you	PRP	PRP	_	_	8	8	SBJ	SBJ	_	_	_	A0
8	put	put	put	VBP	VBP	_	_	6	6	CONJ	CONJ	Y	put.01	_	_
9	back	back	back	RP	RP	_	_	8	8	PRT	PRT	_	_	_	AM-DIR
10	the	the	the	DT	DT	_	_	11	11	NMOD	NMOD	_	_	_	_
11	cap	cap	cap	NN	NN	_	_	8	8	OBJ	OBJ	_	_	_	A1
12	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_

