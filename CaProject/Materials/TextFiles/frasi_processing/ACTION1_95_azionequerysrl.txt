
remove.01	the lens cover 	A1
cover.02	lens 	A1
cover.02	cover 	A0

clean.01	You 	A0
clean.01	the lens 	A1
clean.01	with a soft cloth 	AM-MNR
put.01	you 	A0
put.01	back 	AM-DIR
put.01	the lens cover 	A1
cover.02	you 	A0
cover.02	lens 	A1
cover.02	cover 	A0

assemble.02	You 	A0
assemble.02	the shutter 	A1

tighten.01	You 	A0
tighten.01	the screw that hold it 	A1
hold.01	the screw 	A0
hold.01	that 	R-A0
hold.01	it 	A1

assemble.02	You 	A0
assemble.02	the outer cover 	A1
assemble.02	making sure the O-ring is correctly positioned 	AM-ADV
cover.02	outer 	AM-MNR
cover.02	cover 	A0
make.04	You 	A0
make.04	sure the O-ring is correctly positioned 	A1
position.01	the O-ring 	A1
position.01	correctly 	AM-MNR

tighten.01	You 	A0
tighten.01	the four screw 	A1
put.01	you 	A0
put.01	back 	AM-DIR
put.01	the cap 	A1
