
disconnect.01	the terminal 	A1
remove.01	the cover ( 4 ) 	A1

unscrow.01	the screws ( 6 ) 	A1
remove.01	the retainer ( 7 ) 	A1

extract.01	carefully 	AM-MNR
extract.01	the bulb 	A1

replace.01	with an equivalent one 	A2
tighten.01	the Allen screw 	A1
tighten.01	again 	AM-TMP
scrow.01	Allen 	A1
remount.01	the retainer 	A1
remount.01	in the correct position 	AM-LOC
lock.01	with the screw 	A1

remount.01	the cover with the seal 	A1
cover.02	cover 	A0
cover.02	with the seal 	A1
connect.01	the terminal 	A1
