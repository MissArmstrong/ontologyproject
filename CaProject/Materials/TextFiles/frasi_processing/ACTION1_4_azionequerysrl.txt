

remove.01	You 	A0
remove.01	the seat 	A1
lift.01	You 	A0
lift.01	the battery holder 	A1
holder.01	battery 	A1
holder.01	holder 	A0

unscrew.01	You 	A0
unscrew.01	the finger screw positioned at the centre of the filter and carefully slide the filter out of its case 	A1
screw.01	finger 	A1
position.01	the finger screw 	A1
position.01	at the centre of the filter 	A2
centre.01	of the filter 	A1
slide.02	carefully 	AM-MNR
slide.02	the filter 	A1
slide.02	out of its case 	A3
case.01	its 	A0

wash.01	You 	A0
wash.01	the filter 	A1
wash.01	carefully 	AM-MNR
assemble.02	you 	A0
assemble.02	the air filter 	A1
filter.01	air 	A1
rescrow.01	you 	A0
rescrow.01	the finger screw 	A1
screw.01	finger 	A1

put_back.01	You 	A0
put_back.01	the battery holder 	A1
holder.01	battery 	A1
holder.01	holder 	A0
assemble.02	You 	A0
assemble.02	the seat 	A1
