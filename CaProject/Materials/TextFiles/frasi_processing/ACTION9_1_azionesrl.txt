1	Place	place	place	VB	VB	_	_	0	0	ROOT	ROOT	Y	place.01	_	_	_
2	a	a	a	DT	DT	_	_	3	3	NMOD	NMOD	_	_	_	_	_
3	container	container	container	NN	NN	_	_	1	1	OBJ	OBJ	_	_	A1	_	_
4	under	under	under	IN	IN	_	_	1	1	LOC	LOC	_	_	A2	_	_
5	the	the	the	DT	DT	_	_	6	6	NMOD	NMOD	_	_	_	_	_
6	engine	engine	engine	NN	NN	_	_	4	4	PMOD	PMOD	_	_	_	_	_
7	to	to	to	TO	TO	_	_	1	1	PRP	PRP	_	_	_	_	_
8	collect	collect	collect	VB	VB	_	_	7	7	IM	IM	Y	collect.01	_	_	_
9	the	the	the	DT	DT	_	_	10	10	NMOD	NMOD	_	_	_	_	_
10	oil	oil	oil	NN	NN	_	_	8	8	OBJ	OBJ	_	_	_	A1	A0
11	that	that	that	WDT	WDT	_	_	12	12	SBJ	SBJ	_	_	_	_	R-A0
12	escapes	escape	escape	VBZ	VBZ	_	_	10	10	NMOD	NMOD	Y	escape.01	_	_	_
13	from	from	from	IN	IN	_	_	12	12	ADV	ADV	_	_	_	_	A1
14	the	the	the	DT	DT	_	_	16	16	NMOD	NMOD	_	_	_	_	_
15	filter	filter	filter	NN	NN	_	_	16	16	NMOD	NMOD	_	_	_	_	_
16	compartment	compartment	compartment	NN	NN	_	_	13	13	PMOD	PMOD	_	_	_	_	_
17	.	.	.	.	.	_	_	1	1	P	P	_	_	_	_	_

