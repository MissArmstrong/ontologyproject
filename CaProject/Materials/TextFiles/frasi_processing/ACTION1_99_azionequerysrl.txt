
remove.01	the pressurization 	A1
close.01	Remove the pressurization : 	AM-MOD
close.01	the manual valve upstream from the TMS pressurization circuit 	A1
close.01	if present 	AM-ADV
close.01	. 	C-AM-MOD
upstream.01	manual 	AM-MNR
upstream.01	valve 	A1
upstream.01	from the TMS pressurization circuit 	A0
circuit.01	TMS 	A0
circuit.01	pressurization 	A1
interrupt.01	the flow by means of the DOTMSP bit 	A1
flow.01	by means of the DOTMSP bit 	A0
mean.01	of the DOTMSP bit 	A1

close.01	the shutters 	A1
close.01	to release the compressed air that has accumulated downstream from the circuit 	AM-PNC
release.01	the shutters 	A0
release.01	the compressed air that has accumulated downstream from the circuit 	A1
accumulate.01	the compressed air 	A1
accumulate.01	that 	R-A1
accumulate.01	downstream 	AM-MNR
accumulate.01	from the circuit 	A3

power_off.01	You 	A0
power_off.01	the laser 	A1
unplug.01	the connector ( after powering off ) to prevent accidental movements of the shutter 	A1
prevent.01	accidental movements of the shutter 	A1
movement.01	accidental 	AM-MNR
movement.01	of the shutter 	A1

remove.01	the four M3x40 front screws attaching the cover 	A1
front.02	the four 	A2
front.02	M3x40 	A2
front.02	screws attaching the cover 	C-A2
attach.01	the four M3x40 front screws 	A0
attach.01	the cover 	A1

remove.01	the shutter cap 	A1
remove.01	without damaging the shutter sliding sheet 	AM-MNR
cap.01	shutter 	A1
cap.01	cap 	A2
damage.01	the shutter sliding sheet 	A1
slide.02	the shutter 	A1
slide.02	sheet 	C-A1
sheet.02	shutter 	A1
sheet.02	sliding 	A1

clean.01	You 	A0
clean.01	the dirty components using alcohol : laser glass , shutter cap , shutter sliding sheet 	A1
component.01	dirty 	A1
use.01	the dirty components 	A0
use.01	alcohol : laser glass , shutter cap , shutter sliding sheet 	A1
cap.01	shutter 	A1
cap.01	shutter sliding sheet 	A1
slide.02	sheet 	A1

assemble.02	the shutter cap and close 	A1
assemble.02	making sure the O-ring is correctly positioned 	AM-ADV
cap.01	shutter 	A1
cap.01	cap 	A2
make.04	sure the O-ring is correctly positioned 	A1
position.01	the O-ring 	A1
position.01	correctly 	AM-MNR
