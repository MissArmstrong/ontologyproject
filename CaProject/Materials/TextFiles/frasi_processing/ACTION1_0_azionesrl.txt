1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	A0	_
2	remove	remove	remove	VB	VB	_	_	0	0	ROOT	ROOT	Y	remove.01	_	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_	_
4	seat	seat	seat	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_	_
5	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_	_
6	lift	lift	lift	VB	VB	_	_	5	5	CONJ	CONJ	Y	lift.01	_	_	_
7	the	the	the	DT	DT	_	_	9	9	NMOD	NMOD	_	_	_	_	_
8	battery	battery	battery	NN	NN	_	_	9	9	NMOD	NMOD	_	_	_	_	A1
9	holder	holder	holder	NN	NN	_	_	6	6	OBJ	OBJ	Y	holder.01	_	A1	A0
10	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_	_	_	_	_
2	unscrew	unscrew	unscrew	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	unscrew.01	_	_	_	_	_	_
3	the	the	the	DT	DT	_	_	5	5	NMOD	NMOD	_	_	_	_	_	_	_	_
4	finger	finger	finger	NN	NN	_	_	5	5	NMOD	NMOD	_	_	_	A1	_	_	_	_
5	screw	screw	screw	NN	NN	_	_	2	2	OBJ	OBJ	Y	screw.01	A1	_	A1	_	_	_
6	positioned	position	position	VBN	VBN	_	_	5	5	APPO	APPO	Y	position.01	_	_	_	_	_	_
7	at	at	at	IN	IN	_	_	6	6	LOC	LOC	_	_	_	_	A2	_	_	_
8	the	the	the	DT	DT	_	_	9	9	NMOD	NMOD	_	_	_	_	_	_	_	_
9	centre	centre	centre	NN	NN	_	_	7	7	PMOD	PMOD	Y	centre.01	_	_	_	_	_	_
10	of	of	of	IN	IN	_	_	9	9	NMOD	NMOD	_	_	_	_	_	A1	_	_
11	the	the	the	DT	DT	_	_	12	12	NMOD	NMOD	_	_	_	_	_	_	_	_
12	filter	filter	filter	NN	NN	_	_	10	10	PMOD	PMOD	_	_	_	_	_	_	_	_
13	and	and	and	CC	CC	_	_	6	6	COORD	COORD	_	_	_	_	_	_	_	_
14	carefully	carefully	carefully	RB	RB	_	_	15	15	MNR	MNR	_	_	_	_	_	_	AM-MNR	_
15	slide	slide	slide	VB	VB	_	_	13	13	CONJ	CONJ	Y	slide.02	_	_	_	_	_	_
16	the	the	the	DT	DT	_	_	17	17	NMOD	NMOD	_	_	_	_	_	_	_	_
17	filter	filter	filter	NN	NN	_	_	15	15	OBJ	OBJ	_	_	_	_	_	_	A1	_
18	out	out	out	IN	IN	_	_	15	15	DIR	DIR	_	_	_	_	_	_	A3	_
19	of	of	of	IN	IN	_	_	18	18	PMOD	PMOD	_	_	_	_	_	_	_	_
20	its	its	its	PRP$	PRP$	_	_	21	21	NMOD	NMOD	_	_	_	_	_	_	_	A0
21	case	case	case	NN	NN	_	_	19	19	PMOD	PMOD	Y	case.01	_	_	_	_	_	_
22	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_	_	_	_
2	wash	wash	wash	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	wash.01	_	_	_	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_	_	_	_
4	filter	filter	filter	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_	_	_	_
5	carefully	carefully	carefully	RB	RB	_	_	2	2	MNR	MNR	_	_	AM-MNR	_	_	_	_
6	,	,	,	,	,	_	_	2	2	P	P	_	_	_	_	_	_	_
7	you	you	you	PRP	PRP	_	_	8	8	SBJ	SBJ	_	_	_	A0	_	_	_
8	assemble	assemble	assemble	VBP	VBP	_	_	2	2	COORD	COORD	Y	assemble.02	_	_	_	_	_
9	the	the	the	DT	DT	_	_	11	11	NMOD	NMOD	_	_	_	_	_	_	_
10	air	air	air	NN	NN	_	_	11	11	NMOD	NMOD	_	_	_	_	A1	_	_
11	filter	filter	filter	NN	NN	_	_	8	8	OBJ	OBJ	Y	filter.01	_	A1	_	_	_
12	and	and	and	CC	CC	_	_	8	8	COORD	COORD	_	_	_	_	_	_	_
13	you	you	you	PRP	PRP	_	_	14	14	SBJ	SBJ	_	_	_	_	_	A0	_
14	rescrew	rescrow	rescrow	VBP	VBP	_	_	12	12	CONJ	CONJ	Y	rescrow.01	_	_	_	_	_
15	the	the	the	DT	DT	_	_	17	17	NMOD	NMOD	_	_	_	_	_	_	_
16	finger	finger	finger	NN	NN	_	_	17	17	NMOD	NMOD	_	_	_	_	_	_	A1
17	screw	screw	screw	NN	NN	_	_	14	14	OBJ	OBJ	Y	screw.01	_	_	_	A1	_
18	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	_	A0
2	put_back	put_back	put_back	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	put_back.01	_	_	_
3	the	the	the	DT	DT	_	_	5	5	NMOD	NMOD	_	_	_	_	_
4	battery	battery	battery	NN	NN	_	_	5	5	NMOD	NMOD	_	_	_	A1	_
5	holder	holder	holder	NN	NN	_	_	2	2	OBJ	OBJ	Y	holder.01	A1	A0	_
6	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_	_
7	assemble	assemble	assemble	VB	VB	_	_	6	6	CONJ	CONJ	Y	assemble.02	_	_	_
8	the	the	the	DT	DT	_	_	9	9	NMOD	NMOD	_	_	_	_	_
9	seat	seat	seat	NN	NN	_	_	7	7	OBJ	OBJ	_	_	_	_	A1
10	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_

