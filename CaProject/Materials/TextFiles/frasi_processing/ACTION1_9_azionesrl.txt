1	***standardrearlight2	***standardrearlight2	***standardrearlight2	DT	DT	_	_	0	0	ROOT	ROOT	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	A0
2	unscrew	unscrew	unscrew	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	unscrew.01	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
4	screws	screw	screw	NNS	NNS	_	_	2	2	OBJ	OBJ	_	_	A1	_
5	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_
6	remove	remove	remove	VB	VB	_	_	5	5	CONJ	CONJ	Y	remove.01	_	_
7	the	the	the	DT	DT	_	_	8	8	NMOD	NMOD	_	_	_	_
8	cover	cover	cover	NN	NN	_	_	6	6	OBJ	OBJ	_	_	_	A1
9	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_

1	***cleaningtheairfilter3	***cleaningtheairfilter3	***cleaningtheairfilter3	VBG	VBG	_	_	0	0	ROOT	ROOT	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	A0
2	remove	remove	remove	VB	VB	_	_	0	0	ROOT	ROOT	Y	remove.01	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
4	seat	seat	seat	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_
5	and	and	and	CC	CC	_	_	2	2	COORD	COORD	_	_	_	_
6	lift	lift	lift	VB	VB	_	_	5	5	CONJ	CONJ	Y	lift.01	_	_
7	the	the	the	DT	DT	_	_	8	8	NMOD	NMOD	_	_	_	_
8	battery	battery	battery	NN	NN	_	_	6	6	OBJ	OBJ	_	_	_	A1
9	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_

1	***standardrearlight2	***standardrearlight2	***standardrearlight2	DT	DT	_	_	0	0	ROOT	ROOT	_	_

1	You	you	you	PRP	PRP	_	_	2	2	SBJ	SBJ	_	_	A0	A0	A0
2	loose	loose	loose	VBP	VBP	_	_	0	0	ROOT	ROOT	Y	loose.01	_	_	_
3	the	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_	_
4	screw	screw	screw	NN	NN	_	_	2	2	OBJ	OBJ	_	_	A1	_	_
5	,	,	,	,	,	_	_	2	2	P	P	_	_	_	_	_
6	replace	replace	replace	VB	VB	_	_	2	2	COORD	COORD	Y	replace.01	_	_	_
7	the	the	the	DT	DT	_	_	8	8	NMOD	NMOD	_	_	_	_	_
8	bulb	bulb	bulb	NN	NN	_	_	6	6	OBJ	OBJ	_	_	_	A1	_
9	with	with	with	IN	IN	_	_	6	6	ADV	ADV	_	_	_	A2	_
10	an	an	an	DT	DT	_	_	12	12	NMOD	NMOD	_	_	_	_	_
11	equivalent	equivalent	equivalent	JJ	JJ	_	_	12	12	NMOD	NMOD	_	_	_	_	_
12	one	one	one	CD	CD	_	_	9	9	PMOD	PMOD	_	_	_	_	_
13	and	and	and	CC	CC	_	_	6	6	COORD	COORD	_	_	_	_	_
14	tighten	tighten	tighten	VB	VB	_	_	13	13	CONJ	CONJ	Y	tighten.01	_	_	_
15	the	the	the	DT	DT	_	_	16	16	NMOD	NMOD	_	_	_	_	_
16	screw	screw	screw	NN	NN	_	_	14	14	OBJ	OBJ	_	_	_	_	A1
17	.	.	.	.	.	_	_	2	2	P	P	_	_	_	_	_

1	Assemble	assemble	assemble	VB	VB	_	_	0	0	ROOT	ROOT	Y	assemble.02	_	_
2	the	the	the	DT	DT	_	_	3	3	NMOD	NMOD	_	_	_	_
3	cover	cover	cover	NN	NN	_	_	1	1	OBJ	OBJ	_	_	A1	_
4	and	and	and	CC	CC	_	_	1	1	COORD	COORD	_	_	_	_
5	tighten	tighten	tighten	VB	VB	_	_	4	4	CONJ	CONJ	Y	tighten.01	_	_
6	the	the	the	DT	DT	_	_	7	7	NMOD	NMOD	_	_	_	_
7	screws	screw	screw	NNS	NNS	_	_	5	5	OBJ	OBJ	_	_	_	A1
8	.	.	.	.	.	_	_	1	1	P	P	_	_	_	_

