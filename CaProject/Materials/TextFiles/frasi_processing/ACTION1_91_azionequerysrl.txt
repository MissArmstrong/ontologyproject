

unscrew.01	You 	A0
unscrew.01	the finger screw positioned at the centre of the filter and carefully slide the filter out of its case 	A1
screw.01	finger 	A1
position.01	the finger screw 	A1
position.01	at the centre of the filter 	A2
centre.01	of the filter 	A1
slide.02	carefully 	AM-MNR
slide.02	the filter 	A1
slide.02	out of its case 	A3
case.01	its 	A0

clean.01	You 	A0
clean.01	the filter 	A1
clean.01	carefully 	AM-MNR
assemble.02	you 	A0
assemble.02	the air filter 	A1
filter.01	air 	A1
tighten.01	you 	A0
tighten.01	the finger screw 	A1
screw.01	finger 	A1

put_back.01	You 	A0
put_back.01	the battery 	A1
assemble.02	You 	A0
assemble.02	the seat 	A1
