
remove.01	the shutter cap 	A1
cap.01	shutter 	A1
cap.01	cap 	A2
unscrew.01	the four screws 	A1

remove.01	the external cover 	A1
cover.02	external 	AM-MNR
cover.02	cover 	A0
open.01	then 	AM-DIS
open.01	the shutter to expose the lenses for cleaning 	A1
shutter.01	to expose the lenses for cleaning 	A1
expose.01	the lenses 	A1
expose.01	for cleaning 	A2
cleaning.01	the lenses 	A1

clean.01	You 	A0
clean.01	the lenses using compressed air 	A1
clean.01	from an air can 	A2
use.01	the lenses 	A0
use.01	compressed air 	A1

close.01	the shutter 	A1
unscrew.01	then 	AM-TMP
unscrew.01	the shutter�s screw 	A1
screw.01	shutter�s 	A1
