***cleaningtheairfilter2
You remove the seat and lift the battery.
You unscrew the finger screw positioned at the centre of the filter and carefully slide the filter out of its case.
You clean the filter carefully, you assemble the air filter and you tighten the finger screw.
You put_back the battery and assemble the seat.