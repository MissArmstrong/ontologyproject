Remove the shutter cap and unscrew the four screws.
Remove the external cover, then open the shutter to expose the lenses for cleaning.
You clean the lenses using compressed air from an air can.
Close the shutter, then unscrew the shutter�s screw.