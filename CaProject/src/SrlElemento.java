
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import general.Actor;


public class SrlElemento {
	String term; 
	int order;
	String word;
	String originalword;
	String pos;
	String type;
	
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getOriginalword() {
		return originalword;
	}

	public void setOriginalword(String originalword) {
		this.originalword = originalword;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTree() {
		return tree;
	}

	public void setTree(int tree) {
		this.tree = tree;
	}

	String posPath;
	int tree;
	
	int livello = 0;
	
	SrlElemento srlElementoAggiunto;

	
	SrlElemento_RelazioneSrl collegatoA;
	List<SrlElemento> listaAltriSoggettiCollegati; // inserisco gli altri soggetti (spesso pronomi) ai quali l'srlelemento fa da soggetto
	SrlElemento soggPrincipale; // se sono un pronome, mi collego il sogg principale
	List<SimilaritaGestoSrlElemento> listaSimGesto;
	List<SimilaritaAzioneSrlElemento> listaSimAzione;
	Contesto contesto = null;
	String wordPhrasalWordNet;

	List<SrlElemento> elementiCollegati; // creata per gestire gli SrlElementiAggiunti

	
	List<SrlElemento> listaPredicati;
	List<String> listaRelazioni;
	List<Relazione> listaRelazioniObject;
//	List<PathParole> listaPathParole;
	
//	String azione; 
	String tipo;
	
	// Aggiunte M.Giulia
	
	int numeroAzione = -1;
	int numeroVersioneFrase = -1;
	
	boolean consideraPerHeadOliva;
	
	List<SrlElemento> verbiCollegati = new LinkedList<SrlElemento>();
	List<SrlElemento> oggettiCollegati = new LinkedList<SrlElemento>();
	List<SrlElemento> soggettiCollegati = new LinkedList<SrlElemento>();
	List<SrlElemento> elementiDiLuogo = new LinkedList<SrlElemento>();
	boolean giaCollegatoAAltroElemento = false;
	Map<String, List<SrlElemento>> paroleCollegateAVerboCollegatoASoggetto;
	Map<String, List<SrlElemento>> paroleCollegateAOggettoCollegatoASoggetto;
	Actor attoreCorrispondente = null;
	
	public SrlElemento(String term, int order, String word,
			String originalword, String pos, String type, int tree) {
		this.term = term;
		this.order = order;
		this.word = word;
		this.originalword = originalword;
		this.pos = pos;
		this.type = type;
		this.tree = tree;
		this.wordPhrasalWordNet = null;
		srlElementoAggiunto = null;
		posPath = null;
		collegatoA = null;
		listaAltriSoggettiCollegati = new LinkedList<SrlElemento>();
		soggPrincipale = null;
		listaSimGesto = new LinkedList<SimilaritaGestoSrlElemento>();
		listaSimAzione = new LinkedList<SimilaritaAzioneSrlElemento>();

		paroleCollegateAVerboCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		paroleCollegateAOggettoCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		
		listaPredicati = new LinkedList<SrlElemento>();
		listaRelazioni = new LinkedList<String>();
		listaRelazioniObject = new LinkedList<Relazione>();
//		listaPathParole = new LinkedList<PathParole>();
		consideraPerHeadOliva = true;
	}
//************************************************++
//*************************************************++
//	         AGGIUNTO PER PREDICATO
	//*
	//*
//*************************************************++
	String isPred;
	
	public SrlElemento(String term, int order, String word,
			String originalword, String pos, String type, int tree, String azione, String tipo,String isPred) {
		this.term = term;
		this.order = order;
		this.word = word;
		this.originalword = originalword;
		this.pos = pos;
		this.type = type;
		this.tree = tree;
		this.isPred = isPred;
		this.wordPhrasalWordNet = null;
		srlElementoAggiunto = null;
		posPath = null;
		collegatoA = null;
		listaAltriSoggettiCollegati = new LinkedList<SrlElemento>();
		soggPrincipale = null;
		listaSimGesto = new LinkedList<SimilaritaGestoSrlElemento>();
		listaSimAzione = new LinkedList<SimilaritaAzioneSrlElemento>();
		listaPredicati = new LinkedList<SrlElemento>();
		listaRelazioni = new LinkedList<String>();
		listaRelazioniObject = new LinkedList<Relazione>();
		consideraPerHeadOliva = true;
//		this.azione = azione;
		this.tipo = tipo;
		
		paroleCollegateAVerboCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		paroleCollegateAOggettoCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();

		String temp = azione.replace("ACTION", "");
		String s[] = temp.split("_");
		this.numeroAzione = Integer.parseInt(s[0]);
		this.numeroVersioneFrase = Integer.parseInt(s[1]);
	}
	
	public SrlElemento(String term, int order) {
		this.term = term;
		this.order = order;
		this.word = null;
		this.originalword = null;
		this.pos = null;
		this.type = null;
		this.tree = 0;
		this.wordPhrasalWordNet = null;
		srlElementoAggiunto = null;
		posPath = null;
		collegatoA = null;
		listaAltriSoggettiCollegati = new LinkedList<SrlElemento>();
		soggPrincipale = null;
		listaSimGesto = new LinkedList<SimilaritaGestoSrlElemento>();
		listaSimAzione = new LinkedList<SimilaritaAzioneSrlElemento>();

		paroleCollegateAVerboCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		paroleCollegateAOggettoCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();

		listaPredicati = new LinkedList<SrlElemento>();
		listaRelazioni = new LinkedList<String>();
		listaRelazioniObject = new LinkedList<Relazione>();
//		listaPathParole = new LinkedList<PathParole>();
		consideraPerHeadOliva = true;
	}

	// costrutture creato per gestire gli SrlElementoAggiunti
	public SrlElemento(String term, String wordunita) {
		this.term = term;
		this.order = 0;
		this.word = wordunita;
		this.originalword = null;
		this.pos = null;
		this.type = null;
		this.tree = 0;
		this.wordPhrasalWordNet = null;
		srlElementoAggiunto = null;
		posPath = null;
		collegatoA = null;
		elementiCollegati = new LinkedList<SrlElemento>();
		
		paroleCollegateAVerboCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		paroleCollegateAOggettoCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();

		listaAltriSoggettiCollegati = new LinkedList<SrlElemento>();
		soggPrincipale = null;
		listaSimGesto = new LinkedList<SimilaritaGestoSrlElemento>();
		listaSimAzione = new LinkedList<SimilaritaAzioneSrlElemento>();

	}
	
	public SrlElemento(String term) {
		this.term = term;
		this.order = 0;
		this.word = null;
		this.originalword = null;
		this.pos = null;
		this.type = null;
		this.tree = 0;
		this.wordPhrasalWordNet = null;
		srlElementoAggiunto = null;
		posPath = null;
		collegatoA = null;
		listaAltriSoggettiCollegati = new LinkedList<SrlElemento>();
		soggPrincipale = null;
		listaSimGesto = new LinkedList<SimilaritaGestoSrlElemento>();
		listaSimAzione = new LinkedList<SimilaritaAzioneSrlElemento>();

		paroleCollegateAVerboCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();
		paroleCollegateAOggettoCollegatoASoggetto = new HashMap<String, List<SrlElemento>>();

		listaPredicati = new LinkedList<SrlElemento>();
		listaRelazioni = new LinkedList<String>();
		listaRelazioniObject = new LinkedList<Relazione>();
		consideraPerHeadOliva = true;
	}

	public void setAttoreCorrispondente(Actor a){
		this.attoreCorrispondente = a;
		}
	
	public Actor getAttoreCorrispondente() {
		return this.attoreCorrispondente;
	}

	public void impostaGiaCollegatoAdAltroElemento(){
		this.giaCollegatoAAltroElemento = true;
	}
	
	public boolean eGiaCollegatoAdAltroElemento(){
		return this.giaCollegatoAAltroElemento;
	}
	
	public void aggiungiVerbiCollegati(SrlElemento elemento){
		if (!verbiCollegati.contains(elemento))
			verbiCollegati.add(elemento);
	}

	public List<SrlElemento> getVerbiCollegati(){
		return verbiCollegati;
	}

	public void aggiungiOggettiCollegati(SrlElemento elemento){
		if (!oggettiCollegati.contains(elemento))
			oggettiCollegati.add(elemento);
	}

	public List<SrlElemento> getOggettiCollegati(){
		return oggettiCollegati;
	}

	public void aggiungiElementiDiLuogo(SrlElemento elemento){
		if (!elementiDiLuogo.contains(elemento))
			this.elementiDiLuogo.add(elemento);
	}

	public List<SrlElemento> getElementiDiLuogo(){
		return elementiDiLuogo;
	}

	public void aggiungiSoggettiCollegati(SrlElemento elemento){
		if (!soggettiCollegati.contains(elemento))
			soggettiCollegati.add(elemento);
	}

	public List<SrlElemento> getSoggettiCollegati(){
		return soggettiCollegati;
	}

	public void aggiungiElementiCollegati(SrlElemento elemento){
		elementiCollegati.add(elemento);
	}

	public void aggiungiSimilaritaAzione(SimilaritaAzioneSrlElemento s){
		listaSimAzione.add(s);
	}

	public void aggiungiSimilaritaGesto(SimilaritaGestoSrlElemento s){
		listaSimGesto.add(s);
	}

	
	public void addListaPredicati(SrlElemento elemento){
		listaPredicati.add(elemento);
	}

	public void addListaRelazioni(String s){
		listaRelazioni.add(s);
	}

	public void addListaRelazioniObject(Relazione r){
		listaRelazioniObject.add(r);
	}
	
	public void aggiungiSrlElementoAggiunto(SrlElemento elemento){
		srlElementoAggiunto = elemento;
	}

	//	public void addListaPathParole(PathParole p){
//		listaPathParole.add(p);
//	}
	
	public void setPosPath(String posPath){
		this.posPath = posPath;
		}
	
	public String getTipo() {
		return tipo;
	}

	public int getNumeroAzione() {
		return numeroAzione;
	}

	public int getNumeroVersioneFrase() {
		return numeroVersioneFrase;
	}

	public String stampaElemento(){
		return ""+term +" "+order+" "+word+ " "+ " "+originalword+" "+ pos+ " "+type+ " "+tree+ " "+isPred;
	}

	public String stampaElementoIS(){
		return ""+term +" "+order+" "+word+ " "+ " "+originalword+" "+ pos+ " "+type+ " "+tree+ " ACTION"+this.numeroAzione+ "_"+this.numeroVersioneFrase + " "+this.tipo;
	}
	
	public void aggiungiParoleCollegateAVerboCollegatoASoggetto(SrlElemento verbo, SrlElemento e){
		List<SrlElemento> l = null;
		if (!this.paroleCollegateAVerboCollegatoASoggetto.containsKey(verbo.word)){
			l = new LinkedList<SrlElemento>();
			this.paroleCollegateAVerboCollegatoASoggetto.put(verbo.word, l);
		}else
			l = this.paroleCollegateAVerboCollegatoASoggetto.get(verbo.word);
		l.add(e);
	}

	public void aggiungiParoleCollegateAOggettoCollegatoASoggetto(SrlElemento oggetto, SrlElemento e){
		List<SrlElemento> l = null;
		if (!this.paroleCollegateAOggettoCollegatoASoggetto.containsKey(oggetto.word)){
			l = new LinkedList<SrlElemento>();
			this.paroleCollegateAOggettoCollegatoASoggetto.put(oggetto.word, l);
		}else
			l = this.paroleCollegateAOggettoCollegatoASoggetto.get(oggetto.word);
		l.add(e);
	}

	@Override
	public String toString() {
		return "SrlElemento [word=" + word + ", originalword=" + originalword
				+ "]";
	}
	
	public int getOrder(){
		return this.order;
	}
	
}
