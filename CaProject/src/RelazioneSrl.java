
import java.util.LinkedList;
import java.util.List;


public class RelazioneSrl {
	
	
	String term;
	String predicate;
	String object;
	String relation;
	int numelementi;
	int numelementisplit;
	int idsrlfield;
	SrlElemento_RelazioneSrl collegatoA;
	List<SrlElemento> listaOggetti;

	
	
	List<Relazione> listaRelazioni;
	List<Relazione> listaRelazioniToRemove;
	Boolean trovato;
//	List<SimilaritaTraRelazioniSrl> listaSimilarita;
	Boolean haTrovatoBestMatch;
//	SimilaritaTraRelazioniSrl bestMatch;
	Boolean ripescato;
	RelazioneSrl livelloSuperioreSrl;
	RelazioneSrl livelloSuperioreSrlCopia;
	int livelloProfonditaSrl;
	int livelloProfonditaSrlCopia;
	int elevamentoPotenza;
	int livelliDiDistanzaDopoAverTrovatoMatch;
	int livelliDiDistanzaDopoAverTrovatoMatchFinale;
	List<String> objectArray;
	List<String> objectArrayTemp;
	List<String> allObjectArray;
	List<String> objectArrayToRemove;
	List<String> elencoParoleDiSrl;
	List<RelazioneSrl> relazioniLivInf;
	
//	String azione;
	String tipo;
	int numeroAzione;
	int numeroVersioneFrase;

	
	public RelazioneSrl(String term, String predicate, String object, String relation) {
		this.term = term;
		this.predicate = predicate;
		this.object = object;
		this.relation = relation;
		collegatoA = null;
		listaRelazioni = new LinkedList<Relazione>();
		listaRelazioniToRemove = new LinkedList<Relazione>();
		listaOggetti = new LinkedList<SrlElemento>();
//		listaSimilarita = new LinkedList<SimilaritaTraRelazioniSrl>();
		trovato = false;
		numelementisplit = 0;
		haTrovatoBestMatch = false;
//		bestMatch = null;
		livelloSuperioreSrl = null;
		livelloSuperioreSrlCopia = null;
		livelloProfonditaSrl = -1;
		livelloProfonditaSrlCopia = -1;
		elevamentoPotenza = 0;
		ripescato = false;
		livelliDiDistanzaDopoAverTrovatoMatch = 0;
		livelliDiDistanzaDopoAverTrovatoMatchFinale = 0;
		objectArray = new LinkedList<String>();
		objectArrayTemp = new LinkedList<String>();
		allObjectArray = new LinkedList<String>();
		objectArrayToRemove = new LinkedList<String>();
		elencoParoleDiSrl = new LinkedList<String>();
		relazioniLivInf = new LinkedList<RelazioneSrl>();
	}
	
	public RelazioneSrl(String term, String predicate, String object, String relation, int idsrlfield) {
		this.term = term;
		this.predicate = predicate;
		this.object = object;
		this.relation = relation;
		this.idsrlfield = idsrlfield;
		collegatoA = null;

		listaRelazioni = new LinkedList<Relazione>();
		listaRelazioniToRemove = new LinkedList<Relazione>();
		listaOggetti = new LinkedList<SrlElemento>();
//		listaSimilarita = new LinkedList<SimilaritaTraRelazioniSrl>();
		trovato = false;
		numelementisplit = 0;
		haTrovatoBestMatch = false;
//		bestMatch = null;
		livelloSuperioreSrl = null;
		livelloSuperioreSrlCopia = null;
		livelloProfonditaSrl = -1;
		livelloProfonditaSrlCopia = -1;
		elevamentoPotenza = 0;
		ripescato = false;
		livelliDiDistanzaDopoAverTrovatoMatch = 0;
		livelliDiDistanzaDopoAverTrovatoMatchFinale = 0;
		objectArray = new LinkedList<String>();
		objectArrayTemp = new LinkedList<String>();
		allObjectArray = new LinkedList<String>();
		objectArrayToRemove = new LinkedList<String>();
		elencoParoleDiSrl = new LinkedList<String>();
		relazioniLivInf = new LinkedList<RelazioneSrl>();
	}
	
	public RelazioneSrl(String term, String predicate, String object, String relation, int idsrlfield, String azione, String tipo) {
		this.term = term;
		this.predicate = predicate;
		this.object = object;
		this.relation = relation;
		this.idsrlfield = idsrlfield;
		collegatoA = null;
		this.tipo = tipo;
		
		String temp = azione.replace("ACTION", "");
		String s[] = temp.split("_");
		this.numeroAzione = Integer.parseInt(s[0]);
		this.numeroVersioneFrase = Integer.parseInt(s[1]);

		listaRelazioni = new LinkedList<Relazione>();
		listaRelazioniToRemove = new LinkedList<Relazione>();
		listaOggetti = new LinkedList<SrlElemento>();
//		listaSimilarita = new LinkedList<SimilaritaTraRelazioniSrl>();
		trovato = false;
		numelementisplit = 0;
		haTrovatoBestMatch = false;
//		bestMatch = null;
		livelloSuperioreSrl = null;
		livelloSuperioreSrlCopia = null;
		livelloProfonditaSrl = -1;
		livelloProfonditaSrlCopia = -1;
		elevamentoPotenza = 0;
		ripescato = false;
		livelliDiDistanzaDopoAverTrovatoMatch = 0;
		livelliDiDistanzaDopoAverTrovatoMatchFinale = 0;
		objectArray = new LinkedList<String>();
		objectArrayTemp = new LinkedList<String>();
		allObjectArray = new LinkedList<String>();
		objectArrayToRemove = new LinkedList<String>();
		elencoParoleDiSrl = new LinkedList<String>();
		relazioniLivInf = new LinkedList<RelazioneSrl>();
	}
	
	/*
	public void calcolaNumElementiSplit(){
		object = object.replace(" .", ".");
		object = object.replace(" ,", ",");
		object = object.replace(" ;", ";");
		object = object.replace(" :", ":");
		object = object.replace("( ", "(");
		object = object.replace(" )", ")");
		object = object.replace(" \" ", " \"");
		object = object.replace(" ' ", " '");
		String arr[] = object.split(" ");
		numelementisplit = arr.length;
	}

	public void spezzaStringa(){
		// 1) recupero tutte le relazioni collegate a questa relazionesrl
		// 2) mi salvo i termini (oggetto.term oppure oggetto.word)
		for (Relazione rel : this.listaRelazioni){
			String parola = rel.oggetto.word;
			addObjectArray(parola); // aggiungo la parola all'elenco delle parole da eliminare
		}
		objectArrayToRemove.addAll(objectArray);
	}

	
	public void addObjectArray(String s){
		objectArray.add(s);
		allObjectArray.add(s);
	}
	
	
	public boolean haRelazioneSrlALivSuperiore (RelazioneSrl rel){
		if (this.livelloSuperioreSrlCopia != null){
			if (this.livelloSuperioreSrlCopia.equals(rel))
				return true;
			else
				this.livelloSuperioreSrlCopia.haRelazioneSrlALivSuperiore(rel);
		}
		else
			return false;
		return false;
	}
	
	public void eliminaParoleSeHoBestMatch(List<String> oA){
		List<String> giaTrovati = new LinkedList<String>();
		if (livelloSuperioreSrlCopia != null){
//			System.out.println("livello attuale "+this.stampaRelazione()+" - sup - "+livelloSuperioreSrlCopia.stampaRelazione()+" "+oA);
			livelloSuperioreSrlCopia.removeObjectArrayNuovo(oA, giaTrovati);
			livelloSuperioreSrlCopia.eliminaParoleSeHoBestMatch(oA);
		}
	}

	
	public void removeObjectArrayStopWords(){
		List<SimilaritaTraRelazioni> simreltemp = new LinkedList<SimilaritaTraRelazioni>();
		for (SimilaritaTraRelazioniSrl simrelsrl : this.listaSimilarita){ 
			simreltemp.addAll(simrelsrl.simRelCopia); 
			for (SimilaritaTraRelazioni simrel : simreltemp){
				Relazione io = null;
				Relazione altra = null;
				if (simrel.relazione1.relazioneSrl.equals(this)){
					io = simrel.relazione1;
					altra = simrel.relazione2;
				}
				else if (simrel.relazione2.relazioneSrl.equals(this)){
					io = simrel.relazione2;
					altra = simrel.relazione1;
				}
				if (io !=null && (io.oggetto.pos.compareTo("DT") == 0 || io.oggetto.pos.compareTo("IN") == 0) ){
					simrelsrl.simRelCopia.remove(simrel);
				}
				if (altra !=null && (altra.oggetto.pos.compareTo("DT") == 0 || altra.oggetto.pos.compareTo("IN") == 0) ){
					simrelsrl.simRelCopia.remove(simrel);
				}
			}
		}
	}

	
	
	public void removeObjectArrayNuovo(List<String> s, List<String> giaTrovati){
		List<SimilaritaTraRelazioni> simreltemp = new LinkedList<SimilaritaTraRelazioni>();
		for (String string1 : s){
			for (SimilaritaTraRelazioniSrl simrelsrl : this.listaSimilarita){ // il problema e' a questa riga.. .involve - in the spread of other forms of cancer non va
				simreltemp.clear();
				simreltemp.addAll(simrelsrl.simRelCopia); // copio per evitare concurrent modification
				for (SimilaritaTraRelazioni simrel : simreltemp){
					Relazione io = null;
					Relazione altra = null;
					if (simrel.relazione1.relazioneSrl.equals(this)){
						io = simrel.relazione1;
						altra = simrel.relazione2;
					}
					else if (simrel.relazione2.relazioneSrl.equals(this)){
						io = simrel.relazione2;
						altra = simrel.relazione1;
					}
					if (io !=null && io.oggetto.word.compareTo(string1) == 0 && !giaTrovati.contains(io.predicato.word+"_"+io.oggetto.word+"+"+altra.predicato.word+"_"+altra.oggetto.word)){
						giaTrovati.add(io.predicato.word+"_"+io.oggetto.word+"+"+altra.predicato.word+"_"+altra.oggetto.word);
						simrelsrl.simRelCopia.remove(simrel);
//						System.out.println("********************Ho rimosso "+simrel.relazione1.stampa()+" "+simrel.relazione2.stampa());
					}
				}
			}
		}
		
		removeObjectArray(s);
	}
	
	public void removeObjectArray(List<String> s){
		List<String> listatemp = new LinkedList<String>();
		listatemp.addAll(objectArrayToRemove);
		for (String string1 : s)
			for (String string : listatemp)
				if (string.compareTo(string1) == 0){
					objectArray.remove(string1);
					objectArrayToRemove.add(string1); //verificare!!!
					break;
				}
	}
	
	public void addListaRelazioni(Relazione s){
		listaRelazioni.add(s);
	}

	public void addListaOggetti(SrlElemento s){
		listaOggetti.add(s);
	}

	public void addListaSimilaritaTraRelazioniSrl(SimilaritaTraRelazioniSrl s){
		listaSimilarita.add(s);
	}
	
	public void calcolaSimilaritaParti(){
		// lista di relazioni collegate a relazionesrl
		// per ognuna di queste devo trovare il best match
		List<Relazione> listaRelazioni = this.listaRelazioni;
		
		double maxSimilarity = 0.0;
		SimilaritaTraRelazioni maxSimRel = null;
		for (Relazione r : listaRelazioni){
			List<SimilaritaTraRelazioni> ltemp = r.listasimilarita;
			for (SimilaritaTraRelazioni s : ltemp){
				if ((s.relazione1.bestMatch == null) && (s.relazione2.bestMatch == null)){
					if (s.similarita>maxSimilarity){
						maxSimilarity = s.similarita;
						maxSimRel = s;
					}
				}
			}
		}
		if (maxSimRel!=null){
			maxSimRel.relazione1.bestMatch = maxSimRel;
			maxSimRel.relazione2.bestMatch = maxSimRel;
			System.err.println("similarita tra "+maxSimRel.relazione1.predicato.word+ " "+maxSimRel.relazione1.relazione+ " "+maxSimRel.relazione1.oggetto.word+ " e "+maxSimRel.relazione2.predicato.word+ " "+maxSimRel.relazione2.relazione+ " "+maxSimRel.relazione2.oggetto.word+ " "+maxSimRel.similarita);
			calcolaSimilaritaParti();
		}
		else
			return;
	}
	
	public void calcolaSimilaritaAll(){
		List<Relazione> paroler1 = this.listaRelazioni;
		List<Relazione> paroler2 = null;
		double maxSimilarity = 0.0;
		SimilaritaTraRelazioni maxSimRel = null;
		for (SimilaritaTraRelazioniSrl srl : this.listaSimilarita){
			if (srl.relazionesrl1.equals(this)){
				paroler2 = srl.relazionesrl2.listaRelazioni;
			}
			else{
				paroler2 = srl.relazionesrl1.listaRelazioni;
			}
			for (Relazione r1 : paroler1){
				for (Relazione r2: paroler2){
					List<SimilaritaTraRelazioni> ltemp = r1.listasimilarita;
					for (SimilaritaTraRelazioni stemp : ltemp){
						if ((stemp.relazione1.equals(r1) && stemp.relazione2.equals(r2)) || (stemp.relazione1.equals(r2) && stemp.relazione2.equals(r1))){
							if ((stemp.relazione1.bestMatch == null) && (stemp.relazione2.bestMatch == null)){
								if (stemp.similarita > maxSimilarity){
									maxSimilarity = stemp.similarita;
									maxSimRel = stemp;
								}
							}
						}
					}
				}
			}
			if (maxSimRel!=null){
				maxSimRel.relazione1.bestMatch = maxSimRel;
				maxSimRel.relazione2.bestMatch = maxSimRel;
				srl.newpercentuale += maxSimilarity;
				System.err.println("similarita tra "+maxSimRel.relazione1.predicato.word+ " "+maxSimRel.relazione1.relazione+ " "+maxSimRel.relazione1.oggetto.word+ " e "+maxSimRel.relazione2.predicato.word+ " "+maxSimRel.relazione2.relazione+ " "+maxSimRel.relazione2.oggetto.word+ " "+maxSimRel.similarita);
				calcolaSimilaritaParti();
			}
			else
				return;
		}
	}
	
	public void oldcalcolaSimilaritaParti(){
		// lista di relazioni collegate a relazionesrl
		// per ognuna di queste devo trovare il best match
		List<Relazione> listaRelazioni = this.listaRelazioni;
		
		double maxSimilarity = 0.0;
		SimilaritaTraRelazioni maxSimRel = null;
		SimilaritaTraRelazioniSrl maxSimRelSrl = null;

		for (SimilaritaTraRelazioniSrl srl : listaSimilarita){
			List<Relazione> listathis = null;
//			List<Relazione> listaaltro = null;
			if (srl.relazionesrl1.equals(this)){
				listathis = srl.relazionesrl1.listaRelazioni;
//				listaaltro = srl.relazionesrl2.listaRelazioni;
			}
			else{
				listathis = srl.relazionesrl2.listaRelazioni;
//				listaaltro = srl.relazionesrl1.listaRelazioni;
			}
			
			for (Relazione r1 : listathis){
//				for (Relazione r2 : listaaltro){
				List<SimilaritaTraRelazioni> ltemp = r1.listasimilarita;
				for (SimilaritaTraRelazioni s : ltemp){
					if ((s.relazione1.bestMatch == null) && (s.relazione2.bestMatch == null)){
						if (s.similarita>maxSimilarity){
							maxSimilarity = s.similarita;
							maxSimRel = s;
						}
					}
				}
				
				
				
				
//				}
			}
				
		}
		
		for (Relazione r : listaRelazioni){
			List<SimilaritaTraRelazioni> ltemp = r.listasimilarita;
			for (SimilaritaTraRelazioni s : ltemp){
				if ((s.relazione1.bestMatch == null) && (s.relazione2.bestMatch == null)){
					if (s.similarita>maxSimilarity){
						maxSimilarity = s.similarita;
						maxSimRel = s;
					}
				}
			}
		}
		if (maxSimRel!=null){
			maxSimRel.relazione1.bestMatch = maxSimRel;
			maxSimRel.relazione2.bestMatch = maxSimRel;
			
			calcolaSimilaritaParti();
		}
		else
			return;
	}

	

	public void setLivelloSuperioreCopia(RelazioneSrl r){
		livelloSuperioreSrlCopia = r;
	}

	public ElencoParoleDaRimuovere calcolaObjectArrayBasatoSuRelazInferiori(RelazioneSrl altraSrl) {
		List<String> paroleIo = new LinkedList<String>();
		List<String> paroleAltra = new LinkedList<String>();
		RelazioneSrl altraRelaz = null;
		for (RelazioneSrl inf : this.relazioniLivInf){
			if (inf.bestMatch != null){
				paroleIo.addAll(inf.allObjectArray);
				
				if (inf.bestMatch.relazionesrl1.equals(altraSrl) && inf.bestMatch.relazionesrl2.equals(altraSrl) ){
					// cioe', se tra i best match di chi e' sotto di me non e' presente la relaz con la quale mi sto confrontando (altrimenti eliminerei tutte le parole) 
					if (inf.bestMatch.relazionesrl1.equals(inf))
						altraRelaz = inf.bestMatch.relazionesrl2;
					else if (inf.bestMatch.relazionesrl2.equals(inf))
						altraRelaz = inf.bestMatch.relazionesrl1;
					paroleAltra.addAll(altraRelaz.allObjectArray);
				}
			}
		}
		
		ElencoParoleDaRimuovere e = new ElencoParoleDaRimuovere(paroleIo, paroleAltra);
		return e;
	}
	
	public List<String> calcolaParoleDaConsiderarePerIlMatch(List<String> paroleDaRimuovere){
		List<String> paroleDaConsiderarePerIlMatch = new LinkedList<String>();
		paroleDaConsiderarePerIlMatch.addAll(this.allObjectArray);
		for (String s : this.allObjectArray){
			for (String inf : paroleDaRimuovere){
				if (s.compareTo(inf) == 0){
					paroleDaConsiderarePerIlMatch.remove(s);
					break;
				}
			}
		}
		this.objectArrayTemp = paroleDaConsiderarePerIlMatch;
		return paroleDaConsiderarePerIlMatch;
	}
*/	
	public void setLivelloSuperiore(RelazioneSrl r){
		livelloSuperioreSrl = r;
		// aggiungo anche al contrario (a una RelazioneSrl le relazioni che contiene, o che ha a livello inferiore)
		r.addLivelloInferiore(this);
	}
	
	public void addLivelloInferiore(RelazioneSrl r){
		this.relazioniLivInf.add(r);
	}


	public String stampaRelazione(){
		return predicate+ " "+relation+" "+object;
	}

	@Override
	public String toString() {
		return "RelazioneSrl [term=" + term + ", elencoParoleDiSrl="
				+ elencoParoleDiSrl + ", numeroAzione=" + numeroAzione + "]";
	}


}
