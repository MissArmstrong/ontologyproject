

public class RelazioniStanfordParser {
	StanfordParser primo; 
	StanfordParser secondo;
	String tipoRelazione;
	String tipoRelazioneProcessato;
	
	public RelazioniStanfordParser(StanfordParser primo, StanfordParser secondo, String tipoRelazione) {
		this.primo = primo;
		this.secondo = secondo;
		this.tipoRelazione = tipoRelazione;
	}
	
	public String stampa(){
		if (primo != null && secondo != null)
			return ""+primo.stampa()+ " "+secondo.stampa()+ " "+tipoRelazione;
		else
			if (primo == null && secondo != null)
				return "ROOT-0 "+secondo.stampa()+ " "+tipoRelazione;
			else
			if (primo != null && secondo == null)
				return ""+primo.stampa()+ " ROOT-0 "+tipoRelazione;
		return "ROOT-0 ROOT-0 "+tipoRelazione;
				
	}
}
