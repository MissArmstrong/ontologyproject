


public class TriplaSoggettoPredicato extends Tripla {
	StanfordParser soggetto;
	StanfordParser predicato;
	StanfordParser aCompPredicato;
	StanfordParser aModSoggetto;
	StanfordParser soggettoDiPronome;
	
	public TriplaSoggettoPredicato(StanfordParser soggetto, StanfordParser predicato) {
		super("soggettoPredicato");
		this.soggetto = soggetto;
		this.predicato = predicato;
		aCompPredicato = null;
	}
	
	public String stampa(){
		String stampa = "soggettoPredicato ";
		if (soggettoDiPronome != null)
			stampa += "("+soggettoDiPronome.word+")";
		if (aModSoggetto != null)
			stampa += " "+aModSoggetto.stampa();
		stampa += " "+soggetto.stampa() ;
		if (aCompPredicato != null)
			stampa += " "+aCompPredicato.stampa();
		stampa += predicato.stampa()+ " "+super.relazione;
		return stampa;
	}
	
	public void setACompPredicato(StanfordParser aCompPredicato){
		this.aCompPredicato = aCompPredicato;
	}

	public void setAModSoggetto(StanfordParser aModSoggetto){
		this.aModSoggetto = aModSoggetto;
	}

	public void setSoggettoDiPronome(StanfordParser soggettoDiPronome){
		this.soggettoDiPronome = soggettoDiPronome;
	}

}
