
import java.util.List;


public class Frase {
	
	List<String> contesto;
	List<String> descrizioneAzione;
	String azioneNumero; // numero dell'azione testata (AZIONE1_1, etc.)
	
	public Frase(List<String> contesto, List<String> descrizioneAzione, String azioneNumero) {
		this.contesto = contesto;
		this.descrizioneAzione = descrizioneAzione;
		this.azioneNumero = azioneNumero;
	}

	public Frase(List<String> descrizioneAzione, String azioneNumero) {
		this.descrizioneAzione = descrizioneAzione;
		this.azioneNumero = azioneNumero;
	}

	public List<String> getDescrizioneAzione() {
		return descrizioneAzione;
	}

	public void setDescrizioneAzione(List<String> descrizioneAzione) {
		this.descrizioneAzione = descrizioneAzione;
	}

	public String getAzioneNumero() {
		return azioneNumero;
	}

	public void setAzioneNumero(String azioneNumero) {
		this.azioneNumero = azioneNumero;
	}

	@Override
	public String toString() {
		return "Frase :"+ azioneNumero+ " "  + descrizioneAzione ;
	}
	
	
}
