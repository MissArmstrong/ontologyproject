
import java.util.Comparator;


public class SimilaritaGestoAzioneComparator implements Comparator<SimilaritaGestoAzione>{

	@Override
	public int compare(SimilaritaGestoAzione s1, SimilaritaGestoAzione s2) {
		return (int) - (s1.similarita * 1000 - s2.similarita * 1000);
	}

}
