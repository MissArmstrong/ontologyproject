


import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class NewMultiThreadChatClient extends Thread { // ho tolto implements Runnable
	  private DataOutputStream dOut = null;
	  private Temp temp;
	  private PrintStream os = null;

	  
	  private static Socket clientSocketBlender;
	  

  public NewMultiThreadChatClient(Socket clientSocketBlender, Temp temp) {
		this.clientSocketBlender = clientSocketBlender;
		this.temp = temp;
	}

/*
   * Create a thread to read from the server. (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  public void run() {
    /*
     * Keep on reading from the socket till we receive "Bye" from the
     * server. Once we received that then we want to break.
     */

    try {
        os = new PrintStream(clientSocketBlender.getOutputStream());
    	dOut = new DataOutputStream(clientSocketBlender.getOutputStream());
    	
    	
    	while(true){
    		synchronized (temp){
        		if (temp.ris != null){
        			temp.ris+= "|";
        	    	byte[] message = temp.ris.getBytes("UTF-8") ;
        			dOut.write(message); // genera errore, lo devo decommentare
        			System.out.println("inviato messaggio di lunghezza: "+message.length+ " "+message+ " "+temp.ris);
        			temp.ris = null;
//          		    this.os.println("test : "+temp.ris);

        		}
        		if (temp.ris != null && app.risposta.compareTo("*** Bye") == 0){
        			break;
        		}
    			
    		}
    	}

    	
    	/*
      os = new PrintStream(clientSocketBlender.getOutputStream());
      
      // non devo stampare, bensi' inviare dati
      while ((responseLine = is.readLine()) != null) {
        System.out.println(responseLine);
        if (responseLine.indexOf("*** Bye") != -1)
          break;
      }*/
      
      app.closed = true;
      app.clientSocketBlender.close();
    } catch (IOException e) {
      System.err.println("IOException:  " + e);
    }
  }
}