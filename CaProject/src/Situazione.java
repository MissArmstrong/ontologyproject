
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Situazione {
	private Map<String, List<SrlElemento>> elementiContesto;
	private Map<String, List<SrlElemento>> elementiAzione;
	private Map<String, ElementoSituazioneAzione> simElementiSituazioneAzione;
	private int numero;
	
	private Map<String, List<RelazioneSrl>> relazioniAzione;
	private Map<String, List<RelazioneSrl>> relazioniContesto;

//	private List<RelazioneSrl> listaRelazioniTermineAzione = new LinkedList<RelazioneSrl>();
//	private List<RelazioneSrl> listaRelazioniTermineContesto = new LinkedList<RelazioneSrl>();

	
	public Situazione(int numero) {
		this.elementiContesto = new TreeMap<String, List<SrlElemento>>();
		this.elementiAzione = new TreeMap<String, List<SrlElemento>>();
		this.relazioniAzione = new TreeMap<String, List<RelazioneSrl>>();
		this.relazioniContesto = new TreeMap<String, List<RelazioneSrl>>();;
		this.simElementiSituazioneAzione = new TreeMap<String, ElementoSituazioneAzione>();;
		
		this.numero = numero;
	}


//	public void addElementoContesto(SrlElemento elemento) {
//		if (elementiContesto.containsKey(elemento.term))
//			elementiContesto.get(elemento.term).add(elemento);
//		else{
//			List<SrlElemento> ltemp = new LinkedList<SrlElemento>();
//			ltemp.add(elemento);
//			elementiContesto.put(elemento.term, ltemp);
//		}
//	}

	public Map<String, ElementoSituazioneAzione> getSimElementiSituazioneAzione() {
		return simElementiSituazioneAzione;
	}

//	public Map<String, List<SrlElemento>> getElementiAzione() {
//		return elementiAzione;
//	}
//
//	public Map<String, List<RelazioneSrl>> getRelazioniAzione() {
//		return relazioniAzione;
//	}
//
//	public Map<String, List<RelazioneSrl>> getRelazioniContesto() {
//		return relazioniContesto;
//	}

	public void addElemento(SrlElemento elemento) {
		String tipo = elemento.getTipo();
		String term = elemento.term;
		String stringa = tipo+"_"+term;
		
		if (simElementiSituazioneAzione.containsKey(stringa))
			simElementiSituazioneAzione.get(stringa).addElemento(elemento);
		else{
			ElementoSituazioneAzione e = new ElementoSituazioneAzione(this, tipo);
			simElementiSituazioneAzione.put(stringa, e);
			e.addElemento(elemento);
		}
	}

	public int getNumero() {
		return numero;
	}
	
	public void addRelazione(RelazioneSrl r){
		String tipo = r.tipo;
		String term = r.term;
		String stringa = tipo+"_"+term;
		
		if (simElementiSituazioneAzione.containsKey(stringa))
			simElementiSituazioneAzione.get(stringa).addRelazione(r);
		else{
			ElementoSituazioneAzione e = new ElementoSituazioneAzione(this, tipo);
			simElementiSituazioneAzione.put(stringa, e);
			e.addRelazione(r);
		}
	}

//	public void addRelazioneContesto(RelazioneSrl r){
//		if (relazioniContesto.containsKey(r.term))
//			relazioniContesto.get(r.term).add(r);
//		else{
//			List<RelazioneSrl> ltemp = new LinkedList<RelazioneSrl>();
//			ltemp.add(r);
//			relazioniContesto.put(r.term, ltemp);
//		}
//	}
	

	/*
	public void addRelazioneTermineAzione(RelazioneSrl r){
		this.listaRelazioniTermineAzione.add(r);
	}

	public void addRelazioneTermineContesto(RelazioneSrl r){
		this.listaRelazioniTermineContesto.add(r);
	}
	
	public List<RelazioneSrl> getListaRelazioniTermineAzione(){
		return this.listaRelazioniTermineAzione;
	}

	public List<RelazioneSrl> getListaRelazioniTermineContesto(){
		return this.listaRelazioniTermineContesto;
	}*/
}
