

public class ElementoParlato {
	String soggetto;
	String copione;
	
	public ElementoParlato(String soggetto, String copione) {
		this.soggetto = soggetto;
		this.copione = copione;
	}
	
	public String stampa(){
		return soggetto + " " + copione;
	}
}
