
import java.util.Comparator;


public class SrlElementoComparatorPerTerm implements Comparator<SrlElemento>{

	@Override
	public int compare(SrlElemento e1, SrlElemento e2) {
		int i1 = Integer.parseInt(e1.term);
		int i2 = Integer.parseInt(e2.term);

		if (i1 != i2)
			return i1 - i2;
		return e1.order - e2.order;
	}

}
