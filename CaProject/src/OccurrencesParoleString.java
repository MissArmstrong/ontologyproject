

public class OccurrencesParoleString {
	String parola1;
	String parola2; 
	long occurrences;
	
	public OccurrencesParoleString(String parola1, String parola2, long occurrences) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		this.occurrences = occurrences;
	}
	
}
