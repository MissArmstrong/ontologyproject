
import java.util.ArrayList;
import java.util.List;


public class RisultatoTriplaOriginale {
	


	private SrlElemento srlVerbo  = null;
	private SrlElemento srlOggetto = null;
	private SrlElemento srlStrumento = null;
	private SrlElemento srlPosizione = null;
	private SrlElemento srlManiera = null;
	
	// Add for integrate the old code with the new one
	private String nameFile = "";
	
	private String nomeBOM= null;
	private List<SrlElemento> srlList;
		
	public RisultatoTriplaOriginale(SrlElemento srlVerbo,
			SrlElemento srlOggettoSuCuiAgire, SrlElemento srlOggettoConCuiAgire, SrlElemento srlPosizione, SrlElemento srlManiera) {
		super();
		this.srlVerbo = srlVerbo;
		this.srlOggetto = srlOggettoSuCuiAgire;
		this.srlStrumento = srlOggettoConCuiAgire;
		this.srlPosizione = srlPosizione;
		this.srlManiera = srlManiera;
		this.srlList = new ArrayList<SrlElemento>(); 
		this.srlList.add(this.srlVerbo);
		this.srlList.add(this.srlOggetto);
		this.srlList.add(this.srlStrumento);
		this.srlList.add(this.srlPosizione);
		this.srlList.add(this.srlManiera);
	}

	public RisultatoTriplaOriginale(SrlElemento srlVerbo,
			SrlElemento srlOggettoSuCuiAgire, SrlElemento srlOggettoConCuiAgire,
			SrlElemento srlPosizione, SrlElemento srlManiera,String nomeBOM) {
		super();
		this.srlVerbo = srlVerbo;
		this.srlOggetto = srlOggettoSuCuiAgire;
		this.srlStrumento = srlOggettoConCuiAgire;
		this.srlPosizione = srlPosizione;
		this.srlManiera = srlManiera;
		this.srlList = new ArrayList<SrlElemento>(); 
		this.srlList.add(this.srlVerbo);
		this.srlList.add(this.srlOggetto);
		this.srlList.add(this.srlStrumento);
		this.srlList.add(this.srlPosizione);
		this.srlList.add(this.srlManiera);
		this.nomeBOM = nomeBOM;
	}
	
	// Add for integrate the old code with the new one
	public void setNameFile(String nameFile){
		this.nameFile = nameFile;
	}
	
	public String getNameFile(){
		return nameFile;
	}

	public SrlElemento getSrlVerbo() {
		return srlVerbo;
	}

	public SrlElemento getSrlOggettoSuCuiAgire() {
		return srlOggetto;
	}

	public SrlElemento getSrlOggettoConCuiAgire() {
		return srlStrumento;
	}
	
	public SrlElemento getSrlPosizione() {
		return srlPosizione;
	}
	public SrlElemento getSrlManiera() {
		return srlManiera;
	}
	
	public List<SrlElemento> getSrlList() {
		return srlList;
	}
	public String getNomeBOM(){
		return nomeBOM;
	}
	@Override
	public String toString() {
		return ""+ srlVerbo.toString()+"\n"+ srlOggetto.toString()+ "\n"+ srlStrumento.toString()
				+"\n"+ srlPosizione.toString()+ "\n"+ srlManiera.toString();
	}
    
	public String stampaSoloParole() {
		String s = "";
		if(nomeBOM != null && !nomeBOM.equals("")){
			return s+= nomeBOM;
		}else
			s+="NO_BOM ";
		if(srlVerbo != null){
			s +=srlVerbo.getWord();
		}else
			s+="NO_VERB";
		if(srlOggetto != null){
			s+=" "+ srlOggetto.getWord();
		}else
			s+=" NO_OBJ";
		if(srlStrumento != null){
			s+=" "+ srlStrumento.getWord();
		}else
			s+=" NO_STRUM";
		if(srlPosizione != null){
			s+=" "+ srlPosizione.getWord();
		}else
			s+=" NO_LOC";
		if(srlManiera != null){
			s+=" "+ srlManiera.getWord();
		}else
			s+=" NO_MANIERA";
//		if(nomeBOM != null && !nomeBOM.equals("")){
//			s+=" "+ nomeBOM;
//		}else
//			s+=" NO_BOM";
//		return ""+ srlVerbo.getWord() +" "+ srlOggetto.getWord() + " "+ srlStrumento.getWord();//se uno e nullo da null pointer aggiungere controllo
		return s;
	}
	
	
//	String verbo  = "";
//	String oggettoSuCuiAgire = "";
//	String oggettoConCuiAgire = "";
//	
//	public RisultatoTriplaUmana(String verbo, String oggettoSuCuiAgire,
//			String oggettoConCuiAgire) {
//		super();
//		this.verbo = verbo;
//		this.oggettoSuCuiAgire = oggettoSuCuiAgire;
//		this.oggettoConCuiAgire = oggettoConCuiAgire;
//	}
	
//	public String getVerbo() {
//		return verbo;
//	}
//
//	public String getOggettoSuCuiAgire() {
//		return oggettoSuCuiAgire;
//	}
//
//	public String getOggettoConCuiAgire() {
//		return oggettoConCuiAgire;
//	}
//
//	@Override
//	public String toString() {
//		return ""+ verbo +" "+ oggettoSuCuiAgire + " "+ oggettoConCuiAgire;
//	}

	
}
