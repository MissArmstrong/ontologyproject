

public class PathParoleString {
	String parola1;
	String parola1pos;
	String parola2; 
	String parola2pos;
	double path;
	
	public PathParoleString(String parola1, String parola2, double path) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		this.path = path;
	}

	public PathParoleString(String parola1, String parola2, double path, String parola1pos, String parola2pos) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		this.path = path;
		this.parola1pos = parola1pos;
		this.parola2pos = parola2pos;
	}

	
}
