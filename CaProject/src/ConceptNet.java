

public class ConceptNet {
	String parola; 
	int num;
	String startword;
	String startpos;
	String startdef;
	String relazione;
	String endword;
	String endpos;
	String enddef;
	Double score;
	
	public ConceptNet(String parola, int num, String startword, String startpos, String startdef, String relazione, String endword, String endpos, String enddef, Double score) {
		this.parola = parola;
		this.num = num;
		this.startword = startword;
		this.startpos = startpos;
		this.startdef = startdef;
		this.relazione = relazione;
		this.endword = endword;
		this.endpos = endpos;
		this.enddef = enddef;
		this.score = score;
	}
}
