

public class SimilaritaGestoSrlElemento {
	Gesto gesto;
	SrlElemento srlElemento;
	Double similarita;

	public SimilaritaGestoSrlElemento(Gesto gesto, SrlElemento srlElemento, Double similarita) {
		this.gesto = gesto;
		this.srlElemento = srlElemento;
		this.similarita = similarita;
	}
}
