package general;

import java.util.LinkedList;
import java.util.List;


public class ContenitoreTest {
	String info;
	List<Interaction> interactions = new LinkedList<Interaction>();
	Transformation transformation;
	
	public ContenitoreTest(String info, List<Interaction> interactions) {
		this.info = info;
		this.interactions.addAll(interactions);
	}

	public ContenitoreTest(String info, Transformation transformation) {
		this.info = info;
		this.transformation = transformation;
	}

	
	public String toString(){
		String res = ""; 
		for (Interaction i : interactions)
			res += i.name+ " "+i.type+" ";
		return info+ " "+res+ " "+transformation;
		
	}
	
}
