package general;


public class Transformation {
	String type;
	Double x;
	Double y;
	Double z;
	
	public Transformation(String type, Double x, Double y, Double z) {
		this.type = type;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public String toString(){
		return type+" "+x+ " "+y+ " "+z;
	}
}
