package general;

import java.util.LinkedList;
import java.util.List;


public class Actor {
	private String nome;
	private List<String> azioniEseguibili;
	
	public Actor(String nome) {
		this.nome = nome;
		this.azioniEseguibili = new LinkedList<String>();
	}
	
	public List<String> getAzioniEseguibili(){
		return this.azioniEseguibili;
	}
	
	public boolean haAzioneEseguibile(String azioneEseguibile){
		for (String s : this.azioniEseguibili)
			if (s.compareTo(azioneEseguibile) == 0)
				return true;
		return false;
	}
	
	public void addAzione(String azione){
		this.azioniEseguibili.add(azione);
	}
	
	public String getNome(){
		return nome;
	}
	
	public String stampa(){
		String r = "";
		
		for (String s : azioniEseguibili)
		r += s+" ";
		
		return nome+" "+r; 
	}

}
