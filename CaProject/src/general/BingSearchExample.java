package general;


import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class BingSearchExample {
	String parola1;
	String parola2;
	
	public BingSearchExample(String parola1, String parola2) {
		this.parola1 = parola1;
		this.parola2 = parola2;
	}

	public String getBing() throws Exception {
		
		HttpClient httpclient = new DefaultHttpClient();
		String responseBody = null;
		
		try {
            String accountKey = ":15encPmdLiQSDEoV/lT45mNBJoGTJCkFG/UTmyLgA5c";
            byte[] accountKeyBytes = Base64.encodeBase64(accountKey.getBytes());
            String accountKeyEnc = new String(accountKeyBytes);
			
			
			
//        	https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=%27web%27&Query=%27YOUR_QUERY_HERE%27&$top=1&$format=JSON

            HttpGet httpget = null;
            if (parola1.compareTo("") != 0 && parola2.compareTo("") != 0)
            	httpget = new HttpGet("https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=%27web%27&Query=%27"+parola1+"%20"+parola2+"%27&$top=1&$format=JSON");
            else if (parola1.compareTo("") != 0 && parola2.compareTo("") == 0) // ho solo la parola 1
            	httpget = new HttpGet("https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=%27web%27&Query=%27"+parola1+"%27&$top=1&$format=JSON");
            else if (parola1.compareTo("") == 0 && parola2.compareTo("") != 0) // ho solo la parola 2
            	httpget = new HttpGet("https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=%27web%27&Query=%27"+parola2+"%27&$top=1&$format=JSON");
            
            
//            HttpGet httpget = new HttpGet("https://api.datamarket.azure.com/Data.ashx/Bing/Search/Web?Query=%27Datamarket%27&$top=10&$format=Json");
            httpget.setHeader("Authorization", "Basic " + accountKeyEnc);

            System.out.println("executing request " + httpget.getURI());

            // Create a response handler
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpclient.execute(httpget, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
            System.out.println("----------------------------------------");

        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
		return responseBody;
	}

}