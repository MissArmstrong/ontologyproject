package general;


public class Position {
	Double x;
	Double y;
	Double z;
	
	public Position(Double x, Double y, Double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public String toString(){
		return x+ " "+y+ " "+z;
	}
}
