

public class SimilaritaAzioneSrlElemento {
	Azione azione;
	SrlElemento srlElemento;
	Double similarita;

	public SimilaritaAzioneSrlElemento(Azione azione, SrlElemento srlElemento, Double similarita) {
		this.azione = azione;
		this.srlElemento = srlElemento;
		this.similarita = similarita;
	}
}
