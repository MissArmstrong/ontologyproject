

public class OccurrencesParole {
	SrlElemento parola1;
	SrlElemento parola2; 
	int occurrences;
	boolean view; 
	
	public OccurrencesParole(SrlElemento parola1, SrlElemento parola2) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		occurrences = 0;
		view = false;
	}

	public OccurrencesParole(SrlElemento parola1, SrlElemento parola2, int occurrences) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		this.occurrences = occurrences;
		view = false;
	}
	
	
}
