
import java.util.Comparator;


public class SimilaritaElementoSituazioneAzioneTakeMakeComparator implements Comparator<SimilaritaElementoSituazioneAzione>{

	@Override
	public int compare(SimilaritaElementoSituazioneAzione s1, SimilaritaElementoSituazioneAzione s2) {
		return (int) - (s1.similaritaPiuTakeMake * 1000 - s2.similaritaPiuTakeMake * 1000);
	}

}
