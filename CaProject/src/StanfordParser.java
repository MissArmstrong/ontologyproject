

import java.util.LinkedList;
import java.util.List;


public class StanfordParser implements Comparable<StanfordParser> {
	String term;
	int order;
	String word;
	String originalword;
	String pos;
//	List<StanfordParser> elementiSopra = new LinkedList<StanfordParser>();
//	List<StanfordParser> elementiSotto = new LinkedList<StanfordParser>();
	List<RelazioniStanfordParser> relazioniSopra = new LinkedList<RelazioniStanfordParser>();
	List<RelazioniStanfordParser> relazioniSotto = new LinkedList<RelazioniStanfordParser>();
	List<ParolaConceptNet> paroleCollegateConceptNet = new LinkedList<ParolaConceptNet>();
//	String type;
//	String tree;
	
	public StanfordParser(String term, int order, String word, String pos) {
		this.term = term;
		this.order = order;
		this.word = word;
		this.pos = pos;
	}
	
	public StanfordParser(String term, int order, String word, String originalword, String pos) {
	this.term = term;
	this.order = order;
	this.word = word;
	this.originalword = originalword;
	this.pos = pos;
}

	public String stampa(){
		return term+ " "+order+ " "+word+ " "+originalword+ " "+pos;
	}

//	public void aggiungiElementoSopra(StanfordParser elemento){
//		elementiSopra.add(elemento);
//	}
//
//	public void aggiungiElementoSotto(StanfordParser elemento){
//		elementiSotto.add(elemento);
//	}

	public void aggiungiRelazioneSopra(RelazioniStanfordParser relazione){
		relazioniSopra.add(relazione);
	}

	public void aggiungiRelazioneSotto(RelazioniStanfordParser relazione){
		relazioniSotto.add(relazione);
	}
	
	public void addParoleCollegateConceptNet(ParolaConceptNet parola){
		this.paroleCollegateConceptNet.add(parola);
	}

	public boolean haPosVerbo(){
		if (pos.compareTo("VB") == 0 || pos.compareTo("VBD") == 0 || pos.compareTo("VBG") == 0 || pos.compareTo("VBN") == 0 || pos.compareTo("VBP") == 0 || pos.compareTo("VBZ") == 0)
			return true;
		return false;
	}

	public boolean haPosNome(){
		if (pos.compareTo("NN") == 0 || pos.compareTo("NNS") == 0 || pos.compareTo("NNP") == 0 || pos.compareTo("NNPS") == 0)
			return true;
		return false;
	}

	public boolean haPosPronome(){
		if (pos.compareTo("PRP") == 0 || pos.compareTo("PRP$") == 0)
			return true;
		return false;
	}

	public boolean haPosAvverbio(){
		if (pos.compareTo("RB") == 0 || pos.compareTo("RBR") == 0 || pos.compareTo("RBS") == 0)
			return true;
		return false;
	}
	
	public boolean haPosAggettivo(){
		if (pos.compareTo("JJ") == 0 || pos.compareTo("JJR") == 0 || pos.compareTo("JJS") == 0)
			return true;
		return false;
	}

	public List<StanfordParser> cercaRelazioniSotto(String tipo){
		List<StanfordParser> elementiMatchanti = new LinkedList<StanfordParser>();
		for (RelazioniStanfordParser rsp : this.relazioniSotto){ 
			if (rsp.tipoRelazione.compareTo(tipo) == 0)
				elementiMatchanti.add(rsp.secondo);
		}
		return elementiMatchanti;
	}
	
	@Override
	public int compareTo(StanfordParser other) {
		if (this.term.compareTo(other.term) != 0)
			return this.term.compareTo(other.term);
		return this.order - other.order;
	}
}
