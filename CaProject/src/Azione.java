
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
 * Oggetto Java interpretabile come il verbo della frase da analizzare
 */

public class Azione {
	String nomeAzione;
	List<SimilaritaGestoAzione> listaSimGesto;
	List<SimilaritaAzioneSrlElemento> listaSimSrlElem;
	List<SimilaritaComplexGestoAzione> listaSimComplexGestoAzione;
	List<SimilaritaContestoAzione> listaSimContesto;
	List<SimilaritaElementoSituazioneAzione> listaSimElementoSituazione;
	List<SimilaritaContestoAzione> simFin;
	List<SimilaritaElementoSituazioneAzione> simFinElm;
	String azionepos;
	Map<ParolaConceptNet, Double> paroleCollegateConceptNet = new HashMap<ParolaConceptNet, Double>();
	Double maxSimParoleCollegateConceptNet = 0.0;
	
	Map<String, Double> similaritaParoleContenuteInDefinizioneTakeMake = new HashMap<String, Double>();
	Map<String, Double> similaritaParoleContenuteInDefinizioneParoleSpeciali = new HashMap<String, Double>();
	
	public Azione(String nomeAzione) {
		this.nomeAzione = nomeAzione;
		this.azionepos = null;
		this.listaSimGesto = new LinkedList<SimilaritaGestoAzione>();
		this.listaSimSrlElem = new LinkedList<SimilaritaAzioneSrlElemento>();
		this.listaSimComplexGestoAzione = new LinkedList<SimilaritaComplexGestoAzione>();
		this.listaSimContesto = new LinkedList<SimilaritaContestoAzione>();
		this.listaSimElementoSituazione = new LinkedList<SimilaritaElementoSituazioneAzione>();
		this.simFin = new LinkedList<SimilaritaContestoAzione>();
		this.simFinElm = new LinkedList<SimilaritaElementoSituazioneAzione>();
	}

	public Azione(String nomeAzione, String pos) {
		this.nomeAzione = nomeAzione;
		this.azionepos = pos;
		this.listaSimGesto = new LinkedList<SimilaritaGestoAzione>();
		this.listaSimSrlElem = new LinkedList<SimilaritaAzioneSrlElemento>();
		this.listaSimComplexGestoAzione = new LinkedList<SimilaritaComplexGestoAzione>();
		this.listaSimContesto = new LinkedList<SimilaritaContestoAzione>();
		this.simFin = new LinkedList<SimilaritaContestoAzione>();
		this.listaSimElementoSituazione = new LinkedList<SimilaritaElementoSituazioneAzione>();
		this.simFinElm = new LinkedList<SimilaritaElementoSituazioneAzione>();
	}

	public void aggiungiSimilaritaSrlElemento(SimilaritaAzioneSrlElemento s){
		listaSimSrlElem.add(s);
	}

	public void aggiungiSimilaritaGesto(SimilaritaGestoAzione s){
		listaSimGesto.add(s);
	}

	public void aggiungiSimilaritaContesto(SimilaritaContestoAzione s){
		listaSimContesto.add(s);
	}

	public void aggiungiSimilaritaElementoSituazione(SimilaritaElementoSituazioneAzione s){
		listaSimElementoSituazione.add(s);
	}

	public void aggiungiSimilaritaComplexGestoAzione(SimilaritaComplexGestoAzione s){
		listaSimComplexGestoAzione.add(s);
	}
	
	public SimilaritaGestoAzione getSimGesto(Gesto g){
		for (SimilaritaGestoAzione s : listaSimGesto){
			if (s.gesto.equals(g))
				return s;
		}
		return null;
	}

	public SimilaritaAzioneSrlElemento getSimSrlElemento(SrlElemento e){
		for (SimilaritaAzioneSrlElemento s : listaSimSrlElem){
			if (s.srlElemento.equals(e))
				return s;
		}
		return null;
	}

	public SimilaritaComplexGestoAzione getSimComplexGestoAzione(Gesto g){
		for (SimilaritaComplexGestoAzione s : listaSimComplexGestoAzione){
			if (s.gesto.equals(g))
				return s;
		}
		return null;
	}

	public void calcolaMediaSimSrlElementi(){
		double sim = 0.0;
		int num = 0;
		Contesto c = null;
		for (SimilaritaContestoAzione s : listaSimContesto){
			sim += s.similarita;
			num ++;
			c = s.contesto;
		}
		double simF = sim / num;
		SimilaritaContestoAzione simtemp = new SimilaritaContestoAzione(c, this, simF);
		simFin.add(simtemp);
	}

	public int calcolaNum(){
		int num = 0;
		for (SimilaritaAzioneSrlElemento e : this.listaSimSrlElem){
			num ++;
		}
		return num;
	}
	
	public void calcolaMediaSimElementoSituazioneAzione(ElementoSituazioneAzione elm){
		double sim = 0.0;
		int num = 0;
//		ElementoSituazioneAzione elm = null;
		
		for (SimilaritaAzioneSrlElemento e : this.listaSimSrlElem){
//		for (SimilaritaElementoSituazioneAzione e : this.listaSimElementoSituazione){
//			System.err.println("similarita azione "+e.azione.nomeAzione+ " - "+e.srlElemento.word+ " - "+e.similarita);
			sim += e.similarita;
			num ++;
//			elm = this.listaSimElementoSituazione.get(0).elemento;
		}
		
		double sim2 = 0.0;
		int num2 = 0;
		// aggiungo anche parole take-make
		for (Double s : similaritaParoleContenuteInDefinizioneTakeMake.values()){
			sim2 += s;
			num2++;
		}

		double sim3 = 0.0;
		int num3 = 0;
		for (Double s : similaritaParoleContenuteInDefinizioneParoleSpeciali.values()){
			sim3 += s;
			num3++;
		}

		double simF = sim / num;
		double simFPiuTakeMake = (sim + sim2) / (num+num2);
		double simFPiuParoleSpeciali = (sim +sim3) / (num + num3);
		double simFPiuTakeMakeEParoleSpeciali = (sim + sim2 + sim3) / (num + num2 + num3);
		SimilaritaElementoSituazioneAzione simtemp = new SimilaritaElementoSituazioneAzione(elm, this, simF, simFPiuTakeMake, simFPiuParoleSpeciali, simFPiuTakeMakeEParoleSpeciali);
//		System.out.println("simfinelm "+simtemp.azione.nomeAzione+ " "+simtemp.elemento.stampaListaElementi()+ " "+simtemp.similarita);
		simFinElm.add(simtemp);
	}

	public void clearSimFinElm(){
		simFinElm.clear();
	}
	
	public void clearSimilaritaParoleContenuteInDefinizioneTakeMake(){
		similaritaParoleContenuteInDefinizioneTakeMake.clear();
	}


	public void clearSimilaritaParoleContenuteInDefinizioneParoleSpeciali(){
		similaritaParoleContenuteInDefinizioneParoleSpeciali.clear();
	}

	public String stampaListaSimSrlElemento(boolean disambiguoTakeMake, boolean disambiguoParoleSpeciali){
		String s = "";
		for (SimilaritaAzioneSrlElemento sae : listaSimSrlElem)
			s += ""+sae.srlElemento.word+" "+sae.similarita+ " - ";
//		s = s.substring(0, (s.length()-3));

		if (disambiguoTakeMake && similaritaParoleContenuteInDefinizioneTakeMake.size() > 0){
			for (String ss : similaritaParoleContenuteInDefinizioneTakeMake.keySet())
				s += ""+ss+" "+similaritaParoleContenuteInDefinizioneTakeMake.get(ss)+ " - ";
			s += " disambiguo take e make   ";
		}

		
		if (disambiguoParoleSpeciali && similaritaParoleContenuteInDefinizioneParoleSpeciali.size() > 0){
			for (String ss : similaritaParoleContenuteInDefinizioneParoleSpeciali.keySet())
				s += ""+ss+" "+similaritaParoleContenuteInDefinizioneParoleSpeciali.get(ss)+ " - ";
			s += " disambiguo parole speciali   ";
		}
		
		s = s.substring(0, (s.length()-3));

		return s;
	}

	
	public String stampaListaSimSrlElemento(){
		String s = "";
		for (SimilaritaAzioneSrlElemento sae : listaSimSrlElem)
			s += ""+sae.srlElemento.word+" "+sae.similarita+ " - ";
		s = s.substring(0, (s.length()-3));

		return s;
	}

	public void addSimilaritaParoleContenuteInDefinizioneTakeMake(String parola, Double sim){
		this.similaritaParoleContenuteInDefinizioneTakeMake.put(parola, sim);
	}

	public void addSimilaritaParoleContenuteInDefinizioneParoleSpeciali(String parola, Double sim){
		this.similaritaParoleContenuteInDefinizioneParoleSpeciali.put(parola, sim);
	}

	public void addParoleCollegateConceptNet(ParolaConceptNet parola, Double score){
		this.paroleCollegateConceptNet.put(parola, score);
	}

	public void setMaxSimParoleCollegateConceptNet(Double valore){
		this.maxSimParoleCollegateConceptNet = valore;
	}
}
