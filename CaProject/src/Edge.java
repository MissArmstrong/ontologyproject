
import com.google.gson.annotations.SerializedName;
import java.util.List;


public class Edge{

    private static final String FIELD_REL = "rel";
    private static final String FIELD_SURFACE_TEXT = "surfaceText";
    private static final String FIELD_START = "start";
    private static final String FIELD_DATASET = "dataset";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_WEIGHT = "weight";
    private static final String FIELD_END_LEMMAS = "endLemmas";
    private static final String FIELD_TIMESTAMP = "timestamp";
    private static final String FIELD_LICENSE = "license";
    private static final String FIELD_URI = "uri";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_ID = "id";
    private static final String FIELD_CONTEXT = "context";
    private static final String FIELD_REL_LEMMAS = "relLemmas";
    private static final String FIELD_SOURCES = "sources";
    private static final String FIELD_NODES = "nodes";
    private static final String FIELD_END = "end";
    private static final String FIELD_START_LEMMAS = "startLemmas";
    private static final String FIELD_SOURCE_URI = "source_uri";
    private static final String FIELD_FEATURES = "features";


    @SerializedName(FIELD_REL)
    private String mRel;
    @SerializedName(FIELD_SURFACE_TEXT)
    private String mSurfaceText;
    @SerializedName(FIELD_START)
    private String mStart;
    @SerializedName(FIELD_DATASET)
    private String mDataset;
    @SerializedName(FIELD_SCORE)
    private double mScore;
    @SerializedName(FIELD_WEIGHT)
    private double mWeight;
    @SerializedName(FIELD_END_LEMMAS)
    private String mEndLemma;
    @SerializedName(FIELD_TIMESTAMP)
    private String mTimestamp;
    @SerializedName(FIELD_LICENSE)
    private String mLicense;
    @SerializedName(FIELD_URI)
    private String mUri;
    @SerializedName(FIELD_TEXT)
    private List<String> mTexts;
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_CONTEXT)
    private String mContext;
    @SerializedName(FIELD_REL_LEMMAS)
    private String mRelLemma;
    @SerializedName(FIELD_SOURCES)
    private List<String> mSources;
    @SerializedName(FIELD_NODES)
    private List<String> mNodes;
    @SerializedName(FIELD_END)
    private String mEnd;
    @SerializedName(FIELD_START_LEMMAS)
    private String mStartLemma;
    @SerializedName(FIELD_SOURCE_URI)
    private String mSourceUri;
    @SerializedName(FIELD_FEATURES)
    private List<String> mFeatures;


    public Edge(){

    }

    public void setRel(String rel) {
        mRel = rel;
    }

    public String getRel() {
        return mRel;
    }

    public void setSurfaceText(String surfaceText) {
        mSurfaceText = surfaceText;
    }

    public String getSurfaceText() {
        return mSurfaceText;
    }

    public void setStart(String start) {
        mStart = start;
    }

    public String getStart() {
        return mStart;
    }

    public void setDataset(String dataset) {
        mDataset = dataset;
    }

    public String getDataset() {
        return mDataset;
    }

    public void setScore(double score) {
        mScore = score;
    }

    public double getScore() {
        return mScore;
    }

    public void setWeight(double weight) {
        mWeight = weight;
    }

    public double getWeight() {
        return mWeight;
    }

    public void setEndLemma(String endLemma) {
        mEndLemma = endLemma;
    }

    public String getEndLemma() {
        return mEndLemma;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setLicense(String license) {
        mLicense = license;
    }

    public String getLicense() {
        return mLicense;
    }

    public void setUri(String uri) {
        mUri = uri;
    }

    public String getUri() {
        return mUri;
    }

    public void setTexts(List<String> texts) {
        mTexts = texts;
    }

    public List<String> getTexts() {
        return mTexts;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setContext(String context) {
        mContext = context;
    }

    public String getContext() {
        return mContext;
    }

    public void setRelLemma(String relLemma) {
        mRelLemma = relLemma;
    }

    public String getRelLemma() {
        return mRelLemma;
    }

    public void setSources(List<String> sources) {
        mSources = sources;
    }

    public List<String> getSources() {
        return mSources;
    }

    public void setNodes(List<String> nodes) {
        mNodes = nodes;
    }

    public List<String> getNodes() {
        return mNodes;
    }

    public void setEnd(String end) {
        mEnd = end;
    }

    public String getEnd() {
        return mEnd;
    }

    public void setStartLemma(String startLemma) {
        mStartLemma = startLemma;
    }

    public String getStartLemma() {
        return mStartLemma;
    }

    public void setSourceUri(String sourceUri) {
        mSourceUri = sourceUri;
    }

    public String getSourceUri() {
        return mSourceUri;
    }

    public void setFeatures(List<String> features) {
        mFeatures = features;
    }

    public List<String> getFeatures() {
        return mFeatures;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Edge){
            return ((Edge) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return mId.hashCode();
    }

    @Override
    public String toString(){
        return "rel = " + mRel + ", surfaceText = " + mSurfaceText + ", start = " + mStart + ", dataset = " + mDataset + ", score = " + mScore + ", weight = " + mWeight + ", endLemma = " + mEndLemma + ", timestamp = " + mTimestamp + ", license = " + mLicense + ", uri = " + mUri + ", texts = " + mTexts + ", id = " + mId + ", context = " + mContext + ", relLemma = " + mRelLemma + ", sources = " + mSources + ", nodes = " + mNodes + ", end = " + mEnd + ", startLemma = " + mStartLemma + ", sourceUri = " + mSourceUri + ", features = " + mFeatures;
    }

}