
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Gesto {
	String nomeGesto;
	String gestopos;
	List<SimilaritaGestoAzione> listaSimAzione;
	List<SimilaritaGestoSrlElemento> listaSimSrlElem;
	List<SimilaritaComplexGestoAzione> listaSimComplexGestoAzione;
	List<SimilaritaGestoAzione> simFin;
	Map<ParolaConceptNet, Double> paroleCollegateConceptNet = new HashMap<ParolaConceptNet, Double>();
	Double maxSimParoleCollegateConceptNet = 0.0;
	
	public Gesto(String nomeGesto) {
		this.nomeGesto = nomeGesto;
		this.gestopos = null;
		this.listaSimAzione = new LinkedList<SimilaritaGestoAzione>();
		this.listaSimSrlElem = new LinkedList<SimilaritaGestoSrlElemento>();
		this.listaSimComplexGestoAzione = new LinkedList<SimilaritaComplexGestoAzione>();
		this.simFin = new LinkedList<SimilaritaGestoAzione>();
	}
	
	public Gesto(String nomeGesto, String pos) {
		this.nomeGesto = nomeGesto;
		this.gestopos = pos;
		this.listaSimAzione = new LinkedList<SimilaritaGestoAzione>();
		this.listaSimSrlElem = new LinkedList<SimilaritaGestoSrlElemento>();
		this.listaSimComplexGestoAzione = new LinkedList<SimilaritaComplexGestoAzione>();
		this.simFin = new LinkedList<SimilaritaGestoAzione>();
	}

	
	
	public void aggiungiSimilaritaSrlElemento(SimilaritaGestoSrlElemento s){
		listaSimSrlElem.add(s);
	}

	public void aggiungiSimilaritaAzione(SimilaritaGestoAzione s){
		listaSimAzione.add(s);
	}
	
	public void aggiungiSimilaritaComplexGestoAzione(SimilaritaComplexGestoAzione s){
		listaSimComplexGestoAzione.add(s);
	}

	public SimilaritaGestoAzione getSimAzione(Azione a){
		for (SimilaritaGestoAzione s : listaSimAzione){
			if (s.azione.equals(a))
				return s;
		}
		return null;
	}

	public SimilaritaGestoSrlElemento getSimSrlElemento(SrlElemento e){
		for (SimilaritaGestoSrlElemento s : listaSimSrlElem){
			if (s.srlElemento.equals(e))
				return s;
		}
		return null;
	}
	
	public SimilaritaComplexGestoAzione getSimComplexGestoAzione(Azione a){
		for (SimilaritaComplexGestoAzione s : listaSimComplexGestoAzione){
			if (s.azione.equals(a))
				return s;
		}
		return null;
	}

	public void calcolaSimConOgniAzione(){
		for (Azione a : app.mappaAzioni.values()){
			double sim = 0.0;
			int num = 0;
			for (SimilaritaComplexGestoAzione s : listaSimComplexGestoAzione){
				if (s.azione.equals(a)){
					sim += s.similarita;
					num ++;
				}
			}
			double simF = sim / num;
			SimilaritaGestoAzione simtemp = new SimilaritaGestoAzione(this, a, simF);
			simFin.add(simtemp);
		}
	}

	public void addParoleCollegateConceptNet(ParolaConceptNet parola, Double score){
		this.paroleCollegateConceptNet.put(parola, score);
	}

	public void setMaxSimParoleCollegateConceptNet(Double valore){
		this.maxSimParoleCollegateConceptNet = valore;
	}
}
