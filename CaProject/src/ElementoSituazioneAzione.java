
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class ElementoSituazioneAzione {
	private List<SrlElemento> elementi;
	private List<RelazioneSrl> relazioni;
	private Situazione situazione;
	private SimilaritaElementoSituazioneAzione similarita;
	
	private String tipo;
	
	public ElementoSituazioneAzione(Situazione situazione, String tipo) {
		this.elementi = new LinkedList<SrlElemento>();
		this.relazioni = new LinkedList<RelazioneSrl>();
		this.situazione = situazione;
		this.tipo = tipo;
	}

	public List<SrlElemento> getElementi() {
		return elementi;
	}

	public void addElemento(SrlElemento elemento) {
		elementi.add(elemento);
	}
	
	public void addRelazione(RelazioneSrl r){
		relazioni.add(r);
	}

	public List<RelazioneSrl> getRelazioni() {
		return relazioni;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public String stampaListaElementi(){
		String ris = "";
		for (SrlElemento e : elementi)
			ris += e.word+ " ";
		return ris;
	}
}
