
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Tripla {
	String tipo;
	Tripla triplaDerivataDa;
	String relazione;
	String parolaPerSimilarita;
	String parolaPosPerSimilarita;
	Map<ParolaConceptNet, Double> paroleCollegateConceptNet = new HashMap<ParolaConceptNet, Double>();
	Double maxSimParoleCollegateConceptNet = 0.0;

	public Tripla(String tipo) {
		this.tipo = tipo;
	}
	
	public String stampa(){
		return "";
	}
	
	public void addParoleCollegateConceptNet(ParolaConceptNet parola, Double score){
		boolean trovato = false;
		for (ParolaConceptNet p : this.paroleCollegateConceptNet.keySet())
			if (p.getParola().compareTo(parola.getParola()) == 0)
				trovato = true;
		if (!trovato)
			this.paroleCollegateConceptNet.put(parola, score);
	}

	public void setParolaPerSimilarita(String parola){
		this.parolaPerSimilarita = parola;
	}

	public void setParolaPosPerSimilarita(String parolaPos){
		this.parolaPosPerSimilarita = parolaPos;
	}
	
	public void setMaxSimParoleCollegateConceptNet(Double valore){
		this.maxSimParoleCollegateConceptNet = valore;
	}

}
