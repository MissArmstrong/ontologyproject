
import java.util.Comparator;


public class SrlElementoComparator implements Comparator<SrlElemento>{

	@Override
	public int compare(SrlElemento e1, SrlElemento e2) {
		if (e1.term.compareTo(e2.term) != 0)
			return e1.term.compareTo(e2.term);
		return e1.order - e2.order;
	}

}
