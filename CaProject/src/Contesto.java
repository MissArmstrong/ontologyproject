
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Contesto {
	String internoEsterno;
	List<SrlElemento> elementiAmbientazione;
	List<SrlElemento> elementiFraseIntroduttiva;
	List<RelazioneSrl> relazioniFraseIntroduttiva;
	List<SimilaritaContestoAzione> listaSimAzione;
	List<SimilaritaGestoAzione> listaSimGestoAzione;
	Map<String, String> mappaAzioneGesto;
	Map<String, String> mappaGestoAzione;

	
	Map<Integer, List<SrlElemento>> elementiFrasiSuccessive;
	Map<Integer, List<RelazioneSrl>> relazioniFrasiSuccessive;
	
	public Contesto(String internoEsterno, List<SrlElemento> elementiAmbientazione) {
		this.internoEsterno = internoEsterno;
		this.elementiAmbientazione = new LinkedList<SrlElemento>(elementiAmbientazione);
		this.elementiFraseIntroduttiva = new LinkedList<SrlElemento>();
		this.relazioniFraseIntroduttiva = new LinkedList<RelazioneSrl>();
		elementiFrasiSuccessive = new HashMap<Integer, List<SrlElemento>>();
		relazioniFrasiSuccessive = new HashMap<Integer, List<RelazioneSrl>>();
		listaSimAzione = new LinkedList<SimilaritaContestoAzione>();
		listaSimGestoAzione = new LinkedList<SimilaritaGestoAzione>();
		mappaAzioneGesto = new HashMap<String, String>();
		mappaGestoAzione = new HashMap<String, String>();
	}
	
	public void aggiungiSimilaritaAzione(SimilaritaContestoAzione s){
		listaSimAzione.add(s);
	}

	
	

}
