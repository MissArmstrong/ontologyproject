

public class ParolaConceptNet {
	private String parola;
	private String pos;

	public ParolaConceptNet(String parola, String pos) {
		this.parola = parola;
		this.pos = pos;
	}
	
	public String stampa(){
		return ""+parola+ " "+pos;
	}

	public String getParola() {
		return parola;
	}

	public String getPos() {
		return pos;
	}
}
