
import java.util.Comparator;


public class SimilaritaElementoSituazioneAzioneParoleSpecialiTakeMakeComparator implements Comparator<SimilaritaElementoSituazioneAzione>{

	@Override
	public int compare(SimilaritaElementoSituazioneAzione s1, SimilaritaElementoSituazioneAzione s2) {
		return (int) - (s1.similaritaPiuTakeMakeEParoleSpeciali * 1000 - s2.similaritaPiuTakeMakeEParoleSpeciali * 1000);
	}

}
