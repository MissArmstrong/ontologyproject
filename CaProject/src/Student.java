
import java.util.Map;
import java.util.TreeMap;


public class Student {
	private Map<Integer, StudentAnswer> mappaRisposte = new TreeMap<Integer, StudentAnswer>();
	private String ip;
	
	public Student(String ip) {
		this.ip = ip;
	} 
	
	public void addRisposta(StudentAnswer sa){
		mappaRisposte.put(sa.getPage(), sa);
	}

	public Map<Integer, StudentAnswer> getMappaRisposte() {
		return mappaRisposte;
	}

	public String getIp() {
		return ip;
	}
}
