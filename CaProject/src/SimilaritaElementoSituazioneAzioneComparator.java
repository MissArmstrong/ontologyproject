
import java.util.Comparator;


public class SimilaritaElementoSituazioneAzioneComparator implements Comparator<SimilaritaElementoSituazioneAzione>{

	@Override
	public int compare(SimilaritaElementoSituazioneAzione s1, SimilaritaElementoSituazioneAzione s2) {
		return (int) - (s1.similarita * 1000 - s2.similarita * 1000);
	}

}
