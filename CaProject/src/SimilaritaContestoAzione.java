

public class SimilaritaContestoAzione {
	Azione azione;
	Double similarita;
	Contesto contesto;

	public SimilaritaContestoAzione(Contesto contesto, Azione azione, Double similarita) {
		this.contesto = contesto;
		this.azione = azione;
		this.similarita = similarita;
	}

}
