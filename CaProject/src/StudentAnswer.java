

public class StudentAnswer {
	private String ip; 
	private String date;
	private Integer page;
	private Integer timer;
	private String context; 
	private String action;
	
	public StudentAnswer(String ip, String date, Integer page, Integer timer, String context, String action) {
		this.ip = ip;
		this.date = date;
		this.page = page;
		this.timer = timer;
		this.context = context;
		this.action = action;
	}

	public String getIp() {
		return ip;
	}

	public String getDate() {
		return date;
	}

	public Integer getPage() {
		return page;
	}

	public Integer getTimer() {
		return timer;
	}

	public String getContext() {
		return context;
	}

	public String getAction() {
		return action;
	}
	
	public String stampa(){
		return ""+ip+ " "+date+ " "+page+ " "+timer+ " "+context+ " "+action;
	}
}
