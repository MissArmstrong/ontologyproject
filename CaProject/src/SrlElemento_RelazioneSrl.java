

public class SrlElemento_RelazioneSrl {
	private SrlElemento srlElemento;
	private RelazioneSrl relazioneSrl;
	
	public SrlElemento_RelazioneSrl(SrlElemento srlElemento, RelazioneSrl relazioneSrl) {
		this.srlElemento = srlElemento;
		this.relazioneSrl = relazioneSrl;
	}

	public SrlElemento getSrlElemento() {
		return srlElemento;
	}

//	public void setSrlElemento(SrlElemento srlElemento) {
//		this.srlElemento = srlElemento;
//	}

	public RelazioneSrl getRelazioneSrl() {
		return relazioneSrl;
	}

//	public void setRelazioneSrl(RelazioneSrl relazioneSrl) {
//		this.relazioneSrl = relazioneSrl;
//	}
}
