

public class RisultatoTriplaMacchina {

	private String verboMacchina = "";
	private String oggettoMacchina = "";
	private String strumentoMacchina = "";
	private String posizioneMacchina = "";
	private String manieraMacchina = "";
	private String nomeBOM = null;
	
	// Add for integrate the old code with the new one
	private String nameFile = "";
		
	public RisultatoTriplaMacchina(String verboMacchina,
			String oggettoMacchina, String strumentoMacchina, String posizioneMacchina, String manieraMacchina) {
		super();
		this.verboMacchina = verboMacchina;
		this.oggettoMacchina = oggettoMacchina;
		this.strumentoMacchina = strumentoMacchina;
		this.posizioneMacchina = posizioneMacchina;
		this.manieraMacchina  = manieraMacchina;
	}
	
	public RisultatoTriplaMacchina(String verboMacchina,
			String oggettoMacchina, String strumentoMacchina, 
			String posizioneMacchina, String manieraMacchina,String nomeBOM) {
		super();
		this.verboMacchina = verboMacchina;
		this.oggettoMacchina = oggettoMacchina;
		this.strumentoMacchina = strumentoMacchina;
		this.posizioneMacchina = posizioneMacchina;
		this.manieraMacchina  = manieraMacchina;
		this.nomeBOM = nomeBOM;
	}
	
	// Add for integrate the old code with the new one
	public void setNameFile(String nameFile){
		this.nameFile = nameFile;
	}
	
	public String getNameFile(){
		return nameFile;
	}

	
	public String getVerboMacchina() {
		return verboMacchina;
	}
	public String getOggettoMacchina() {
		return oggettoMacchina;
	}
	public String getStrumentoMacchina() {
		return strumentoMacchina;
	}
	public String getPosizioneMacchina() {
		return posizioneMacchina;
	}
	public String getManieraMacchina() {
		return manieraMacchina;
	}
	public String getNomeBOM(){
		return nomeBOM;
	}
	@Override
	public String toString() {
		String s = "";
		if(nomeBOM != null && !nomeBOM.equals("")){
			return s+= nomeBOM;
		}else
			s+="NO_BOM ";
		if(verboMacchina.equals("")==false){
			s +=verboMacchina;
		}else
			s+="NO_VERB";
		if(oggettoMacchina.equals("")==false){
			s+=" "+ oggettoMacchina;
		}else
			s+=" NO_OBJ";
		if(strumentoMacchina.equals("")==false){
			s+=" "+ strumentoMacchina;
		}else
			s+=" NO_STRUM";
		if(posizioneMacchina.equals("")==false){
			s+=" "+ posizioneMacchina;
		}else
			s+=" NO_LOC";
		if(manieraMacchina.equals("")==false){
			s+=" "+ manieraMacchina;
		}else
			s+=" NO_MANIERA";
//		if(nomeBOM != null && !nomeBOM.equals("")){
//			s+=" "+ nomeBOM;
//		}else
//			s+=" NO_BOM";
//		return ""+this.verboMacchina+" "+this.oggettoMacchina+" "+this.strumentoMacchina
//				+" "+this.posizioneMacchina+" "+this.manieraMacchina;
		return s;
	}
	
	
}
