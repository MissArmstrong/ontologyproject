
import java.util.Comparator;

public class SrlStringLenghtComparator implements Comparator<String>{
	
	  public int compare(String o1, String o2) {
		  o1.trim();
		  o2.trim();
		  
		  o1 = o1.replace("   ", " ");
		  o2 = o2.replace("   ", " ");
		  o1 = o1.replace("  ", " ");
		  o2 = o2.replace("  ", " ");
		  
		  String [] sentence1 = o1.split(" ");  
	      int words1 = sentence1.length;  
		  
		  String [] sentence2 = o2.split(" ");  
	      int words2 = sentence2.length;  

//	      if (o1.length() > o2.length()) {
	      if (words1 > words2) {
		      return 1;
		    } else if (words1 < words2) {
		      return -1;
		    } else {
		      return 0;
		    }
		  }

}
