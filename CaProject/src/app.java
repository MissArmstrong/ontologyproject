import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.*;
import general.*;
//import triple.*;

import org.apache.commons.lang3.StringUtils;

import com.mysql.cj.jdbc.MysqlDataSource;

import edu.cmu.lti.jawjaw.db.WordDAO;
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.*;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import se.lth.cs.srl.CompletePipeline;
import se.lth.cs.srl.corpus.*;
import se.lth.cs.srl.io.CoNLL09Writer;
import se.lth.cs.srl.io.SentenceWriter;
import se.lth.cs.srl.languages.Language;
import se.lth.cs.srl.options.CompletePipelineCMDLineOptions;
import se.lth.cs.srl.util.ChineseDesegmenter;
import se.lth.cs.srl.util.FileExistenceVerifier;
import se.lth.cs.srl.util.Util;

public class app {
	private static final boolean onlyPath = true;
	private static final Pattern WHITESPACE_PATTERN = Pattern.compile("\\s+");
	private static final HashSet<String> styleSheetArgs = new HashSet<String>();
	public static String db = "manutenzione";
	public static Map<String, Azione> mappaAzioni = new HashMap<String, Azione>();
	public static Map<String, Gesto> mappaGesti = new HashMap<String, Gesto>();
	public static Map<String, PathParoleString> mappapath = new HashMap<String, PathParoleString>();
	public static Map<String, PathParoleString> mappares = new HashMap<String, PathParoleString>();
	public static Map<String, OccurrencesParoleString> mappaoccurrences = new HashMap<String, OccurrencesParoleString>();
	public static boolean usaWordPos = true; // se true, calcolo la similarita' solo tra parole che hanno lo stesso pos
	public static String approccio = "srl"; // "bag of words"; // bag of words, srl
	public static boolean normalizzaLesk = false; // --> aggiunto perche' normalizzazione Lesk non funziona bene 
	public static String similarita = "";
	public static String animale = "elephantIS"; // elephant or snail // or pocoyo08 or pocoyo01 // era pocoyo08
	public static List<String> listaStopWords = new LinkedList<String>();
	public static List<SrlElemento> paroleInseriteGlobali = new LinkedList<SrlElemento>();
	public static List<String> paroleInizioOContinuazione = new LinkedList<String>();
	public static List<String> paroleTermine = new LinkedList<String>();
	public static boolean consideraSoloVerbiPerGesti = true;
	public static Statement st = null;
	public static int interoContaTriple = 1;
	public static Map<String, Student> mappaStudentForStudentAnswer = new TreeMap<String, Student>();
	private static double sogliaSimilaritaParoleAttori = 0.0; //0.3
	private static int azioneConsiderata = 0; //-1 o 0 o numero da 1 a 4
	
	private static String soggettoPrincipale = "you";
	private static List<RisultatoTriplaOriginale> listaRisTriplaOriginale = new ArrayList<RisultatoTriplaOriginale>();
	private static List<SrlElemento> listaRisSrlVerbi = new ArrayList<SrlElemento>();
	private static List<SrlElemento> listaRisSrlOggetti = new ArrayList<SrlElemento>();
	private static List<SrlElemento> listaRisSrlStrumenti = new ArrayList<SrlElemento>();
	private static List<SrlElemento> listaRisSrlPosizioni = new ArrayList<SrlElemento>();
	private static List<SrlElemento> listaRisSrlManiere = new ArrayList<SrlElemento>();
	
	private static List<String> listaAzioni = new ArrayList<String>();
	private static List<String> listaOggetti = new ArrayList<String>();
	private static List<String> listaStrumenti = new ArrayList<String>();
	private static List<String> listaPosizioni = new ArrayList<String>();
	private static List<String> listaManiere = new ArrayList<String>();
	private static List<RisultatoTriplaMacchina> listaRisTriplaMacchina = new ArrayList<RisultatoTriplaMacchina>();
	private static List<Frase> listaFrasi = new ArrayList<Frase>();
	
	// non modificabili
	public static String tabelladef = "";
	public static String tabellasrlrel = "";
	public static List<String> queryTree = new LinkedList<String>();
	public static List<String> querySRL = new LinkedList<String>();
	public static List<String> queryParoleUnite = new LinkedList<String>();
	public static List<String> queryTabella = new LinkedList<String>();
	public static Connection conn = null;
	public static List<String> soggetti = new LinkedList<String>();
	public static Map<String, Actor> attori = new TreeMap<String, Actor>();
	public static Map<String, String> oggetti = new TreeMap<String,String>();
	public static Map<String, String> scene = new TreeMap<String,String>();
	private static boolean scritturaSuFileCoppieDivise = false;
	//
	public static BufferedWriter bw = null;
	private static List<SrlElemento> paroleAggiunteDaTakeMake = new LinkedList<SrlElemento>();
	private static List<SrlElemento> paroleAggiunteDaParoleParticolari = new LinkedList<SrlElemento>();
	private static Map<String, ParolaContenutaInDefinition> paroleContenuteInDefinizione = new HashMap<String, ParolaContenutaInDefinition>();
	public static List<String> paroleParticolari = new LinkedList<String>();
	public static boolean disambiguoConTakeEMake = false;
	public static String testoAzione = "";
	private static Map<String, Boolean> mappaRelazioniSoggetto = new TreeMap<String, Boolean>();
	public static Map<String, Integer> paroleDefinitionCount = new TreeMap<String, Integer>();
	public static Map<String, List<String>> risultatoContesto = new TreeMap<String, List<String>>();

	
	// parametri per confronto semantico
	public static Double sogliaSimGestoContesto = 0.3;
	

	private static ServerSocket serverSocket = null;
	private static ServerSocket serverSocketBlenderRisp = null;
	private static Socket clientSocket = null;
	private static Socket clientSocketBlenderRisp = null;
	private static final int maxClientsCount = 1;
	public static List<Tripla> listaTripleComplessive = new LinkedList<Tripla>();
	public static Map<String, ConceptNet> mappaParoleCollegateGestoComplessive = new HashMap<String, ConceptNet>();

	// Blender
	public static Socket clientSocketBlender = null;
	private static PrintStream os = null;
	private static DataInputStream is = null;
	private static BufferedReader inputLine = null;
	public static boolean closed = false;
  
	public static String risposta = null;
	
	private static ILexicalDatabase dbI = new NictWordNet();
    private static RelatednessCalculator[] rcs = {new Path(dbI)}; // per usare tutte le misure di similarita': private static RelatednessCalculator[] rcs = {new HirstStOnge(dbI), new LeacockChodorow(dbI), new Lesk(dbI),  new WuPalmer(dbI),new Resnik(dbI), new JiangConrath(dbI), new Lin(dbI), new Path(dbI)};
	
    private static String ontologyName = "";
	
	public static void main(String[] args) {

		String dbhost="127.0.0.1";
		String dbuser="root";
		String dbpass="giulia";
		String dbname="manutenzione";

		
		String url = "jdbc:mysql://127.0.0.1:3306/yourdatabase";
		String user = "username";
		String password = "password";

	    try {
			/*//Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		    conn = DriverManager.getConnection("jdbc:mysql://"+dbhost+":3306"+"/"+dbname+"", dbuser, dbpass);
			st = conn.createStatement();*/
	    	MysqlDataSource dataSource = new MysqlDataSource();
	    	dataSource.setUrl("jdbc:mysql://"+dbhost+":3306"+"/"+dbname + "?useLegacyDatetimeCode=false&serverTimezone=Europe/Rome");
	    	dataSource.setUser(dbuser);
	    	dataSource.setPassword(dbpass);
	    	dataSource.setDatabaseName(dbname);
	    	conn = dataSource.getConnection();
	    	st = conn.createStatement();

		} catch (SQLException e) {e.printStackTrace();}
	    	
		
		String similarity = "path"; 
		app.similarita = similarity;
		app.listaStopWords.add("then");
	    
		app.ontologyName = "TMS_Laser";//"Scooter";//"TMS_Laser";
		
		definisciNomeTabelle();
		popolaParoleTemporali();

		caricoSimilaritaCoppieDivise();  //NUOVO
		if(onlyPath){}else{
		caricoSimilaritaCoppieDiviseRes(); // Per aggiungere calcolo anche di res insieme a path
		}
		// caricaParoleDefinitionCount();
	    // decommento la riga seguente quando devo caricare le risposte degli studenti
	    // caricaRisposteStudenti();


//      se voglio scrivere su file coppie pongo true	
		app.scritturaSuFileCoppieDivise = false;   //NUOVO
		
//	    calcolare prima la similarita da riga di comando digitando:
//per altri cmd --> similarity.pl --type WordNet::Similarity::path --file C:\Users\Sofia\workspace\Loccisano_per_IS_per_tesista\xFileCoppieDivise\fileCoppieDiviseUnico.txt > C:\Users\Sofia\workspace\Loccisano_per_IS_per_tesista\xFileCoppieDivise\fileCoppieDiviseSimilarity.txt	
//per cygwin path  --> similarity.pl --type WordNet::Similarity::path --file C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseUnico.txt > C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseSimilarity.txt
//per cygwin res   --> similarity.pl --type WordNet::Similarity::res --file C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseUnico.txt > C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseSimilarityRes.txt
		
//	    inserisciRisultatiSimilaritaCoppieDiviseInDbIS("Path"); // NUOVO
//      carica il file sql manualmente queryPathCoppieDivise.sql
		caricaFrasi();
	}
	
	private static void caricoSimilaritaCoppieDivise() {
		app.animale = "manuale";
		String sql;
		ResultSet rs;
		String tabellasimilarita = "";

		tabellasimilarita = similarita+"possimilarity"+app.animale;

		sql="SELECT DISTINCT * from "+app.db+"."+tabellasimilarita+";";
		Double valore = 0.0;
		int it = 0;
		System.out.println("Inizio caricamento similarita' da "+tabellasimilarita);
		try {
			rs = st.executeQuery(sql); 
			while (rs.next()){
				valore = rs.getDouble("similarity");
				String word1 = "";
				String word2 = "";
				String word1pos = rs.getString("word1tipo");
				String word2pos = rs.getString("word2tipo");
				word1 = rs.getString("word1");
				word2 = rs.getString("word2");
				
				if (valore < 0.0)
					valore = 0.0;
				
				PathParoleString p = null;
				PathParoleString p1 = null;
				p = new PathParoleString(word1, word2, valore, word1pos, word2pos);
				p1 = new PathParoleString(word2, word1, valore, word2pos, word1pos);
				
				
				
				app.mappapath.put(word1+"°"+word1pos+"___"+word2+"°"+word2pos, p);
				app.mappapath.put(word2+"°"+word2pos+"___"+word1+"°"+word1pos, p1); 
//				it++;
			}
			
			app.animale = "elephantIS";
		} catch (SQLException e) {e.printStackTrace();}
		System.out.println("Fine caricamento similarita'");
	}
	
	private static void caricoSimilaritaCoppieDiviseRes() {
		app.animale = "manualeres";
		String sql;
		ResultSet rs;
		String tabellasimilarita = "";

		tabellasimilarita = similarita+"possimilarity"+app.animale;

		sql="SELECT DISTINCT * from "+app.db+"."+tabellasimilarita+";";
		Double valore = 0.0;
		int it = 0;
		System.out.println("Inizio caricamento similarita' da "+tabellasimilarita);
		try {
			rs = st.executeQuery(sql); 
			while (rs.next()){
				valore = rs.getDouble("similarity");
				String word1 = "";
				String word2 = "";
				String word1pos = rs.getString("word1tipo");
				String word2pos = rs.getString("word2tipo");
				word1 = rs.getString("word1");
				word2 = rs.getString("word2");
				
				if (valore < 0.0)
					valore = 0.0;
				
				PathParoleString p = null;
				PathParoleString p1 = null;
				p = new PathParoleString(word1, word2, valore, word1pos, word2pos);
				p1 = new PathParoleString(word2, word1, valore, word2pos, word1pos);
				
				
				
				app.mappares.put(word1+"°"+word1pos+"___"+word2+"°"+word2pos, p);
				app.mappares.put(word2+"°"+word2pos+"___"+word1+"°"+word1pos, p1); 
//				it++;
			}
			
			app.animale = "elephantIS";
		} catch (SQLException e) {e.printStackTrace();}
		System.out.println("Fine caricamento similarita'");
	}
	
	private static void inserisciRisultatiSimilaritaCoppieDiviseInDbIS(String algoritmoSimilarity) {
		try {
			String name="";
			app.animale = "manuale";
			String similarity = app.similarita;
			if(algoritmoSimilarity.equals("Path")){
				algoritmoSimilarity ="";
				name  = "CoppieDivise.sql";
			}else if(algoritmoSimilarity.equals("Res")){
				name  = "CoppieDiviseRes.sql";
			}
			BufferedWriter wri = new BufferedWriter(new FileWriter("xDB/Query/query"+similarity+name));
			wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
			BufferedReader a = new BufferedReader(new FileReader("xFileCoppieDivise/fileCoppieDiviseSimilarity"+algoritmoSimilarity+".txt"));
			
			String query = "";
			query = "SHOW TABLES LIKE '"+similarity+"possimilarity"+app.animale+"';";

			ResultSet rs = st.executeQuery(query); 
			if (!rs.next()){
//				System.out.println("CREATE TABLE IF NOT EXISTS `"+similarity+"similarity` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL;");
				st.executeUpdate("CREATE TABLE IF NOT EXISTS `"+similarity+"possimilarity"+app.animale+"` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL);");
			}
			else{
				st.executeUpdate("DELETE from `"+similarity+"possimilarity"+app.animale+"`");
			}
			rs.close();

			String line;
			int numquery = 0;
			while ((line = a.readLine()) != null) {
				line = line.replace("'", "''");
			    StringTokenizer stk = new StringTokenizer(line, "  ");
			    int count = StringUtils.countMatches(line, "#");
			    if (stk.hasMoreTokens() && count>=4){
				    String primoterminepiece = stk.nextToken().trim();
				    String secondoterminepiece = stk.nextToken().trim();
				    String valore = stk.nextToken().trim();
				    StringTokenizer primoterminetok = new StringTokenizer(primoterminepiece, "#");
				    StringTokenizer secondoterminetok = new StringTokenizer(secondoterminepiece, "#");
				    String primotermine = primoterminetok.nextToken();
				    String primoterminetipo = primoterminetok.nextToken();
				    String secondotermine = secondoterminetok.nextToken();
				    String secondoterminetipo = secondoterminetok.nextToken();
				    String sql = "";
				   	sql="SELECT * FROM "+similarity+"possimilarity"+app.animale+" where (word1 = '"+primotermine+"' AND word2 = '"+secondotermine+"') OR (word2 = '"+primotermine+"' AND word1 = '"+secondotermine+"');";
				    
					rs = st.executeQuery(sql);
					if (!rs.next()){
				    	if ((numquery%299)==0){
							wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"');\n");
							wri.flush();
							wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
					    }
					    else{
//					    	if(rs.isLast())
//					    		wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"')");
//					    	else
					    		wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
							wri.flush();
					    	
					    }
						System.out.println("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
					    numquery++;
					}
//					qui qualcosa per eseguire wri
			    }
			}
			
			app.animale = "elephantIS";
		} catch (Exception e) { e.printStackTrace();}
	}	    
	
	private static void caricaParoleDefinitionCount(){
		String query = "SELECT * FROM  `parolecontenuteindefinitioncount`;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String parola = rs.getString("parola");
				Integer count = rs.getInt("count");
				app.paroleDefinitionCount.put(parola, count);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	private static void definisciNomeTabelle(){
		tabelladef = "definitionsrl"+app.animale;
		tabellasrlrel = "srlrelations"+app.animale;
	}
	
	private static String replacePunctuation(String line){
    	line = line.replace(" ", "");
    	line = line.replace(".", "");
    	line = line.replace(":", "");
    	line = line.replace(",", "");
    	line = line.replace("(", "");
    	line = line.replace(")", "");
    	line = line.replace("'", "");
    	line = line.replace("\"", "");
    	line = line.replace(";", "");
    	line = line.replace("-", "");
    	line = line.replace("_", "");
    	return line;
	}

	private static void lanciaSRL(String nomeFile, String nomeAzione){
		List<String> contesto = new LinkedList<String>();
		List<String> descrizioneAzione = new LinkedList<String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(nomeFile));
			System.out.println(nomeFile);
	        String line = "";
	        while((line = br.readLine()) != null){
//	        	String lt = replacePunctuation(line);
//	        	if (StringUtils.isAllUpperCase(lt)){
//	        		line = line.toLowerCase(); 
//	        		line = line.replace(" .", ".");
//	        		line = line.replace(".", " .");
//	        		line = line.replace(" ,", ",");
//	        		line = line.replace(",", " ,");
//	        		contesto.add(line.toLowerCase()); // aggiunto il toLowerCase perche' crea problemi il parsing di parole tutte maiuscole!
	        		
//	        	}
//	        	else
	        		descrizioneAzione.add(line);
	        }
			
//	        Frase f = new Frase (contesto, descrizioneAzione, nomeAzione);
	        Frase f = new Frase (descrizioneAzione, nomeAzione);
	        System.out.println(f.toString());
	        listaFrasi.add(f);
	        identificaSRLdaFrase(f);
	        
		} catch (IOException e) {e.printStackTrace();}
	}
	
	private static void identificaSRLdaFrase(Frase f){
		if (!filePresente(f.azioneNumero)){
			System.out.println("debug "+f.azioneNumero);
			
			preprocessStoryboardIS(f.azioneNumero, f, "azione");
//			preprocessStoryboardIS(f.azioneNumero, f, "contesto");

			avviaSrlMSRIS(f, "azione");
//			avviaSrlMSRIS(f, "contesto");
		}
	}
	
	private static void caricaSoggetti(){
		Actor man = new Actor("man");
		app.attori.put("man", man);
		man.addAzione("give"); // offer
		man.addAzione("walk");
		
		Actor elephant = new Actor("elephant");
		app.attori.put("elephant", elephant);
		
		app.soggetti.add("elephant");
		app.soggetti.add("you");
		app.soggetti.add("screw");
		app.soggetti.add("screwdriver");
		app.soggetti.add("baby");
		app.soggetti.add("boy");
		app.soggetti.add("man");
		app.soggetti.add("nut");
		app.soggetti.add("peanut");
	}
	
	private static void caricaOggettiVecchio(){
		app.oggetti.put("peanut", "");
	}

	private static void caricaParoleParticolari(){
		app.paroleParticolari.add("on");
		app.paroleParticolari.add("with");
		app.paroleParticolari.add("in");
	}

	// TODO questo va sostituito dai dati e i metodi dell'ontologia
	private static void caricaFrasi(){
		List<String> listaFile = trovaListaFileDescrizioni();
		caricaSoggetti();
//		caricaScene();
		caricaOggettiVecchio();
		caricaParoleParticolari();
		
		for (String s : listaFile){
			s = s.replace(".txt", "");
			String sql="SELECT * from "+app.db+"."+app.tabelladef+" WHERE AZIONE LIKE '"+s+"';";
			try {
				ResultSet rs = st.executeQuery(sql); 
				if (!rs.next() ) {
					File origin = new File("frasi/"+s+".txt");
					File destination = new File("apache-opennlp-1.5.3-bin/apache-opennlp-1.5.3/bin/Storyboard-temp.txt");
					copyFile(origin, destination);
					BufferedWriter bw = new BufferedWriter(new FileWriter("shell.sh"));
					bw.write("cd apache-opennlp-1.5.3-bin/apache-opennlp-1.5.3/bin\nsh opennlp TokenizerME en-token.bin < Storyboard-temp.txt > Storyboard-temp-tokenized.txt");
					bw.close();
					String cmd = "sh shell.sh";
					Runtime.getRuntime().exec(cmd);
					
					System.out.println("e ora aspetto ... ");
					// il thread sleep e' stato messo per far attendere al sistema la fine del comando cmd
				    Thread.sleep(3000);
					System.out.println("Fine attesa ");
					
					lanciaSRL("apache-opennlp-1.5.3-bin/apache-opennlp-1.5.3/bin/Storyboard-temp-tokenized.txt", s);
//					debug
				    System.out.println("no data "+ s);
				}
				rs.close();
				
				bw = new BufferedWriter(new FileWriter("ElephantIS_risultati.csv"));
//				bw.write("");
			}
			catch (Exception e){
				e.printStackTrace();
			}			
		}
		
		populateDbMsrIS(listaFile);
		
		String tabelladef = "definitionsrl"+app.animale;
		String tabellasrlrel = "srlrelations"+app.animale;
		String tabellaUnite = "paroleunitewordnet"+app.animale;

		try {
			// carico tree
			java.sql.PreparedStatement preparedStatement = null;
			
			String query = "INSERT INTO  `"+app.db+"`.`"+tabelladef+"` (`azione` ,`term` , `order`, `word`, `originalword`, `pos`, `type`, `tree`, `tipo`,`ispred`) VALUES "
					+ "(?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
			
			preparedStatement = app.conn.prepareStatement(query);
			
			for (String s : app.queryTree){
				String ar [] = s.split("!!!");
				preparedStatement.setString(1, ar[0]);
				preparedStatement.setString(2, ar[1]);
				preparedStatement.setInt(3, Integer.parseInt(ar[2]));
				preparedStatement.setString(4, ar[3]);
				preparedStatement.setString(5, ar[4]);
				preparedStatement.setString(6, ar[5]);
				preparedStatement.setString(7, ar[6]);
				preparedStatement.setInt(8, Integer.parseInt(ar[7]));
				preparedStatement.setString(9, ar[8]);
				preparedStatement.setString(10, ar[9]);//modificato per ispred
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			
			// carico srl
			preparedStatement = null;
			
			query = "INSERT INTO `"+app.db+"`.`"+tabellasrlrel+"` (`azione` , `term`, `predicate`, `object`, `relation`, `idsrlfield`, `tipo`) VALUES "
					+ "(?, ?, ?, ?, ?, ?, ?)";
			
			preparedStatement = app.conn.prepareStatement(query);
			
			for (String s : app.querySRL){
				String ar [] = s.split("!!!");
				preparedStatement.setString(1, ar[0]);
				preparedStatement.setString(2, ar[1]);
				preparedStatement.setString(3, ar[2]);
				preparedStatement.setString(4, ar[3]);
				preparedStatement.setString(5, ar[4]);
				if (ar[5].compareTo("") == 0 || ar[5].compareTo(" ") == 0)
					preparedStatement.setInt(6, 0);
				else
					preparedStatement.setInt(6, Integer.parseInt(ar[5]));
				preparedStatement.setString(7, ar[6]);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();

		} catch (SQLException e1) {e1.printStackTrace();}

		trovaParoleUniteWordNetIS(listaFile);
		
		try {
			// carico parole unite
			java.sql.PreparedStatement preparedStatement = null;
			
			String query = "INSERT INTO `"+app.db+"`.`"+tabellaUnite+"`(`azione`, `term`, `order`, `wordunita`, `tipo`) VALUES "
					+ "(?, ?, ?, ?, ?)";
			
			preparedStatement = app.conn.prepareStatement(query);
			
			for (String s : app.queryParoleUnite){
				String ar [] = s.split("!!!");
				preparedStatement.setString(1, ar[0]);
				preparedStatement.setString(2, ar[1]);
				preparedStatement.setInt(3, Integer.parseInt(ar[2]));
				preparedStatement.setString(4, ar[3]);
				preparedStatement.setString(5, ar[4]);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();

		} catch (Exception e1) {e1.printStackTrace();}

		identificaParoleDaConfrontareIS();
		determinaTripleMacchina();
//		qui passo a elaborazione successiva strutturata
//		per prima cosa costruisco la mia struttura
		
//		List<AzioneModel> azioni = new ArrayList<AzioneModel>();
//		List<StrumentoModel> strumenti = new ArrayList<StrumentoModel>();
//		List<OggettoModel> oggetti = new ArrayList<OggettoModel>();
		
		Map<String,AzioneModel> azioni = new HashMap<String,AzioneModel>();
		Map<String,StrumentoModel> strumenti = new HashMap<String,StrumentoModel>();
		Map<String,OggettoModel> oggetti = new HashMap<String,OggettoModel>();
		List<LivelloModel> listaBOM = new ArrayList<LivelloModel>();
		
		
		// TODO da qui, guarda bene come fare i metodi e i vari inserimenti
		
//***********		inserisco i verbi
		boolean isFileModelActive =true;
		
//		creo gli oggetti per non mandare in errore la BOM
//		successivamente da cancellare
		OggettoModel oAllenScrew = null;
		OggettoModel oBulb = null;
		OggettoModel oCover = null;
		OggettoModel oFlatBladeScrew = null;
		OggettoModel oSeat = null;
		OggettoModel oFingerScrew = null;
		OggettoModel oFilter = null;
		
		if(isFileModelActive){
			String percorsoCartella = "xBOMandModel/";
			String file = "Model.txt";
	        String line = "";
	        String fileSplitBy = ",";
	        boolean flag = false; // decide quando cominciare a riempire le liste con le relazioni fra oggetti
	        try (BufferedReader br1 = new BufferedReader(new FileReader(percorsoCartella+file))) {
	        	
	            while ((line = br1.readLine()) != null) {
	                // use comma as separator
	                String[] word = line.split(fileSplitBy);
	                
	                if(word[0].startsWith("***")){
	                	flag = true;
	                }
	                
	                if(!flag){
		                if(word[0].startsWith("a")){ // se ï¿½ un verbo
		                	if(word.length == 2){
			                	azioni.put(word[0],new AzioneModel(word[1]));
			                }else if (word.length == 3 ){
			                	azioni.put(word[0],new AzioneModel(word[1],Integer.parseInt(word[2])));
			                }
		                }
		                else if(word[0].startsWith("o")){
		                	if(word.length == 2){
			                	oggetti.put(word[0],new OggettoModel(word[1]));
			                }else if (word.length == 3 ){
			                	oggetti.put(word[0],new OggettoModel(word[1],word[2]));
			                }
		                }
		                else if(word[0].startsWith("s")){
		                	if(word.length == 2){
			                	strumenti.put(word[0],new StrumentoModel(word[1]));
			                }else if (word.length == 3 ){
			                	strumenti.put(word[0],new StrumentoModel(word[1],word[2]));
			                }
		                }
		            }else{
		            	List<AzioneModel> azioniTemp = new ArrayList<AzioneModel>();
		            	List<StrumentoModel> strumentiTemp = new ArrayList<StrumentoModel>();
		        		List<OggettoModel> oggettiTemp = new ArrayList<OggettoModel>();
		            	if(word[0].startsWith("a")){ // se ï¿½ un verbo
		                	
		                }
		                else if(word[0].startsWith("o")){
		                	
		                }
		                else if(word[0].startsWith("s")){
		                	
		                	for(int i = 1; i <word.length; i++){
			            		if(word[i].startsWith("a")){
			            			azioniTemp.add(azioni.get(word[i]));
			            		}else if(word[i].startsWith("o")){
			            			oggettiTemp.add(oggetti.get(word[i]));
			            		}
			            	}
		                	StrumentoModel strumento = strumenti.get(word[0]);
		                	strumento.AggiungiAzioniUtilizzabili(azioniTemp);
		                	strumento.AggiungiOggettiUtilizzabili(oggettiTemp);
		                }
		            }
	        	}
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        /* DEBUG - M.Giulia */
	        System.out.println("-------------------------------------------------------------------------------");
	        
	        System.out.println(azioni);
	        System.out.println(oggetti);
	        System.out.println(strumenti);
	        System.out.println("-------------------------------------------------------------------------------");
	        
	        // TODO da vedere in seguito 
	        
//	        creo le BOM leggendole da file
	        percorsoCartella = "xBOMandModel/";
			file = "BOM.txt";
	        line = "";
	        fileSplitBy = ",";
	        flag = false; 
	        try (BufferedReader br1 = new BufferedReader(new FileReader(percorsoCartella+file))) {
	        	Map<String,LivelloModel> tempLivelliTotali = new HashMap<String,LivelloModel>();
	        	int maxLV = 0;
	            while ((line = br1.readLine()) != null) {
	                // use comma as separator
	                String[] word = line.split(fileSplitBy);
	                List<Object> oggettiTemp = new ArrayList<Object>();
	            	for(int i = 1; i <word.length; i++){
	            		if(word[i].startsWith("lv")){
	            			oggettiTemp.add(tempLivelliTotali.get(word[i]));
	            		}else{
	            			oggettiTemp.add(oggetti.get(word[i]));
	            		}
	            	}
	                if(word[0].startsWith("lv")){
	                	int lv = Character.getNumericValue(word[0].charAt(2));
	                	if (lv > maxLV){
	                		maxLV = lv;
	                	}
	                	tempLivelliTotali.put(word[0],new LivelloModel(lv));
	                	tempLivelliTotali.get(word[0]).aggiungiComponenti(oggettiTemp);
	                	
	                }else if(word[0].startsWith("***")){
	                	listaBOM.add(new LivelloModel(0,maxLV,word[0], tempLivelliTotali.values()));
	                	listaBOM.get(listaBOM.size()-1).aggiungiComponenti(oggettiTemp);
	                	maxLV = 0;
	                	tempLivelliTotali.clear();
	                }  
	        	}
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
			
		}else{		
		System.out.println("inserisco i verbi");
		AzioneModel aRemove = new AzioneModel("remove");
		AzioneModel aPut_Back = new AzioneModel("put_back");
		AzioneModel aUnscrew = new AzioneModel("unscrew");
		AzioneModel aScrew = new AzioneModel("screw");
//		AzioneModel aPull_Down = new AzioneModel("pull_down");
		AzioneModel aClean = new AzioneModel("clean",1);
		AzioneModel aReplace = new AzioneModel("replace",2);
//		AzioneModel aPull_up = new AzioneModel("pull_up");
		AzioneModel aAssemble = new AzioneModel("assemble");
		AzioneModel aOpen = new AzioneModel("open");
		AzioneModel aClose = new AzioneModel("close");
		
		
//		azioni.add(aRemove);
//		azioni.add(aUnscrew);
////		azioni.add(aPull_Down);
//		azioni.add(aClean);
////		azioni.add(aPull_up);
//		azioni.add(aPut_Back);
//		azioni.add(aScrew);
//		azioni.add(aReplace);
//		azioni.add(aAssemble);
//		azioni.add(aOpen);
//		azioni.add(aClose);
//***********		inserisco gli strumenti
		System.out.println("inserisco gli strumenti");
//		CacciaviteAStella sPhillipsScrewdriver = new CacciaviteAStella("screwdriver");
//		CacciaviteATaglio sFlatBladeScrewdriver = new CacciaviteATaglio("screwdriver");
//		CacciaviteABrugola sAllenScrewdriver= new CacciaviteABrugola("screwdriver");
//		Mano sHand = new Mano("hand");
//		Panno sCloth = new Panno("cloth");
//		Martello sHammer = new Martello("hammer");
//		FiltroStrum sFilter = new FiltroStrum("filter");
//		ScatolaStrum sBox = new ScatolaStrum("box");
		
		StrumentoModel sPhillipsScrewdriver = new StrumentoModel("screwdriver","Phillips_screwdriver");
		StrumentoModel sFlatBladeScrewdriver = new StrumentoModel("screwdriver","flat-blade_screwdriver");
		StrumentoModel sAllenScrewdriver= new StrumentoModel("screwdriver","Allen_screwdriver");
		StrumentoModel sHand = new StrumentoModel("hand");
		StrumentoModel sCloth = new StrumentoModel("cloth");
		StrumentoModel sHammer = new StrumentoModel("hammer");
		StrumentoModel sFilter = new StrumentoModel("filter");
		StrumentoModel sBox = new StrumentoModel("box");
		StrumentoModel sAir = new StrumentoModel("air");
		
//		strumenti.add(sPhillipsScrewdriver);
//		strumenti.add(sAllenScrewdriver);
//		strumenti.add(sFlatBladeScrewdriver);
//		strumenti.add(sHand);
//		strumenti.add(sCloth);
//		strumenti.add(sHammer);
////		strumenti.add(sFilter);
//		strumenti.add(sBox);
//		strumenti.add(sAir);
		
//***********		inserisco gli oggetti
		System.out.println("inserisco gli oggetti");
//		ViteAStella oPhillipsScrew = new ViteAStella("screw");
//		ViteATaglio oFlatBladeScrew = new ViteATaglio("screw");
//		ViteABrugola oAllenScrew = new ViteABrugola("screw");
//		FiltroOgg oFilter = new FiltroOgg("filter");
//		ScatolaOgg oBox = new ScatolaOgg("box");
//		Area oArea = new Area("area");
//		Tappo oCap = new Tappo("cap");
//		Lampadina oBulb = new Lampadina("bulb");
//		Coperchio oCover = new Coperchio("cover");
		
		OggettoModel oPhillipsScrew = new OggettoModel("screw","Phillips_screw");
		 oFlatBladeScrew = new OggettoModel("screw","Slot_screw");
		 oAllenScrew = new OggettoModel("screw","Allen_screw");
		 oFingerScrew = new OggettoModel("screw","Finger_screw");
		 oFilter = new OggettoModel("filter");
		OggettoModel oBox = new OggettoModel("box");
		OggettoModel oArea = new OggettoModel("area");
		OggettoModel oCap = new OggettoModel("cap");
		 oBulb = new OggettoModel("bulb");
		 oCover = new OggettoModel("cover");
		 oSeat = new OggettoModel("seat");
		OggettoModel oLens = new OggettoModel("lens");
		OggettoModel oShutter = new OggettoModel("shutter");
		
//		oggetti.add(oPhillipsScrew);
//		oggetti.add(oAllenScrew);
//		oggetti.add(oFlatBladeScrew);
//		oggetti.add(oFingerScrew);
//		oggetti.add(oBox);
//		oggetti.add(oArea);
//		oggetti.add(oCap);
//		oggetti.add(oBulb);
//		oggetti.add(oCover);
//		oggetti.add(oFilter);
//		oggetti.add(oSeat);
//		oggetti.add(oLens);
//		oggetti.add(oShutter);
//		inserisco le limitazioni ai verbi
//		System.out.println("inserisco le limitazioni ai verbi");
		aRemove.AggiungiOggettiUtilizzabili(Stream.of(oBox,oCap,oFilter,oBulb,oCover,oSeat).collect(Collectors.toList()));
		aRemove.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		aOpen.AggiungiOggettiUtilizzabili(Stream.of(oShutter).collect(Collectors.toList()));
		aOpen.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		aClose.AggiungiOggettiUtilizzabili(Stream.of(oShutter).collect(Collectors.toList()));
		aClose.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		aPut_Back.AggiungiOggettiUtilizzabili(Stream.of(oLens,oShutter,oBox,oCap,oFilter,oBulb,oCover,oSeat).collect(Collectors.toList()));
		aPut_Back.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		aUnscrew.AggiungiOggettiUtilizzabili(Stream.of(oPhillipsScrew, oAllenScrew,oFlatBladeScrew,oFingerScrew,oCap,oBulb).collect(Collectors.toList()));
		aUnscrew.AggiungiStrumentiUtilizzabili(Stream.of(sPhillipsScrewdriver,sFlatBladeScrewdriver,sHand).collect(Collectors.toList()));
						
		aScrew.AggiungiOggettiUtilizzabili(Stream.of(oPhillipsScrew,oAllenScrew,oFlatBladeScrew,oFingerScrew,oCap,oBulb).collect(Collectors.toList()));
		aScrew.AggiungiStrumentiUtilizzabili(Stream.of(sPhillipsScrewdriver,sFlatBladeScrewdriver,sHand).collect(Collectors.toList()));
		
		aClean.AggiungiOggettiUtilizzabili(Stream.of(oArea,oBulb,oBox,oCap,oFilter,oCover,oSeat).collect(Collectors.toList()));
		aClean.AggiungiStrumentiUtilizzabili(Stream.of(sCloth,sAir).collect(Collectors.toList()));
	
		
		aReplace.AggiungiOggettiUtilizzabili(Stream.of(oFilter, oBulb).collect(Collectors.toList()));
		aReplace.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));

		aAssemble.AggiungiOggettiUtilizzabili(Stream.of(oBox,oCap,oFilter,oBulb,oCover,oSeat).collect(Collectors.toList()));
		aAssemble.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
//		inserisco le limitazioni agli strumenti
//		System.out.println("inserisco le limitazioni agli strumenti");
//		(sPhillipsScrewdriver),(sFlatBladeScrewdriver);(sHand);sCloth);(sHammer);(sFilter);(sBox);
		
		sPhillipsScrewdriver.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		sPhillipsScrewdriver.AggiungiOggettiUtilizzabili(Stream.of(oPhillipsScrew).collect(Collectors.toList()));
		
		sFlatBladeScrewdriver.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		sFlatBladeScrewdriver.AggiungiOggettiUtilizzabili(Stream.of(oFlatBladeScrew).collect(Collectors.toList()));
		
		sAllenScrewdriver.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		sAllenScrewdriver.AggiungiOggettiUtilizzabili(Stream.of(oAllenScrew).collect(Collectors.toList()));
				
		sHand.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew,aPut_Back,aRemove,aReplace,aAssemble,aOpen,aClose).collect(Collectors.toList()));
		sHand.AggiungiOggettiUtilizzabili(Stream.of(oBox,oBulb,oCap,oFilter,oCover,oFilter,oFingerScrew,oSeat,oShutter).collect(Collectors.toList()));
		
		sCloth.AggiungiAzioniUtilizzabili(Stream.of(aClean).collect(Collectors.toList()));
		sCloth.AggiungiOggettiUtilizzabili(Stream.of(oBox,oBulb,oCap,oFilter,oArea,oCover,oSeat,oLens).collect(Collectors.toList()));
		
		sAir.AggiungiAzioniUtilizzabili(Stream.of(aClean).collect(Collectors.toList()));
		sAir.AggiungiOggettiUtilizzabili(Stream.of(oLens).collect(Collectors.toList()));
//		sHammer.AggiungiAzioniUtilizzabili(Stream.of(a).collect(Collectors.toList()));
//		sHammer.AggiungiOggettiUtilizzabili(Stream.of(oBox,oBulb,oCap,oFilter,oArea).collect(Collectors.toList()));

		sFilter.AggiungiAzioniUtilizzabili(Stream.of(aRemove,aPut_Back,aReplace).collect(Collectors.toList()));
		sFilter.AggiungiOggettiUtilizzabili(Stream.of(oBox).collect(Collectors.toList()));
		
		sBox.AggiungiAzioniUtilizzabili(Stream.of(aRemove,aPut_Back,aReplace).collect(Collectors.toList()));
		sBox.AggiungiOggettiUtilizzabili(Stream.of(oBox).collect(Collectors.toList()));
		
//		inserisco le limitazioni agli oggetti
//		System.out.println("inserisco le limitazioni agli oggetti");
//		(oPhillipsScrew);(oFlatBladeScrew);oBox);(oArea);oCap);(oBulb); (oCover)
	
		oPhillipsScrew.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		oPhillipsScrew.AggiungiStrumentiUtilizzabili(Stream.of(sPhillipsScrewdriver).collect(Collectors.toList()));
		
		oFlatBladeScrew.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		oFlatBladeScrew.AggiungiStrumentiUtilizzabili(Stream.of(sFlatBladeScrewdriver).collect(Collectors.toList()));
		
		oFingerScrew.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		oFingerScrew.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		oAllenScrew.AggiungiAzioniUtilizzabili(Stream.of(aScrew,aUnscrew).collect(Collectors.toList()));
		oAllenScrew.AggiungiStrumentiUtilizzabili(Stream.of(sAllenScrewdriver).collect(Collectors.toList()));
		
		oSeat.AggiungiAzioniUtilizzabili(Stream.of(aClean,aRemove,aReplace,aPut_Back).collect(Collectors.toList()));
		oSeat.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		oBox.AggiungiAzioniUtilizzabili(Stream.of(aClean,aRemove,aReplace,aPut_Back).collect(Collectors.toList()));
		oBox.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		oArea.AggiungiAzioniUtilizzabili(Stream.of(aClean).collect(Collectors.toList()));
		oArea.AggiungiStrumentiUtilizzabili(Stream.of(sCloth).collect(Collectors.toList()));
		
		oBulb.AggiungiAzioniUtilizzabili(Stream.of(aClean,aScrew,aUnscrew,aRemove,aReplace,aPut_Back).collect(Collectors.toList()));
		oBulb.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
		oCover.AggiungiAzioniUtilizzabili(Stream.of(aClean,aRemove,aReplace,aPut_Back).collect(Collectors.toList()));
		oCover.AggiungiStrumentiUtilizzabili(Stream.of(sHand).collect(Collectors.toList()));
		
//		Lista di tutte le BOM
		listaBOM = new ArrayList<LivelloModel>();
//		Costruisco la BOM di Fanalino
		
		
		LivelloModel lv1Fanalino = new LivelloModel(1);
		LivelloModel fanalino = new LivelloModel(0,1,"***standardrearlight",Stream.of(lv1Fanalino).collect(Collectors.toList()));

//		lv1F.aggiungiComponenti(Stream.of(oCover,oFlatBladeScrew).collect(Collectors.toList()));
		lv1Fanalino.aggiungiComponenti(Stream.of(oggetti.get("oAllenScrew"),oggetti.get("oBulb")).collect(Collectors.toList()));
		fanalino.aggiungiComponenti(Stream.of(lv1Fanalino,oggetti.get("oCover"),oggetti.get("oFlatBladeScrew")).collect(Collectors.toList()));
		
		LivelloModel lv1Fanalino2 = new LivelloModel(1);
		LivelloModel fanalino2 = new LivelloModel(0,1,"***standardrearlight2",Stream.of(lv1Fanalino2).collect(Collectors.toList()));
		lv1Fanalino2.aggiungiComponenti(Stream.of(oggetti.get("oAllenScrew"),oggetti.get("oBulb")).collect(Collectors.toList()));
		fanalino2.aggiungiComponenti(Stream.of(lv1Fanalino2,oggetti.get("oCover"),oggetti.get("oFlatBladeScrew")).collect(Collectors.toList()));
//		fanalino.aggiungiLivelli(Stream.of(lv1F,lv2F).collect(Collectors.toList()));

//		costruisca la BOM della pulizia filtro	
		LivelloModel lv1Filtro = new LivelloModel(1);
		LivelloModel filtro = new LivelloModel(0,1,"***cleaningtheairfilter",Stream.of(lv1Filtro).collect(Collectors.toList()));
		lv1Filtro.aggiungiComponenti(Stream.of(oggetti.get("oFingerScrew"),oggetti.get("oFilter")).collect(Collectors.toList()));
		filtro.aggiungiComponenti(Stream.of(lv1Filtro,oggetti.get("oSeat"),oggetti.get("oFilter")).collect(Collectors.toList()));
																					  //mattici oCover
//--------------------------------------------------------------------------------
		LivelloModel lv1Filtro2 = new LivelloModel(1);
		LivelloModel filtro2 = new LivelloModel(0,1,"***cleaningtheairfilter2",Stream.of(lv1Filtro2).collect(Collectors.toList()));
		lv1Filtro2.aggiungiComponenti(Stream.of(oggetti.get("oFingerScrew"),oggetti.get("oFilter")).collect(Collectors.toList()));
		filtro2.aggiungiComponenti(Stream.of(lv1Filtro2,oggetti.get("oSeat"),oggetti.get("oCover")).collect(Collectors.toList()));
		
		LivelloModel lv1Filtro3 = new LivelloModel(1);
		LivelloModel filtro3 = new LivelloModel(0,1,"***cleaningtheairfilter3",Stream.of(lv1Filtro3).collect(Collectors.toList()));
		lv1Filtro3.aggiungiComponenti(Stream.of(oggetti.get("oFingerScrew"),oggetti.get("oFilter")).collect(Collectors.toList()));
		filtro3.aggiungiComponenti(Stream.of(lv1Filtro3,oggetti.get("oSeat"),oggetti.get("oCover")).collect(Collectors.toList()));
//		aggiungo tutte le BOM alla lista di BOM
		
		listaBOM.addAll(Stream.of(fanalino,fanalino2,filtro,filtro2,filtro3).collect(Collectors.toList()));

		
		}
//		Controllo le triple trovate e se necessario le completo con lo strumento implicito opportuno
//		travaso le stringhe macchina trovate in liste di stringhe
		
		List<String> azioniMacchina = new ArrayList<String>();
		List<String> strumentiMacchina = new ArrayList<String>();
		List<String> oggettiMacchina = new ArrayList<String>();
		List<TriplaModel> listaTripleModelComplete = new ArrayList<TriplaModel>();
		List<TriplaModel> listaTripleModelBOM = new ArrayList<TriplaModel>();
		List<TriplaModel> listaTripleModelNoBOM = new ArrayList<TriplaModel>();
		
//		for (RisultatoTriplaMacchina rtm : listaRisTriplaMacchina){
//			azioniMacchina.add(rtm.getVerboMacchina());
//			strumentiMacchina.add(rtm.getStrumentoMacchina());
//			oggettiMacchina.add(rtm.getOggettoMacchina());
//		}
		int i = 1;
		LivelloModel BOM = null;
		int lvMaxBOM=0 ;
		boolean isBOMActive = false;
		boolean flag1 = false;
		for (RisultatoTriplaMacchina rtm : listaRisTriplaMacchina){
			String azioneMacchina = rtm.getVerboMacchina();
			String strumentoMacchina = rtm.getStrumentoMacchina();
			String oggettoMacchina = rtm.getOggettoMacchina();
			System.out.println("TRIPLA:" + azioneMacchina + "-" + oggettoMacchina + "-" + strumentoMacchina);
			
			AzioneModel am = isAzioneModel(azioni.values(), azioneMacchina);
			StrumentoModel sm = isStrumentoModel(strumenti.values(),strumentoMacchina);
			OggettoModel om = null;
			String nomeBOM = null;
			
			
			if(app.ontologyName.equals("Scooter"))
	    	   {
	    	   if(rtm.getNameFile().compareTo("ACTION1_99") == 0)
	    		   continue;
	    	   if(rtm.getNameFile().compareTo("ACTION1_95") == 0)
	    		   continue;
	    	   if(rtm.getNameFile().compareTo("ACTION1_94") == 0)
	    		   continue;
	    	   }
	    	   else
	    	   {
		    	   if(rtm.getNameFile().compareTo("ACTION1_0") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_1") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_2") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_3") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_4") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_7") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_9") == 0)
		    		   continue;
		    	   if(rtm.getNameFile().compareTo("ACTION1_91") == 0)
		    		   continue;
	    		   
	    	   }
			
			if(rtm.getNomeBOM()!=null){
				boolean flag=false;
				nomeBOM = rtm.getNomeBOM();
				isBOMActive = true;
				for(LivelloModel lm : listaBOM){
					if(lm.getIdName().equals(nomeBOM)){
						BOM = lm;
						flag = true;
						lvMaxBOM = BOM.getLvMaxBOM();
						
					}
				}
				if(flag)
					System.out.println("__________________"+nomeBOM);
					continue;
			}
			
			
			if(!isBOMActive || BOM.getStatoLivello()==2){
				om = isOggettoModel(oggetti.values(), oggettoMacchina);
				flag1 = false;
				
			}
			else if(isBOMActive){
//				if(islv0Done(fanalino)){
//					System.out.println("falso");
//				}
				if(lvMaxBOM!=0){
					if(isLvMaxRimontato(BOM, lvMaxBOM)){
						
						lvMaxBOM--;
					}
				}
				om = isOggettoModelBOM(null,BOM,lvMaxBOM, oggettoMacchina,am,islv0Done(BOM));
				isMaintenanceDone(BOM);
				flag1 = true;
			}
			
			if(sm == null){
				sm = assegnaStrumentoImplicito(am,om,strumenti.values(),azioni.values(),oggetti.values());
			}

			
			TriplaModel tm = new TriplaModel(am, om, sm);
			listaTripleModelComplete.add(tm);
			if(flag1){
				listaTripleModelBOM.add(tm);
			}else{
				listaTripleModelNoBOM.add(tm);
				
			}
			
			System.out.println("TriplaModelFinale"+i+"::-"+tm.toString());
			
			i++;
		}
//		carico  i risultati corretti
		boolean flag3=true;
		if(flag3){
		String percorsoCartella = "xRisultatiManuale/";
		String csvFile = "risultatiCorretti" + app.ontologyName + ".csv";
        String line = "";
        String cvsSplitBy = ",";
        List<RisultatoTriplaMacchina> listaParoleCorrette = new ArrayList<RisultatoTriplaMacchina>();
        try (BufferedReader br = new BufferedReader(new FileReader(percorsoCartella+csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] word = line.split(cvsSplitBy);

               
                RisultatoTriplaMacchina r = new RisultatoTriplaMacchina(word[0], word[1], word[2], null, null);
                listaParoleCorrette.add(r);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        int paroleBOMCorrette = 0;
        int paroleBOMErrate = 0;
        int tripleBOMCorrette = 0;
        int tripleNoBOMCorrette = 0;
        int tripleCorrette = 0;
        try (BufferedReader br1 = new BufferedReader(new FileReader(percorsoCartella+"risultatiCorrettiBOM" + app.ontologyName + ".csv"))) {
        	int temp =0;
            while ((line = br1.readLine()) != null) {
            	boolean verb = false;
            	boolean obj = false;
            	boolean strum = false;
                // use comma as separator
                String[] word = line.split(cvsSplitBy);
                TriplaModel tm = null;
                if(listaTripleModelBOM.size()<=temp){
                	
                }else
                  tm = listaTripleModelBOM.get(temp);
                
               if(tm!=null){
                if(tm.getAzione()!=null && tm.getAzione().getNome().equals(word[0])){
                	paroleBOMCorrette++;
                	verb = true;
                }else{
                	paroleBOMErrate++;
                	
                }
                if(tm.getOggetto()!=null && tm.getOggetto().getNomeSpecifico().equals(word[1])){
                	paroleBOMCorrette++;
                	obj = true;
                	
                }else{
                	paroleBOMErrate++;
                }
                if(tm.getStrumento()!= null && tm.getStrumento().getNomeSpecifico().equals(word[2])){
                	paroleBOMCorrette++;
                	strum = true;
                }else{
                	paroleBOMErrate++;
                }
               }else{
            	   paroleBOMErrate = paroleBOMErrate+3;
               }
               if(verb && obj && strum){
            	   tripleCorrette++;
            	   tripleBOMCorrette++;
               }
                temp++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        int paroleNoBOMCorrette = 0;
        int paroleNoBOMErrate = 0;
        try (BufferedReader br2 = new BufferedReader(new FileReader(percorsoCartella+"risultatiCorrettiNoBOM"+app.ontologyName+".csv"))) {
           int temp = 0;
            while ((line = br2.readLine()) != null) {
            	boolean verb = false;
            	boolean obj = false;
            	boolean strum = false;
                // use comma as separator
                String[] word = line.split(cvsSplitBy);
                TriplaModel tm = null;
                if(listaTripleModelNoBOM.size()<=temp){
                	
                }else
                  tm = listaTripleModelNoBOM.get(temp);
                if(tm!=null){
                if(tm.getAzione()!=null && tm.getAzione().getNome().equals(word[0])){
                	paroleNoBOMCorrette++;
                	verb = true;
                }else{
                	paroleNoBOMErrate++;
                }
                if(tm.getOggetto()!=null && tm.getOggetto().getNomeSpecifico().equals(word[1])){
                	paroleNoBOMCorrette++;
                	obj = true;
                }else{
                	paroleNoBOMErrate++;
                }
                if(tm.getStrumento()!=null && tm.getStrumento().getNomeSpecifico().equals(word[2])){
                	paroleNoBOMCorrette++;
                	strum = true;
                }else{
                	paroleNoBOMErrate++;
                }
                }else{
             	   paroleNoBOMErrate = paroleNoBOMErrate+3;
                }
                if(verb && obj && strum){
             	   tripleCorrette++;
             	   tripleNoBOMCorrette++;
                }
                 temp++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
		
		int tripleTot=i-1;
		int count = 0;
		int paroleTotaliTriple = tripleTot*3;
		int paroleTrovate = 0;
		int paroleMancanti = 0;
		int paroleCorrette = 0 ;
		int paroleErrate = 0;
		
		try{
		String fileDaScrivere = ""+percorsoCartella+"risultatiManuale.csv";
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileDaScrivere));
		
		String azione = "";
		String oggetto = "";
		String strumento = "";
		
		RisultatoTriplaMacchina r = null;
			for (TriplaModel tm: listaTripleModelComplete){
				r = listaParoleCorrette.get(count);
				if(tm != null ){
					if(tm.getAzione()!=null){
						azione  = tm.getAzione().getNome();
						if(azione.equals(r.getVerboMacchina())){
							paroleCorrette++;
						}else{
							paroleErrate++;
						}
						paroleTrovate++;
					}else{
						azione = "NO_VERB";
						paroleMancanti++;
					}
					if(tm.getOggetto()!=null){
						oggetto =tm.getOggetto().getNomeSpecifico();
						if(oggetto.equals(r.getOggettoMacchina())){
							paroleCorrette++;
						}else{
							paroleErrate++;
						}
						paroleTrovate++;
					}else{
						oggetto = " NO_OBJ";
						paroleMancanti++;
					}
					if(tm.getStrumento()!= null){
						strumento = tm.getStrumento().getNomeSpecifico();
						if(strumento.equals(r.getStrumentoMacchina())){
							paroleCorrette++;
						}else{
							paroleErrate++;
						}
						paroleTrovate++;
					}else{
						strumento = " NO_STRUM";
						paroleMancanti++;
					}
						writer.write(azione+","+oggetto+","+strumento+"\n");
						writer.flush();
						
						
					}else{
						paroleMancanti = paroleMancanti+3;
					}
				count++;
				}
			writer.close();
			
		} catch (Exception e) {
			e.printStackTrace();

		}
		
		int tripleBOMTot = listaTripleModelBOM.size();
		int tripleNoBOMTot = listaTripleModelNoBOM.size();
		System.out.print("paroleTotaliTriple :   "+paroleTotaliTriple+"\n"+
						 "paroleMancanti     :   "+ paroleMancanti+"        "+(String.format("%.2f", ((double)paroleMancanti/(double)paroleTotaliTriple*100)))+"%  percParoleMancanti \n"+
						 "paroleTrovate      :   "+paroleTrovate+"          "+(String.format("%.2f",((double)paroleTrovate/(double)paroleTotaliTriple*100)))+"%   percParoleTrovate  \n"+
						 "paroleCorrette     :   "+paroleCorrette+"         "+(String.format("%.2f",((double)paroleCorrette/(double)paroleTotaliTriple*100)))+"%  percParoleCorrette :\n"+
						 "paroleErrate       :   "+paroleErrate+"           "+(String.format("%.2f",((double)paroleErrate/(double)paroleTotaliTriple*100)))+"%   "+(String.format("%.2f",(((double)paroleErrate+(double)paroleMancanti)/(double)paroleTotaliTriple*100)))+"%  totErr\n"+
						 "paroleBOMCorrette  :   "+paroleBOMCorrette+"      "+(String.format("%.2f",((double)paroleBOMCorrette/((double)paroleBOMCorrette+(double)paroleBOMErrate)*100)))+"%  percParoleBOMCorrette \n"+
						 "paroleBOMErrate    :   "+paroleBOMErrate+"        "+(String.format("%.2f",((double)paroleBOMErrate/((double)paroleBOMCorrette+(double)paroleBOMErrate)*100)))+"%  percParoleBOMErrate \n"+
						 "paroleNoBOMCorrette:   "+paroleNoBOMCorrette+"    "+(String.format("%.2f",((double)paroleNoBOMCorrette/((double)paroleNoBOMCorrette+(double)paroleNoBOMErrate)*100)))+"%  percParoleNoBOMCorrette \n"+
						 "paroleNoBOMErrate  :   "+paroleNoBOMErrate+"      "+(String.format("%.2f",((double)paroleNoBOMErrate/((double)paroleNoBOMCorrette+(double)paroleNoBOMErrate)*100)))+"%  percParoleNoBOMErrate \n"+
						 "tripleTotali       :   "+tripleTot+"\n"+
						 "tripleCorrette     :   "+tripleCorrette+"      "+(String.format("%.2f",(((double)tripleBOMCorrette+(double)tripleNoBOMCorrette)/(double)tripleTot*100)))+"%  percTripleCorrettte \n"+
						 "tripleBOMCorrette  :   "+tripleBOMCorrette+"      "+(String.format("%.2f",((double)tripleBOMCorrette/(double)tripleBOMTot*100)))+"%  perctripleBOMCorrette \n"+
						 "tripleNoBOMCorrette:   "+tripleNoBOMCorrette+"    "+(String.format("%.2f",((double)tripleNoBOMCorrette/(double)tripleNoBOMTot*100)))+"%  perctripleNoBOMCorrette \n");
						
		}
		boolean stampaFileRis = false; 
		if(stampaFileRis){
		try {
			// carico risultati in tabella
			java.sql.PreparedStatement preparedStatement = null;
			
			st.executeUpdate("DELETE FROM `"+app.db+"`.`risultati`");

			String query = "INSERT INTO `"+app.db+"`.`risultati`(`azione`, `testo`, `risultato`, `similarita`, `azionerisultante`) VALUES "
					+ "(?, ?, ?, ?, ?)";
			
			preparedStatement = app.conn.prepareStatement(query);
			
			for (String s : app.queryTabella){
				String ar [] = s.split("!!!");
				preparedStatement.setString(1, ar[0]);
				preparedStatement.setString(2, ar[1]);
				preparedStatement.setString(3, ar[2]);
				preparedStatement.setString(4, ar[3]);
				preparedStatement.setString(5, ar[4]);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();

		} catch (Exception e1) {e1.printStackTrace();}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {e.printStackTrace();}
		
		creaFileRisultatiFinaliAzione();
		creaFileRisultatiFinaliContesto();
		}
	}
	
	
	private static StrumentoModel assegnaStrumentoImplicito(AzioneModel am,	OggettoModel om,
															Collection<StrumentoModel> strumenti,
															Collection<AzioneModel> azioni,
															Collection<OggettoModel> oggetti) {
		for(StrumentoModel sm : strumenti){
			if(sm.getAzioni().contains(am) && sm.getOggetti().contains(om)){
				return sm;
				
			}
			
		}
		return null;
	}

	private static  boolean islv0Done(LivelloModel lv0){
		Map<Object,Integer> componenti = lv0.getComponenti();
		for( Integer i : componenti.values()){
			if(i==2){
				return true;
			}
			if(i!=0){
				return false;
			}
		}
		return true;
	}
	
	private static  boolean isMaintenanceDone(LivelloModel lv0){
		
		Map<Object,Integer> componenti = lv0.getComponenti();
		
		for( Integer i : componenti.values()){
			if(i==0 || i ==1){
				return false;
			}
			
		}
		lv0.finito();
		return true;
			
	}
	
	private static boolean isLvMaxRimontato(LivelloModel BOM, int lvMaxBOMReached){
		List<LivelloModel> listaTotaleLivelli = BOM.getListaTotaleLivelli();
		for(LivelloModel lm : listaTotaleLivelli){
			int lv = lm.getLivelloBOM();
			if(lv ==lvMaxBOMReached){
				if(lm.getStatoLivello()!=2){
					return false;
				}
			}
		}
		return true;
	}
	private static OggettoModel isOggettoModelBOM(Map<Object,Integer> componentiSup,LivelloModel fan,int lvMax, String oggettoMacchina,AzioneModel am, boolean islv0Done) {
		
		Map<Object,Integer> componenti = fan.getComponenti();
		OggettoModel oggettoBOM = null; 
		if(islv0Done  ){// se lo stato e' 0 ed e' il primo livello devo cominciare a rimontare
				
			for(Map.Entry<Object, Integer> entry : componenti.entrySet()){
			if(fan.getLivelloBOM() == lvMax){	
				if(entry.getValue() == 0 && entry.getKey() instanceof OggettoModel){
					if(((OggettoModel) entry.getKey()).getNome().equals(oggettoMacchina)){
						oggettoBOM = (OggettoModel) entry.getKey();
						fan.rimonta(oggettoBOM,am);
						if(fan.getStatoLivello()==2){
							componentiSup.put(fan, 2);
							
						}
						return oggettoBOM;
					}
				}
			}
		}
			for(Map.Entry<Object, Integer> entry : componenti.entrySet()){
				if(entry.getValue() == 0 && entry.getKey() instanceof LivelloModel){
					 componentiSup = componenti;
					 oggettoBOM = isOggettoModelBOM(componentiSup,(LivelloModel)entry.getKey(),lvMax, oggettoMacchina ,am,islv0Done);
					 if(oggettoBOM != null){
						 return oggettoBOM;
					 }
				}
			}
			
			boolean reverse=false;
			if(reverse){
			List<Object> reverseOrderedKeys = new ArrayList<Object>(componenti.keySet());
			Collections.reverse(reverseOrderedKeys);
			for(Object descEntry : reverseOrderedKeys){
				if(componenti.get(descEntry) == 0 && descEntry instanceof OggettoModel){
					if((((OggettoModel) descEntry).getNome().equals(oggettoMacchina))){
						oggettoBOM = (OggettoModel) descEntry;
//						fan.rimonta((OggettoModel)descEntry); disattiva per sequenziale
						if(fan.getStatoLivello()==2){
							componentiSup.put(fan, 2);
						}
						return oggettoBOM;
					}
				}
			}
//			se invece non c' e' nessun oggetto nel livello ma solo altri livelli cerco la parola negli altri livelli
			for(Object descEntry : reverseOrderedKeys){
				if(componenti.get(descEntry) == 0 && descEntry instanceof LivelloModel){
					componentiSup = componenti;
//					oggettoBOM = isOggettoModelBOM(componentiSup,(LivelloModel)descEntry, oggettoMacchina,am,islv0Done);
					 if(oggettoBOM != null){
						 return oggettoBOM;
					 }
				}
			}
		}
		}else{
		for(Map.Entry<Object, Integer> entry : componenti.entrySet()){
			if(entry.getValue() == 1 && entry.getKey() instanceof OggettoModel){
				if(((OggettoModel) entry.getKey()).getNome().equals(oggettoMacchina)){
					oggettoBOM = (OggettoModel) entry.getKey();
//					componenti.put(entry.getKey(), 0);
					fan.smonta(oggettoBOM,am);
					if(fan.getStatoLivello()==0){
						componentiSup.put(fan, 0);
					}
					return oggettoBOM;
				}
			}
		}
//		se invece non c' e' nessun oggetto nel livello ma solo altri livelli cerco la parola negli altri livelli
		for(Map.Entry<Object, Integer> entry : componenti.entrySet()){
			if(entry.getValue() == 1 && entry.getKey() instanceof LivelloModel){
				 componentiSup = componenti;
				 oggettoBOM = isOggettoModelBOM(componentiSup,(LivelloModel)entry.getKey(),lvMax, oggettoMacchina ,am,islv0Done);
				 if(oggettoBOM != null){
					 return oggettoBOM;
				 }
			}
		}
		
		}
		return null;
	}

	private static OggettoModel isOggettoModel(Collection<OggettoModel> collection,String oggettoMacchina) {
		for(OggettoModel om : collection){
			if(om.getNome().equals(oggettoMacchina)){
				return om;
			}
			
		}
		return null;
	}

	private static StrumentoModel isStrumentoModel(
			Collection<StrumentoModel> strumenti, String strumentoMacchina) {
		for(StrumentoModel sm : strumenti){
			if(sm.getNome().equals(strumentoMacchina)){
				return sm;
			}
			
		}
		return null;
	}

	private static AzioneModel isAzioneModel(Collection<AzioneModel> collection, String azioneMacchina) {
		for(AzioneModel am : collection){
			if(am.getNome().equals(azioneMacchina)){
				return am;
			}
			
		}
		return null;
	}

	private static void determinaTripleMacchina() {
		for(RisultatoTriplaOriginale rtu : listaRisTriplaOriginale){
			SrlElemento srlVerbo = rtu.getSrlVerbo();
			SrlElemento srlOggetto = rtu.getSrlOggettoSuCuiAgire();
			SrlElemento srlStrumento = rtu.getSrlOggettoConCuiAgire();
			SrlElemento srlPosizione = rtu.getSrlPosizione();
			SrlElemento srlManiera = rtu.getSrlManiera();
			String nomeBOM = rtu.getNomeBOM();
//			modifica per Resnik e PATH insieme
			List <SrlElemento> verbiMacchinaPath = new ArrayList<SrlElemento>();
			List <SrlElemento> oggettiMacchinaPath = new ArrayList<SrlElemento>();
			List <SrlElemento> StrumentiMacchinaPath = new ArrayList<SrlElemento>();
			
//			SrlElemento srlVerboMacchina = null;
//			SrlElemento srlOggettoMacchina = null;
//			SrlElemento srlStrumentoMacchina = null;
			
			String verboMacchina = "";
			String oggettoMacchina = "";
			String strumentoMacchina = "";
			String posizioneMacchina = "";
			String manieraMacchina = "";
			
			
			double simMaxVerbo = 0.0;
			double simMaxOggetto = 0.0;
			double simMaxStrumento = 0.0;
			double simMaxPosizione = 0.0;
			double simMaxManiera = 0.0;
//			if(listaAzioni.contains(srlVerbo.getWord())){
//				verboMacchina = srlVerbo.getWord();
//				
//			}else{
//				for(String s : listaAzioni){
//					if(srlVerbo!=null){
//					double simVerbo = calcolaSimilaritaTraParole(srlVerbo.getWord(), s, null, null);
//					if(simVerbo > simMaxVerbo){// non ci sono phrasal verbs nel db non ne calcola la similarita
//						simMaxVerbo = simVerbo;
//						verboMacchina = s;
//						}
//					}
//				}
//			}
			if(onlyPath){
			if(srlVerbo!=null){
				if(listaAzioni.contains(srlVerbo.getWord())){
					verboMacchina = srlVerbo.getWord();
				}else{
				for(String s : listaAzioni){
					double simVerbo = calcolaSimilaritaTraParole(srlVerbo.getWord(), s, null, null);
					if(simVerbo > simMaxVerbo){// non ci sono phrasal verbs nel db non ne calcola la similarita
						simMaxVerbo = simVerbo;
						verboMacchina = s;
						}
					}
				}
			}
			
			if(srlOggetto!=null){
				if(listaOggetti.contains(srlOggetto.getWord())){
					oggettoMacchina = srlOggetto.getWord();
				}else{
				for(String s : listaOggetti){
					double simOggetto = calcolaSimilaritaTraParole(srlOggetto.getWord(), s, null, null);
					if(simOggetto > simMaxOggetto){
						simMaxOggetto = simOggetto;
						oggettoMacchina = s;
						}
					}
				}
			}
			
				
			if(srlStrumento != null){
				if(listaStrumenti.contains(srlStrumento.getWord())){
					strumentoMacchina = srlStrumento.getWord();
				}else{
				for(String s : listaStrumenti){
					double simStrumento = calcolaSimilaritaTraParole(srlStrumento.getWord(), s, null, null);
					if(simStrumento > simMaxStrumento){
						simMaxStrumento = simStrumento;
						strumentoMacchina = s ;
						}
					}
				}
			}
//			modificare anche per le posizioni e le maniere se serve
			for(String s : listaPosizioni){
				if(srlPosizione != null){
				double simPosizione = calcolaSimilaritaTraParole(srlPosizione.getWord(), s, null, null);
				if(simPosizione > simMaxPosizione){
					simMaxPosizione = simPosizione;
					posizioneMacchina = s ;
					}
				}
			}
			for(String s : listaManiere){
				if(srlManiera != null){
				double simManiera = calcolaSimilaritaTraParole(srlManiera.getWord(), s, null, null);
				if(simManiera > simMaxManiera){
					simMaxManiera = simManiera;
					manieraMacchina = s ;
					}
				}
			}
			}else{
			double simMaxVerbRes = 0.0;
			double simMaxObjRes = 0.0;
			double simMaxStrumRes = 0.0;
			if(srlVerbo!=null){
				if(listaAzioni.contains(srlVerbo.getWord())){
					verboMacchina = srlVerbo.getWord();
				}else{
				for(String s : listaAzioni){
					double simVerbo = calcolaSimilaritaTraParole(srlVerbo.getWord(), s, null, null);
					if(simVerbo >= simMaxVerbo){// non ci sono phrasal verbs nel db non ne calcola la similarita
						 if(simVerbo >simMaxVerbo){
							 simMaxVerbo = simVerbo;
							 verboMacchina = s;
							 simMaxVerbRes = 0.0;
						 }
						 else if(simVerbo == simMaxVerbo){
							 double simVerbRes = calcolaSimilaritaConCombinazioniResV(srlVerbo.getWord(), s);
							 if(simVerbRes >simMaxVerbRes){
								 simMaxVerbRes = simVerbRes;
								 verboMacchina = s;
							 }
						 }
						}
					}
				}
			}
			
			if(srlOggetto!=null){
				if(listaOggetti.contains(srlOggetto.getWord())){
					oggettoMacchina = srlOggetto.getWord();
				}else{
				for(String s : listaOggetti){
					double simObj = calcolaSimilaritaTraParole(srlOggetto.getWord(), s, null, null);
					if(simObj >= simMaxOggetto){
						 if(simObj >simMaxOggetto){
							 simMaxOggetto = simObj;
							 oggettoMacchina = s;
							 simMaxObjRes = 0.0;
						 }
						 else{
							 double simObjRes = calcolaSimilaritaConCombinazioniResO(srlOggetto.getWord(), s);
							 if(simObjRes >simMaxObjRes){
								 simMaxObjRes = simObjRes;
								 oggettoMacchina = s;
							 }
						 }
						}
					}
				}
			}
			
				
			if(srlStrumento != null){
				if(listaStrumenti.contains(srlStrumento.getWord())){
					strumentoMacchina = srlStrumento.getWord();
				}else{
				for(String s : listaStrumenti){
					double simStrumento = calcolaSimilaritaTraParole(srlStrumento.getWord(), s, null, null);
					if(simStrumento >= simMaxStrumento){
						 if(simStrumento >simMaxStrumento){
							 simMaxStrumento = simStrumento;
							 strumentoMacchina = s;
							 simMaxStrumRes = 0.0;
						 }
						 else{
							 double simStrumRes = calcolaSimilaritaConCombinazioniResS(srlStrumento.getWord(), s);
							 if(simStrumRes >simMaxStrumRes){
								 simMaxStrumRes = simStrumRes;
								 strumentoMacchina = s;
							 }
						 }
						}
					}
				}
			}
			}
			RisultatoTriplaMacchina rtm_tmp = new RisultatoTriplaMacchina(verboMacchina,oggettoMacchina,strumentoMacchina,posizioneMacchina,manieraMacchina,nomeBOM);

			//listaRisTriplaMacchina.add(new RisultatoTriplaMacchina(verboMacchina,oggettoMacchina,strumentoMacchina,posizioneMacchina,manieraMacchina,nomeBOM));
			// TODO: Aggiunta per dividere i file per sistema
			rtm_tmp.setNameFile(rtu.getNameFile());
			listaRisTriplaMacchina.add(rtm_tmp);
		}
		int i1 = 1;
		for(RisultatoTriplaOriginale rtu : listaRisTriplaOriginale){
			
			if(rtu.getNomeBOM()!=null){
				System.out.println("______________"+rtu.stampaSoloParole());
			}else{
				System.out.println("TriplaOriginale"+i1+"::-"+rtu.stampaSoloParole() + "-NOMEFILE:" + rtu.getNameFile());
			i1++;
			}
		}
		i1 = 1;
		for (RisultatoTriplaMacchina r : listaRisTriplaMacchina){
			
			if(r.getNomeBOM()!=null){
				System.out.println("______________"+r.toString());
			}else{
				System.out.println("TriplaMacchina"+i1+"::-"+r.toString());
			i1++;
			}
		}
	}

	private static void creaFileRisultatiFinaliContesto(){
		Map<String, RisultatoFinale> mappa = new TreeMap<String, RisultatoFinale>();
		String query = "";
		ResultSet rs;

		int num = 0;
		List<String> azione = new LinkedList<String>();
		if (app.azioneConsiderata != 0){
			query = "SELECT * FROM  `"+app.db+"`.`contesticonsiderati` a WHERE a.tipo = "+app.azioneConsiderata+" ;";
			try {
				rs = st.executeQuery(query);
				while (rs.next()){
					azione.add(rs.getString("azione"));
					num++;
				}
			} catch (SQLException e) {e.printStackTrace();}
		}else{
			query = "SELECT * FROM  `"+app.db+"`.`contesticonsiderati`;";
			try {
				rs = st.executeQuery(query);
				while (rs.next()){
					azione.add(rs.getString("azione"));
					num++;
				}
			} catch (SQLException e) {e.printStackTrace();}
		}
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("ElephantIS_contestoFin.csv"));

			for (String s : azione){
				if (app.risultatoContesto.containsKey(s)){
					for (String str : app.risultatoContesto.get(s)){
						bw.write(str);
						bw.flush();
					}
				}
			}

			bw.close();
			
		}catch (Exception e){e.printStackTrace();}
	}
	
	private static void creaFileRisultatiFinaliAzione(){
		Map<String, RisultatoFinale> mappa = new TreeMap<String, RisultatoFinale>();
		String query = "";
		
		Map<Integer, String> ris = new HashMap<Integer, String>();
		ris.put(1, "walk_around");
		ris.put(2, "curtsy");
		ris.put(3, "eat");
		ris.put(4, "huddle");

		ResultSet rs;
		int num = 0;
		if (app.azioneConsiderata != 0){
			query = "SELECT * FROM  `"+app.db+"`.`azioniconsiderate` a WHERE a.tipo = "+app.azioneConsiderata+" ;";
			try {
				rs = st.executeQuery(query);
				while (rs.next()){
					num++;
				}
			} catch (SQLException e) {e.printStackTrace();}
		}

		if (app.azioneConsiderata == 0)
			query = "SELECT * FROM  `"+app.db+"`.`risultati` r, `"+app.db+"`.`azioniconsiderate` a WHERE r.azione LIKE a.azione ;";
		else if (app.azioneConsiderata == -1)
			query = "SELECT * FROM  `manutenzione`.`risultati` r WHERE r.azione NOT IN (SELECT azione FROM `manutenzione`.`azioniconsiderate`) ;";
		else
			query = "SELECT * FROM  `"+app.db+"`.`risultati` r, `"+app.db+"`.`azioniconsiderate` a WHERE r.azione LIKE a.azione AND a.tipo = "+app.azioneConsiderata+";";

		System.out.println(query);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("ElephantIS_risultatiFin.csv"));
			BufferedWriter bw1 = new BufferedWriter(new FileWriter("ElephantIS_risultatiFinCorti.csv"));
			
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String azione = rs.getString("azione");
				String testo = rs.getString("testo");
				String risultato = rs.getString("risultato");
				Double similarita = rs.getDouble("similarita");
				String azionerisultante = rs.getString("azionerisultante");
				
				RisultatoFinale r = null;
				RisultatoFinaleParte rfp = null;
				if (mappa.containsKey(azione)){
					r = mappa.get(azione);
				}else{
					r = new RisultatoFinale(azione, testo, azionerisultante);
					mappa.put(azione, r);
				}
				rfp = new RisultatoFinaleParte(risultato, similarita);
				r.addRisultato(rfp);
			}
			
			if (app.azioneConsiderata != 0 && app.azioneConsiderata != -1){
				String daTrovare = ris.get(app.azioneConsiderata);
				int trovate = 0;
				for (RisultatoFinale r : mappa.values()){
					if (r.azionerisultante.compareTo(daTrovare) == 0)
						trovate++;
				}
				double percent = (double)trovate / (double) num;
				bw.write("num tot "+num+ ";trovate "+percent+ "\n\n");
				bw.flush();
				bw1.write("num tot "+num+ ";trovate "+percent+ ";;primo ris;secondo ris;terzo ris\n\n");
				bw1.flush();
			}


			for (RisultatoFinale r : mappa.values()){
				bw.write(r.numAzione + ";"+r.testo+"\n");
				bw.flush();
				bw1.write(r.numAzione + ";"+r.testo+";");
				bw1.flush();
				int n = 0;
				for (RisultatoFinaleParte rfp : r.lista){
					bw.write(";"+rfp.risultato+"\n");
					bw.flush();
					if (n<3){
						bw1.write(";"+rfp.risultato+"");
						bw1.flush();
						n++;
					} 
					/*
					else if (n == 2){
						bw1.write("\n");
						bw1.flush();
						n++;
					}*/
				}
				bw.write("\n");
				bw.flush();
				bw1.write("\n");
				bw1.flush();
			}
			bw.close();
			bw1.close();
		}catch (Exception e){e.printStackTrace();}
	}
	
	private static void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	
	  private static boolean filePresente(String nomeFile){
		  boolean found1 = false; 
		  boolean found2 = false; 
		  
	    	String path = "frasi_processing/"+nomeFile+"_contesto.txt";
	    	
	    	File f = new File(path);
	    	if(f.exists() && !f.isDirectory())
	    		found1 = true;

	    	path = "frasi_processing/"+nomeFile+"_azione.txt";
	    	f = new File(path);
	    	if(f.exists() && !f.isDirectory())
	    		found2 = true;
	    	
	    	if (found1 && found2)
	    		return true;
	    	
	    	return false;
	    }
	  
	  private static List<String> trovaListaFileDescrizioni(){
		  Map<String, String> mtemp = new TreeMap<String, String>();
		  
	    	List<String> nomiFile = new LinkedList<String>();
	    	
	    	String path = "frasi/";
	    	 
	    	  File folder = new File(path);
	    	  File[] listOfFiles = folder.listFiles(); 
	    	 
	    	  for (int i = 0; i < listOfFiles.length; i++) 
	    	  {
	    	   
	    	   if (listOfFiles[i].isFile() && listOfFiles[i].getName().compareTo(".DS_Store") != 0){
	    		   mtemp.put(listOfFiles[i].getName(), "");
		    	   nomiFile.add(listOfFiles[i].getName());
		    	   }
	    	  }
	    	  
	    	  List<String> l = new LinkedList<String> (mtemp.keySet());
	    	  return l;
	    }
	  
		private static void avviaSrlMSRIS(Frase f, String tipo){
			// vuoto il file querysrl
		    String nomeFilein = "";
		    String nomeFileout = "";
		    String nomeFilequerysrlout = "";
			try {
			    BufferedWriter out;
//			    if (tipo.compareTo("azione") == 0){
//			    	nomeFilein = "frasi_processing/"+f.azioneNumero+"_contesto.txt";
//			    	nomeFileout = "frasi_processing/"+f.azioneNumero+"_contestosrl.txt";
//			    	nomeFilequerysrlout = "frasi_processing/"+f.azioneNumero+"_contestoquerysrl.txt";
//			    	System.out.println("avviaSRL leggo "+nomeFilein+" scrivo "+nomeFileout+" query "+nomeFilequerysrlout);
//			    }
//			    if (tipo.compareTo("contesto") == 0){
			    if (tipo.compareTo("azione") == 0){
			    	nomeFilein = "frasi_processing/"+f.azioneNumero+"_azione.txt";
			    	nomeFileout = "frasi_processing/"+f.azioneNumero+"_azionesrl.txt";
			    	nomeFilequerysrlout = "frasi_processing/"+f.azioneNumero+"_azionequerysrl.txt";
			    	System.out.println("avviaSRL leggo "+nomeFilein+" scrivo "+nomeFileout+" query "+nomeFilequerysrlout);
			    }
		    	out = new BufferedWriter(new FileWriter(nomeFileout));
		        out.close();
		    	out = new BufferedWriter(new FileWriter(nomeFilequerysrlout));
		    	out.close();
			} catch (IOException e) {e.printStackTrace();}
			
			
		    CompletePipelineCMDLineOptions options = new CompletePipelineCMDLineOptions();
		    String [] arg = new String[] {"eng", "-lemma", "src/file/CoNLL2009-ST-English-ALL.anna-3.3.lemmatizer.model", "-tagger", "src/file/CoNLL2009-ST-English-ALL.anna-3.3.postagger.model", "-parser", "src/file/CoNLL2009-ST-English-ALL.anna-3.3.parser.model", "-srl", "src/file/CoNLL2009-ST-English-ALL.anna-3.3.srl-4.1.srl.model", "-test", nomeFilein, "-out", nomeFileout};
//		    String [] arg = new String[] {"eng", "-lemma", "C:\\Users\\valentina\\workspace\\WordNetpos\\Altro parser\\CoNLL2009-ST-English-ALL.anna-3.3.lemmatizer.model", "-tagger", "C:\\Users\\valentina\\workspace\\WordNetpos\\Altro parser\\CoNLL2009-ST-English-ALL.anna-3.3.postagger.model", "-parser", "C:\\Users\\valentina\\workspace\\WordNetpos\\Altro parser\\CoNLL2009-ST-English-ALL.anna-3.3.parser.model", "-srl", "C:\\Users\\valentina\\workspace\\WordNetpos\\Altro parser\\CoNLL2009-ST-English-ALL.anna-3.3.srl-4.1.srl.model", "-test", "testMSRtraintemp.txt", "-out", "engMSRtraintemp.txt"};
		    options.parseCmdLineArgs(arg);
		    String error = FileExistenceVerifier.verifyCompletePipelineAllNecessaryModelFiles(options);
		    if (error != null) {
		      System.err.println(error);
		      System.err.println();
		      System.err.println("Aborting.");
		      System.exit(1);
		    }

		    CompletePipeline pipeline;
			try {
				pipeline = CompletePipeline.getCompletePipeline(options);
			    BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(options.input), Charset.forName("UTF-8")));
			    SentenceWriter writer = new CoNLL09Writer(options.output);
			    long start = System.currentTimeMillis();
			    int senCount;
			    if (options.loadPreprocessorWithTokenizer){
			        senCount = parseNonSegmentedLineByLine(options, pipeline, in, writer);
			        System.out.println("ho un errore!!! ");
			    }
			      else {
			        senCount = parseCoNLL09IS(options, pipeline, in, writer, nomeFilequerysrlout);
			      }
			    
				in.close();
			    writer.close();
			    
			    long time = System.currentTimeMillis() - start;
			    System.out.println(pipeline.getStatusString());
			    System.out.println();
			    System.out.println("Total parsing time (ms):  " + Util.insertCommas(time));
			} catch (Exception e1) {
				e1.printStackTrace();
			} 
		}
	  
		private static void preprocessStoryboardIS(String nomeFile, Frase f, String tipo){
			String filedascrivere = "";
			List<String> listaElem = new LinkedList<String>();
			
			if (tipo.compareTo("azione") == 0){
				filedascrivere = "frasi_processing/"+f.azioneNumero+"_azione.txt";
				System.out.println("scrivo "+filedascrivere);
				listaElem.addAll(f.descrizioneAzione);
			}

			if (tipo.compareTo("contesto") == 0){
				filedascrivere = "frasi_processing/"+f.azioneNumero+"_contesto.txt";
				System.out.println("scrivo "+filedascrivere);
				listaElem.addAll(f.contesto);
			}
			
			PrintWriter wri;
			try {
				wri = new PrintWriter(filedascrivere, "UTF-8");
				String s = "";
				List<ElementoParlato> listaElementiParlato = new LinkedList<ElementoParlato>();
				
				for (String line : listaElem){
					int contatoreparolefrase = 1;
					StringTokenizer stk = new StringTokenizer(line, " ");
				    while (stk.hasMoreTokens()){
				    	s = contatoreparolefrase +" "+stk.nextToken()+"\n";
				    	wri.write(s);
				    	wri.flush();
				    	contatoreparolefrase ++;
				    }
				    if (s.compareTo("\n") != 0){
					    s = "\n";
					    wri.write(s);
					    wri.flush();
				    }
				}
			} catch (Exception e) {		e.printStackTrace();	} 
		}
	
		public static StringBuilder getHTMLResponseIS(Sentence sen, String nomeFilequerysrlout)
		  {
			
		    BufferedWriter out;
		    StringBuilder ret = new StringBuilder("<html><head>\n<title>Semantic Role Labeler Demo</title>\n<style type=\"text/css\">\n  table { background-color:#000000 }\n  td { background-color: #EEEEEE}\n  th { background-color: #EEEEEE}\n  .topRowCell {border-bottom: 1px solid black}\n  .A0, .C-A0 {background-color:#CCCC00}\n  .A1, .C-A1 {background-color:#CC0000}\n  .A2, .C-A2 {background-color:#00CC00}\n  .A3, .C-A3 {background-color:#0000CC}\n  .AM-NEG {background-color:#CC00CC}\n  .AM-MNR {background-color:#00CCCC}\n  .ARG_DEFAULT {background-color:#CCCCCC}\n</style>\n</head>\n<body>\n");
		    StringBuilder vale = new StringBuilder("______________________________________________________________________");
			try {
				out = new BufferedWriter(new FileWriter(nomeFilequerysrlout, true));
			    out.write(System.getProperty("line.separator"));

		        
		        vale.append(System.getProperty("line.separator"));
		        StringBuilder query = new StringBuilder("");
			    
			    ret.append("<table cellpadding=10 cellspacing=1>\n<tr><td class=\"topRowCell\">&nbsp;</td>");
			    for (int i = 1; i < sen.size(); i++) {
			      ret.append("<td align=\"center\" class=\"topRowCell\">").append(((Word)sen.get(i)).getForm()).append("</td>");
			    }
			    StringBuilder errors = new StringBuilder();
			    int conta = 0;
			    for (Predicate pred : sen.getPredicates()) {
			      int indexCount = 1;
			      ret.append("\n<tr><td>");
			      String URL = Language.getLanguage().getLexiconURL(pred);
			      ret.append(pred.getSense());

			      ret.append("</td>\n");

			      SortedSet<Yield> yields = new TreeSet();
			      Map<Word, String> argmap = pred.getArgMap();
			      
			      for (Word arg : argmap.keySet()) {
			        yields.addAll(arg.getYield(pred, (String)argmap.get(arg), argmap.keySet()).explode());
			      }
			      for (Yield y : yields) {
			        if (!y.isContinuous()) {
			          errors.append(new StringBuilder().append("((Discontinous yield of argument '").append(y).append("' of predicate '").append(pred.getSense()).append("'. Yield contains tokens [").toString());
			          for (Word w : y)
			            errors.append(new StringBuilder().append("'").append(w.getForm()).append("', ").toString());
			          errors.delete(errors.length() - 2, errors.length());
			          errors.append("])).\n");
			        }
			        int blankColSpan = sen.indexOf(y.first()) - indexCount;
			        if (blankColSpan > 0) {
				      conta ++;
			          ret.append("<td colspan=\"").append(blankColSpan).append("\">&nbsp;</td>");
			        } else if (blankColSpan < 0) {
			          errors.append(new StringBuilder().append("Argument '").append(y.getArgLabel()).append("' of '").append(pred.getSense()).append("' at index ").append(indexCount).append(" overlaps with previous argument(s), ignored.\n").toString());
			          continue;
			        }
			        int argColWidth = sen.indexOf(y.last()) - sen.indexOf(y.first()) + 1;
			        String argLabel = y.getArgLabel();
			        ret.append("<td colspan=\"").append(argColWidth).append("\" class=\"").append(styleSheetArgs.contains(argLabel) ? argLabel : "ARG_DEFAULT").append("\" align=\"center\">").append(argLabel).append("</td>");

			        indexCount += blankColSpan;
			        vale.append(pred.getSense()+" ");
			        String predicato = pred.getSense();
			        predicato = predicato.replace("'", "''");
			        query.append("('"+predicato+"'"+", ");
//			        out.write("('"+pred.getSense()+"'"+", '");
			        out.write(predicato+"\t");
			        out.flush();
			          for (int u = 0; u<argColWidth; u++){
			        	  vale.append(((Word)sen.get(indexCount+u)).getForm()+" ");
			        	  query.append("'"+((Word)sen.get(indexCount+u)).getForm()+"'"+", ");
			        	  String text = ((Word)sen.get(indexCount+u)).getForm();
							text = text.replace("  ", " ");
							text = text.replace(" ,", ",");
							text = text.replace(" :", ":");
							text = text.replace(" ;", ";");
							text = text.replace(" .", ".");
							
							text = text.replace("—", "-");
							text = text.replace("’", "'");
							text = text.replace("£", "�");
							text = text.replace("‘", "'");
							text = text.replace("“", "\"");
							text = text.replace("…", "...");
							text = text.replace("´", "'");
							text = text.replace("–", "-");
							text = text.replace("€", "�");
							text = text.replace("½", "1/2");
							text = text.replace("'", "''");
					      out.write(""+text+" ");
					      out.flush();
			          }
			          vale.append(argLabel+" ");
			          query.append("'"+argLabel+"'), ");
//				      out.write("', '"+argLabel+"'), ");
				      out.write("\t"+argLabel);
				      out.flush();
				      out.write(System.getProperty("line.separator"));
				      out.flush();
//				        vale.append(argColWidth+" ");
			        indexCount += argColWidth;
			        vale.append(System.getProperty("line.separator"));
			        
			      }
			      if (indexCount < sen.size())
			        ret.append(new StringBuilder().append("<td colspan=\"").append(sen.size() - indexCount).append("\">&nbsp;</td>").toString());
			      ret.append("</tr>");
			    }
			    ret.append("\n</table><br/>\n");
		        vale.append(System.getProperty("line.separator"));
			    vale.append("___________________________________________________________");
			    System.out.println(vale);
		        out.close();
		        
			} catch (IOException e) {e.printStackTrace();}
			
			return ret;
			
		  }
		
		private static int parseCoNLL09IS(CompletePipelineCMDLineOptions options,
				CompletePipeline pipeline, BufferedReader in, SentenceWriter writer, String nomeFilequerysrlout) {
		    List forms = new ArrayList();
		    forms.add("<root>");
		    List isPred = new ArrayList();
		    isPred.add(Boolean.valueOf(false));

		    int senCount = 0;
		    String str;
		    try {
		    	System.out.println("in... " +in);
				while ((str = in.readLine()) != null) {
					System.out.println("stringa... "+str);
				  if (str.trim().equals(""))
				  {
				    Sentence s;
				    if (options.desegment)
				      s = pipeline.parse(ChineseDesegmenter.desegment((String[])forms.toArray(new String[0])));
				    else {
				      s = options.skipPI ? pipeline.parseOraclePI(forms, isPred) : pipeline.parse(forms);
				    }
				    System.err.println(s);
				    
				    getHTMLResponseIS(s, nomeFilequerysrlout);
				    forms.clear();
				    forms.add("<root>");
				    isPred.clear();
				    isPred.add(Boolean.valueOf(false));
				    writer.write(s);
				    			    
				    senCount++;
				    if (senCount % 100 == 0)
				      System.out.println("Processing sentence " + senCount);
				  }
				  else {
				    String[] tokens = WHITESPACE_PATTERN.split(str);
				    forms.add(tokens[1]);
				    if (options.skipPI) {
				      isPred.add(Boolean.valueOf(tokens[12].equals("Y")));
				    }
				  }
				}
			    if (forms.size() > 1) {
				      writer.write(pipeline.parse(forms));
				      senCount++;
				    }
			} catch (Exception e) {
				e.printStackTrace();
			} 
		    return senCount;
		}
		
		private static void creaQuerySRLTree(String tabelladef, String nomefilereadtree, BufferedWriter wri, String nomeAzione, String tipo){
			String t = "";
            
			BufferedReader a;
			String query = null;
			int tempterm = 0;
			String line;
			
			try {
				a = new BufferedReader(new FileReader(nomefilereadtree));
				tempterm = -1;
				while ((line = a.readLine()) != null) {
				    StringTokenizer stk = new StringTokenizer(line, "\t");
				    if (stk.hasMoreTokens()){
					    String order = stk.nextToken().trim();
					    if (order.compareTo("1")==0)
						    tempterm++;
					    String originalword = stk.nextToken();
					    String word = stk.nextToken().trim();
					    word = word.replace("\'", "''");
					    originalword = originalword.replace("\'", "''");
					    stk.nextToken().trim();
					    String pos = stk.nextToken().trim();
					    stk.nextToken().trim();
					    stk.nextToken().trim();
					    stk.nextToken().trim();
					    String tree = stk.nextToken().trim();
					    stk.nextToken().trim();
					    String type = stk.nextToken().trim();
					    //righe aggiunte per valutare anche l' informazione
					    // sul predicato 
					    stk.nextToken().trim();
					    String ispred = stk.nextToken().trim();
				    	t += "('"+nomeAzione+"', '"+tempterm+"', '"+order+"', '"+word+"', '"+originalword+"', '"+pos+"', '"+type+"', '"+tree+"', '"+tipo+"', '"+ispred+"'),\n";
			    		app.queryTree.add(nomeAzione+"!!!"+tempterm+"!!!"+order+"!!!"+word+"!!!"+originalword+"!!!"+pos+"!!!"+type+"!!!"+tree+"!!!"+tipo+"!!!"+ispred);
				    }
				}
				if (t.length() >2 ){
		            wri.write("INSERT INTO  `"+app.db+"`.`"+tabelladef+"` (`azione` ,`term` , `order`, `word`, `originalword`, `pos`, `type`, `tree`, `tipo`,`ispred`) VALUES\n");
//		            wri.write("INSERT INTO "+tabelladef+" (azione ,term , order, word, originalword, pos, type, tree, tipo) VALUES\n");
		            wri.flush();

					t = t.substring(0, t.length()-2);

					wri.write(t+";\n");
					wri.flush();
				}
			} catch (Exception e) { e.printStackTrace();}
		}
		
		private static void creaQuerySRLRel(String tabellasrlrel, String nomefilereadtree, BufferedWriter wri, String nomeAzione, String tipo){
			String t = "";
            
			BufferedReader a;
			String query = null;
			int tempterm = 0;
			String line;
			
			try {
				a = new BufferedReader(new FileReader(nomefilereadtree));
				tempterm = -1;
				while ((line = a.readLine()) != null) {
					if ( line.trim().length() == 0 ) {
				    	tempterm++;
					}
					else{
					    StringTokenizer stk = new StringTokenizer(line, "\t");
					    if (stk.hasMoreTokens()){
						    String predicate = stk.nextToken().trim();
						    String object = stk.nextToken().trim();
						    String relation = stk.nextToken().trim();
						    
						    predicate = predicate.replace("'", "''");
						    t += "('"+nomeAzione+"', '"+tempterm+"', '"+predicate+"', '"+object+"', '"+relation+"', '', '"+tipo+"'),\n";
						    app.querySRL.add(nomeAzione+"!!!"+tempterm+"!!!"+predicate+"!!!"+object+"!!!"+relation+"!!! !!!"+tipo);
					    }
					}
				}
				if (t.length()>2){
		            wri.write("INSERT INTO `"+app.db+"`.`"+tabellasrlrel+"` (`azione` , `term`, `predicate`, `object`, `relation`, `idsrlfield`, `tipo`) VALUES \n");
		            wri.flush();
					t = t.substring(0, t.length()-2);
					wri.write(t+";\n");
					wri.flush();
				}
			} catch (Exception e) { e.printStackTrace();}
		}
		
		private static void populateDbMsrIS(List<String> listaNomiFileAzioni){
			String tabelladef = "";
			String tabellasrlrel = "";
			String nomefiletree = "";
			String nomefilesrl = "";
			String nomefilereadtree = "";
			String nomefilereadsrl = "";
			
			tabelladef = "definitionsrl"+app.animale;
			tabellasrlrel = "srlrelations"+app.animale;
			
			nomefiletree = "frasi_processing/engMSRsqlIS.sql";
			nomefilesrl = "frasi_processing/engMSRsqlsrlIS.sql";
			
			BufferedWriter wri = null;
			BufferedWriter wri1 = null;
			
			try {
				st.executeUpdate("DELETE FROM "+tabelladef+"");
				st.executeUpdate("DELETE FROM "+tabellasrlrel+"");

				wri = new BufferedWriter(new FileWriter(nomefiletree));
				wri1 = new BufferedWriter(new FileWriter(nomefilesrl));
				
			} catch (Exception e1) {e1.printStackTrace();}
			
			for (String s : listaNomiFileAzioni){
				s = s.replace(".txt", "");
				
				nomefilereadtree = "frasi_processing/"+s+"_azionesrl.txt";
				creaQuerySRLTree(tabelladef, nomefilereadtree, wri, s, "azione");

//				nomefilereadtree = "frasi_processing/"+s+"_contestosrl.txt";
//				creaQuerySRLTree(tabelladef, nomefilereadtree, wri, s, "contesto");

				nomefilereadsrl = "frasi_processing/"+s+"_azionequerysrl.txt";
				creaQuerySRLRel(tabellasrlrel, nomefilereadsrl, wri1, s, "azione");

//				nomefilereadsrl = "frasi_processing/"+s+"_contestoquerysrl.txt";
//				creaQuerySRLRel(tabellasrlrel, nomefilereadsrl, wri1, s, "contesto");
				
			}
	        try {
				wri.close();
				wri1.close();
			} catch (IOException e) {e.printStackTrace();}			
		}		
		
		private static SrlElemento restituisciSrlElementoDaWordIS(String originalword, List<SrlElemento> ltemp, String nomeAzione){
			SrlElemento elemento = null;
			for (SrlElemento e : ltemp){
				if (e.originalword.compareTo(originalword) == 0 && nomeAzione.compareTo("ACTION"+e.numeroAzione+"_"+e.numeroVersioneFrase) == 0){
					elemento = e;
				}
			}
			return elemento;
		}

		private static void creaParoleUniteIS(String tabellasrlrel, String nomefilereadazioni, BufferedWriter wri, String nomeAzione, String tipo, List<SrlElemento> ltemp, String tabellaUnite){
			String t = "";
            
			BufferedReader a;
			String query = null;
			int tempterm = 0;
			String line;
			
			Map<SrlElemento, SrlElemento> mappaPrecSucc = new HashMap<SrlElemento, SrlElemento>();

			try {
				a = new BufferedReader(new FileReader(nomefilereadazioni));
				System.out.println("NOME FILE ANALIZZARE - DEBUG M.GIULIA - " + nomefilereadazioni);

				SrlElemento elementoPrec = null;
				while ((line = a.readLine()) != null) {
				    StringTokenizer stk = new StringTokenizer(line, " ");
				    while (stk.hasMoreTokens()){
					    String parola = stk.nextToken().trim();
					    SrlElemento e = restituisciSrlElementoDaWordIS(parola, ltemp, nomeAzione); 
					    
					    if (elementoPrec != null && e != null){
					    	mappaPrecSucc.put(elementoPrec, e);
							System.out.println("+ sono in " +elementoPrec.word+ " "+e.word);
					    }
					    elementoPrec = e;
				    }
				}
				for (SrlElemento e : mappaPrecSucc.keySet()){
					System.out.println("sono in " +e.word+ " "+mappaPrecSucc.get(e).word);
				    if (nomeAzione.compareTo("ACTION101_2") == 0)
				    	System.out.println("ACTION101_2 sono in " +e.word+ " "+mappaPrecSucc.get(e).word+ " "+e.order+ " "+mappaPrecSucc.get(e).order);
					String p = testWs4j(e.word, mappaPrecSucc.get(e).word);
					if (p != null){

						System.out.println(p);
						app.queryParoleUnite.add(nomeAzione+"!!!"+e.term+"!!!"+e.order+"!!!"+p+"!!!"+tipo);
						app.queryParoleUnite.add(nomeAzione+"!!!"+mappaPrecSucc.get(e).term+"!!!"+mappaPrecSucc.get(e).order+"!!!"+p+"!!!"+tipo);
						t += "INSERT INTO `"+tabellaUnite+"`(`azione`, `term`, `order`, `wordunita`, `tipo`) VALUES ('"+nomeAzione+"', '"+e.term+"',"+e.order+",'"+p+"', '"+tipo+"'); \n";
						t += "INSERT INTO `"+tabellaUnite+"`(`azione`, `term`, `order`, `wordunita`, `tipo`) VALUES ('"+nomeAzione+"', '"+mappaPrecSucc.get(e).term+"',"+mappaPrecSucc.get(e).order+",'"+p+"', '"+tipo+"'); \n";
					}
				}
				
				if (t.length()>0){
		            wri.write(t);
		            wri.flush();
				}

			}
			catch(Exception e){e.printStackTrace();}
		}
		
		private static void trovaParoleUniteWordNetIS(List<String> listaNomiFileAzioni){
			String tabelladefinitionsrlmsr = "";
			tabelladefinitionsrlmsr = "definitionsrl"+app.animale+"";
			
			String tabellaUnite = "paroleunitewordnet"+app.animale;
			String nomefileunite = "frasi_processing/engMSRsqluniteIS.sql";
			
			BufferedWriter wri = null;
			try {
				st.executeUpdate("DELETE FROM "+tabellaUnite+"");

				wri = new BufferedWriter(new FileWriter(nomefileunite));
			} catch (Exception e1) {e1.printStackTrace();}

			Map<String, SrlElemento> mappaelementi = new TreeMap<String, SrlElemento>();
			mappaelementi = caricoDefinitionsrlIS(tabelladefinitionsrlmsr);
			
			List<SrlElemento> ltemp = new LinkedList<SrlElemento>(mappaelementi.values());
			Collections.sort(ltemp, new SrlElementoComparator()); // le ordino per numero di parole crescente
			app.eliminaErroriSrl(ltemp);

			for (String s : listaNomiFileAzioni){
				s = s.replace(".txt", "");

				String nomefilereadazioni = "frasi/"+s+".txt";
				creaParoleUniteIS(tabelladefinitionsrlmsr, nomefilereadazioni, wri, s, "azione", ltemp, tabellaUnite);
			}
			try {
				wri.close();
			} catch (IOException e) {e.printStackTrace();}
		}
		
		
		private static void caricoParolaContenutaInDefinition(){
			String query = "SELECT * FROM  `parolecontenuteindefinition` ;";
			ResultSet rs;
			try {
				rs = st.executeQuery(query);
				while (rs.next())
				{
					String parola = rs.getString("parola");
					String contenutain = rs.getString("contenutain");
					String definizione = rs.getString("definizione");
					
					ParolaContenutaInDefinition p = null;
					if (!app.paroleContenuteInDefinizione.containsKey(parola)){
						p = new ParolaContenutaInDefinition(parola);
						app.paroleContenuteInDefinizione.put(parola, p);
					}
					else{
						p = app.paroleContenuteInDefinizione.get(parola);
					}
					p.addParola(contenutain, definizione);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		private static Map<String, SrlElemento> caricoDefinitionsrlIS(String tabelladefinitionsrlmsr){
			Map<String, SrlElemento> mappaelementifrase = new TreeMap<String, SrlElemento>();
			String query = "SELECT * FROM  `"+tabelladefinitionsrlmsr+"` ;";
			System.out.println("*** stampa *** "+query);
			ResultSet rs;
			try {
				rs = st.executeQuery(query);
				while (rs.next())
				{
					String azione = rs.getString("azione");
					String term = rs.getString("term");
					int order = rs.getInt("order");
					String word = rs.getString("word");
					String tipo = rs.getString("tipo");
					if (!controllaFormato(word)){
						word = word.replace("\"", "");
						word = word.replace(",", "");
						String originalword = rs.getString("originalword");
						String pos = rs.getString("pos");
						String type = rs.getString("type");
						int tree = rs.getInt("tree");
						//aggiuto per il predicato
						String isPred = rs.getString("ispred");
						SrlElemento e = new SrlElemento(term, order, word, originalword, pos, type, tree, azione, tipo, isPred);
						//System.out.println("ACTION"+ e.getNumeroAzione() + "_" + e.getNumeroVersioneFrase());
						e.setPosPath(tipoParolaPerPath(e));
						mappaelementifrase.put(azione+"_"+tipo+"!"+term+"_"+String.valueOf(order), e);
					}
				}
				return mappaelementifrase;
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return null;
		}
		

		private static Map<String, SrlElemento> caricoSrlElementiAggiuntiIS(Map<String, SrlElemento> mappaSrlElementi){
			Map<String, SrlElemento> mappaSrlElementiAggiunti = new HashMap<String, SrlElemento>();
			String query = "SELECT * FROM  `paroleunitewordnet"+app.animale+"` ;";
			ResultSet rs;
			try {
				rs = st.executeQuery(query);
				while (rs.next())
				{
					String azione = rs.getString("azione");
					String term = rs.getString("term");
					int order = rs.getInt("order");
					String wordunita = rs.getString("wordunita");
					String tipo = rs.getString("tipo");

					SrlElemento e = null;
					if (!mappaSrlElementiAggiunti.containsKey(azione+"_"+wordunita)){
						e = new SrlElemento(term, wordunita);
						mappaSrlElementiAggiunti.put(azione+"_"+wordunita, e);
					}
					else
						e = mappaSrlElementiAggiunti.get(azione+"_"+wordunita);
					if (mappaSrlElementi.containsKey(azione+"_"+tipo+"!"+term+"_"+String.valueOf(order))){
						e.elementiCollegati.add(mappaSrlElementi.get(azione+"_"+tipo+"!"+term+"_"+String.valueOf(order))); // collego srlelemento
						mappaSrlElementi.get(azione+"_"+tipo+"!"+term+"_"+String.valueOf(order)).aggiungiSrlElementoAggiunto(e);
					}
				}
				return mappaSrlElementiAggiunti;
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return null;
		}
		
		private static RitornoSrlRelationMsr caricoSrlrelationmsrIS(String tabellasrlrelationsmsr){
			List<RelazioneSrl> listarelazioni = new LinkedList<RelazioneSrl>();
			Map<Integer, RelazioneSrl> mapparelazionisrlfrase = new HashMap<Integer, RelazioneSrl>();
			String query = "SELECT * FROM  `"+tabellasrlrelationsmsr+"`;";
			ResultSet rs;
			try {
				rs = st.executeQuery(query);
				while (rs.next())
				{
					String term = rs.getString("term");
					String predicate = rs.getString("predicate");
					String object = rs.getString("object");
					String relation = rs.getString("relation");
					int idsrlfield = rs.getInt("idsrlfield");
					String azione = rs.getString("azione");
					String tipo = rs.getString("tipo");
					predicate = predicate.replace("\"", "");
					predicate = predicate.replace(",", "");
					predicate = predicate.replace("`", "");
		
					if (!controllaFormato(object)){
						RelazioneSrl e = new RelazioneSrl(term, predicate, object, relation, idsrlfield, azione, tipo);
						//System.out.println("ACTION" + e.numeroAzione + e.numeroVersioneFrase);
						listarelazioni.add(e);
						mapparelazionisrlfrase.put(idsrlfield, e);
					}
				}
				return new RitornoSrlRelationMsr(listarelazioni, mapparelazionisrlfrase);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return null;
		}
		
		private static Boolean hoNuovoSoggettoIS(SrlElemento e, List<SrlElemento> soggetti){
			for (SrlElemento s : soggetti){
				if (s.word.compareTo(e.word) == 0)
					return false;
			}
			if (app.soggetti.contains(e.word.toLowerCase()))
				return true;
			return false;
		}

		private static List<SrlElemento> trovaElementiDaAggiungerePerConfrontoIS(List<RelazioneSrl> listaRelazioni, List<SrlElemento> listatemp){
			for (RelazioneSrl r : listaRelazioni){
				if (!r.relation.contains("TMP")){ // elimino relaz temporali
					/*
					if (r.livelloSuperioreSrl != null){
						System.out.println(" -----------_______ r "+r.stampaRelazione()+ " "+r.livelloSuperioreSrl.stampaRelazione());
					}
					else
						System.out.println(" -----------_______ r "+r.stampaRelazione());
					*/
					for (SrlElemento e : r.listaOggetti){
						if (e.pos.compareTo("DT") != 0 && e.pos.compareTo("IN") != 0 && e.pos.compareTo("PRP") != 0){
							if (!listatemp.contains(e))
								listatemp.add(e);
						}
					}
					String parole[] = r.predicate.split(".");
//					listatemp.add(trovaSrlElementoDaParola(List<SrlElemento> lista, parole[0]));
				}
			}
			return listatemp;
		}
		
		private static boolean hoSoloNomiInFraseOltreATakeMake(SrlElemento elemento, List<SrlElemento> ltempOrdinataPerTerm){
			for (SrlElemento e : ltempOrdinataPerTerm){
				if (e.getNumeroAzione() == elemento.getNumeroAzione() && e.getNumeroVersioneFrase() == elemento.getNumeroVersioneFrase() &&
						e.getTipo().compareTo(elemento.getTipo()) == 0
					&& e.word.compareTo("take") != 0
					&& e.word.compareTo("make") != 0
					&& e.word.compareTo("have") != 0
					&& e.word.compareTo("give") != 0
					&& e.word.compareTo("do") != 0
					&& tipoParolaPerPath(e) != null && tipoParolaPerPath(e).compareTo("v") == 0)
						return false;
			}
			app.disambiguoConTakeEMake = true;
			return true;
		}
		
		private static SrlElemento trovaSrlElementoDaParolaIS(List<SrlElemento> lista, String parola, String term, SrlElemento elemEsempio){
			for (SrlElemento e : lista){
//				System.out.println(elemEsempio);
				if (e.word.compareTo(parola) == 0 && e.term.compareTo(term) == 0 && elemEsempio.numeroAzione == e.numeroAzione && elemEsempio.numeroVersioneFrase == e.numeroVersioneFrase && elemEsempio.tipo.compareTo(e.tipo) == 0) // e.srlElementoAggiunto == null &&
					return e;
				else{
					// se non lo trova (caso strano!) prova a cercare eliminando la "s" finale (magari il srl e grammatical parser non funge bene)
					if (e.term.compareTo(term) == 0 && e.word.substring(e.word.length()-1, e.word.length()).compareTo("s") == 0 && elemEsempio.numeroAzione == e.numeroAzione && elemEsempio.numeroVersioneFrase == e.numeroVersioneFrase && elemEsempio.tipo.compareTo(e.tipo) == 0){ // se finisco con "s"
						String nuovaword = e.word.substring(0, e.word.length()-1);
						if (nuovaword.compareTo(parola) == 0 && e.term.compareTo(term) == 0){
							System.out.println("sono nel substring "+nuovaword);
							e.word = nuovaword;
							return e;
						}
					}
				}
				
			}
			
			return null;
		}
		private static Map<Integer, List<SrlElemento>> trovaElementiDaAggiungerePerConfrontoSRLIS(List<RelazioneSrl> listaRelazioni, List<SrlElemento> ltempOrdinataPerTerm, SrlElemento elemEsempio){
			// prendo tutti gli elementi di una frase che saranno descritti nel copione e trovo gli elementi da aggiungere
			/*
			try {
				if (elemEsempio.numeroAzione == 102 && elemEsempio.numeroVersioneFrase == 6){
					Thread.sleep(3000);
					System.out.println("--------------------------------");
					System.out.println("--------------------------------");
					System.out.println("--------------------------------");
				}
			} catch (InterruptedException e2) {e2.printStackTrace();}*/
			
//			guardo se parola che voglio inserire in parole inserite Ã¨ sinonimo di uno dei soggetti
			
			Map<Integer, List<SrlElemento>> mappa = new HashMap<Integer, List<SrlElemento>>();
			paroleAggiunteDaTakeMake.clear();
			paroleAggiunteDaParoleParticolari.clear();
			Integer num = 0;
			List<String> listaPredicati = new LinkedList<String>();
//*******************************************************
//********** aggiungo un controllo per vedere se e presente il soggetto per ogni srlElemento di una frase
//********** le frasi sono memorizzate una per ogni riga della mappa
//			System.out.println(listaRelazioni.toString());
			System.out.println(ltempOrdinataPerTerm.toString());
			boolean a = false;
			boolean b = false;
			int numFrase = 0; 
			for (SrlElemento e : ltempOrdinataPerTerm){
				if(e.type.compareToIgnoreCase("SBJ")==0){
					a = true;
				}
				
				if(a==false && e.getType().compareTo("ROOT")==0 && (e.getPos().compareTo("VB")==0 ||
		                   e.getPos().compareTo("VBP")==0)){
					b = true;
				}
					
				
			}
			
			
			if(b){
				try{
				numFrase = listaRelazioni.get(1).numeroAzione;
				String nomeFile = "ACTION"+numFrase+"_1";
				System.out.println(nomeFile);
				BufferedReader br = new BufferedReader(new FileReader("frasi/"+nomeFile+".txt"));
				
				
		        String line = "";
		        String s ="";
		        while((line = br.readLine()) != null){
		           		s = line;
		        }
				br.close();
				BufferedWriter bw = new BufferedWriter(new FileWriter("frasi/"+nomeFile+".txt"));
				s = "You "+s.toLowerCase();
				bw.write(s);
				bw.close();
				boolean bool = false;
				File origin1 = new File("frasi_processing/"+nomeFile+"_azione.txt");
				origin1.createNewFile();
				bool =  origin1.delete();
				bool = false;
				File origin2 = new File("frasi_processing/"+nomeFile+"_azionequerysrl.txt");
//				origin2.createNewFile();
				System.gc();
				bool = origin2.delete();
				
				File origin3 = new File("frasi_processing/"+nomeFile+"_azionesrl.txt");
//				origin3.createNewFile();
				origin3.delete();
				
				caricaFrasi();
				}catch (IOException e) {e.printStackTrace();}
			}
			
			
			for (RelazioneSrl r : listaRelazioni){
				if (srlContieneSoggetto(r.listaOggetti))
					if (!listaPredicati.contains(r.predicate))
						listaPredicati.add(r.predicate);
			}
			
			List<RelazioneSrl> relazioniGiaInserite = new LinkedList<RelazioneSrl>();
			// sposto fuori paroleInserite (per non considerare i predicati come uno diverso dall'altro)
			List<SrlElemento> paroleInserite = new LinkedList<SrlElemento>();
			for (String s : listaPredicati){
				for (RelazioneSrl r : listaRelazioni){
					List<RelazioneSrl> gv = new LinkedList<RelazioneSrl>();
					boolean trovato = false;
					if (!r.relation.contains("TMP") && r.predicate.compareTo(s) == 0 && app.mappaRelazioniSoggetto.containsKey(r.predicate) && app.mappaRelazioniSoggetto.get(r.predicate) == true){ // ho aggiunto && verificaSeRelazioniSottoUnaRelazioneContengonoSoggetto(listaRelazioni, r)
						List<RelazioneSrl> relazioniSotto = new LinkedList<RelazioneSrl>();
						trovaTutteRelazioniSottoUnaRelazione(listaRelazioni, r, s, relazioniSotto);
						
						System.out.println("*** "+r.stampaRelazione());
						
						try {
							bw.write(";"+r.stampaRelazione()+"\n ");
							bw.flush();
						} catch (IOException e1) {e1.printStackTrace();}
						
						for (RelazioneSrl rrr : relazioniSotto){
							if (!rrr.relation.contains("TMP") && !relazioniGiaInserite.contains(rrr) && !srlContieneSoggetto(rrr.listaOggetti)){
									System.out.println(" -----------_______ r "+r.stampaRelazione()+ " "+rrr.stampaRelazione());
									relazioniGiaInserite.add(rrr);

									for  (SrlElemento e : rrr.listaOggetti){
										if (e.srlElementoAggiunto != null)
											if (!paroleInserite.contains(e.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(e.srlElementoAggiunto)){
												if (cercaAttoreCorrispondente(e) == null){
//												if (cercaAttoreCorrispondente(e) == null && cercaScenaCorrispondente(e) == null){
													paroleInserite.add(e.srlElementoAggiunto);
													app.paroleInseriteGlobali.add(e.srlElementoAggiunto);
												}
											}

										if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0  && e.pos.compareTo("PRP$") != 0 && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word) && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e), e) )){
												//////////////////////////////////////////////////////////////////////////////////////
												// IMPORTANTE : aggiungo anche entrambe le parole che compongono la parola composta //
												//////////////////////////////////////////////////////////////////////////////////////
											if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
												if (cercaAttoreCorrispondente(e) == null){
//												if (cercaAttoreCorrispondente(e) == null && cercaScenaCorrispondente(e) == null){
													paroleInserite.add(e);
													app.paroleInseriteGlobali.add(e);
												}
											}
										}

										/*
										if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0  && e.pos.compareTo("PRP$") != 0 && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word) && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e), e) )){
											if (e.srlElementoAggiunto != null){
												if (!paroleInserite.contains(e.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(e.srlElementoAggiunto)){
													paroleInserite.add(e.srlElementoAggiunto);
													app.paroleInseriteGlobali.add(e.srlElementoAggiunto);
												}
												//////////////////////////////////////////////////////////////////////////////////////
												// IMPORTANTE : aggiungo anche entrambe le parole che compongono la parola composta //
												//////////////////////////////////////////////////////////////////////////////////////
												if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
													paroleInserite.add(e);
													app.paroleInseriteGlobali.add(e);
												}
											}
											else{
												if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
													paroleInserite.add(e);
													app.paroleInseriteGlobali.add(e);
												}
											}
											
										}*/
										
										// qui devo controllare se sono take/make e agire di conseguenza
										if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0  && e.pos.compareTo("PRP$") != 0 && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word) && (controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e), e) ) 
												&& hoSoloNomiInFraseOltreATakeMake(e, ltempOrdinataPerTerm)){
											// prendo l'OBJ e calcolo la similarita con tutte le parole simili a lui (cioe' che lo contengono), selezionando quella massima!
												for (SrlElemento es : trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e))
													if (es.type.compareTo("OBJ") == 0 )
														if (!paroleAggiunteDaTakeMake.contains(es))
														if (cercaAttoreCorrispondente(es) == null)
															paroleAggiunteDaTakeMake.add(es);
											}
										
										if (app.paroleParticolari.contains(e.word)){
											List<SrlElemento> elsotto = new LinkedList<SrlElemento> (); 
											trovaElementiSotto(e, elsotto, ltempOrdinataPerTerm);
											for (SrlElemento es : elsotto)
												if (es.pos.compareTo("DT") != 0 && !(es.pos.compareTo("IN") == 0 && es.srlElementoAggiunto == null) && es.pos.compareTo("PRP") != 0  && es.pos.compareTo("PRP$") != 0 && es.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(es.word) && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, es), es) ))
													if (!paroleAggiunteDaParoleParticolari.contains(es)){
														if (cercaAttoreCorrispondente(es) == null){
															paroleAggiunteDaParoleParticolari.add(es);
															System.out.println("*** +++ *** ho aggiunto "+es.stampaElementoIS());
														}
													}
										}
									}
									String parola = rrr.predicate.substring(0, (rrr.predicate.length() - 3));

									SrlElemento predicato = trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, rrr.term, elemEsempio);
									System.out.println("       cerco proceed "+parola+" "+predicato + " ");
									if (!paroleInserite.contains(predicato) && predicato != null && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) ) && !app.paroleInseriteGlobali.contains(predicato)){
										if (cercaAttoreCorrispondente(predicato) == null){
											paroleInserite.add(trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio));
											app.paroleInseriteGlobali.add(predicato);
										}
										
										// controllo: se predicato fa parte di parola composta, aggiungo al confronto anche l'elemento composto (e quello al quale lui Ã¨ collegato)
										if (predicato.srlElementoAggiunto != null && !paroleInserite.contains(predicato.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(predicato.srlElementoAggiunto)){
											paroleInserite.add(predicato.srlElementoAggiunto);
											app.paroleInseriteGlobali.add(predicato.srlElementoAggiunto);
											
											for (SrlElemento eee : predicato.srlElementoAggiunto.elementiCollegati)
												if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee)){
													if (cercaAttoreCorrispondente(eee) == null){
														paroleInserite.add(eee);
														app.paroleInseriteGlobali.add(eee);
													}
												}
										}
									}
									
									if (predicato != null && (controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )
											&& hoSoloNomiInFraseOltreATakeMake(predicato, ltempOrdinataPerTerm)){
										for (SrlElemento es : trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato))
											if (es.type.compareTo("OBJ") == 0 )
												if (!paroleAggiunteDaTakeMake.contains(es))
													if (cercaAttoreCorrispondente(es) == null)
														paroleAggiunteDaTakeMake.add(es);
									}
								}							
						}	
						
						if (!relazioniGiaInserite.contains(r) && !srlContieneSoggetto(r.listaOggetti)){
							relazioniGiaInserite.add(r);
							
							for (SrlElemento e : r.listaOggetti){
								if (e.srlElementoAggiunto != null)
									if (!paroleInserite.contains(e.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(e.srlElementoAggiunto)){
										if (cercaAttoreCorrispondente(e.srlElementoAggiunto) == null ){
											paroleInserite.add(e.srlElementoAggiunto);
											app.paroleInseriteGlobali.add(e.srlElementoAggiunto);
										}
									}
								if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0 && e.pos.compareTo("PRP$") != 0  && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word)  && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e) , e) )){
										//////////////////////////////////////////////////////////////////////////////////////
										// IMPORTANTE : aggiungo anche entrambe le parole che compongono la parola composta //
										//////////////////////////////////////////////////////////////////////////////////////
									if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
										if (cercaAttoreCorrispondente(e) == null){
											paroleInserite.add(e);
											app.paroleInseriteGlobali.add(e);
										}
									}
								}
							
								
								if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0  && e.pos.compareTo("PRP$") != 0 && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word) && (controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e), e) )
										&& hoSoloNomiInFraseOltreATakeMake(e, ltempOrdinataPerTerm)){

									// prendo l'OBJ e calcolo la similarita con tutte le parole simili a lui (cioe' che lo contengono), selezionando quella massima!
										for (SrlElemento es : trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e))
											if (es.type.compareTo("OBJ") == 0 ){
												if (!paroleAggiunteDaTakeMake.contains(es))
													if (cercaAttoreCorrispondente(es) == null )
														paroleAggiunteDaTakeMake.add(es);
											}
									}
								
								if (app.paroleParticolari.contains(e.word)){
									List<SrlElemento> elsotto = new LinkedList<SrlElemento> (); 
									trovaElementiSotto(e, elsotto, ltempOrdinataPerTerm);
									for (SrlElemento es : elsotto)
										if (es.pos.compareTo("DT") != 0 && !(es.pos.compareTo("IN") == 0 && es.srlElementoAggiunto == null) && es.pos.compareTo("PRP") != 0  && es.pos.compareTo("PRP$") != 0 && es.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(es.word) && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, es), es) ))
											if (!paroleAggiunteDaParoleParticolari.contains(es)){
												if (cercaAttoreCorrispondente(es) == null){
													paroleAggiunteDaParoleParticolari.add(es);
													System.out.println("*** +++ *** ho aggiunto "+es.stampaElementoIS());
												}
											}
								}
							}
							String parola = r.predicate.substring(0, (r.predicate.length() - 3));

							SrlElemento predicato = trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio);
							System.out.println("       cerco proceed "+parola+" "+predicato.stampaElemento() + " ");

							if (!paroleInserite.contains(trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio)) && trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio) != null && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )  && !app.paroleInseriteGlobali.contains(predicato)){
								if (cercaAttoreCorrispondente(predicato) == null ){
									paroleInserite.add(trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio));
									app.paroleInseriteGlobali.add(predicato);
								}

								// controllo: se predicato fa parte di parola composta, aggiungo al confronto anche l'elemento composto (e quello al quale lui Ã¨ collegato)
								if (predicato.srlElementoAggiunto != null && !paroleInserite.contains(predicato.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(predicato.srlElementoAggiunto)){
									if (cercaAttoreCorrispondente(predicato.srlElementoAggiunto) == null){
										paroleInserite.add(predicato.srlElementoAggiunto);
										app.paroleInseriteGlobali.add(predicato.srlElementoAggiunto);
									}
									
									for (SrlElemento eee : predicato.srlElementoAggiunto.elementiCollegati)
										if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee)){
											if (cercaAttoreCorrispondente(eee) == null){
												paroleInserite.add(eee);
												app.paroleInseriteGlobali.add(eee);
											}
										}
								}
							}
							
							if (predicato != null && (controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )
									&& hoSoloNomiInFraseOltreATakeMake(predicato, ltempOrdinataPerTerm)){
								for (SrlElemento es : trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato))
									if (es.type.compareTo("OBJ") == 0 ){
										if (!paroleAggiunteDaTakeMake.contains(es)){
											if (cercaAttoreCorrispondente(es) == null )
												paroleAggiunteDaTakeMake.add(es);
										}
									}
							}
						}
						else{
							String parola = r.predicate.substring(0, (r.predicate.length() - 3));

							SrlElemento predicato = trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio);
							System.out.println("       cerco proceed "+parola+" "+predicato.stampaElemento() + " ");

							if (!paroleInserite.contains(trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio)) && trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio) != null && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )  && !app.paroleInseriteGlobali.contains(predicato)){
								if (cercaAttoreCorrispondente(predicato) == null){
									paroleInserite.add(trovaSrlElementoDaParolaIS(ltempOrdinataPerTerm, parola, r.term, elemEsempio));
									app.paroleInseriteGlobali.add(predicato);
								}
								
								// controllo: se predicato fa parte di parola composta, aggiungo al confronto anche l'elemento composto (e quello al quale lui Ã¨ collegato)
								if (predicato.srlElementoAggiunto != null && !paroleInserite.contains(predicato.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(predicato.srlElementoAggiunto)){
									if (cercaAttoreCorrispondente(predicato.srlElementoAggiunto) == null){
										paroleInserite.add(predicato.srlElementoAggiunto);
										app.paroleInseriteGlobali.add(predicato.srlElementoAggiunto);
									}
									
									for (SrlElemento eee : predicato.srlElementoAggiunto.elementiCollegati)
										if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee)){
											if (cercaAttoreCorrispondente(eee) == null ){
												paroleInserite.add(eee);
												app.paroleInseriteGlobali.add(eee);
											}
										}
								}
							}
							
							if (predicato != null && (controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )
									&& hoSoloNomiInFraseOltreATakeMake(predicato, ltempOrdinataPerTerm)){
								for (SrlElemento es : trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato))
									if (es.type.compareTo("OBJ") == 0 )
										if (!paroleAggiunteDaTakeMake.contains(es))
											if (cercaAttoreCorrispondente(es) == null)
												paroleAggiunteDaTakeMake.add(es);
							}
						}
					}
				}
			}
			mappa.put(num, paroleInserite);
			for (SrlElemento srle : paroleInserite)
				System.out.println("s      "+srle.stampaElemento());
			num++;
			
			return mappa;
		}
		
		private static void usaParoleAggiunte(List<SrlElemento> listaParoleAggiunte, String tipo){
			for (SrlElemento s : listaParoleAggiunte){
				Double simMax = 0.0;
				String parolaMax = null;
				System.out.println("***** +++++ ***** parole aggiunte da "+tipo+" "+s.stampaElementoIS());
				Map<Double, List<String>> simmap = new HashMap<Double, List<String>>();

				for (Azione a : mappaAzioni.values()){
					// cerco sim con pos di verbo
					Double sim = 0.0;
					if (s.word.compareTo(a.nomeAzione) == 0)
						sim = 1.0;
					else if (app.mappapath.containsKey(s.word+"°v___"+a.nomeAzione+"°"+a.azionepos))
						sim = app.mappapath.get(s.word+"°v___"+a.nomeAzione+"°"+a.azionepos).path;
					
					if (simmap.containsKey(sim))
						simmap.get(sim).add(s.word);
					else{
						List<String> l = new LinkedList<String>();
						if (!l.contains(s.word));
							l.add(s.word);
						simmap.put(sim, l);
					}
					
					if (sim > simMax){
						simMax = sim;
						parolaMax = s.word;
					}
					
					// cerco max sim tra parole identificate in wn
					
					if (app.paroleContenuteInDefinizione.containsKey(s.word))
						for (String str : app.paroleContenuteInDefinizione.get(s.word).getDefinizioni().keySet()){
							sim = calcolaSimilaritaTraParole(str, a.nomeAzione, null, null);
							
							if (simmap.containsKey(sim))
								simmap.get(sim).add(str);
							else{
								List<String> l = new LinkedList<String>();
								if (!l.contains(str));
									l.add(str);
								simmap.put(sim, l);
							}
							
							if (sim > simMax){
								simMax = sim;
								parolaMax = str;
							}
						}
				}
				
				String parolaMaxConMinOccorrenze = null;
				Integer minOcc = 1000;
				if (parolaMax != null){
//					System.out.println(simmap);
					for (String simm : simmap.get(simMax)){
						Integer occ = app.paroleDefinitionCount.get(simm);
						if (app.paroleDefinitionCount.containsKey(simm) && occ < minOcc){
							minOcc = occ;
							parolaMaxConMinOccorrenze = simm;
						}
					}
				}
				
				for (Azione a : mappaAzioni.values()){
					if (parolaMax != null && parolaMaxConMinOccorrenze != null){
						Double sim = calcolaSimilaritaTraParole(parolaMaxConMinOccorrenze, a.nomeAzione, null, null);// era parolaMax
						if (sim > 0){
							if (tipo.compareTo("takemake") == 0)
								a.addSimilaritaParoleContenuteInDefinizioneTakeMake(parolaMaxConMinOccorrenze, sim);
							if (tipo.compareTo("parolespeciali") == 0)
								a.addSimilaritaParoleContenuteInDefinizioneParoleSpeciali(parolaMaxConMinOccorrenze, sim);
						}
					}
				}
				
			}
		}
		
		private static List<SimilaritaElementoSituazioneAzione> calcolaSimilaritaSoloConAzioneIS(List<SrlElemento> ltemp, ElementoSituazioneAzione elm){
			List<SimilaritaElementoSituazioneAzione> listaSimilarita = new LinkedList<SimilaritaElementoSituazioneAzione>();
			Double simElemGesto = 0.0;
			Double simElemAzione = 0.0;
			for (SrlElemento e : ltemp){
				for (Azione a : mappaAzioni.values()){
					a.clearSimFinElm();
					a.clearSimilaritaParoleContenuteInDefinizioneTakeMake();
					a.clearSimilaritaParoleContenuteInDefinizioneParoleSpeciali();
					double sim = 0.0;
					if (!app.usaWordPos){
						if (app.mappapath.containsKey(e.word+"___"+a.nomeAzione))
							sim = app.mappapath.get(e.word+"___"+a.nomeAzione).path;
						else if (app.mappapath.containsKey(a.nomeAzione+"___"+e.word))
							sim = app.mappapath.get(a.nomeAzione+"___"+e.word).path;
					}
					else{
						if (e.posPath != null && a.azionepos != null){
//								System.err.println(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos);
							if (app.mappapath.containsKey(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos))
								sim = app.mappapath.get(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos).path;
							else if (app.mappapath.containsKey(a.nomeAzione+"°"+a.azionepos+"___"+e.word+"°"+e.posPath))
								sim = app.mappapath.get(a.nomeAzione+"°"+a.azionepos+"___"+e.word+"°"+e.posPath).path;
						}
						else if (e.posPath == null && a.azionepos != null){
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
							}
						else if (e.posPath != null && a.azionepos == null){
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
						}
						else
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
					}
					
					if (sim > 1 && app.normalizzaLesk)
						sim = 1;
					
					SimilaritaAzioneSrlElemento simAzSrlElem = new SimilaritaAzioneSrlElemento(a, e, sim);
					e.aggiungiSimilaritaAzione(simAzSrlElem);
					a.aggiungiSimilaritaSrlElemento(simAzSrlElem);
					
					SimilaritaElementoSituazioneAzione simEsAzione = new SimilaritaElementoSituazioneAzione(elm, a, sim);
					a.aggiungiSimilaritaElementoSituazione(simEsAzione);
				}
			}
			
			// qui devo aggiungere la similarita' con parole trovate incrociando le definizioni di wordnet
			usaParoleAggiunte(app.paroleAggiunteDaTakeMake, "takemake");
			usaParoleAggiunte(app.paroleAggiunteDaParoleParticolari, "parolespeciali");
			
			for (Azione a : mappaAzioni.values()){
				a.calcolaMediaSimElementoSituazioneAzione(elm);
				for (SimilaritaElementoSituazioneAzione s : a.simFinElm)
					listaSimilarita.add(s);
			}
			
			return listaSimilarita;
		}
		
		public static void aggiungoParoleUniteARelazione(RelazioneSrl r, Map<String, SrlElemento> maggiunti, List<SrlElemento> elementiOk){
			// aggiungo parole unite
			// le prendo da mappa m, solo se numero azione coincide, se tipo coincide e se term e order pure
			// e le aggiungo se entrambe le parole sono presenti (occhio che una puÃ² essere presente dentro "relazioni"
			
			String pred = r.predicate.substring(0, r.predicate.length()-3);
			System.out.println("pred "+pred+" "+r.predicate);
			
		}
		
		public static String cercaVerboCorrispondente(SrlElemento e){
			for (String s : app.attori.keySet()){
				String tipo = tipoParolaPerPath(e);
				SrlElemento verbo = null;
				for (SrlElemento v : e.getVerbiCollegati())
					if (v.word.compareTo("be") != 0 )
						verbo = v;
				if (verbo != null){
					for (String str : app.attori.get(s).getAzioniEseguibili()){
						if (e.getAttoreCorrispondente().equals(app.attori.get(s)) && tipo != null && (str.compareTo(verbo.word) == 0 || calcolaSimilaritaTraParole(str, verbo.word, "v", tipo) >= app.sogliaSimilaritaParoleAttori))
							return str;
						if (e.getAttoreCorrispondente().equals(app.attori.get(s)) && tipo == null && (s.compareTo(verbo.word) == 0 || calcolaSimilaritaTraParole(str, verbo.word, null, null) >= app.sogliaSimilaritaParoleAttori))
							return str;
					}
				}
			}
			return null;
		}

		public static String cercaOggettoCorrispondente(SrlElemento e){
			SrlElemento oggetto = null;
			String tipo = tipoParolaPerPath(e);
			if (e.getOggettiCollegati().size() >0 )
				oggetto = e.getOggettiCollegati().get(0);
			if (oggetto != null){
				for (String s : app.oggetti.keySet()){
						
					if (tipo != null && (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, "n", tipo) >= app.sogliaSimilaritaParoleAttori))
						return s;
					if (tipo == null && (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, null, null) >= app.sogliaSimilaritaParoleAttori))
						return s;
				}
			}
			return null;
		}

		
		public static Actor cercaAttoreCorrispondentePerContesto(SrlElemento e){
			Actor max = null;
			Double simMax = 0.0;
			
			for (String s : app.attori.keySet()){
				String tipo = tipoParolaPerPath(e);
				if (tipo != null){
					if (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, "n", tipo) >= app.sogliaSimilaritaParoleAttori)
						if (s.compareTo(e.word) == 0){
							max = attori.get(s);
							simMax = 1.0;
						}
						else if (calcolaSimilaritaTraParole(s, e.word, "n", tipo) > simMax){
							max = attori.get(s);
							simMax = calcolaSimilaritaTraParole(s, e.word, "n", tipo);
						}
				} else{
					if (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, null, null) >= app.sogliaSimilaritaParoleAttori)
						if (s.compareTo(e.word) == 0){
							max = attori.get(s);
							simMax = 1.0;
						}
						else if (calcolaSimilaritaTraParole(s, e.word, "n", tipo) > simMax){
							max = attori.get(s);
							simMax = calcolaSimilaritaTraParole(s, e.word, "n", tipo);
						}
				}
			}
			return max;
			
		}
		
		public static Actor cercaAttoreCorrispondente(SrlElemento e){
			Actor max = null;
			Double simMax = 0.0;
			
			/*
			if (e.type.compareTo("SBJ") == 0 || e.type.compareTo("OBJ") == 0 ){
				for (String s : app.attori.keySet()){
					String tipo = tipoParolaPerPath(e);
					if (tipo != null){
						if (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, "n", tipo) >= app.sogliaSimilaritaParoleAttori)
							if (s.compareTo(e.word) == 0){
								max = attori.get(s);
								simMax = 1.0;
							}
							else if (calcolaSimilaritaTraParole(s, e.word, "n", tipo) > simMax){
								max = attori.get(s);
								simMax = calcolaSimilaritaTraParole(s, e.word, "n", tipo);
							}
					} else{
						if (s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, null, null) >= app.sogliaSimilaritaParoleAttori)
							if (s.compareTo(e.word) == 0){
								max = attori.get(s);
								simMax = 1.0;
							}
							else if (calcolaSimilaritaTraParole(s, e.word, "n", tipo) > simMax){
								max = attori.get(s);
								simMax = calcolaSimilaritaTraParole(s, e.word, "n", tipo);
							}
					}
				}
			}
			*/
			return max;
			
		}
		
		public static String cercaScenaCorrispondente(SrlElemento e){
			for (String s : app.scene.keySet()){
				String tipo = tipoParolaPerPath(e);
				if (tipo != null && s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, "n", tipo) >= app.sogliaSimilaritaParoleAttori)
					return scene.get(s);
				if (tipo == null && s.compareTo(e.word) == 0 || calcolaSimilaritaTraParole(s, e.word, null, null) >= app.sogliaSimilaritaParoleAttori)
					return scene.get(s);
			}
			return null;
		}

		public static SrlElemento cercaSeSubitoSopraOSottoHoAltriVerbi(List<SrlElemento> ltemp, SrlElemento e, SrlElemento sogg){
			// ma devo anche controllare se a questi verbi sono subito collegati altri soggetti!

			// cerco sopra
			for (SrlElemento elm : ltemp)
				if (e.tree == elm.order && e.numeroAzione == elm.numeroAzione && e.numeroVersioneFrase == elm.numeroVersioneFrase && e.term.compareTo(elm.term) == 0 && e.tipo.compareTo(elm.tipo) == 0){
					String tipo = tipoParolaPerPath(elm);
					if (tipo != null && tipo.compareTo("v") == 0){
						List<SrlElemento> listasotto = new LinkedList<SrlElemento>();
						trovaElementiSotto(e, listasotto, ltemp);
						boolean hoAltriSogg = false;
						for (SrlElemento st : listasotto){
							if (st.type.compareTo("SBJ") == 0 && !st.equals(sogg)) // forse Ã¨ il contrario!
								hoAltriSogg = true;
						}
						if (!hoAltriSogg)
							return elm;
					}
				}
			for (SrlElemento elm : ltemp){
				if (e.numeroAzione == elm.numeroAzione && e.numeroVersioneFrase == elm.numeroVersioneFrase && elm.type != null && e.order == elm.tree && e.term.compareTo(elm.term) == 0 && e.tipo.compareTo(elm.tipo) == 0){
					String tipo = tipoParolaPerPath(elm);
					if (tipo != null && tipo.compareTo("v") == 0){
						List<SrlElemento> listasotto = new LinkedList<SrlElemento>();
						trovaElementiSotto(e, listasotto, ltemp);
						boolean hoAltriSogg = false;
						for (SrlElemento st : listasotto){
							if (st.type.compareTo("SBJ") == 0 && !st.equals(sogg))
								hoAltriSogg = true;
						}
						if (!hoAltriSogg)
							return elm;
					}
				}
			}
			return null;
		}
		
		private static void cercaVerbiSotto(List<SrlElemento> ltemp, SrlElemento e, SrlElemento sogg, List<SrlElemento> verbiSotto){
			List<SrlElemento> listasotto = new LinkedList<SrlElemento>();
			trovaElementiSotto(e, listasotto, ltemp);

			for (SrlElemento elm : listasotto){
				String tipo = tipoParolaPerPath(elm);
				System.out.println("verbo "+elm.stampaElementoIS());
				if (tipo != null && tipo.compareTo("v") == 0){
					verbiSotto.add(elm);
				}
			}
		} 
		
		public static List<SrlElemento> cercaVerboCollegato(List<SrlElemento> ltemp, SrlElemento e){
			List<SrlElemento> verbi = new LinkedList<SrlElemento>();
			for (SrlElemento elm : ltemp)
				if (e.tree == elm.order && e.numeroAzione == elm.numeroAzione && e.numeroVersioneFrase == elm.numeroVersioneFrase && e.term.compareTo(elm.term) == 0 && e.tipo.compareTo(elm.tipo) == 0 && tipoParolaPerPath(elm).compareTo("v") == 0){
					if (elm.word.compareTo("be") == 0){
						SrlElemento verboSopraAux = cercaSeSubitoSopraOSottoHoAltriVerbi(ltemp, elm, e);
						if (verboSopraAux != null)
							verbi.add(verboSopraAux);
							verbi.add(elm);
					}
					else
						verbi.add(elm);
					List<SrlElemento> verbiSotto = new LinkedList<SrlElemento>(); 
					cercaVerbiSotto(ltemp, elm, e, verbiSotto); 
					verbi.addAll(verbiSotto);
				}
			return verbi;
		}

		public static void cercaOggettoCollegatoAVerboCollegatoVersioneSemplice(List<SrlElemento> ltemp, SrlElemento e, List<SrlElemento> giaVisti, List<SrlElemento> oggetti){
			List<SrlElemento> listasotto = new LinkedList<SrlElemento>();
			trovaElementiSotto(e, listasotto, ltemp);
			List<SrlElemento> listasottoverbi = new LinkedList<SrlElemento>();
			for (SrlElemento elm : listasotto){
				String tipo = tipoParolaPerPath(elm);
				if (tipo != null && tipo.compareTo("v") == 0){
					trovaElementiSotto(elm, listasottoverbi, ltemp);
					listasottoverbi.add(elm);
				}
				
				if (elm.pos.compareTo("DT") != 0 && elm.pos.compareTo("CC") != 0 && !listasottoverbi.contains(elm) && elm.type.compareTo("P") != 0){
						if (!oggetti.contains(elm))
							oggetti.add(elm);
				}
				else{
					giaVisti.add(e);
					cercaOggettoCollegatoAVerboCollegato(ltemp, elm, giaVisti, oggetti);
				}
			}
		}
		
		public static void cercaOggettoCollegatoAVerboCollegato(List<SrlElemento> ltemp, SrlElemento e, List<SrlElemento> giaVisti, List<SrlElemento> oggetti){
			List<SrlElemento> listasotto = new LinkedList<SrlElemento>();
			trovaElementiSotto(e, listasotto, ltemp);
			for (SrlElemento elm : listasotto){
				if (elm.type != null && elm.type.compareTo("OBJ") == 0 ){//!giaVisti.contains(elm) && 
					String tipo = tipoParolaPerPath(elm);
					if (tipo != null && tipo.compareTo("v") != 0)
						if (!oggetti.contains(elm))
							oggetti.add(elm);
					}
					else{
						giaVisti.add(e);
						cercaOggettoCollegatoAVerboCollegato(ltemp, elm, giaVisti, oggetti);
					}
				}
}

		private static void trovaElementiSotto(SrlElemento e, List<SrlElemento> listasotto, List<SrlElemento> tuttiElementi){
			for (SrlElemento et : tuttiElementi)
				if (e.numeroAzione == et.numeroAzione && e.numeroVersioneFrase == et.numeroVersioneFrase && e.term.compareTo(et.term) == 0 && e.order == et.tree && e.tipo.compareTo(et.tipo) == 0 )
					if (!listasotto.contains(et)){
						listasotto.add(et);
						trovaElementiSotto(et, listasotto, tuttiElementi);
					}
		}
		
		private static void trovaParoleSottoVerbo(SrlElemento e, List<SrlElemento> listasotto, List<SrlElemento> tuttiElementi){
			for (SrlElemento et : tuttiElementi)
				if (e.order == et.tree && e.numeroAzione == et.numeroAzione && e.numeroVersioneFrase == et.numeroVersioneFrase && e.tipo.compareTo(et.tipo) == 0 )
					if (!listasotto.contains(et) && !et.eGiaCollegatoAdAltroElemento()){
						listasotto.add(et);
						et.impostaGiaCollegatoAdAltroElemento();
						trovaParoleSottoVerbo(et, listasotto, tuttiElementi);
					}
		}

		private static SrlElemento trovaVerboSopra(SrlElemento e, List<SrlElemento> tuttiElementi){
			SrlElemento verboSopra = null;
			for (SrlElemento et : tuttiElementi)
				if (e.tree == et.order && e.numeroAzione == et.numeroAzione && e.numeroVersioneFrase == et.numeroVersioneFrase && e.tipo.compareTo(et.tipo) == 0 && et.getSoggettiCollegati().size()>0 )
					verboSopra = et;
				else if (e.tree == et.order && e.numeroAzione == et.numeroAzione && e.numeroVersioneFrase == et.numeroVersioneFrase && e.tipo.compareTo(et.tipo) == 0 )
					trovaVerboSopra(et, tuttiElementi);
			return verboSopra;
		}
		
		private static void aggiungiElementiQuestoContesto(List<SrlElemento> ltemp, List<SrlElemento> lista, ElementoSituazioneAzione elm){
			SrlElemento temp = null;
			if (elm.getElementi().size() > 0)
				temp = elm.getElementi().get(0);
			
			for (SrlElemento e : ltemp){
				if (e.numeroAzione == temp.numeroAzione && e.numeroVersioneFrase == temp.numeroVersioneFrase && e.term.compareTo(temp.term) == 0 && e.tipo.compareTo(temp.tipo) == 0){
					lista.add(e);
				}
			}
		}
		
		private static SrlElemento cercaElementoDaTree(int tree, List<SrlElemento> lista){
			for (SrlElemento e : lista)
				if (e.order == tree)
					return e;
			return null;
		}
		
		private static void calcolaLivelloElementi(SrlElemento el, List<SrlElemento> lista, SrlElemento temp){
			el.livello ++;
			if (temp != null && temp.tree != 0){
				temp = cercaElementoDaTree(temp.tree, lista);
				calcolaLivelloElementi(el, lista, temp);
			}
		}
		
		private static SrlElemento trovaVerboBeSopra(SrlElemento e, List<SrlElemento> tuttiElementi){
			SrlElemento verboSopra = null;
			for (SrlElemento et : tuttiElementi)
				if (e.tree == et.order && et.word.compareTo("be") == 0)
					verboSopra = et;
			return verboSopra;
		}

		
		public static List<SrlElemento> identificaParoleDaConfrontareIS(){
			String nomeFileAzioni = "";
			String nomeFileGesti = "";
			String nomeFileOggetti = "";
			String nomeFileStrumenti = "";
			String nomeFilePosizioni = "";
			String nomeFileManiere = "";
			if (app.animale.compareTo("elephantIS") == 0){
				nomeFileAzioni = "ElephantIS_actions.txt";
				nomeFileGesti = "ElephantIS_gestures.txt";
			}
			if(app.animale.compareTo("manuale") == 0){
				String percorsoCartella = "xFileCoppieDivise/";
				nomeFileAzioni = "xFileCoppieDivise/Azioni.txt";
				nomeFileOggetti = "xFileCoppieDivise/Oggetti.txt";
				nomeFileStrumenti = "xFileCoppieDivise/Strumenti.txt";
				nomeFilePosizioni = "xFileCoppieDivise/Posizioni.txt";
				nomeFileManiere = "xFileCoppieDivise/Maniere.txt";
			}

			
			List<Contesto> contesti = new LinkedList<Contesto>();
			
			// 1) carica tutti gli SrlElementi
			// 2) carica tutte le RelazioniSrl
			
			// 3) prima cerco i possibili gesti che hanno la maggior relazione con le parole di ogni frase
			// 4) poi identifico le azioni collegate, triangolando

			String tabelladefinitionsrlmsr = "";
			String tabellasrlrelationsmsr = "";
			String tabellasrltriplemsr = "";

			tabelladefinitionsrlmsr = "definitionsrl"+app.animale;
			tabellasrlrelationsmsr = "srlrelations"+app.animale;
			
			//superfluo
			String percorsoCartella = "xFileCoppieDivise/";//
			nomeFileAzioni = "xFileCoppieDivise/Azioni.txt";//
			nomeFileOggetti = "xFileCoppieDivise/Oggetti.txt";//
			nomeFileStrumenti = "xFileCoppieDivise/Strumenti.txt";//
			nomeFilePosizioni = "xFileCoppieDivise/Posizioni.txt";//
			nomeFileManiere = "xFileCoppieDivise/Maniere.txt";//
			//superfluo
			
			caricaListaAzioni(nomeFileAzioni);
			caricaListaOggetti(nomeFileOggetti);
			caricaListaStrumenti(nomeFileStrumenti);
			caricaListaPosizioni(nomeFilePosizioni);
			caricaListaManiere(nomeFileManiere);
			
			
			caricaAzioni(nomeFileAzioni);
//			caricaOggetti(nomeFileOggetti);
//			caricaStrumenti(nomeFileStrumenti);
			caricaGesti(nomeFileGesti);
            
			
			Map<Integer, Studente> mappaStudenti = new TreeMap<Integer, Studente>();
			
			try {
				BufferedWriter bwc = new BufferedWriter(new FileWriter("ElephantIS_contesto.csv"));
				Map<String, SrlElemento> mappaelementi = new TreeMap<String, SrlElemento>();
				List<RelazioneSrl> listarelazioni = new LinkedList<RelazioneSrl>();
				Map<Integer, RelazioneSrl> mapparelazionisrl = new HashMap<Integer, RelazioneSrl>();

				// carico le definitionsrl
				mappaelementi = caricoDefinitionsrlIS(tabelladefinitionsrlmsr);
//				Map<String, SrlElemento> m = caricoSrlElementiAggiuntiIS(mappaelementi); // carica parole unite tipo walk_around  vedere righe dopo
//				caricoParolaContenutaInDefinition();  // carica definizioni
				
//				for (SrlElemento e : mappaelementi.values())
//					if (e.srlElementoAggiunto != null)
//						System.err.println(e.stampaElemento()+ " " +e.srlElementoAggiunto.wordUnite);
				
				// carico le srlrelationsmsr
				RitornoSrlRelationMsr rit = caricoSrlrelationmsrIS(tabellasrlrelationsmsr);
				listarelazioni = rit.listarelazioni;
				mapparelazionisrl = rit.mapparelazionisrlfrase;
				
				List<SrlElemento> ltemp = new LinkedList<SrlElemento>(mappaelementi.values());
//				ltemp.addAll(m.values()); // aggiungere se si aggiunge m
				List<SrlElemento> ltempOrdinataPerTerm = new LinkedList<SrlElemento>(mappaelementi.values());
				Map<String, Integer> occorrenze = new HashMap<String, Integer>(findAllSentences(ltemp));
				
				// SCRITTURA COPPIE SU DB
				// devo sostituire ltemp con altro
				/*
				String nomeFile = "fileCoppie.txt";
				scriviCoppieSuFile(ltemp, nomeFile);			
				*/
				
				app.eliminaErroriSrl(ltemp);
				
				
				Thread.sleep(3000);
				// faccio il sort della lista ltemp? 
				Collections.sort(ltemp, new SrlElementoComparator()); // le ordino per numero di parole crescente
				Collections.sort(ltempOrdinataPerTerm, new SrlElementoComparatorPerTerm()); // le ordino per numero di parole crescente
				
				for (SrlElemento e : ltempOrdinataPerTerm){
//					System.out.println(e.stampaElementoIS());
					
					Studente s = null;
					if (!mappaStudenti.containsKey(e.getNumeroVersioneFrase())){
						s = new Studente(e.getNumeroVersioneFrase());
						mappaStudenti.put(e.getNumeroVersioneFrase(), s);
					}
					else{
						s = mappaStudenti.get(e.getNumeroVersioneFrase());
					}
//					System.out.println(s.getNumeroStudente());
					
					Situazione sit = null;
					if (!s.getSituazioni().containsKey(e.getNumeroAzione())){
						sit = new Situazione(e.getNumeroAzione());
//						System.out.println(sit.getNumero()+ " "+sit.getTipo());
						s.addSituazione(sit);
//						System.out.println(s.getSituazioni());
					}
					else{
						sit = s.getSituazioni().get(e.getNumeroAzione());
						
					}
					
//					System.out.println("aggiunto "+sit);
//					if (e.getTipo().compareTo("contesto") == 0)
						sit.addElemento(e);
//					else if (e.getTipo().compareTo("azione") == 0)
//						sit.addElementoAzione(e);
					
					
				}

				
				
				// non ragiono piu' sui contesti come prima, bensi sulle azioni. 
				// TOLGO DA QUI
				for (Studente s : mappaStudenti.values()){
					System.out.println("studente "+s.getNumeroStudente());
					for (Situazione sit : s.getSituazioni().values()){
						System.out.println("situazione "+sit.getNumero());
						
						// scrivo sul file csv
						app.testoAzione = "";
						for (ElementoSituazioneAzione esa: sit.getSimElementiSituazioneAzione().values()){
							if (esa.getTipo().compareTo("azione") == 0)
								for (SrlElemento e : esa.getElementi()){
									bw.write(e.word+" ");
									bw.flush();
									testoAzione += e.word+" ";
								}
						}
						bw.write("\n");
						bw.flush();
						
						// 1) identifico prima i soggetti principali
						// 2) questi possono essere gli stessi per la stessa situazione, in contesto e azione
						List<SrlElemento> listaSoggetti = new LinkedList<SrlElemento>();
						
						List<RelazioneSrl> relazioniTermine = new LinkedList<RelazioneSrl>();
						List<SrlElemento> soggettiFrase = new LinkedList<SrlElemento>();
						SrlElemento soggetto = null;
						List<SrlElemento> soggPronomi = new LinkedList<SrlElemento>();
						
						if (hasSubjectIS(sit)){ // se ho un soggetto --> sono in presenza di una frase complessa da analizzare! (altrim. dï¿½ ambientazione)
							List<String> termContesto = new LinkedList<String>();
							List<String> termAzione = new LinkedList<String>();
							
							/*
							for (List<SrlElemento> l : sit.getElementiContesto().values()){
								for (SrlElemento e : l){
									// dovrebbe andare bene aggiornare il soggetto man mano che vengono processati i termini. 
									// comunque, verificare che l'ultimo soggetto aggiornato sia effettivamente l'ultimo inserito nella lista
									// si basa il ragionamento sul fatto che se inserisco un pronome, questo e' collegato all'ultimo soggetto
									// esplicito di cui ho parlato.
									if (hoNuovoSoggettoIS(e, listaSoggetti) && e.pos.compareTo("PRP") != 0 && e.type.compareTo("SBJ") == 0 ){//&& e.term.compareTo(term) == 0
										listaSoggetti.add(e);
										soggetto = e;
									}
									else
										if (e.pos.compareTo("PRP") == 0 && e.type.compareTo("SBJ") == 0 ){// && e.term.compareTo(term) == 0
											soggPronomi.add(e);
											e.soggPrincipale = soggetto;
										}
								}
							}*/
							
							for (ElementoSituazioneAzione elm: sit.getSimElementiSituazioneAzione().values()){
								for (SrlElemento e : elm.getElementi()){
									if (elm.getTipo().compareTo("azione") == 0){
										// dovrebbe andare bene aggiornare il soggetto man mano che vengono processati i termini. 
										// comunque, verificare che l'ultimo soggetto aggiornato sia effettivamente l'ultimo inserito nella lista
										// si basa il ragionamento sul fatto che se inserisco un pronome, questo e' collegato all'ultimo soggetto
										// esplicito di cui ho parlato.
										if (hoNuovoSoggettoIS(e, listaSoggetti) && e.pos.compareTo("PRP") != 0 && e.type.compareTo("SBJ") == 0 ){//&& e.term.compareTo(term) == 0
											listaSoggetti.add(e);
											soggetto = e;
										}
										else
											if (e.pos.compareTo("PRP") == 0 && e.type.compareTo("SBJ") == 0 ){// && e.term.compareTo(term) == 0
												soggPronomi.add(e);
												e.soggPrincipale = soggetto;
											}
									}
								}
							}

							if (soggetto != null){
//								for (ElementoSituazioneAzione esa : sit.getSimElementiSituazioneAzione().values())
//									System.out.println("situazione "+esa.stampaListaElementi()+ " "+soggetto);
								soggetto.listaAltriSoggettiCollegati.addAll(soggPronomi);
								soggettiFrase.add(soggetto);
								soggettiFrase.addAll(soggPronomi);
							}
						}
						
						// definitionsrl collegate a srlrelations
//						Thread.sleep(2000);
						for (ElementoSituazioneAzione elm: sit.getSimElementiSituazioneAzione().values()){
							if (elm.getTipo().compareTo("azione") == 0){
								List<SrlElemento> l = elm.getElementi();
								
								SrlElemento elemEsempio = null;
								if (l.size() > 0)
									elemEsempio = l.get(0);
								
								// ho la lista delle situazioni (term diversi)
								String term = "";
								int azione = -1;
								int numeroVersioneFrase = -1;
								String tipo = "";
								
								if (l.size() > 0){
									term = l.get(0).term; 
									azione = l.get(0).getNumeroAzione();
									numeroVersioneFrase = l.get(0).getNumeroVersioneFrase();
									tipo = l.get(0).getTipo();
								}
								
							
								for (RelazioneSrl r : listarelazioni){
	//								System.out.println("rterm "+r.term+ " "+term+" numazione " +r.numeroAzione+ " "+azione+" num versione "+r.numeroVersioneFrase+ " "+numeroVersioneFrase+ " tipo "+r.tipo+ " "+tipo);
									
									if (r.term.compareTo(term) == 0 && r.numeroAzione == azione && r.numeroVersioneFrase == numeroVersioneFrase && r.tipo.compareTo(tipo) ==0 ){
										sit.addRelazione(r); // devo fare lo stesso sul contesto, quando ciclerÃ² il contesto
										relazioniTermine.add(r);
//										System.out.println("--- r "+r.stampaRelazione());
		
										Map<Integer, CombinazioneLista> mappaCombinazioni = new HashMap<Integer, CombinazioneLista>();
										Map<Integer, CombinazioneLista> mappaCombinazioniNuova = new HashMap<Integer, CombinazioneLista>();
										List<SrlElemento> dubbi = new LinkedList<SrlElemento>();
										Map<String, Integer> quantitaDubbi = new HashMap<String, Integer>();
										List<SrlElemento> elementiOk = new LinkedList<SrlElemento>();
										String parole [] = r.object.split(" ");
	//									if (parole.length == 1 && trovaSrlElementoDaParola(ltempOrdinataPerTerm, r.predicate.substring(0, (r.predicate.length() - 3)), r.term) != null){
										if (parole.length == 1 && trovaSrlElementoDaParolaIS(l, r.predicate.substring(0, (r.predicate.length() - 3)) , r.term, elemEsempio) != null){
		//									qui devo aggiungere anche il predicato, altrimenti rischio di avere errori quando ho un predicato e solo un ogg
//											System.out.println(" errore "+r.stampaRelazione()+ " "+r.predicate.substring(0, (r.predicate.length() - 3))+" " +trovaSrlElementoDaParola(ltempOrdinataPerTerm, r.predicate.substring(0, (r.predicate.length() - 3)), r.term));
											
	//										elementiOk.add(trovaSrlElementoDaParola(ltempOrdinataPerTerm, r.predicate.substring(0, (r.predicate.length() - 3)), r.term));
											elementiOk.add(trovaSrlElementoDaParolaIS(l, r.predicate.substring(0, (r.predicate.length() - 3)), r.term, elemEsempio));
											
										}
											
										for (String sorig : parole){
											String s1 = "";
	//										for (SrlElemento e : ltemp){
											for (SrlElemento e : l){
												if (e.term.compareTo(term) == 0){
													if (e.originalword.toLowerCase().compareTo(sorig.toLowerCase()) == 0 || e.word.toLowerCase().compareTo(sorig.toLowerCase()) == 0){
														s1 = e.word;
													}
												}
											}
											// aggiungo temporaneamente tutto a elementiOk
	//										for (SrlElemento e : ltemp){
											for (SrlElemento e : l){
												if (e.word.compareTo(s1) == 0 && e.term.compareTo(term) == 0 && !elementiOk.contains(e)){
													elementiOk.add(e);
												}
											}
											
	//										List<SrlElemento> tuttiElementi = new LinkedList<SrlElemento>(elementiMatchantiInSrl(s1, term, ltemp));
											List<SrlElemento> tuttiElementi = new LinkedList<SrlElemento>(elementiMatchantiInSrl(s1, term, l));
											if (tuttiElementi.size() > 1){
												aggiungi(dubbi, tuttiElementi);
		
												if (quantitaDubbi.containsKey(s1))
													quantitaDubbi.put(s1, quantitaDubbi.get(s1) + 1);
												else
													quantitaDubbi.put(s1, 1);
											}
		
											if (quantitaDubbi.containsKey(s1)){
												creaRicorsivamenteCombinazioni(dubbi, 0, mappaCombinazioni, quantitaDubbi.get(s1), 0, true, 1);
												mappaCombinazioniNuova = ripulisciMappaCombinazioni(mappaCombinazioni, quantitaDubbi.get(s1));
											}
										}
										
										elementiOk.removeAll(dubbi);
										
//										for (SrlElemento sss : elementiOk)
//											System.out.println("stampa elementoOk " +sss.stampaElemento());
										
										List<SrlElemento> giaProcessati = new LinkedList<SrlElemento>();
		
										if (dubbi.size() == 0){
											if (r.listaOggetti.size() == 0)
												r.listaOggetti.addAll(elementiOk);
										}
										
										for (SrlElemento e : dubbi){
											for (CombinazioneLista c : mappaCombinazioniNuova.values()){
												List<SrlElemento> daValutare = new LinkedList<SrlElemento>();
												daValutare.addAll(elementiOk);
												daValutare.addAll(c.listaSrlElemento);
												List<SrlElemento> elementiVisti = new LinkedList<SrlElemento>();
												SrlElemento head = null;
												List<SrlElemento> elementi = new LinkedList<SrlElemento>(daValutare);
												
												controllaSeParoleSonoCollegateNellAlbero(elementi, elementiVisti, head);
												if (elementi.size() == elementiVisti.size()){
													if (r.listaOggetti.size() == 0)
														r.listaOggetti.addAll(elementiVisti);
												}
											}
										}						
//										System.out.println(" relazione "+r.stampaRelazione());
//										for (SrlElemento e : r.listaOggetti)
//											System.out.println(" - "+e.stampaElementoIS());
									}
									
									
									// aggiungo parole unite
									// le prendo da mappa m, solo se numero azione coincide, se tipo coincide e se term e order pure
									// e le aggiungo se entrambe le parole sono presenti (occhio che una puÃ² essere presente dentro "relazioni"
									
								}
							}
							
							// se invece sono in un contesto, devo identificare i soggetti principali
							if (elm.getTipo().compareTo("contesto") == 0){
								List<SrlElemento> elemContesto = new LinkedList<SrlElemento>();
								List<SrlElemento> l = elm.getElementi();
								
								SrlElemento elemEsempio = null;
								if (l.size() > 0)
									elemEsempio = l.get(0);

								String daScrivere = "";
								String intestazione = "ACTION"+elemEsempio.numeroAzione+"_"+elemEsempio.numeroVersioneFrase+";";
								
								List<SrlElemento> sogg = new LinkedList<SrlElemento>();
								List<SrlElemento> elemGiaAggiunti = new LinkedList<SrlElemento>();
								String frase = "";
								for (SrlElemento srle : elm.getElementi())
									frase += srle.word+ " ";
								
								intestazione += frase+"\n";
								daScrivere += intestazione;
//								bwc.write(frase+"\n");
//								bwc.flush();
								
								Actor attoreCorrisp = null;
								for (SrlElemento e : elm.getElementi()){
									Actor act = null;
									if (e.type.compareTo("SBJ") == 0){
										act = cercaAttoreCorrispondentePerContesto(e);
										if (e.pos.compareTo("PRP") == 0){//act == null && 
											elemGiaAggiunti.add(e);
											sogg.add(e);
											e.setAttoreCorrispondente(attoreCorrisp);
										}else{
											attoreCorrisp = act;
											elemGiaAggiunti.add(e);
											sogg.add(e);
											e.setAttoreCorrispondente(act);
										}
									}							
								}
								
								aggiungiElementiQuestoContesto(ltemp, elemContesto, elm);
								// devo ordinare gli elementi in base alla loro profondita' nell'albero
								// cosi' parto dal basso e poi risalgo
								
								for (SrlElemento el : elemContesto)
									calcolaLivelloElementi(el, elemContesto, el);
								
								Collections.sort(elemContesto, new SrlElementoComparatorProfonditaAlbero());
								
//								Thread.sleep(2000);
//								for (SrlElemento e : elemContesto){
//									System.out.println("elem "+e.stampaElementoIS()+ " "+e.livello);
//								}
//								Thread.sleep(2000);
								

								for (SrlElemento e : elemContesto){ //elm.getElementi()
//									System.out.println("IIIIIIII "+e.stampaElementoIS());
									
									// trovo elementi collegati a loc-prd --> posizione
									// trovo sbj con verbo collegati
									// a tal proposito, collego al verbo anche prd

									
									
//									if (e.getElementiDiLuogo().size() >0)
//										for (SrlElemento el : e.getElementiDiLuogo())
//											System.out.println("luogo "+el.stampaElementoIS());
//									
									if (e.getAttoreCorrispondente() != null){
										
										System.out.println("act "+e.getAttoreCorrispondente().stampa());
										List<SrlElemento> verbiCollegati = cercaVerboCollegato(elemContesto, e);
										Collections.sort(verbiCollegati, new SrlElementoComparatorProfonditaAlbero());

										for (SrlElemento verboCollegato : verbiCollegati){
											if (!elemGiaAggiunti.contains(verboCollegato)){

												elemGiaAggiunti.add(verboCollegato);
												e.aggiungiVerbiCollegati(verboCollegato);
												verboCollegato.aggiungiSoggettiCollegati(e);
												e.impostaGiaCollegatoAdAltroElemento();
												verboCollegato.impostaGiaCollegatoAdAltroElemento();
												List<SrlElemento> giaVisti = new LinkedList<SrlElemento>();
												List<SrlElemento> oggetti = new LinkedList<SrlElemento>();

												List<SrlElemento> giaVistiVersioneSemplice = new LinkedList<SrlElemento>();
												List<SrlElemento> oggettiVersioneSemplice = new LinkedList<SrlElemento>();
												
												cercaOggettoCollegatoAVerboCollegatoVersioneSemplice(ltemp, verboCollegato, giaVistiVersioneSemplice, oggettiVersioneSemplice);

												// temporaneamente commentato per far funzionare
												//cercaOggettoCollegatoAVerboCollegato(ltemp, verboCollegato, giaVisti, oggetti);
												if (oggettiVersioneSemplice.size() > 0){
													for (SrlElemento oggettoCollegato : oggettiVersioneSemplice){
														if (!elemGiaAggiunti.contains(oggettoCollegato)){
															elemGiaAggiunti.add(oggettoCollegato);
															e.aggiungiOggettiCollegati(oggettoCollegato);
															oggettoCollegato.aggiungiSoggettiCollegati(e);
															verboCollegato.aggiungiOggettiCollegati(oggettoCollegato);
															e.impostaGiaCollegatoAdAltroElemento();
															oggettoCollegato.impostaGiaCollegatoAdAltroElemento();
//															System.out.println("oggetto collegato "+oggettoCollegato.stampaElementoIS());
															
															List<SrlElemento> elsotto = new LinkedList<SrlElemento> (); 
															trovaElementiSotto(oggettoCollegato, elsotto, elm.getElementi());
															for (SrlElemento es : elsotto){
																e.aggiungiParoleCollegateAVerboCollegatoASoggetto(oggettoCollegato, es);
																e.aggiungiParoleCollegateAOggettoCollegatoASoggetto(oggettoCollegato, es);
																es.impostaGiaCollegatoAdAltroElemento();
															}	
														}
													}
												}
												
											}
										}
									}

									
									// prendo relazioni LOC-PRD e tutto cio che e sotto e la mia ambientazione
									
									
	
									// dovrebbe andare bene aggiornare il soggetto man mano che vengono processati i termini. 
									// comunque, verificare che l'ultimo soggetto aggiornato sia effettivamente l'ultimo inserito nella lista
									// si basa il ragionamento sul fatto che se inserisco un pronome, questo e' collegato all'ultimo soggetto
									// esplicito di cui ho parlato.
//									if (hoNuovoSoggettoIS(e, listaSoggetti) && e.pos.compareTo("PRP") != 0 && e.type.compareTo("SBJ") == 0 ){//&& e.term.compareTo(term) == 0
//										listaSoggetti.add(e);
//										soggetto = e;
//									}
//									else
//										if (e.pos.compareTo("PRP") == 0 && e.type.compareTo("SBJ") == 0 ){// && e.term.compareTo(term) == 0
//											soggPronomi.add(e);
//											e.soggPrincipale = soggetto;
//										}

								}
								
								// commentato temporaneamente per vedere cosa succede senza identificazione delle relazioni di luogo
								/*
								for (SrlElemento e : elm.getElementi()){
									if (e.type.compareTo("LOC-PRD") == 0){
										List<SrlElemento> elsotto = new LinkedList<SrlElemento> (); 
										trovaElementiSotto(e, elsotto, elm.getElementi());
										for (SrlElemento es : elsotto){
											e.aggiungiElementiDiLuogo(es);
											es.impostaGiaCollegatoAdAltroElemento();
											e.impostaGiaCollegatoAdAltroElemento();
										}
										SrlElemento verboSopra = trovaVerboSopra(e, elm.getElementi());
										
										if (verboSopra != null){
											SrlElemento soggColl = verboSopra.getSoggettiCollegati().get(0);
											soggColl.aggiungiElementiDiLuogo(e);
											soggColl.elementiDiLuogo.addAll(e.getElementiDiLuogo());
										}
									}
								}*/
								
								for (SrlElemento e : sogg){
//									System.out.println("sogg stampa "+e.stampaElementoIS());
									for (SrlElemento ee : e.getVerbiCollegati()){
										if (e.getVerbiCollegati().size() > 1 && ee.word.compareTo("be") != 0){
//											System.out.println("verbo stampa "+ ee.stampaElementoIS());
											List<SrlElemento> listaSotto = new LinkedList<SrlElemento>();
											trovaParoleSottoVerbo(ee, listaSotto, elm.getElementi());
											
											for (SrlElemento es : listaSotto){
												ee.aggiungiParoleCollegateAVerboCollegatoASoggetto(e, es);
//												System.out.println("verbo: parola sotto verbo "+ es.stampaElementoIS());
											}
											// devo anche beccare le altre parole che sono sotto il verbo --> lo faccio qui
										}
									}
									/*
									for (SrlElemento ee : e.getElementiDiLuogo()){
										System.out.println("luogo stampa "+ee.stampaElementoIS());
									}*/
								}

								for (SrlElemento e : sogg){
									
									if (e.attoreCorrispondente != null){
										System.out.println("- att " +e.attoreCorrispondente.stampa());
										System.out.println("- verbo " +cercaVerboCorrispondente(e));
										
										String verbi = "";
										String attore = ";;"+e.attoreCorrispondente.getNome()+"#";
										Collections.sort(e.getVerbiCollegati(), new SrlElementoComparatorPerOrder());

										for (SrlElemento vc : e.getVerbiCollegati()){
											String ogg = "";
											
											Collections.sort(vc.getOggettiCollegati(), new SrlElementoComparatorPerOrder());

											for (SrlElemento og : vc.getOggettiCollegati())
												if (og.pos.compareTo("PRP") == 0)
													ogg += "elephant_";
												else if (og.pos.compareTo("PRP$") == 0)
													ogg += "";
												else
													ogg += og.word+ "_";
											if (ogg.contains("_"))
												ogg = ogg.substring(0, ogg.length()-1);
											
											ogg = ogg.replace(".", "");
											
											if (!(vc.word.compareTo("be") == 0 && ogg.compareTo("") == 0)){
												
												SrlElemento verboSup = trovaVerboBeSopra(vc, elemContesto);
												
												if (ogg.compareTo("") != 0){
													if (verboSup != null && vc.word.compareTo("be") != 0)
														verbi += attore+""+verboSup.originalword+"_"+vc.originalword+"#";
													else
														verbi += attore+""+vc.originalword+"#";
													verbi += ogg+"\n";
												}
												else{
													if (verboSup != null && vc.word.compareTo("be") != 0)
														verbi += attore+""+verboSup.originalword+"_"+vc.originalword+"#";
													else
														verbi += attore+""+vc.originalword+"";
													verbi += ogg+"\n";
												}
//												verbi += attore+""+vc.originalword+"#";
//												verbi += ogg+"\n";
											}
										}
										
										daScrivere += verbi+"\n";
										
										bwc.write("att;"+e.attoreCorrispondente.getNome()+"\n");
										bwc.flush();
										
										bwc.write("verbo;"+verbi+"\n");
//										bwc.write("verbo;"+cercaVerboCorrispondente(e)+"\n");
										bwc.flush();

										for (SrlElemento el : e.elementiDiLuogo){
											bwc.write("luogo;"+el.word+"\n");
											bwc.flush();
										}
										
										
										for (SrlElemento og : e.getOggettiCollegati()){
											System.out.println("- oggetto "+cercaOggettoCorrispondente(og)+ " vecchio "+og.word);
//											System.out.println("- oggetto "+cercaOggettoCorrispondente(og)+ " vecchio "+og.word);
											bwc.write("ogg;"+og.word+ "\n");
											bwc.flush();
//											System.out.println("- oggetto "+og.word);
										}
										bwc.write("\n");
										bwc.flush();
										
//										for (SrlElemento ee : e.getVerbiCollegati()){
//											
//											List<SrlElemento> listaSotto = new LinkedList<SrlElemento>();
//											trovaParoleSottoVerbo(ee, listaSotto, elm.getElementi());
//											for (SrlElemento es : listaSotto){
//												ee.aggiungiParoleCollegateAVerboCollegatoASoggetto(e, es);
//												System.out.println("verbo: parola sotto verbo "+ es.stampaElementoIS());
//											}
//											System.out.println("verbo stampa "+ ee.stampaElementoIS());
//											// devo anche beccare le altre parole che sono sotto il verbo --> lo faccio qui
//										}
										
									}
									
								}
								
								
								
								
								if (soggetto != null){
//									for (ElementoSituazioneAzione esa : sit.getSimElementiSituazioneAzione().values())
//										System.out.println("situazione "+esa.stampaListaElementi()+ " "+soggetto);
									soggetto.listaAltriSoggettiCollegati.addAll(soggPronomi);
									soggettiFrase.add(soggetto);
									soggettiFrase.addAll(soggPronomi);
								}

								List<String> t = null;
								if (app.risultatoContesto.containsKey("ACTION"+elemEsempio.numeroAzione+"_"+elemEsempio.numeroVersioneFrase)){
									t = app.risultatoContesto.get("ACTION"+elemEsempio.numeroAzione+"_"+elemEsempio.numeroVersioneFrase);
									t.add(daScrivere);
								}
								else{
									t = new LinkedList<String>();
									t.add(daScrivere);
									app.risultatoContesto.put("ACTION"+elemEsempio.numeroAzione+"_"+elemEsempio.numeroVersioneFrase, t);
								}
							}

							
						}
//						Thread.sleep(2000);
						
						// a questo punto ho trovato tutte le relazioni srl con gli elementi srl collegati
						// ora devo calcolare il match
						
						/*
						for (SrlElemento sogg : soggettiFrase){
//							System.out.println(" sogg frase "+sogg.stampaElemento());
							// 1) prendo il soggetto (e memorizzo se ha un soggetto di livello superiore, cioe' se e' o meno un pronome)
							// 2) ciclo su ogni relazionesrl
							// 3) guardo le relazionisrl che sono collegate a lui (passando per definitionsrl)
							// 4) se sono collegate a lui, me le segno e cerco quali altre relazioni sono collegate a queste
							
							for (RelazioneSrl r : sit.getListaRelazioniAzione()){
//								System.err.println(r.stampaRelazione());
//								for (SrlElemento e : r.listaOggetti){
//									System.err.println(e.stampaElementoIS()+ " "+sogg.stampaElementoIS());
//								}
								if (r.listaOggetti.contains(sogg)){
									System.out.println("         collegato 1 liv "+r.stampaRelazione());
									
								}
							}
							
						}	
						*/

						/*
						for (RelazioneSrl r : sit.getListaRelazioniAzione()){
							if (r.livelloSuperioreSrl != null)
								System.err.println(" - r "+r.stampaRelazione()+ " "+r.livelloSuperioreSrl.stampaRelazione());
							
//							
//							if (relazioniTerminePerContesto.containsKey(term))
//								relazioniTerminePerContesto.get(term).add(r);
//							else{
//								List<RelazioneSrl> lt = new LinkedList<RelazioneSrl>();
//								lt.add(r);
//								relazioniTerminePerContesto.put(term, lt);
//							}
						}*/
						
						for (ElementoSituazioneAzione elm: sit.getSimElementiSituazioneAzione().values()){
							app.disambiguoConTakeEMake = false;
							
							SrlElemento elemEsempio = null;
							if (elm.getElementi().size() > 0){
								for (SrlElemento e : elm.getElementi())
									if (e.tipo != null)
										elemEsempio = e;
							}
	
//						for (String stringa : sit.getElementiAzione().keySet()){
							// HA SENSO METTERLO QUI?
							List<SimilaritaElementoSituazioneAzione> miglioriSimilaritaElementoSituazioneAzione = new LinkedList<SimilaritaElementoSituazioneAzione>();

							List<SrlElemento> lsrlel = elm.getElementi();
							List<RelazioneSrl> lsrl = elm.getRelazioni();
							
							preprocessaAlberoSrl(lsrl);
							
							app.mappaRelazioniSoggetto.clear();
							creaMappaOccurrencesSoggetto(lsrl);
							
							List<SrlElemento> listatemp = new LinkedList<SrlElemento>();
							Map<Integer, List<SrlElemento>> mappatemp = new HashMap<Integer, List<SrlElemento>>();

							listatemp = trovaElementiDaAggiungerePerConfrontoIS(lsrl, listatemp);
							System.err.println("CONFRONTO SRL");
//							mappatemp = trovaElementiDaAggiungerePerConfrontoSRLIS(lsrl, lsrlel, elemEsempio);
							
							mappatemp.put(1, lsrlel);
							for (Integer i : mappatemp.keySet()){
								int a = mappatemp.get(i).size();
								if (a != 0){
									SrlElemento srlVerbo  = null;
									SrlElemento srlOggetto = null;
									SrlElemento srlStrumento = null;
									SrlElemento srlPosizione = null;
									SrlElemento srlManiera = null;
									SrlElemento srlAppo = null;
									String nomeBOM = null;
									boolean isLoc = false;
									boolean attivaNomeBOM = true;
									boolean disattivaScambioLoc = true;
									String numAzione = "";
									for (SrlElemento e : mappatemp.get(i)){
										
									}
									
									System.out.println("******************* "+i+ " **************************");
									for (SrlElemento e : mappatemp.get(i)){
										
										if (e.getNumeroAzione() != 0 && numAzione.compareTo("") == 0){
											numAzione = "ACTION"+e.getNumeroAzione()+ "_"+e.getNumeroVersioneFrase();
											System.out.println("DEBUG: " + numAzione);
										}
										System.out.println(""+e.stampaElemento());
										String originalWord = e.getOriginalword();
										if(originalWord.startsWith("***")){
											nomeBOM = originalWord;
											
										}
										if(e.getType().compareTo("ROOT")==0 && (e.getPos().compareTo("VB")==0 ||
												                   e.getPos().compareTo("VBP")==0)){//verifico le condizioni per creare le triple
											srlVerbo = e;
										}
										
										if(((e.getType().compareTo("CONJ")==0)||(e.getType().compareTo("COORD")==0)
												||(e.getType().compareTo("IM")==0)) && (e.getPos().compareTo("VB")==0 || e.getPos().compareTo("VB")==0 ||
								                   e.getPos().compareTo("VBP")==0)){//verifico le condizioni per creare le triple della frase coordinata
										    //  se la frase parla di posizionamenti
									     	//  scambio oggetto e strumento
											if(disattivaScambioLoc){}
											else{
											if(isLoc){ 
												SrlElemento temp1 = srlStrumento;
												srlStrumento = srlOggetto;
												srlOggetto = temp1;
											}}
											SrlElemento srlManieraTemp = null;
											if(isManieraCollegataVerbo(srlManiera,srlVerbo)){
												srlManieraTemp = srlManiera;
												srlManiera = null;
											}
											String nomeFile = "";
											
//										if(srlVerbo.numeroAzione != -1){
//											System.out.println("VERBO DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
//										}
//										if(srlOggetto.numeroAzione != -1){
//											System.out.println("OGGETTO DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
//										}
											//if(srlStrumento.numeroAzione != -1){
											//	System.out.println("STRUMENTO DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
											//}
											
											//if(srlManieraTemp.numeroAzione != -1){
											//	System.out.println("MANIERA DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
											//}
											
											//if(nomeBOM != null){
											//	nomeFile = "AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase;
												//nomeFile = "AZIONE"+ srlOggetto.numeroAzione+ srlOggetto.numeroVersioneFrase;
												
												//System.out.println("VERBO DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
												//System.out.println("OGGETTO DEBUG: AZIONE"+ srlOggetto.numeroAzione+ srlOggetto.numeroVersioneFrase);
											//}
											RisultatoTriplaOriginale rtu =null;
											if(attivaNomeBOM){
												rtu = new RisultatoTriplaOriginale(srlVerbo,srlOggetto,srlStrumento,srlPosizione,srlManieraTemp,nomeBOM);
												
											}else{
												rtu = new RisultatoTriplaOriginale(srlVerbo,srlOggetto,srlStrumento,srlPosizione,srlManieraTemp);
											}
											rtu.setNameFile(numAzione);
											//System.out.println("NOME FILE DEBUG:" + nomeFile);

											listaRisSrlVerbi.add(srlVerbo);
											listaRisSrlOggetti.add(srlOggetto);
											listaRisSrlStrumenti.add(srlStrumento);
											listaRisSrlPosizioni.add(srlPosizione);
											listaRisSrlManiere.add(srlManieraTemp);
											listaRisTriplaOriginale.add(rtu);
											srlVerbo = e; 
											srlOggetto=null;
											srlStrumento=null;
											srlPosizione=null;
											//ATTENZIONE!!!!
											//controllare se maniera e prima o dopo con un ciclo if
											
											isLoc = false;
											
										}
										if((e.getType().compareTo("MNR")==0)){// ||
//											(e.getType().compareTo("ADV")==0)){
											srlManiera = e;
										}
										if((e.getType().compareTo("LOC")==0) ||
												(e.getType().compareTo("DIR")==0)){
											if(locIsPerifrasi(e))
												srlPosizione = getParolaCollegata(mappatemp.get(i),e);
											else
												srlPosizione = e;
											isLoc = true; 
										}
										
										if((e.getType().compareTo("OBJ")==0) && ((e.getPos().compareTo("NN")==0)
																				|| (e.getPos().compareTo("NNS")==0))){
											if(srlAppo!=null ){
												System.out.println(getParolaCollegata(mappatemp.get(i),e).getWord());
												srlStrumento = e;
											}else
											srlOggetto = e;
										}
										if((e.getType().compareTo("PMOD")==0)&& ((e.getPos().compareTo("NN")==0)
																				|| (e.getPos().compareTo("NNS")==0))){
											srlStrumento = e;
										}
										if((e.getType().compareTo("APPO")==0)&& ((e.getPos().compareTo("VBG")==0))){
											srlAppo = e;
										}

										bw.write(e.getNumeroAzione()+ "_"+e.getNumeroVersioneFrase()+";;"+e.word+"\n");
										bw.flush();
									}
									//**************************************
									//	QUA CREO OGGETTI RISULTATI UMANI									
//**************************************				
									//  se la frase parla di posizionamenti
							     	//  scambio oggetto e strumento
									if(disattivaScambioLoc){}
									else{
									if(isLoc){ 
										SrlElemento temp1 = srlStrumento;
										srlStrumento = srlOggetto;
										srlOggetto = temp1;
									}}
									
									//String nomeFile = "";
									//if(nomeBOM == null){
									//	nomeFile = "AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase;
									//	System.out.println("VERBO DEBUG: AZIONE"+ srlVerbo.numeroAzione+ srlVerbo.numeroVersioneFrase);
									//	System.out.println("OGGETTO DEBUG: AZIONE"+ srlOggetto.numeroAzione+ srlOggetto.numeroVersioneFrase);
									//}
									
									RisultatoTriplaOriginale rtu=null;
									if(attivaNomeBOM){
										rtu = new RisultatoTriplaOriginale(srlVerbo,srlOggetto,srlStrumento,srlPosizione,srlManiera, nomeBOM);
									}else{
										rtu = new RisultatoTriplaOriginale(srlVerbo,srlOggetto,srlStrumento,srlPosizione,srlManiera);
									}
									rtu.setNameFile(numAzione);
									//System.out.println("NOME FILE DEBUG:" + rtu.getNameFile());

									listaRisSrlVerbi.add(srlVerbo);
									listaRisSrlOggetti.add(srlOggetto);
									listaRisSrlStrumenti.add(srlStrumento);
									listaRisSrlPosizioni.add(srlPosizione);
									listaRisSrlManiere.add(srlManiera);
									listaRisTriplaOriginale.add(rtu);
									

									List<SrlElemento> listaElementiPerContesto = new LinkedList<SrlElemento>();
									List<SimilaritaElementoSituazioneAzione> listSimilaritaElementoSituazioneAzione = new LinkedList<SimilaritaElementoSituazioneAzione>(); 
									listaElementiPerContesto.addAll(mappatemp.get(i));
									
									/*
									List<SimilaritaGestoAzione> listSimilaritaGestoAzione = new LinkedList<SimilaritaGestoAzione>(); 
									listaElementiPerContesto.addAll(mappatemp.get(i));
									*/
									

									clearAllAzioniGestiSrlElementi(listaElementiPerContesto);

//									listSimilaritaGestoAzione = calcolaSimilarita(listaElementiPerContesto, c);
									
									
//									Collections.sort(listSimilaritaGestoAzione, new SimilaritaGestoAzioneComparator());
//									for (SimilaritaGestoAzione s : listSimilaritaGestoAzione)
//										if (s.similarita >0.1)
//										System.out.println(" "+s.gesto.nomeGesto+" "+s.azione.nomeAzione+ " "+s.similarita+ " "+s.contesto.elementiAmbientazione.get(0).word);

//									for (SrlElemento ee : listaElementiPerContesto)
//										System.out.println(" ################# "+ee.stampaElemento());
									
									listSimilaritaElementoSituazioneAzione = calcolaSimilaritaSoloConAzioneIS(listaElementiPerContesto, elm);
									
									if (!app.disambiguoConTakeEMake)
										Collections.sort(listSimilaritaElementoSituazioneAzione, new SimilaritaElementoSituazioneAzioneComparator());
									else
										Collections.sort(listSimilaritaElementoSituazioneAzione, new SimilaritaElementoSituazioneAzioneTakeMakeComparator());
									
									boolean devoDisambiguarePerParoleSpeciali = false;
									boolean devoDisambiguarePerParoleSpecialiETakeEMake = false;
									if (listSimilaritaElementoSituazioneAzione.size() > 1 ){
										if (!app.disambiguoConTakeEMake && Double.compare(listSimilaritaElementoSituazioneAzione.get(0).similarita, listSimilaritaElementoSituazioneAzione.get(1).similarita) == 0){
											devoDisambiguarePerParoleSpeciali = true;
										}
										else if (app.disambiguoConTakeEMake && Double.compare(listSimilaritaElementoSituazioneAzione.get(0).similaritaPiuTakeMake, listSimilaritaElementoSituazioneAzione.get(1).similaritaPiuTakeMake) == 0){
											devoDisambiguarePerParoleSpecialiETakeEMake = true;
										}
									}
									
									
									

						    		String ri = "";

						    		if (app.disambiguoConTakeEMake && !devoDisambiguarePerParoleSpeciali && !devoDisambiguarePerParoleSpecialiETakeEMake){
							    		String azioneRisultante = listSimilaritaElementoSituazioneAzione.get(0).azione.nomeAzione;
										for (SimilaritaElementoSituazioneAzione si : listSimilaritaElementoSituazioneAzione){
											System.out.println("risultato "+si.azione.nomeAzione+ " "+si.similarita+" "+si.azione.stampaListaSimSrlElemento(app.disambiguoConTakeEMake, devoDisambiguarePerParoleSpeciali)+ "");
											bw.write(";"+si.azione.nomeAzione+ " "+si.similaritaPiuTakeMake+ " "+si.azione.stampaListaSimSrlElemento(app.disambiguoConTakeEMake, devoDisambiguarePerParoleSpeciali)+ "\n");
											bw.flush();
											ri = si.azione.nomeAzione+ " "+si.similaritaPiuTakeMake+ " "+si.azione.stampaListaSimSrlElemento(app.disambiguoConTakeEMake, devoDisambiguarePerParoleSpeciali);
								    		app.queryTabella.add(numAzione+"!!!"+testoAzione+"!!!"+ri+"!!!"+si.similarita+"!!!"+azioneRisultante);
										}
									}
						    		else if (!devoDisambiguarePerParoleSpeciali && !devoDisambiguarePerParoleSpecialiETakeEMake){
							    		String azioneRisultante = listSimilaritaElementoSituazioneAzione.get(0).azione.nomeAzione;
										for (SimilaritaElementoSituazioneAzione si : listSimilaritaElementoSituazioneAzione){
											System.out.println("risultato "+si.azione.nomeAzione+ " "+si.similarita+" "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "");
											bw.write(";"+si.azione.nomeAzione+ " "+si.similarita+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "\n");
											bw.flush();
											ri = si.azione.nomeAzione+ " "+si.similarita+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali);
								    		app.queryTabella.add(numAzione+"!!!"+testoAzione+"!!!"+ri+"!!!"+si.similarita+"!!!"+azioneRisultante);
										}
									}
									
									else if (devoDisambiguarePerParoleSpeciali){
										Collections.sort(listSimilaritaElementoSituazioneAzione, new SimilaritaElementoSituazioneAzioneParoleSpecialiComparator());
							    		String azioneRisultante = listSimilaritaElementoSituazioneAzione.get(0).azione.nomeAzione;
										for (SimilaritaElementoSituazioneAzione si : listSimilaritaElementoSituazioneAzione){
											System.out.println("risultato "+si.azione.nomeAzione+ " "+si.similaritaPiuParoleSpeciali+" "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "");
											bw.write(";"+si.azione.nomeAzione+ " "+si.similaritaPiuParoleSpeciali+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "\n");
											bw.flush();
											ri = si.azione.nomeAzione+ " "+si.similaritaPiuParoleSpeciali+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali);
								    		app.queryTabella.add(numAzione+"!!!"+testoAzione+"!!!"+ri+"!!!"+si.similarita+"!!!"+azioneRisultante);
										}
									}

									else if (devoDisambiguarePerParoleSpecialiETakeEMake){
										Collections.sort(listSimilaritaElementoSituazioneAzione, new SimilaritaElementoSituazioneAzioneParoleSpecialiTakeMakeComparator());
							    		String azioneRisultante = listSimilaritaElementoSituazioneAzione.get(0).azione.nomeAzione;
										for (SimilaritaElementoSituazioneAzione si : listSimilaritaElementoSituazioneAzione){
											System.out.println("risultato "+si.azione.nomeAzione+ " "+si.similaritaPiuTakeMakeEParoleSpeciali+" "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "");
											bw.write(";"+si.azione.nomeAzione+ " "+si.similaritaPiuTakeMakeEParoleSpeciali+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali)+ "\n");
											bw.flush();
											ri = si.azione.nomeAzione+ " "+si.similaritaPiuTakeMakeEParoleSpeciali+ " "+si.azione.stampaListaSimSrlElemento(devoDisambiguarePerParoleSpecialiETakeEMake, devoDisambiguarePerParoleSpeciali);
								    		app.queryTabella.add(numAzione+"!!!"+testoAzione+"!!!"+ri+"!!!"+si.similarita+"!!!"+azioneRisultante);
										}
									}
									
									miglioriSimilaritaElementoSituazioneAzione.add(listSimilaritaElementoSituazioneAzione.get(0));
									
									
//									System.out.println(" "+s.contesto.elementiAmbientazione.get(0).word+ " "+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());

								}
							}
							
						}


						
						
						
					}
//					int i1 = 1;
//					for(RisultatoTriplaOriginale rtu : listaRisTriplaOriginale){
//						
//						System.out.print("Tripla "+i1+"::---");
//						System.out.println(rtu.stampaSoloParole());
//						i1++;
//					}
//************************************************************************************
//************					SE VUOI CHE STAMPI LE LISTE        ***********************************
//**********************		                              *********************			
					
//					System.out.print("Verbi_______\n");
//					for(SrlElemento e : listaRisSrlVerbi){
//						if(e!=null)
//						System.out.println(e.stampaElemento());
//					}
//					System.out.print("Oggetti _______\n");
//					for(SrlElemento e : listaRisSrlOggetti){
//						if(e!=null)
//						System.out.println(e.stampaElemento());
//					}
//					System.out.print("Strumenti________\n");
//					for(SrlElemento e : listaRisSrlStrumenti){
//						if(e!=null)
//						System.out.println(e.stampaElemento());
//					}
//					System.out.print("Posizioni________\n");
//					for(SrlElemento e : listaRisSrlPosizioni){
//						if(e!=null)
//						System.out.println(e.stampaElemento());
//					}
//					System.out.print("Maniere________\n");
//					for(SrlElemento e : listaRisSrlManiere){
//						if(e!=null)
//						System.out.println(e.stampaElemento());
//					}
					percorsoCartella = "xFileCoppieDivise/";
					if (app.scritturaSuFileCoppieDivise){
						String fileDaScrivere = ""+percorsoCartella+"fileCoppieVerbiTemp.txt";
						String fileDaLeggere =""+percorsoCartella+"Azioni.txt";
						scritturaSuFileCoppieDivise(listaRisSrlVerbi, fileDaLeggere, fileDaScrivere,"#v" );
						//
						fileDaScrivere = ""+percorsoCartella+"fileCoppieOggettiTemp.txt";
						fileDaLeggere =""+percorsoCartella+"Oggetti.txt";
						scritturaSuFileCoppieDivise(listaRisSrlOggetti, fileDaLeggere, fileDaScrivere,"#n" );
						//
						fileDaScrivere = ""+percorsoCartella+"fileCoppieStrumentiTemp.txt";
						fileDaLeggere =""+percorsoCartella+"Strumenti.txt";
						scritturaSuFileCoppieDivise(listaRisSrlStrumenti, fileDaLeggere, fileDaScrivere,"#n" );
						//
						fileDaScrivere = ""+percorsoCartella+"fileCoppiePosizioniTemp.txt";
						fileDaLeggere =""+percorsoCartella+"Posizioni.txt";
						scritturaSuFileCoppieDivise(listaRisSrlPosizioni, fileDaLeggere, fileDaScrivere,"" );
						//
						fileDaScrivere = ""+percorsoCartella+"fileCoppieManiereTemp.txt";
						fileDaLeggere =""+percorsoCartella+"Maniere.txt";
						scritturaSuFileCoppieDivise(listaRisSrlManiere, fileDaLeggere, fileDaScrivere,"" );
					
					
						scritturaCoppieDiviseSuFileUnico(""+percorsoCartella+"fileCoppieVerbiTemp.txt",
						""+percorsoCartella+"fileCoppieOggettiTemp.txt",""+percorsoCartella+"fileCoppieStrumentiTemp.txt",
						""+percorsoCartella+"fileCoppiePosizioniTemp.txt",""+percorsoCartella+"fileCoppieManiereTemp.txt",
						""+percorsoCartella+"fileCoppieDiviseUnico.txt");
						
					}
					
					
				}
				
				
				Map<String, List<RelazioneSrl>> relazioniTerminePerContesto = new HashMap<String, List<RelazioneSrl>>();
				/*
				Map<String, List<RelazioneSrl>> relazioniTerminePerContesto = new HashMap<String, List<RelazioneSrl>>();
				if (app.approccio.compareTo("srl") == 0){
					stampaContesti(contesti);
					List<SrlElemento> listatemp = new LinkedList<SrlElemento>();
					Map<Integer, List<SrlElemento>> mappatemp = new HashMap<Integer, List<SrlElemento>>();
					List<SimilaritaContestoAzione> miglioriSimilaritaContestoAzione = new LinkedList<SimilaritaContestoAzione>();

					for (Contesto c : contesti){
						// 1) info logistiche
						// 2) parte introduttiva
						// 3) singole frasi
						app.paroleInseriteGlobali.clear();
						System.out.println(" Contesto ");
						
						listatemp.addAll(c.elementiAmbientazione);
						listatemp = trovaElementiDaAggiungerePerConfronto(c.relazioniFraseIntroduttiva, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
						
						for (List<RelazioneSrl> lr : c.relazioniFrasiSuccessive.values()){
//							for (RelazioneSrl rrr : lr)
//								System.err.println("rel frasi succ" +rrr.stampaRelazione());
							
							mappatemp = trovaElementiDaAggiungerePerConfrontoSRL(lr, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
							for (Integer i : mappatemp.keySet()){
								if (mappatemp.get(i).size() != 0){
									
									System.out.println("******************* "+i+ " **************************");
									for (SrlElemento e : mappatemp.get(i))
										System.out.println(""+e.stampaElemento());

									List<SrlElemento> listaElementiPerContesto = new LinkedList<SrlElemento>();
									List<SimilaritaContestoAzione> listSimilaritaContestoAzione = new LinkedList<SimilaritaContestoAzione>(); 
									listaElementiPerContesto.addAll(mappatemp.get(i));

									clearAllAzioniGestiSrlElementi(listaElementiPerContesto);

//									listSimilaritaGestoAzione = calcolaSimilarita(listaElementiPerContesto, c);
//									Collections.sort(listSimilaritaGestoAzione, new SimilaritaGestoAzioneComparator());
//									for (SimilaritaGestoAzione s : listSimilaritaGestoAzione)
//										if (s.similarita >0.1)
//										System.out.println(" "+s.gesto.nomeGesto+" "+s.azione.nomeAzione+ " "+s.similarita+ " "+s.contesto.elementiAmbientazione.get(0).word);

//									for (SrlElemento ee : listaElementiPerContesto)
//										System.out.println(" ################# "+ee.stampaElemento());
									
									listSimilaritaContestoAzione = calcolaSimilaritaSoloConAzione(listaElementiPerContesto, c, st);
									Collections.sort(listSimilaritaContestoAzione, new SimilaritaContestoAzioneComparator());
									for (SimilaritaContestoAzione s : listSimilaritaContestoAzione)
										System.out.println(""+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());
									
									miglioriSimilaritaContestoAzione.add(listSimilaritaContestoAzione.get(0));
									
//									System.out.println(" "+s.contesto.elementiAmbientazione.get(0).word+ " "+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());

								}
							}
//							listatemp = trovaElementiDaAggiungerePerConfrontoSRL(lr, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
						}
						
						for (SrlElemento e : listatemp)
							System.out.println("° "+e.stampaElemento());
						
						listatemp.clear();
					}
					for (SimilaritaContestoAzione s : miglioriSimilaritaContestoAzione){
						System.out.println(""+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());
					}
					calcolaSimilaritaAzioneGesto(miglioriSimilaritaContestoAzione, contesti);
					
				}
				
				*/
				
				
				
				
				
				////////////////////////////////////////
				////////////////////////////////////////
				////////////////////////////////////////
				////////////////////////////////////////
				////////////////////////////////////////
				////////////////////////////////////////
				////////////////////////////////////////
				
/*
				
				
				Map<String, List<SrlElemento>> soggettiFrasePerContesto = new HashMap<String, List<SrlElemento>>();
				// per ogni termine
				for (String term : occorrenze.keySet()){
					List<RelazioneSrl> relazioniTermine = new LinkedList<RelazioneSrl>();
					List<SrlElemento> soggettiFrase = new LinkedList<SrlElemento>();
					SrlElemento soggetto = null;
					List<SrlElemento> soggPronomi = new LinkedList<SrlElemento>();
			
					
					
	
		
					
					for (SrlElemento e : soggettiFrase){
						if (!soggettiFrasePerContesto.containsKey(term)){
							List<SrlElemento> le = new LinkedList<SrlElemento>();
							le.add(e);
							soggettiFrasePerContesto.put(term, le);
						}
						else
							soggettiFrasePerContesto.get(term).add(e);
					}
					
					preprocessaAlberoSrl(relazioniTermine);
					
					for (RelazioneSrl r : relazioniTermine){
//						if (r.livelloSuperioreSrl != null)
//							System.out.println(" - r "+r.stampaRelazione()+ " "+r.livelloSuperioreSrl.stampaRelazione());
						
						if (relazioniTerminePerContesto.containsKey(term))
							relazioniTerminePerContesto.get(term).add(r);
						else{
							List<RelazioneSrl> lt = new LinkedList<RelazioneSrl>();
							lt.add(r);
							relazioniTerminePerContesto.put(term, lt);
						}
					}

//					for (RelazioneSrl r : relazioniTermine)
//						if (r.relazioniLivInf.size() != 0){
//							for (SrlElemento e : r.relazioniLivInf)
//							System.out.println(" - r "+r.stampaRelazione()+ " "+r.livelloSuperioreSrl.stampaRelazione());
//							
//						}

				}
				*/
				
				// soggetti
//				for (SrlElemento e : listaSoggetti){
//					System.out.println(" lista "+e.stampaElemento());
//					for (SrlElemento ee : e.listaAltriSoggettiCollegati)
//						System.out.println(" altri sogg "+ee.stampaElemento());
//						
//				}
				
				// verbi collegati all'elefante
				
				
				
				// calcola match con bag of words
				/*
				String nomeFile = "fileCoppie.txt";
				scriviCoppieSuFileIS(ltemp, nomeFile);			
				*/

				/*
				String nomeFile = "fileCoppietemp.txt";
				scriviCoppieSuFileTipoParolaIS(ltemp, nomeFile);
				*/

				trovaFrasiContesto(contesti, ltempOrdinataPerTerm, listarelazioni);
				
				// calcolo match con bag of words
				if (app.approccio.compareTo("bag of words") == 0){
					List<SimilaritaGestoAzione> listaComplessivaSimilarita = new LinkedList<SimilaritaGestoAzione>();
					for (Contesto c : contesti){
						List<SrlElemento> listaElementiPerContesto = new LinkedList<SrlElemento>();
						listaElementiPerContesto.addAll(c.elementiAmbientazione);
						listaElementiPerContesto.addAll(c.elementiFraseIntroduttiva);
						for (List<SrlElemento> l : c.elementiFrasiSuccessive.values())
							listaElementiPerContesto.addAll(l);
						
//						for (SrlElemento e : listaElementiPerContesto)
//							System.err.println(" ******** "+e.stampaElemento());
//						
						clearAllAzioniGestiSrlElementi(listaElementiPerContesto);
						List<SimilaritaGestoAzione> listSimilaritaGestoAzione = new LinkedList<SimilaritaGestoAzione>(); 
						
//						listSimilaritaGestoAzione = calcolaSimilarita(ltemp); // vecchio
						listSimilaritaGestoAzione = calcolaSimilarita(listaElementiPerContesto, c);
						Collections.sort(listSimilaritaGestoAzione, new SimilaritaGestoAzioneComparator());
						listaComplessivaSimilarita.addAll(listSimilaritaGestoAzione);
						
//						for (SimilaritaGestoAzione s : listSimilaritaGestoAzione)
//							System.out.println(" "+s.gesto.nomeGesto+ " "+s.azione.nomeAzione+ " "+s.similarita+ " "+c.elementiAmbientazione.get(0).word);

						List<SimilaritaGestoAzione> listSimilaritaPerOgniAzione = new LinkedList<SimilaritaGestoAzione>();
						listSimilaritaPerOgniAzione = trovaMatchPerOgniAzione(listSimilaritaGestoAzione);
						
						System.out.println();
						System.out.println("Contesto "+c.internoEsterno+ " "+c.elementiAmbientazione.get(0).word);
						for (SimilaritaGestoAzione s : listSimilaritaPerOgniAzione)
							System.out.println(""+s.gesto.nomeGesto+ "-"+s.azione.nomeAzione+ " "+s.similarita);
						listaElementiPerContesto.clear();
						
					}

				}
				else if (app.approccio.compareTo("srl") == 0){
					stampaContesti(contesti);
					List<SrlElemento> listatemp = new LinkedList<SrlElemento>();
					Map<Integer, List<SrlElemento>> mappatemp = new HashMap<Integer, List<SrlElemento>>();
					List<SimilaritaContestoAzione> miglioriSimilaritaContestoAzione = new LinkedList<SimilaritaContestoAzione>();

					for (Contesto c : contesti){
						// 1) info logistiche
						// 2) parte introduttiva
						// 3) singole frasi
						app.paroleInseriteGlobali.clear();
						System.out.println(" Contesto ");
						
						listatemp.addAll(c.elementiAmbientazione);
						listatemp = trovaElementiDaAggiungerePerConfronto(c.relazioniFraseIntroduttiva, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
						
						for (List<RelazioneSrl> lr : c.relazioniFrasiSuccessive.values()){
//							for (RelazioneSrl rrr : lr)
//								System.err.println("rel frasi succ" +rrr.stampaRelazione());
							
							mappatemp = trovaElementiDaAggiungerePerConfrontoSRL(lr, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
							for (Integer i : mappatemp.keySet()){
								if (mappatemp.get(i).size() != 0){
									
									System.out.println("******************* "+i+ " **************************");
									for (SrlElemento e : mappatemp.get(i))
										System.out.println(""+e.stampaElemento());

									List<SrlElemento> listaElementiPerContesto = new LinkedList<SrlElemento>();
									List<SimilaritaContestoAzione> listSimilaritaContestoAzione = new LinkedList<SimilaritaContestoAzione>(); 
									listaElementiPerContesto.addAll(mappatemp.get(i));

									/*
									List<SimilaritaGestoAzione> listSimilaritaGestoAzione = new LinkedList<SimilaritaGestoAzione>(); 
									listaElementiPerContesto.addAll(mappatemp.get(i));
									*/

									clearAllAzioniGestiSrlElementi(listaElementiPerContesto);

//									listSimilaritaGestoAzione = calcolaSimilarita(listaElementiPerContesto, c);
//									Collections.sort(listSimilaritaGestoAzione, new SimilaritaGestoAzioneComparator());
//									for (SimilaritaGestoAzione s : listSimilaritaGestoAzione)
//										if (s.similarita >0.1)
//										System.out.println(" "+s.gesto.nomeGesto+" "+s.azione.nomeAzione+ " "+s.similarita+ " "+s.contesto.elementiAmbientazione.get(0).word);

//									for (SrlElemento ee : listaElementiPerContesto)
//										System.out.println(" ################# "+ee.stampaElemento());
									
									listSimilaritaContestoAzione = calcolaSimilaritaSoloConAzione(listaElementiPerContesto, c, st);
									Collections.sort(listSimilaritaContestoAzione, new SimilaritaContestoAzioneComparator());
									for (SimilaritaContestoAzione s : listSimilaritaContestoAzione)
										System.out.println(""+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());
									
									miglioriSimilaritaContestoAzione.add(listSimilaritaContestoAzione.get(0));
									
//									System.out.println(" "+s.contesto.elementiAmbientazione.get(0).word+ " "+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());

								}
							}
//							listatemp = trovaElementiDaAggiungerePerConfrontoSRL(lr, relazioniTerminePerContesto, listatemp, ltempOrdinataPerTerm);
						}
						
						for (SrlElemento e : listatemp)
							System.out.println("° "+e.stampaElemento());
						
						listatemp.clear();
					}
					for (SimilaritaContestoAzione s : miglioriSimilaritaContestoAzione){
						System.out.println(""+s.azione.nomeAzione+ " "+s.similarita+" "+s.azione.stampaListaSimSrlElemento());
					}
					calcolaSimilaritaAzioneGesto(miglioriSimilaritaContestoAzione, contesti);
					
				}
				
				
			}
			catch (Exception e ){
				e.printStackTrace();
			}
				
			
			
			return null;
		}
		
		

		private static void caricaListaAzioni(String nomeFileAzioni) {
				try {
					BufferedReader readera = new BufferedReader(new FileReader(nomeFileAzioni));
					String line;

					while ((line = readera.readLine()) != null) {
						listaAzioni.add(line.trim());
						
					}
					readera.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		 private static void caricaListaOggetti(String nomeFileOggetti) {
				try {
					BufferedReader readera = new BufferedReader(new FileReader(nomeFileOggetti));
					String line;

					while ((line = readera.readLine()) != null) {
						listaOggetti.add(line.trim());
						
					}
					readera.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		  private static void caricaListaStrumenti(String nomeFileStrumenti) {
				try {
					BufferedReader readera = new BufferedReader(new FileReader(nomeFileStrumenti));
					String line;

					while ((line = readera.readLine()) != null) {
						listaStrumenti.add(line.trim());
						
					}
					readera.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		  private static void caricaListaPosizioni(String nomeFilePosizioni) {
				 try {
						BufferedReader readera = new BufferedReader(new FileReader(nomeFilePosizioni));
						String line;

						while ((line = readera.readLine()) != null) {
							listaPosizioni.add(line.trim());
							
						}
						readera.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				
			}
		  private static void caricaListaManiere(String nomeFileManiere) {
				 try {
						BufferedReader readera = new BufferedReader(new FileReader(nomeFileManiere));
						String line;

						while ((line = readera.readLine()) != null) {
							listaManiere.add(line.trim());
							
						}
						readera.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				
			}
	
		private static void scritturaCoppieDiviseSuFileUnico(String file1,
				String file2, String file3,String file4, String file5, String fileDaScrivere) {
			  try{
				  BufferedWriter writer = new BufferedWriter(new FileWriter(fileDaScrivere));
				  BufferedReader reader1 = new BufferedReader(new FileReader(file1));
				  BufferedReader reader2 = new BufferedReader(new FileReader(file2));
				  BufferedReader reader3 = new BufferedReader(new FileReader(file3));
				  BufferedReader reader4 = new BufferedReader(new FileReader(file4));
				  BufferedReader reader5 = new BufferedReader(new FileReader(file5));
				  List<String> listaa = new LinkedList<String>();
				  String line;
				  while ((line = reader1.readLine()) != null) {
						listaa.add(line);
					}
				  while ((line = reader2.readLine()) != null) {
						listaa.add(line);
					}
				  while ((line = reader3.readLine()) != null) {
						listaa.add(line);
					}
				  while ((line = reader4.readLine()) != null) {
						listaa.add(line);
					}
				  while ((line = reader5.readLine()) != null) {
						listaa.add(line);
					}
				  
				  for(String l : listaa){
					  writer.write(l+"\n");
					  writer.flush();
				  }
			  }catch (Exception e) {
					e.printStackTrace();
			  }

			
		}

		private static void scritturaSuFileCoppieDivise(List<SrlElemento> ltemp, String fileDaLeggere, String fileDaScrivere,String tipoP) {
//			if(tipoP.equals("")) {
//			try {
//				  List<String> paroleSpeciali = new LinkedList<String>();
//					paroleSpeciali.add("take");
//					paroleSpeciali.add("make");
//					paroleSpeciali.add("have");
//					paroleSpeciali.add("give");
//					paroleSpeciali.add("do");
//
//					paroleSpeciali.add("on");
//					paroleSpeciali.add("with");
//					paroleSpeciali.add("in");
//					
//					Map<String, String> paroleAgg = new TreeMap<String, String>();
//					String query = null;
//					ResultSet rs = null;
//					ResultSet rs1 = null;
//
//					Map<String, String> perQuery = new TreeMap<String, String>();
//					Map<String, String> parole = trovaParoleInFrasiConTakeMakeTipo(paroleSpeciali, ltemp, "azione");
//					for (String s : parole.keySet()){
//
//						boolean trovato = false;
//						List<String> lemmi = new LinkedList<String>();
//						query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE pos LIKE 'v' AND definition LIKE '% "+s+" %';";
//						rs = st.executeQuery(query); 
//						if (!rs.next()){
//							trovato = true;
//						}
//
//						if (!trovato){
//							while (rs.next()){
//								String parola = rs.getString("lemma");
//								paroleAgg.put(parola, "");
//								perQuery.put("('"+s+"', '"+parola.replace("'", "''")+"' , '"+rs.getString("definition").replace("'", "''")+"')", "" );
//							}
//						}
//						else{
//							query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE synsetid IN (SELECT synsetid FROM wordnet30.wordsxsensesxsynsets WHERE lemma LIKE '"+s+"')";
//							System.out.println(query);
//							rs1 = st.executeQuery(query); 
//							while (rs1.next()){
//								lemmi.add(rs1.getString("lemma").toLowerCase().replace("'", "''"));
////								System.err.println("aggiungo "+rs1.getString("lemma"));
//							}
//							
//							for (String l : lemmi){
//								query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE pos LIKE 'v' AND definition LIKE '% "+l+" %';";
//								rs1 = st.executeQuery(query); 
//								while (rs1.next()){
//									String parola = rs1.getString("lemma");
//									paroleAgg.put(parola, "");
//									perQuery.put("('"+s+"', '"+parola.replace("'", "''")+"' , '"+rs1.getString("definition").replace("'", "''")+"')", "" );
////									System.err.println("aggiungo "+"('"+s+"', '"+rs1.getString("lemma").replace("'", "''")+"' , '"+rs1.getString("definition").replace("'", "''")+"')");
//								}
//							}
//						}
//						
//					}
//					rs.close();
//					
//					String iniziale = "INSERT INTO `parolecontenuteindefinition`(`parola`, `contenutain`, `definizione`) VALUES\n";
//					BufferedWriter bw = new BufferedWriter(new FileWriter("queryparolecontenuteindefinition.sql"));
//					bw.write(iniziale);
//					bw.flush();
//					int numquery = 0;
//					for (String s : perQuery.keySet()){
//					    if ((numquery%299)==0){
//					    	bw.write(s+";\n");
//					    	bw.flush();
//					    	bw.write(iniziale);
//					    	bw.flush();
//					    }
//					    else{
//					    	bw.write(s+",\n");
//					    	bw.flush();
//					    }
//					    numquery++;
//					}
//					bw.close();
//					
//					
//					BufferedWriter writer = new BufferedWriter(new FileWriter(fileDaScrivere));
//					String file1 = "";
//					String file2 = "";
//					
//					if (app.animale.compareTo("elephant") == 0){
//						file1 = "Elephant_actions.txt";
//						file2 = "Elephant_gestures.txt";
//					}
//					if (app.animale.compareTo("elephantIS") == 0){
//						file1 = "ElephantIS_actions.txt";
//						file2 = "ElephantIS_gestures.txt";
//					}
//					else if (app.animale.compareTo("snail") == 0){
//						file1 = "Snail_actions.txt";
//						file2 = "Snail_gestures.txt";
//					}
//					else if (app.animale.compareTo("pocoyo08") == 0){
//						file1 = "Pocoyo08_actions.txt";
//						file2 = "Pocoyo08_gestures.txt";
//					}
//
//					BufferedReader readera = new BufferedReader(new FileReader(fileDaLeggere));
//					Map<String, String> mappa = new TreeMap<String, String>();
//					List<String> listaa = new LinkedList<String>();
//					List<String> listab = new LinkedList<String>();
//
//					List<String> listaattori = new LinkedList<String>();
//					List<String> listaazioniattori = new LinkedList<String>();
//					List<String> listascene = new LinkedList<String>();
//					List<String> listaoggetti = new LinkedList<String>();
//					
//					listaattori.addAll(app.attori.keySet());
//					for (Actor a : app.attori.values())
//						listaazioniattori.addAll(a.getAzioniEseguibili());
//					listascene.addAll(app.scene.keySet());
//					listaoggetti.addAll(app.oggetti.keySet());
//					
//					String line;
//					while ((line = readera.readLine()) != null) {
//						listaa.add(line);
//					}
//					
//					
//					
//					for (String s : paroleAgg.keySet()){
//						for (String a : listaa){
//							String tipoa = tipoParolaSelezionato(a);
//							mappa.put(s+"ï¿½ï¿½ï¿½v:::::"+a+"ï¿½ï¿½ï¿½"+tipoa, "");
//						}
//					}
//					
//					for (SrlElemento e : ltemp){
//						String tipo =tipoParolaPerPath(e); 
//						for (String a : listaa){
//							String tipoa = tipoParolaSelezionato(a);
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+a+"ï¿½ï¿½ï¿½"+tipoa, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+a+"ï¿½ï¿½ï¿½"+tipoa, ""); // aggiungo anche il verbo corrispondente a un nome
//							}
//							else 
//								mappa.put(e.word+":::::"+a+"ï¿½ï¿½ï¿½"+tipoa, "");
//						}
//
//						for (String b : listab){
//							String tipob = tipoParolaSelezionato(b);
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+b+"ï¿½ï¿½ï¿½"+tipob, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+b+"ï¿½ï¿½ï¿½"+tipob, ""); // aggiungo anche il verbo corrispondente a un nome
//							}
//							else
//								mappa.put(e.word+":::::"+b+"ï¿½ï¿½ï¿½"+tipob, "");
//						}
//						
//						// aggiungo attori, azioniattori e scene
//						for (String att : listaattori){
//							String tipoatt = "n";
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+att+"ï¿½ï¿½ï¿½"+tipoatt, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+att+"ï¿½ï¿½ï¿½"+tipoatt, "");
//							}
//							else
//								mappa.put(e.word+":::::"+att+"ï¿½ï¿½ï¿½"+tipoatt, "");
//						}
//
//						for (String azatt : listaazioniattori){
//							String tipoazatt = "v";
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+azatt+"ï¿½ï¿½ï¿½"+tipoazatt, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+azatt+"ï¿½ï¿½ï¿½"+tipoazatt, "");
//							}
//							else
//								mappa.put(e.word+":::::"+azatt+"ï¿½ï¿½ï¿½"+tipoazatt, "");
//						}
//
//						for (String sce : listascene){
//							String tiposce = "n";
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+sce+"ï¿½ï¿½ï¿½"+tiposce, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+sce+"ï¿½ï¿½ï¿½"+tiposce, "");
//							}
//							else
//								mappa.put(e.word+":::::"+sce+"ï¿½ï¿½ï¿½"+tiposce, "");
//						}
//
//						for (String ogg : listaoggetti){
//							String tipoogg = "n";
//							if (tipo != null){
//								mappa.put(e.word+"ï¿½ï¿½ï¿½"+tipo+":::::"+ogg+"ï¿½ï¿½ï¿½"+tipoogg, "");
//								if (tipo.compareTo("n") == 0)
//									mappa.put(e.word+"ï¿½ï¿½ï¿½v:::::"+ogg+"ï¿½ï¿½ï¿½"+tipoogg, "");
//							}
//							else
//								mappa.put(e.word+":::::"+ogg+"ï¿½ï¿½ï¿½"+tipoogg, "");
//						}
//						
//						// se ho delle parole composte
//						if (e.srlElementoAggiunto != null){
//							for (String a : listaa){
//								String tipoa = tipoParolaSelezionato(a);
//								mappa.put(e.srlElementoAggiunto.word+":::::"+a+"ï¿½ï¿½ï¿½"+tipoa, "");
//							}
//
//							for (String b : listab){
//								String tipob = tipoParolaSelezionato(b);
//								mappa.put(e.srlElementoAggiunto.word+":::::"+b+"ï¿½ï¿½ï¿½"+tipob, "");
//							}
//							
//							for (String att : listaattori){
//								mappa.put(e.srlElementoAggiunto.word+":::::"+att+"ï¿½ï¿½ï¿½n", "");
//							}
//
//							for (String azatt : listaazioniattori){
//								mappa.put(e.srlElementoAggiunto.word+":::::"+azatt+"ï¿½ï¿½ï¿½v", "");
//							}
//
//							for (String sce : listascene){
//								mappa.put(e.srlElementoAggiunto.word+":::::"+sce+"ï¿½ï¿½ï¿½n", "");
//							}
//
//						}
//					}
//					
//					for (String a : listaa){
//						String tipoa = tipoParolaSelezionato(a);
//						for (String b : listab){
//							String tipob = tipoParolaSelezionato(b);
//							if (tipoa.compareTo(tipob) == 0)
//								mappa.put(a+"ï¿½ï¿½ï¿½"+tipoa+":::::"+b+"ï¿½ï¿½ï¿½"+tipob, "");
//							else{
//								// qui devo:
//								// 1) prendere la parola che ha il tipo grammaticale piu basso, nella gerarchia (quindi parto da avverbi, aggettivi, nomi e verbi)
//								// 2) vedere se l'altra parola ha lo stesso tipo
//								// 3) se si, le scrivo entrambe, altrimenti scrivo come nel caso "identici"
//								String potenzialePosComune = trovaPosMinore(tipoa, tipob);
//								Boolean posConfrontabili = false;
//								if (tipoa.compareTo(potenzialePosComune) == 0)
//									posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(b, potenzialePosComune);
//								else if (tipob.compareTo(potenzialePosComune) == 0)
//									posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(a, potenzialePosComune);
//								if (posConfrontabili)
//									mappa.put(a+"ï¿½ï¿½ï¿½"+potenzialePosComune+":::::"+b+"ï¿½ï¿½ï¿½"+potenzialePosComune, "");
//								else
//									mappa.put(a+"ï¿½ï¿½ï¿½"+tipoa+":::::"+b+"ï¿½ï¿½ï¿½"+tipob, "");
//							}
//						}
//					}
//					
//					for (String s : mappa.keySet()){
//						String ar [] = s.split(":::::");
//						String ss1 [] = ar[0].split("ï¿½ï¿½ï¿½");
//						String ss2 [] = ar[1].split("ï¿½ï¿½ï¿½");
//						String prima = "";
//						String seconda = "";
//						if (ss1.length > 1 )
//							prima = ss1[0]+"#"+ss1[1];
//						else
//							prima = ss1[0];
//
//						if (ss2.length > 1 )
//							seconda = ss2[0]+"#"+ss2[1];
//						else
//							seconda = ss2[0];
//						
//						writer.write(prima+" "+seconda+"\n");
//						writer.flush();
//						System.out.println(ar[0]+" "+ar[1]);
//					}
//					writer.close();
//			  }catch (Exception e) {
//					e.printStackTrace();
//
//				}
//			}else{
				  try{
//***************************************NUOVO METODO
					BufferedWriter writer = new BufferedWriter(new FileWriter(fileDaScrivere));
					String file1 = fileDaLeggere;
				
					
								
					BufferedReader readera = new BufferedReader(new FileReader(file1));
					Map<String, String> mappa = new TreeMap<String, String>();
					List<String> listaa = new LinkedList<String>();
					
					String line; // dentro per ogni riga ci sono im iei verbi, ogg,strumenti permessi
					while ((line = readera.readLine()) != null) {
						listaa.add(line);
					}
					
					String prima = "";
					String seconda = "";
					for (SrlElemento e : ltemp){
						if(e != null ){
							prima  = e.word;
							for (String a : listaa){
								seconda = a;
//								writer.write(prima+"#"+tipo+" "+seconda+"#"+tipo+"\n");
								writer.write(prima+tipoP+" "+seconda+tipoP+"\n");
								writer.flush();
								System.out.println(prima+" "+seconda);}
							}
						}
					writer.close();
					
				} catch (Exception e) {
					e.printStackTrace();

				}
//		}
		    }
			
			
		

		private static void creaMappaOccurrencesSoggetto(List<RelazioneSrl> lsrl) {
			  for (RelazioneSrl r : lsrl){
				  if (!app.mappaRelazioniSoggetto.containsKey(r.predicate))
					  app.mappaRelazioniSoggetto.put(r.predicate, false);
				  
				  if (app.mappaRelazioniSoggetto.get(r.predicate) == false){
						List<RelazioneSrl> gv = new LinkedList<RelazioneSrl>();
						boolean trovato = false;
						trovato = verificaSeRelazioniSottoUnaRelazioneContengonoSoggetto(trovato, lsrl, r, gv);
						if (trovato)
							app.mappaRelazioniSoggetto.put(r.predicate, true);
				  }
			  }
		}

		private static void scriviCoppieSuFileIS(List<SrlElemento> ltemp, String nomeFileAzioni){
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(nomeFileAzioni));
					BufferedReader readera = new BufferedReader(new FileReader("ElephantIS_actions.txt"));
					BufferedReader readerb = new BufferedReader(new FileReader("ElephantIS_gestures.txt"));
					Map<String, String> mappa = new TreeMap<String, String>();
					List<String> listaa = new LinkedList<String>();
					List<String> listab = new LinkedList<String>();

					String line;
					while ((line = readera.readLine()) != null) {
						listaa.add(line);
					}
					
					String lineb;
					while ((lineb = readerb.readLine()) != null) {
						listab.add(lineb);
					}

					for (SrlElemento e : ltemp){
						for (String a : listaa)
							mappa.put(e.word+":::::"+a, "");

						for (String b : listab)
							mappa.put(e.word+":::::"+b, "");
						
						if (e.srlElementoAggiunto != null){
							for (String a : listaa)
								mappa.put(e.srlElementoAggiunto.word+":::::"+a, "");

							for (String b : listab)
								mappa.put(e.srlElementoAggiunto.word+":::::"+b, "");
						}
							
					}
					
					for (String a : listaa)
						for (String b : listab){
							mappa.put(a+":::::"+b, "");
						}
					
					for (String s : mappa.keySet()){
						String ar [] = s.split(":::::");
						writer.write(ar[0]+" "+ar[1]+"\n");
						writer.flush();
						System.out.println(ar[0]+" "+ar[1]);
					}
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		
		private static Map<String, String> trovaParoleInFrasiConTakeMakeTipo(List<String> paroleSpeciali, List<SrlElemento> ltemp, String tipo){
			Map<String, String> parole = new TreeMap<String, String>();
			List<String> numAzioneContesto = new LinkedList<String>();
			for (SrlElemento e : ltemp){
				for (String s : paroleSpeciali){
					if (e.word.compareTo(s) == 0 && e.tipo.compareTo(tipo) == 0)
						if (!numAzioneContesto.contains(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo()))
							numAzioneContesto.add(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo());
				}
			}
			
			for (SrlElemento e : ltemp){
				for (String s : numAzioneContesto){
					if (s.compareTo(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo()) == 0 && e.tipo.compareTo(tipo) == 0){
						if (e.pos.compareTo("DT") != 0 && e.pos.compareTo("IN") != 0 && e.pos.compareTo("PRP") != 0 && e.pos.compareTo("TO") != 0  && e.pos.compareTo("CC") != 0 && !paroleSpeciali.contains(e.word))	
							parole.put(e.word, "");
					}
				}
			}
			return parole;
		}
		
		private static Map<String, String> trovaParoleInFrasiConTakeMake(List<String> paroleSpeciali, List<SrlElemento> ltemp){
			Map<String, String> parole = new TreeMap<String, String>();
			List<String> numAzioneContesto = new LinkedList<String>();
			for (SrlElemento e : ltemp){
				for (String s : paroleSpeciali){
					if (e.word.compareTo(s) == 0)
						if (!numAzioneContesto.contains(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo()))
							numAzioneContesto.add(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo());
				}
			}
			
			for (SrlElemento e : ltemp){
				for (String s : numAzioneContesto){
					if (s.compareTo(e.getNumeroAzione()+"_"+e.getNumeroVersioneFrase()+"_"+e.getTipo()) == 0){
						if (e.pos.compareTo("DT") != 0 && e.pos.compareTo("IN") != 0 && e.pos.compareTo("PRP") != 0 && e.pos.compareTo("TO") != 0  && e.pos.compareTo("CC") != 0 && !paroleSpeciali.contains(e.word))	
							parole.put(e.word, "");
					}
				}
			}
			return parole;
		}
		
		private static boolean verificaSeRelazioniSottoUnaRelazioneContengonoSoggetto(boolean hoSogg, List<RelazioneSrl> listaRelazioni, RelazioneSrl r, List<RelazioneSrl> giavisitati){
			
			System.err.println(r.stampaRelazione());
			// prendo relazioni di livello superiore
//			if (r.livelloSuperioreSrl != null && !giavisitati.contains(r.livelloSuperioreSrl)){
//				giavisitati.add(r.livelloSuperioreSrl);
//				if (hoSogg == false)
//					hoSogg = verificaSeRelazioniSottoUnaRelazioneContengonoSoggetto(hoSogg, listaRelazioni, r.livelloSuperioreSrl, giavisitati);
//			}

			// cerco prima nella relazione
			for (SrlElemento e : r.listaOggetti){
				if (e.word.toLowerCase().compareTo(app.soggettoPrincipale) == 0 || e.word.toLowerCase().compareTo("it") == 0)
					return true;
			}
			
			// se non trovo, cerco in quelle sotto
			for (RelazioneSrl rel : listaRelazioni){
				if (!giavisitati.contains(rel) && rel.livelloSuperioreSrl != null && rel.livelloSuperioreSrl.equals(r)){
					giavisitati.add(rel);
						for (SrlElemento e : rel.listaOggetti){
							if (e.word.toLowerCase().compareTo(app.soggettoPrincipale) == 0 || e.word.toLowerCase().compareTo("it") == 0)
								return true;
						}
						if (hoSogg == false)
							hoSogg = verificaSeRelazioniSottoUnaRelazioneContengonoSoggetto(hoSogg, listaRelazioni, r, giavisitati);
				}
			}
			return hoSogg;
		}
		
		/*
		private static void caricaCountParole(){
			// RICORDA! NON CANCELLARE IL COUNT PRECEDENTE PERCHE' IMPIEGA MOLTO A CARICARE!!!
			try {
				Map<String, String> numParole = new TreeMap<String, String>();
				Map<String, String> parola = new HashMap<String, String>();
				String query = "SELECT * FROM `parolecontenuteindefinition`;";
				ResultSet rs;
				rs = st.executeQuery(query);
				
				while (rs.next()){
					if (!app.paroleDefinitionCount.containsKey(rs.getString("contenutain")))
						parola.put(rs.getString("contenutain"), "");
				}
				
				for (String s : parola.keySet()){
					query = "SELECT COUNT(*) from wordnet30.wordsxsensesxsynsets WHERE lemma LIKE '"+s.replace("'", "''")+"';";
					rs = st.executeQuery(query);
					while (rs.next()){
						numParole.put("('"+s.replace("'", "''")+"', "+rs.getInt("COUNT(*)")+")", "");
						System.out.println("('"+s.replace("'", "''")+"', "+rs.getInt("COUNT(*)")+")");
					}
				}
				
				String iniziale = "INSERT INTO `parolecontenuteindefinitioncount`(`parola`, `count`) VALUES\n";
				bw = new BufferedWriter(new FileWriter("queryparolecontenuteindefinitioncount.sql"));
				bw.write(iniziale);
				bw.flush();
				int numquery = 0;
				for (String s : numParole.keySet()){
				    if ((numquery%299)==0){
				    	bw.write(s+";\n");
				    	bw.flush();
				    	bw.write(iniziale);
				    	bw.flush();
				    }
				    else{
				    	bw.write(s+",\n");
				    	bw.flush();
				    }
				    numquery++;
				}
				bw.close();


			} catch (Exception e) {e.printStackTrace();}
		}*/

	    private static void scriviCoppieSuFileTipoParolaIS(List<SrlElemento> ltemp, String nomeFileAzioni){
			try {
				List<String> paroleSpeciali = new LinkedList<String>();
				paroleSpeciali.add("take");
				paroleSpeciali.add("make");
				paroleSpeciali.add("have");
				paroleSpeciali.add("give");
				paroleSpeciali.add("do");

				paroleSpeciali.add("on");
				paroleSpeciali.add("with");
				paroleSpeciali.add("in");
				
				Map<String, String> paroleAgg = new TreeMap<String, String>();
				String query = null;
				ResultSet rs = null;
				ResultSet rs1 = null;

				Map<String, String> perQuery = new TreeMap<String, String>();
				Map<String, String> parole = trovaParoleInFrasiConTakeMakeTipo(paroleSpeciali, ltemp, "azione");
				for (String s : parole.keySet()){

					boolean trovato = false;
					List<String> lemmi = new LinkedList<String>();
					query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE pos LIKE 'v' AND definition LIKE '% "+s+" %';";
					rs = st.executeQuery(query); 
					if (!rs.next()){
						trovato = true;
					}

					if (!trovato){
						while (rs.next()){
							String parola = rs.getString("lemma");
							paroleAgg.put(parola, "");
							perQuery.put("('"+s+"', '"+parola.replace("'", "''")+"' , '"+rs.getString("definition").replace("'", "''")+"')", "" );
						}
					}
					else{
						query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE synsetid IN (SELECT synsetid FROM wordnet30.wordsxsensesxsynsets WHERE lemma LIKE '"+s+"')";
						System.out.println(query);
						rs1 = st.executeQuery(query); 
						while (rs1.next()){
							lemmi.add(rs1.getString("lemma").toLowerCase().replace("'", "''"));
//							System.err.println("aggiungo "+rs1.getString("lemma"));
						}
						
						for (String l : lemmi){
							query = "SELECT * from wordnet30.wordsxsensesxsynsets WHERE pos LIKE 'v' AND definition LIKE '% "+l+" %';";
							rs1 = st.executeQuery(query); 
							while (rs1.next()){
								String parola = rs1.getString("lemma");
								paroleAgg.put(parola, "");
								perQuery.put("('"+s+"', '"+parola.replace("'", "''")+"' , '"+rs1.getString("definition").replace("'", "''")+"')", "" );
//								System.err.println("aggiungo "+"('"+s+"', '"+rs1.getString("lemma").replace("'", "''")+"' , '"+rs1.getString("definition").replace("'", "''")+"')");
							}
						}
					}
					
				}
				rs.close();
				
				String iniziale = "INSERT INTO `parolecontenuteindefinition`(`parola`, `contenutain`, `definizione`) VALUES\n";
				BufferedWriter bw = new BufferedWriter(new FileWriter("queryparolecontenuteindefinition.sql"));
				bw.write(iniziale);
				bw.flush();
				int numquery = 0;
				for (String s : perQuery.keySet()){
				    if ((numquery%299)==0){
				    	bw.write(s+";\n");
				    	bw.flush();
				    	bw.write(iniziale);
				    	bw.flush();
				    }
				    else{
				    	bw.write(s+",\n");
				    	bw.flush();
				    }
				    numquery++;
				}
				bw.close();
				
				
				BufferedWriter writer = new BufferedWriter(new FileWriter(nomeFileAzioni));
				String file1 = "";
				String file2 = "";
				
				if (app.animale.compareTo("elephant") == 0){
					file1 = "Elephant_actions.txt";
					file2 = "Elephant_gestures.txt";
				}
				if (app.animale.compareTo("elephantIS") == 0){
					file1 = "ElephantIS_actions.txt";
					file2 = "ElephantIS_gestures.txt";
				}
				else if (app.animale.compareTo("snail") == 0){
					file1 = "Snail_actions.txt";
					file2 = "Snail_gestures.txt";
				}
				else if (app.animale.compareTo("pocoyo08") == 0){
					file1 = "Pocoyo08_actions.txt";
					file2 = "Pocoyo08_gestures.txt";
				}

				BufferedReader readera = new BufferedReader(new FileReader(file1));
				BufferedReader readerb = new BufferedReader(new FileReader(file2));
				Map<String, String> mappa = new TreeMap<String, String>();
				List<String> listaa = new LinkedList<String>();
				List<String> listab = new LinkedList<String>();

				List<String> listaattori = new LinkedList<String>();
				List<String> listaazioniattori = new LinkedList<String>();
				List<String> listascene = new LinkedList<String>();
				List<String> listaoggetti = new LinkedList<String>();
				
				listaattori.addAll(app.attori.keySet());
				for (Actor a : app.attori.values())
					listaazioniattori.addAll(a.getAzioniEseguibili());
				listascene.addAll(app.scene.keySet());
				listaoggetti.addAll(app.oggetti.keySet());
				
				String line;
				while ((line = readera.readLine()) != null) {
					listaa.add(line);
				}
				
				String lineb;
				while ((lineb = readerb.readLine()) != null) {
					listab.add(lineb);
				}
				
				for (String s : paroleAgg.keySet()){
					for (String a : listaa){
						String tipoa = tipoParolaSelezionato(a);
						mappa.put(s+"°°°v:::::"+a+"°°°"+tipoa, "");
					}
				}
				
				for (SrlElemento e : ltemp){
					String tipo =tipoParolaPerPath(e); 
					for (String a : listaa){
						String tipoa = tipoParolaSelezionato(a);
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+a+"°°°"+tipoa, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+a+"°°°"+tipoa, ""); // aggiungo anche il verbo corrispondente a un nome
						}
						else 
							mappa.put(e.word+":::::"+a+"°°°"+tipoa, "");
					}

					for (String b : listab){
						String tipob = tipoParolaSelezionato(b);
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+b+"°°°"+tipob, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+b+"°°°"+tipob, ""); // aggiungo anche il verbo corrispondente a un nome
						}
						else
							mappa.put(e.word+":::::"+b+"°°°"+tipob, "");
					}
					
					// aggiungo attori, azioniattori e scene
					for (String att : listaattori){
						String tipoatt = "n";
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+att+"°°°"+tipoatt, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+att+"°°°"+tipoatt, "");
						}
						else
							mappa.put(e.word+":::::"+att+"°°°"+tipoatt, "");
					}

					for (String azatt : listaazioniattori){
						String tipoazatt = "v";
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+azatt+"°°°"+tipoazatt, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+azatt+"°°°"+tipoazatt, "");
						}
						else
							mappa.put(e.word+":::::"+azatt+"°°°"+tipoazatt, "");
					}

					for (String sce : listascene){
						String tiposce = "n";
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+sce+"°°°"+tiposce, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+sce+"°°°"+tiposce, "");
						}
						else
							mappa.put(e.word+":::::"+sce+"°°°"+tiposce, "");
					}

					for (String ogg : listaoggetti){
						String tipoogg = "n";
						if (tipo != null){
							mappa.put(e.word+"°°°"+tipo+":::::"+ogg+"°°°"+tipoogg, "");
							if (tipo.compareTo("n") == 0)
								mappa.put(e.word+"°°°v:::::"+ogg+"°°°"+tipoogg, "");
						}
						else
							mappa.put(e.word+":::::"+ogg+"°°°"+tipoogg, "");
					}
					
					// se ho delle parole composte
					if (e.srlElementoAggiunto != null){
						for (String a : listaa){
							String tipoa = tipoParolaSelezionato(a);
							mappa.put(e.srlElementoAggiunto.word+":::::"+a+"°°°"+tipoa, "");
						}

						for (String b : listab){
							String tipob = tipoParolaSelezionato(b);
							mappa.put(e.srlElementoAggiunto.word+":::::"+b+"°°°"+tipob, "");
						}
						
						for (String att : listaattori){
							mappa.put(e.srlElementoAggiunto.word+":::::"+att+"°°°n", "");
						}

						for (String azatt : listaazioniattori){
							mappa.put(e.srlElementoAggiunto.word+":::::"+azatt+"°°°v", "");
						}

						for (String sce : listascene){
							mappa.put(e.srlElementoAggiunto.word+":::::"+sce+"°°°n", "");
						}

					}
				}
				
				for (String a : listaa){
					String tipoa = tipoParolaSelezionato(a);
					for (String b : listab){
						String tipob = tipoParolaSelezionato(b);
						if (tipoa.compareTo(tipob) == 0)
							mappa.put(a+"°°°"+tipoa+":::::"+b+"°°°"+tipob, "");
						else{
							// qui devo:
							// 1) prendere la parola che ha il tipo grammaticale piu basso, nella gerarchia (quindi parto da avverbi, aggettivi, nomi e verbi)
							// 2) vedere se l'altra parola ha lo stesso tipo
							// 3) se si, le scrivo entrambe, altrimenti scrivo come nel caso "identici"
							String potenzialePosComune = trovaPosMinore(tipoa, tipob);
							Boolean posConfrontabili = false;
							if (tipoa.compareTo(potenzialePosComune) == 0)
								posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(b, potenzialePosComune);
							else if (tipob.compareTo(potenzialePosComune) == 0)
								posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(a, potenzialePosComune);
							if (posConfrontabili)
								mappa.put(a+"°°°"+potenzialePosComune+":::::"+b+"°°°"+potenzialePosComune, "");
							else
								mappa.put(a+"°°°"+tipoa+":::::"+b+"°°°"+tipob, "");
						}
					}
				}
				
				for (String s : mappa.keySet()){
					String ar [] = s.split(":::::");
					String ss1 [] = ar[0].split("°°°");
					String ss2 [] = ar[1].split("°°°");
					String prima = "";
					String seconda = "";
					if (ss1.length > 1 )
						prima = ss1[0]+"#"+ss1[1];
					else
						prima = ss1[0];

					if (ss2.length > 1 )
						seconda = ss2[0]+"#"+ss2[1];
					else
						seconda = ss2[0];
					
					writer.write(prima+" "+seconda+"\n");
					writer.flush();
					System.out.println(ar[0]+" "+ar[1]);
				}
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
//				try {
//					Thread.sleep(3000);
//				} catch (InterruptedException e1) {e1.printStackTrace();}
			}
	    }
		
	    /*
	    private static void inserisciRisultatiSimilaritaInDbIS(){
			try {
				String similarity = app.similarita;
				BufferedWriter wri = new BufferedWriter(new FileWriter("Query/query"+similarity+".sql"));
				if (!app.usaWordPos)
					wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"similarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
				else
					wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
				BufferedReader a = new BufferedReader(new FileReader("WordNetSimilarity/fileCoppie-"+similarity+".txt"));
				
				String query = "";
				if (!app.usaWordPos)
					query = "SHOW TABLES LIKE '"+similarity+"similarity"+app.animale+"';";
				else
					query = "SHOW TABLES LIKE '"+similarity+"possimilarity"+app.animale+"';";

				ResultSet rs = st.executeQuery(query); 
				if (!rs.next()){
//					System.out.println("CREATE TABLE IF NOT EXISTS `"+similarity+"similarity` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL;");
					if (!app.usaWordPos)
						st.executeUpdate("CREATE TABLE IF NOT EXISTS `"+similarity+"similarity"+app.animale+"` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL);");
					else
						st.executeUpdate("CREATE TABLE IF NOT EXISTS `"+similarity+"possimilarity"+app.animale+"` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL);");
				}
				else{
					if (!app.usaWordPos)
						st.executeUpdate("DELETE from `"+similarity+"similarity"+app.animale+"`");
					else
						st.executeUpdate("DELETE from `"+similarity+"possimilarity"+app.animale+"`");
				}
				rs.close();

				String line;
				int numquery = 0;
				while ((line = a.readLine()) != null) {
					line = line.replace("'", "''");
				    StringTokenizer stk = new StringTokenizer(line, "  ");
				    int count = StringUtils.countMatches(line, "#");
				    if (stk.hasMoreTokens() && count>=4){
					    String primoterminepiece = stk.nextToken().trim();
					    String secondoterminepiece = stk.nextToken().trim();
					    String valore = stk.nextToken().trim();
					    StringTokenizer primoterminetok = new StringTokenizer(primoterminepiece, "#");
					    StringTokenizer secondoterminetok = new StringTokenizer(secondoterminepiece, "#");
					    String primotermine = primoterminetok.nextToken();
					    String primoterminetipo = primoterminetok.nextToken();
					    String secondotermine = secondoterminetok.nextToken();
					    String secondoterminetipo = secondoterminetok.nextToken();
					    String sql = "";
					    if (!app.usaWordPos)
					    	sql="SELECT * FROM "+similarity+"similarity"+app.animale+" where (word1 = '"+primotermine+"' AND word2 = '"+secondotermine+"') OR (word2 = '"+primotermine+"' AND word1 = '"+secondotermine+"');";
					    else
					    	sql="SELECT * FROM "+similarity+"possimilarity"+app.animale+" where (word1 = '"+primotermine+"' AND word2 = '"+secondotermine+"') OR (word2 = '"+primotermine+"' AND word1 = '"+secondotermine+"');";
					    
						rs = st.executeQuery(sql);
						if (!rs.next()){
					    	if ((numquery%299)==0){
								wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"');\n");
								wri.flush();
								if (!app.usaWordPos)
									wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"similarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
								else
									wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
						    }
						    else{
//						    	if(rs.isLast())
//						    		wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"')");
//						    	else
						    		wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
								wri.flush();
						    	
						    }
							System.out.println("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
						    numquery++;
						}
//						qui qualcosa per eseguire wri
				    }
				}
			} catch (Exception e) { e.printStackTrace();}
		}	    
		*/
	    
    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
    
	
	private static void popolaParoleTemporali(){
		app.paroleInizioOContinuazione.add("start");
		app.paroleInizioOContinuazione.add("resume");
		app.paroleInizioOContinuazione.add("begin");
		app.paroleInizioOContinuazione.add("initiate");
		app.paroleInizioOContinuazione.add("commence");
		app.paroleInizioOContinuazione.add("keep");
		app.paroleInizioOContinuazione.add("continue");
		app.paroleTermine.add("stop");
		app.paroleTermine.add("finish");
		app.paroleTermine.add("complete");
		app.paroleTermine.add("end");
		app.paroleTermine.add("cease");
		app.paroleTermine.add("terminate");
		app.paroleTermine.add("halt");
	}

	private static StanfordParser trovaStanfordParser(String term, int order, List<StanfordParser> lista){
		for (StanfordParser sp: lista){
			if (sp.term.compareTo(term) == 0 && sp.order == order)
				return sp;
		}
		return null;
	}
	  
	
	private static int parseNonSegmentedLineByLine(
			CompletePipelineCMDLineOptions options, CompletePipeline pipeline,
			BufferedReader in, SentenceWriter writer) {
		return 0;
	}

	public static List<Tripla> verificaSeTriplaLivSupEDaEliminare(List<StanfordParser> listaParoleSotto, StanfordParser verboSopra, Tripla triplaEventualmDaEliminareLivSup, Tripla triplaEventualmDaEliminareLivInf){
		List<Tripla> tripleDaEliminare = new LinkedList<Tripla>();
		for (StanfordParser sp : listaParoleSotto){
			if (sp.pos.compareTo(verboSopra.pos) != 0){
				System.out.println("sono nel ciclo "+verboSopra.word);
				if (app.paroleInizioOContinuazione.contains(verboSopra.word) && !tripleDaEliminare.contains(triplaEventualmDaEliminareLivSup)){
					tripleDaEliminare.add(triplaEventualmDaEliminareLivSup);
//					System.out.println(" ... elimino ... "+triplaEventualmDaEliminareLivSup.stampa());
				}
				if (app.paroleTermine.contains(verboSopra.word) && !tripleDaEliminare.contains(triplaEventualmDaEliminareLivSup) && triplaEventualmDaEliminareLivInf != null && !tripleDaEliminare.contains(triplaEventualmDaEliminareLivInf)){
					tripleDaEliminare.add(triplaEventualmDaEliminareLivSup);
					tripleDaEliminare.add(triplaEventualmDaEliminareLivInf); // in questo caso, elimino anche liv inf ("stop doing", etc.)
//					System.out.println(" ... elimino sup ... "+triplaEventualmDaEliminareLivSup.stampa());
//					System.out.println(" ... elimino inf ... "+triplaEventualmDaEliminareLivInf.stampa());
				}
			}
		}
		return tripleDaEliminare;
	}
	
	public static List<Tripla> cercaSeAltreTripleDaEliminare(List<Tripla> listaTriple, List<Tripla> tripleDaEliminare){
		List<Tripla> listaTripleAggiuntePerEliminazione = new LinkedList<Tripla>();
		for (Tripla t : tripleDaEliminare){
			StanfordParser soggetto = null;
			StanfordParser predicato = null;
			String term = "";
			if (t instanceof TriplaSoggettoPredicato){
				soggetto = ((TriplaSoggettoPredicato) t).soggetto;
				predicato = ((TriplaSoggettoPredicato) t).predicato;
				term = predicato.term;
			}else if (t instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
				soggetto = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).soggetto;
				predicato = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).predicato;
				term = predicato.term;
			}else if (t instanceof TriplaSoggettoPredicatoOggetto){
				soggetto = ((TriplaSoggettoPredicatoOggetto) t).soggetto;
				predicato = ((TriplaSoggettoPredicatoOggetto) t).predicato;
				term = predicato.term;
			}
			for (Tripla t1 : listaTriple){
				StanfordParser soggetto1 = null;
				StanfordParser predicato1 = null;
				String term1 = "";
				if (t1 instanceof TriplaSoggettoPredicato){
					soggetto1 = ((TriplaSoggettoPredicato) t1).soggetto;
					predicato1 = ((TriplaSoggettoPredicato) t1).predicato;
					term1 = predicato1.term;
				}else if (t1 instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
					soggetto1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).soggetto;
					predicato1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).predicato;
					term1 = predicato1.term;
				}else if (t1 instanceof TriplaSoggettoPredicatoOggetto){
					soggetto1 = ((TriplaSoggettoPredicatoOggetto) t1).soggetto;
					predicato1 = ((TriplaSoggettoPredicatoOggetto) t1).predicato;
					term1 = predicato1.term;
				}
				
				if (predicato.equals(predicato1) && !listaTripleAggiuntePerEliminazione.contains(t1) && term.compareTo(term1) == 0 && predicato.order == predicato1.order){
					listaTripleAggiuntePerEliminazione.add(t1);
				}
			}
		}
		return listaTripleAggiuntePerEliminazione;
//		for (Tripla t : listaTripleAggiuntePerEliminazione)
//			if (!app.tripleDaEliminare.contains(t))
//				app.tripleDaEliminare.add(t);
	}
	
	public static List<Tripla> eliminaRelazioniConAvverbio(List<Tripla> listaTriple, List<Tripla> tripleDaEliminare){
		List<Tripla> listaTripleAggiuntePerEliminazione = new LinkedList<Tripla>();
		for (Tripla t1 : listaTriple){
			StanfordParser soggetto1 = null;
			StanfordParser predicato1 = null;
			String term1 = "";
			if (t1 instanceof TriplaSoggettoPredicato){
				soggetto1 = ((TriplaSoggettoPredicato) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicato) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
				soggetto1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoOggetto){
				soggetto1 = ((TriplaSoggettoPredicatoOggetto) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoOggetto) t1).predicato;
				term1 = predicato1.term;
			}
			
			if (predicato1 != null && predicato1.pos.compareTo("RB") == 0 && !listaTripleAggiuntePerEliminazione.contains(t1))
				listaTripleAggiuntePerEliminazione.add(t1);
		}
		return listaTripleAggiuntePerEliminazione;
//		for (Tripla t : listaTripleAggiuntePerEliminazione)
//			if (!app.tripleDaEliminare.contains(t))
//				app.tripleDaEliminare.add(t);
	}

	public static List<Tripla> eliminaRelazioniDoppie(List<Tripla> listaTriple, List<Tripla> tripleDaEliminare){
		List<Tripla> listaTripleGiaViste = new LinkedList<Tripla>();
		List<Tripla> listaTripleAggiuntePerEliminazione = new LinkedList<Tripla>();
		for (Tripla t1 : listaTriple){
			boolean giaVisto = false;
			StanfordParser soggetto1 = null;
			StanfordParser predicato1 = null;
			String term1 = "";
			if (t1 instanceof TriplaSoggettoPredicato){
				soggetto1 = ((TriplaSoggettoPredicato) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicato) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
				soggetto1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoOggetto){
				soggetto1 = ((TriplaSoggettoPredicatoOggetto) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoOggetto) t1).predicato;
				term1 = predicato1.term;
			}
			
			System.out.println("confronto le triple: "+t1.stampa());
			for (Tripla t : listaTripleGiaViste){
				StanfordParser soggetto = null;
				StanfordParser predicato = null;
				String term = "";
				if (t instanceof TriplaSoggettoPredicato){
					soggetto = ((TriplaSoggettoPredicato) t).soggetto;
					predicato = ((TriplaSoggettoPredicato) t).predicato;
					term = predicato.term;
				}else if (t instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
					soggetto = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).soggetto;
					predicato = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).predicato;
					term = predicato.term;
				}else if (t instanceof TriplaSoggettoPredicatoOggetto){
					soggetto = ((TriplaSoggettoPredicatoOggetto) t).soggetto;
					predicato = ((TriplaSoggettoPredicatoOggetto) t).predicato;
					term = predicato.term;
				}

				if (!t.equals(t1) && soggetto1.equals(soggetto) && predicato1.equals(predicato) && term1.compareTo(term) == 0 && predicato1.order == predicato.order){
					// ho un doppione
//					System.out.println("doppione "+t.stampa()+ " "+t1.stampa());
					listaTripleAggiuntePerEliminazione.add(t1);
					giaVisto = true;
				}
			}
			if (!giaVisto)
				listaTripleGiaViste.add(t1);
		}
		return listaTripleAggiuntePerEliminazione;
//		for (Tripla t : listaTripleAggiuntePerEliminazione)
//			if (!app.tripleDaEliminare.contains(t)){
//				app.tripleDaEliminare.add(t);
//				System.out.println("triple eliminazione vale "+t.stampa());
//			}
	}

	public static List<Tripla> eliminaRelazioniConDoppioSoggetto(List<Tripla> listaTriple, List<Tripla> tripleDaEliminare){
		// DA MODIFICARE!!!
		List<Tripla> listaTripleGiaViste = new LinkedList<Tripla>();
		List<Tripla> listaTripleAggiuntePerEliminazione = new LinkedList<Tripla>();
		for (Tripla t1 : listaTriple){
			boolean giaVisto = false;
			StanfordParser soggetto1 = null;
			StanfordParser predicato1 = null;
			String term1 = "";
			if (t1 instanceof TriplaSoggettoPredicato){
				soggetto1 = ((TriplaSoggettoPredicato) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicato) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
				soggetto1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoAvverbiopreposizione) t1).predicato;
				term1 = predicato1.term;
			}else if (t1 instanceof TriplaSoggettoPredicatoOggetto){
				soggetto1 = ((TriplaSoggettoPredicatoOggetto) t1).soggetto;
				predicato1 = ((TriplaSoggettoPredicatoOggetto) t1).predicato;
				term1 = predicato1.term;
			}
			
			System.out.println("confronto le triple: "+t1.stampa());
			for (Tripla t : listaTripleGiaViste){
				StanfordParser soggetto = null;
				StanfordParser predicato = null;
				String term = "";
				if (t instanceof TriplaSoggettoPredicato){
					soggetto = ((TriplaSoggettoPredicato) t).soggetto;
					predicato = ((TriplaSoggettoPredicato) t).predicato;
					term = predicato.term;
				}else if (t instanceof TriplaSoggettoPredicatoAvverbiopreposizione){
					soggetto = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).soggetto;
					predicato = ((TriplaSoggettoPredicatoAvverbiopreposizione) t).predicato;
					term = predicato.term;
				}else if (t instanceof TriplaSoggettoPredicatoOggetto){
					soggetto = ((TriplaSoggettoPredicatoOggetto) t).soggetto;
					predicato = ((TriplaSoggettoPredicatoOggetto) t).predicato;
					term = predicato.term;
				}

				if (!t.equals(t1) && soggetto1.word.compareTo(soggetto.word) == 0 && predicato1.equals(predicato) && term1.compareTo(term) == 0 && predicato1.order == predicato.order){
					// ho un doppione
//					System.out.println("doppione soggetto "+t.stampa()+ " "+t1.stampa());
					listaTripleAggiuntePerEliminazione.add(t1);
					giaVisto = true;
				}
			}
			if (!giaVisto)
				listaTripleGiaViste.add(t1);
		}
		return listaTripleAggiuntePerEliminazione;
	}

	private static List<SrlElemento> trovaElementiDaAggiungerePerConfronto(List<RelazioneSrl> listaRelazioni, Map<String, List<RelazioneSrl>> relazioniTerminePerContesto, List<SrlElemento> listatemp, List<SrlElemento> ltempOrdinataPerTerm){
		for (RelazioneSrl r : listaRelazioni){
			if (relazioniTerminePerContesto.containsKey(r.term) && !r.relation.contains("TMP")){ // elimino relaz temporali
				if (r.livelloSuperioreSrl != null){
					System.out.println(" -----------_______ r "+r.stampaRelazione()+ " "+r.livelloSuperioreSrl.stampaRelazione());
				}
				else
					System.out.println(" -----------_______ r "+r.stampaRelazione());
				for (SrlElemento e : r.listaOggetti){
					if (e.pos.compareTo("DT") != 0 && e.pos.compareTo("IN") != 0 && e.pos.compareTo("PRP") != 0){
						if (!listatemp.contains(e))
							listatemp.add(e);
					}
				}
				String parole[] = r.predicate.split(".");
//				listatemp.add(trovaSrlElementoDaParola(List<SrlElemento> lista, parole[0]));
			}
		}
		return listatemp;
	}
	
	private static Map<Integer, List<SrlElemento>> trovaElementiDaAggiungerePerConfrontoSRL(List<RelazioneSrl> listaRelazioni, Map<String, List<RelazioneSrl>> relazioniTerminePerContesto, List<SrlElemento> listatemp, List<SrlElemento> ltempOrdinataPerTerm){
		// prendo tutti gli elementi di una frase che saranno descritti nel copione e trovo gli elementi da aggiungere
		Map<Integer, List<SrlElemento>> mappa = new HashMap<Integer, List<SrlElemento>>();
		Integer num = 0;
		List<String> listaPredicati = new LinkedList<String>();
		for (RelazioneSrl r : listaRelazioni){
			if (srlContieneSoggetto(r.listaOggetti))
				if (!listaPredicati.contains(r.predicate))
					listaPredicati.add(r.predicate);
		}
		
		List<RelazioneSrl> relazioniGiaInserite = new LinkedList<RelazioneSrl>();
		for (String s : listaPredicati){
			List<SrlElemento> paroleInserite = new LinkedList<SrlElemento>();
//			System.out.println(" bla bla "+s);
			for (RelazioneSrl r : listaRelazioni){
				if (!r.relation.contains("TMP") && r.predicate.compareTo(s) == 0){
					System.out.println("*** "+r.stampaRelazione());
					List<RelazioneSrl> relazioniSotto = new LinkedList<RelazioneSrl>();
					trovaTutteRelazioniSottoUnaRelazione(listaRelazioni, r, s, relazioniSotto);
					
					for (RelazioneSrl rrr : relazioniSotto){
						if (!rrr.relation.contains("TMP") && !relazioniGiaInserite.contains(rrr) && !srlContieneSoggetto(rrr.listaOggetti)){
								if (relazioniTerminePerContesto.containsKey(r.term) ){ 
									System.out.println(" -----------_______ r "+r.stampaRelazione()+ " "+rrr.stampaRelazione());
									relazioniGiaInserite.add(rrr);

									for (SrlElemento e : rrr.listaOggetti){
										if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0  && e.pos.compareTo("PRP$") != 0 && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word) && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e), e) )){
											if (e.srlElementoAggiunto != null){
												if (!paroleInserite.contains(e.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(e.srlElementoAggiunto)){
													paroleInserite.add(e.srlElementoAggiunto);
													app.paroleInseriteGlobali.add(e.srlElementoAggiunto);
												}
												//////////////////////////////////////////////////////////////////////////////////////
												// IMPORTANTE : aggiungo anche entrambe le parole che compongono la parola composta //
												//////////////////////////////////////////////////////////////////////////////////////
												if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
													paroleInserite.add(e);
													app.paroleInseriteGlobali.add(e);
												}
											}
											else{
												if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
													paroleInserite.add(e);
													app.paroleInseriteGlobali.add(e);
												}
											}
										}
									}
									String parola = rrr.predicate.substring(0, (rrr.predicate.length() - 3));

									SrlElemento predicato = trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, rrr.term);
									System.out.println("       cerco proceed "+parola+" "+predicato + " ");
									if (!paroleInserite.contains(predicato) && predicato != null && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) ) && !app.paroleInseriteGlobali.contains(predicato)){
										paroleInserite.add(trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, r.term));
										app.paroleInseriteGlobali.add(predicato);
										
										// controllo: se predicato fa parte di parola composta, aggiungo al confronto anche l'elemento composto (e quello al quale lui Ã¨ collegato)
										if (predicato.srlElementoAggiunto != null && !paroleInserite.contains(predicato.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(predicato.srlElementoAggiunto)){
											paroleInserite.add(predicato.srlElementoAggiunto);
											app.paroleInseriteGlobali.add(predicato.srlElementoAggiunto);
											
											for (SrlElemento eee : predicato.srlElementoAggiunto.elementiCollegati)
//												if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee) && eee.pos.compareTo("TO") != 0 && eee.pos.compareTo("DT") != 0 && eee.pos.compareTo("IN") != 0 && eee.pos.compareTo("PRP") != 0 && eee.pos.compareTo("PRP$") != 0){
												if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee)){
													paroleInserite.add(eee);
													app.paroleInseriteGlobali.add(eee);
												}
										}
									}
								}
							}							
					}	
					
					if (!relazioniGiaInserite.contains(r) && !srlContieneSoggetto(r.listaOggetti)){
//						System.out.println(" +++++++++++_______ r "+r.stampaRelazione());
						relazioniGiaInserite.add(r);
						
						for (SrlElemento e : r.listaOggetti){
//							questa riga
							if (e.pos.compareTo("DT") != 0 && !(e.pos.compareTo("IN") == 0 && e.srlElementoAggiunto == null) && e.pos.compareTo("PRP") != 0 && e.pos.compareTo("PRP$") != 0  && e.pos.compareTo("TO") != 0 && !app.listaStopWords.contains(e.word)  && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, e) , e) )){
								if (e.srlElementoAggiunto != null){
									if (!paroleInserite.contains(e.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(e.srlElementoAggiunto)){
										paroleInserite.add(e.srlElementoAggiunto);
										app.paroleInseriteGlobali.add(e.srlElementoAggiunto);
									}
									//////////////////////////////////////////////////////////////////////////////////////
									// IMPORTANTE : aggiungo anche entrambe le parole che compongono la parola composta //
									//////////////////////////////////////////////////////////////////////////////////////
									if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
										paroleInserite.add(e);
										app.paroleInseriteGlobali.add(e);
									}
								}
								else{
									if (!paroleInserite.contains(e) && !app.paroleInseriteGlobali.contains(e)){
										paroleInserite.add(e);
										app.paroleInseriteGlobali.add(e);
									}
								}
							}
						}
//						String parole[] = r.predicate.split(".");
						String parola = r.predicate.substring(0, (r.predicate.length() - 3));
//						System.out.println(parola);

						SrlElemento predicato = trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, r.term);
						System.out.println("       cerco proceed "+parola+" "+predicato + " ");

						if (!paroleInserite.contains(trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, r.term)) && trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, r.term) != null && !(controllaSeElemSottoTakeMakeEObj(trovaTuttiElementiDirettiSottoUnElemento(ltempOrdinataPerTerm, predicato), predicato) )  && !app.paroleInseriteGlobali.contains(predicato)){
							paroleInserite.add(trovaSrlElementoDaParola(ltempOrdinataPerTerm, parola, r.term));
							app.paroleInseriteGlobali.add(predicato);
							
							// controllo: se predicato fa parte di parola composta, aggiungo al confronto anche l'elemento composto (e quello al quale lui Ã¨ collegato)
							if (predicato.srlElementoAggiunto != null && !paroleInserite.contains(predicato.srlElementoAggiunto) && !app.paroleInseriteGlobali.contains(predicato.srlElementoAggiunto)){
								paroleInserite.add(predicato.srlElementoAggiunto);
								app.paroleInseriteGlobali.add(predicato.srlElementoAggiunto);
								
								for (SrlElemento eee : predicato.srlElementoAggiunto.elementiCollegati)
//									if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee) && eee.pos.compareTo("TO") != 0 && eee.pos.compareTo("DT") != 0 && eee.pos.compareTo("IN") != 0 && eee.pos.compareTo("PRP") != 0 && eee.pos.compareTo("PRP$") != 0){
									if (!paroleInserite.contains(eee) && !app.paroleInseriteGlobali.contains(eee)){
										paroleInserite.add(eee);
										app.paroleInseriteGlobali.add(eee);
									}
							}
						}
					}
				}
			}
			mappa.put(num, paroleInserite);
			num++;
		}
		return mappa;
	}
	
	private static boolean controllaSeElemSottoTakeMakeEObj(List<SrlElemento> listaElementi, SrlElemento elemento){
		for (SrlElemento e : listaElementi)
			if (e.type.compareTo("OBJ") == 0 && (elemento.word.compareTo("take") == 0 || elemento.word.compareTo("make") == 0 || elemento.word.compareTo("have") == 0 || elemento.word.compareTo("give") == 0 || elemento.word.compareTo("do") == 0))
				return true;
		return false;
	}

	
	private static List<SrlElemento> trovaTuttiElementiDirettiSottoUnElemento(List<SrlElemento> listaElementi, SrlElemento elemento){
		List<SrlElemento> listaElementiSotto = new LinkedList<SrlElemento>();
		for (SrlElemento e : listaElementi){
			if (e.tree == elemento.order && e.term.compareTo(elemento.term) == 0){
				listaElementiSotto.add(e);
			}
		}
		return listaElementiSotto;
	}

	private static void trovaTutteRelazioniSottoUnaRelazione(List<RelazioneSrl> listaRelazioni, RelazioneSrl r, String predicato, List<RelazioneSrl> relazioniSotto){
		for (RelazioneSrl rel : listaRelazioni){
			if (rel.livelloSuperioreSrl != null && rel.livelloSuperioreSrl.equals(r)){
				if (!relazioniSotto.contains(rel)){
					relazioniSotto.add(rel);
				}
				trovaTutteRelazioniSottoUnaRelazione(listaRelazioni, rel, predicato, relazioniSotto);
			}
		}
	}

	
	private static boolean srlContieneSoggetto(List<SrlElemento> lista){
		System.out.println(lista.toString());
		for (SrlElemento e : lista)
			if (e.type.compareTo("SBJ") == 0)
				return true;
		return false;
	}
	
	private static SrlElemento trovaSrlElementoDaParola(List<SrlElemento> lista, String parola, String term){
		for (SrlElemento e : lista){
			if (e.word.compareTo(parola) == 0 && e.term.compareTo(term) == 0) // e.srlElementoAggiunto == null &&
				return e;
			else{
				// se non lo trova (caso strano!) prova a cercare eliminando la "s" finale (magari il srl e grammatical parser non funge bene)
				if (e.term.compareTo(term) == 0 && e.word.substring(e.word.length()-1, e.word.length()).compareTo("s") == 0 ){ // se finisco con "s"
					String nuovaword = e.word.substring(0, e.word.length()-1);
					if (nuovaword.compareTo(parola) == 0 && e.term.compareTo(term) == 0){
						System.out.println("sono nel substring "+nuovaword);
						e.word = nuovaword;
						return e;
					}
				}
			}
			
		}
		
		return null;
	}
	
	private static void stampaContesti(List<Contesto> contesti){
		for (Contesto c : contesti){
			System.out.println("Contesto "+c.internoEsterno);
			for (SrlElemento e : c.elementiAmbientazione)
				System.out.println(" - "+e.stampaElemento());
			for (SrlElemento e : c.elementiFraseIntroduttiva)
				System.out.println(" + "+e.stampaElemento());
			for (List<SrlElemento> l : c.elementiFrasiSuccessive.values())
				for (SrlElemento e : l)
					System.out.println(e.stampaElemento());
			
			for (RelazioneSrl r : c.relazioniFraseIntroduttiva)
				System.out.println("     *****   " +r.stampaRelazione());

			for (List<RelazioneSrl> l : c.relazioniFrasiSuccessive.values())
				for (RelazioneSrl r : l)
					System.out.println("         *****   " +r.stampaRelazione());
		}

	}
	
	
	private static void trovaFrasiContesto(List<Contesto> contesti, List<SrlElemento> ltempOrdinataPerTerm, List<RelazioneSrl> listarelazioni){
		List<String> l = new LinkedList<String>();

		for (Contesto c : contesti){
			int elemMappa = 0;
			Map<Integer, List<SrlElemento>> elementiFrasi = new HashMap<Integer, List<SrlElemento>>();
			List<SrlElemento> elemFrase = new LinkedList<SrlElemento>();
			boolean trovatoPrimaFrase = false;
			String termIniziale = c.elementiAmbientazione.get(0).term; // do per scontato che ci sia almeno un elemento ambientazione nella mia descriz.
			for (SrlElemento e : ltempOrdinataPerTerm){
				if (e.contesto != null && e.contesto.equals(c)){
					elemFrase.add(e);
					if (e.word.compareTo(".") == 0 && e.type.compareTo("P") == 0){
						List<SrlElemento> etemp = new LinkedList<SrlElemento>();
						etemp.addAll(elemFrase);
						elementiFrasi.put(elemMappa, etemp);
						elemFrase.clear();
						elemMappa++;
					}
				}
			}
			
			for (Integer i : elementiFrasi.keySet()){
				if (i == 0)
					c.elementiFraseIntroduttiva.addAll(elementiFrasi.get(i));
				else
					c.elementiFrasiSuccessive.put(c.elementiFrasiSuccessive.size(), elementiFrasi.get(i));
			}
			
			List<RelazioneSrl> listaRelFraseIntroduttiva = new LinkedList<RelazioneSrl>();
			List<RelazioneSrl> listaRelAltreFrasi = new LinkedList<RelazioneSrl>();
			for (RelazioneSrl r : listarelazioni){
//				System.out.println(c.elementiFrasiSuccessive);
				if (r.term.compareTo(c.elementiFrasiSuccessive.get(0).get(0).term) == 0){
					for (SrlElemento e : r.listaOggetti){
						if (c.elementiFraseIntroduttiva.contains(e)){
							if (!listaRelFraseIntroduttiva.contains(r)){
								listaRelFraseIntroduttiva.add(r);
							}
						}
						else{
							if (!listaRelAltreFrasi.contains(r))
								listaRelAltreFrasi.add(r);
						}
					}
				}
			}
			
			c.relazioniFraseIntroduttiva.addAll(listaRelFraseIntroduttiva);
			c.relazioniFrasiSuccessive.put(c.relazioniFrasiSuccessive.size(), listaRelAltreFrasi);
			
			
		}
		
		
	}
	
	private static List<Contesto> caricaContesti(List<SrlElemento> lista){
		List<Contesto> contesti = new LinkedList<Contesto>();
		String internoEsterno = "";
		List<SrlElemento> elementiAmbientazione = new LinkedList<SrlElemento>();
		String termPrec = "";
		
		for (SrlElemento e : lista){
			if ((e.word.compareTo("int.") == 0 || e.word.compareTo("ext.") == 0) && e.order == 1){
				// creo un nuovo elemento di contesto
				internoEsterno = e.word;
				termPrec = e.term;
			}
			else
				if (e.term.compareTo(termPrec) == 0){
					elementiAmbientazione.add(e);
				}
				else{
					if (elementiAmbientazione.size() > 0){
						// creo un nuovo COntesto
						Contesto c = new Contesto(internoEsterno, elementiAmbientazione);
						contesti.add(c);
						elementiAmbientazione.clear();
						e.contesto = c;
					}
					else{
						Contesto c = contesti.get(contesti.size()-1);
						e.contesto = c;
					}
//						// sono elementi delle frasi
				}
		}
		return contesti;
	}
	
	private static void aggiungi(List<SrlElemento> dubbi, List<SrlElemento> tuttiElementi){
		for (SrlElemento e : tuttiElementi)
			if (!dubbi.contains(e))
				dubbi.add(e);
	}

	private static Map<String, SrlElemento> caricoSrlElementiAggiunti(Statement st, Map<String, SrlElemento> mappaSrlElementi){
		Map<String, SrlElemento> mappaSrlElementiAggiunti = new HashMap<String, SrlElemento>();
		String query = "SELECT * FROM  `paroleunitewordnet"+app.animale+"` ;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String term = rs.getString("term");
				int order = rs.getInt("order");
				String wordunita = rs.getString("wordunita");
				SrlElemento e = null;
				if (!mappaSrlElementiAggiunti.containsKey(wordunita)){
					e = new SrlElemento(term, wordunita);
					mappaSrlElementiAggiunti.put(wordunita, e);
				}
				else
					e = mappaSrlElementiAggiunti.get(wordunita);
				if (mappaSrlElementi.containsKey(term+"_"+String.valueOf(order))){
					e.elementiCollegati.add(mappaSrlElementi.get(term+"_"+String.valueOf(order))); // collego srlelemento
					mappaSrlElementi.get(term+"_"+String.valueOf(order)).aggiungiSrlElementoAggiunto(e);
				}
			}
			return mappaSrlElementiAggiunti;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	
	private static Map<String, SrlElemento> caricoDefinitionsrl(Statement st, String tabelladefinitionsrlmsr){
		Map<String, SrlElemento> mappaelementifrase = new TreeMap<String, SrlElemento>();
		String query = "SELECT * FROM  `"+tabelladefinitionsrlmsr+"` ;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String term = rs.getString("term");
				int order = rs.getInt("order");
				String word = rs.getString("word");
				if (!controllaFormato(word)){
					word = word.replace("\"", "");
					word = word.replace(",", "");
					String originalword = rs.getString("originalword");
					String pos = rs.getString("pos");
					String type = rs.getString("type");
					int tree = rs.getInt("tree");
					SrlElemento e = new SrlElemento(term, order, word, originalword, pos, type, tree);
					e.setPosPath(tipoParolaPerPath(e));
					mappaelementifrase.put(term+"_"+String.valueOf(order), e);
				}
			}
			return mappaelementifrase;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	private static Map<String, StanfordParser> caricoDefinitionsrlStanford(Statement st, String tabelladefinitionsrlmsr){
		Map<String, StanfordParser> mappaelementifrase = new TreeMap<String, StanfordParser>();
		String query = "SELECT * FROM  `"+tabelladefinitionsrlmsr+"` ;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String term = rs.getString("term");
				int order = rs.getInt("order");
				String word = rs.getString("word");
				if (!controllaFormato(word)){
					word = word.replace("\"", "");
					word = word.replace(",", "");
					String originalword = rs.getString("originalword");
					String pos = rs.getString("pos");
					StanfordParser e = new StanfordParser(term, order, word, originalword, pos);
					mappaelementifrase.put(term+"_"+String.valueOf(order), e);
				}
			}
			return mappaelementifrase;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	private static RitornoSrlRelationMsr caricoSrlrelationmsr(Statement st, String tabellasrlrelationsmsr){
		List<RelazioneSrl> listarelazioni = new LinkedList<RelazioneSrl>();
		Map<Integer, RelazioneSrl> mapparelazionisrlfrase = new HashMap<Integer, RelazioneSrl>();
		String query = "SELECT * FROM  `"+tabellasrlrelationsmsr+"`;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				String term = rs.getString("term");
				String predicate = rs.getString("predicate");
				String object = rs.getString("object");
				String relation = rs.getString("relation");
				int idsrlfield = rs.getInt("idsrlfield");
				predicate = predicate.replace("\"", "");
				predicate = predicate.replace(",", "");
				predicate = predicate.replace("`", "");
	
				if (!controllaFormato(object)){
					RelazioneSrl e = new RelazioneSrl(term, predicate, object, relation, idsrlfield);
					listarelazioni.add(e);
					mapparelazionisrlfrase.put(idsrlfield, e);
				}
			}
			return new RitornoSrlRelationMsr(listarelazioni, mapparelazionisrlfrase);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	private static List<RelazioniStanfordParser> caricoSrlrelationmsrStanford(Statement st, String tabellasrlrelationsmsr, List<StanfordParser> lista){
		List<RelazioniStanfordParser> listarelazioni = new LinkedList<RelazioniStanfordParser>();
		String query = "SELECT * FROM  `"+tabellasrlrelationsmsr+"`;";
		ResultSet rs;
		try {
			rs = st.executeQuery(query);
			while (rs.next())
			{
				Integer term = rs.getInt("term");
				Integer pos1 = rs.getInt("pos1");
				Integer pos2 = rs.getInt("pos2");
				String relation = rs.getString("relation");
	    		StanfordParser sp1 = trovaStanfordParser(Integer.toString(term), pos1, lista);
	    		StanfordParser sp2 = trovaStanfordParser(Integer.toString(term), pos2, lista);
				RelazioniStanfordParser rsp = new RelazioniStanfordParser(sp1, sp2, relation);
				listarelazioni.add(rsp);
			}
			return listarelazioni;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
	}


	static class RitornoSrlRelationMsr{
		List<RelazioneSrl> listarelazioni = new LinkedList<RelazioneSrl>();
		Map<Integer, RelazioneSrl> mapparelazionisrlfrase = new HashMap<Integer, RelazioneSrl>();
    	
    	public RitornoSrlRelationMsr(List<RelazioneSrl> listarelazioni, Map<Integer, RelazioneSrl> mapparelazionisrlfrase){
    		this.listarelazioni = listarelazioni;
    		this.mapparelazionisrlfrase = mapparelazionisrlfrase;
    	}
    }

    static class CombinazioneLista{
		List<SrlElemento> listaSrlElemento = new LinkedList<SrlElemento>();
    	
//    	public CombinazioneLista(List<SrlElemento> listaSrlElemento){
//    		this.listaSrlElemento = listaSrlElemento;
//    	}
    	public void addSrlElemento(SrlElemento e){
    		listaSrlElemento.add(e);
    	}
    }


    private static Boolean controllaFormato(String s){
		List<String> elemCompare = new LinkedList<String>(Arrays.asList("\"", "!", "_", "]", "[", ",", ":", "'", "+", "-", "="));
		for (String carattere : elemCompare){
			if (s.compareTo(carattere) == 0)
				return true;
		}
		return false;
	}

	private static Boolean hasSubjectIS(Situazione s){
		for (ElementoSituazioneAzione elm : s.getSimElementiSituazioneAzione().values())
			for (SrlElemento e : elm.getElementi())
				if (e.type.compareTo("SBJ") == 0)
					return true;
		return false;
	}
    
	private static Boolean hasSubject(String term, List<SrlElemento> elementi){
		for (SrlElemento e : elementi)
			if (e.term.compareTo(term) == 0 && e.type.compareTo("SBJ") == 0)
				return true;
		return false;
	}
	
	private static Map<String, Integer> findAllSentences(List<SrlElemento> elementi){
		Map<String, Integer> mappa = new HashMap<String, Integer>();
		for (SrlElemento e : elementi){
			if (!mappa.containsKey(e.term))
				mappa.put(e.term, 1);
			else
				mappa.put(e.term, (mappa.get(e.term) +1));
		}
		return mappa;
	}
	
	private static Boolean hoNuovoSoggetto(SrlElemento e, List<SrlElemento> soggetti, String term){
		for (SrlElemento s : soggetti){
			if (s.word.compareTo(e.word) == 0 && s.term.compareTo(term) == 0)
				return false;
		}
		return true;
	}
	
	private static Boolean hoNuovoSoggetto(SrlElemento e, List<SrlElemento> soggetti){
		for (SrlElemento s : soggetti){
			if (s.word.compareTo(e.word) == 0)
				return false;
		}
		return true;
	}

	
	private static List<SrlElemento> elementiMatchantiInSrl(String s, String term, List<SrlElemento> elementi){
		List<SrlElemento> tuttiElementi = new LinkedList<SrlElemento>();
		for (SrlElemento e : elementi){
			if (e.word.compareTo(s) == 0 && e.term.compareTo(term) == 0)
				tuttiElementi.add(e);
		}
		return tuttiElementi;
	}

	private static void creaRicorsivamenteCombinazioni(List<SrlElemento> dubbi, int indiceMappa, Map<Integer, CombinazioneLista> mappa, int numElementiDaCombinare, int partenza, boolean sonoPrimo, int livAlQualeSono){
		int num = partenza;
//		for (int i = partenza; i < (dubbi.size() - (numElementiDaCombinare - livAlQualeSono)); i++){ // provo a eliminarlo
			if (numElementiDaCombinare != 1){
				while (num < dubbi.size()){
					CombinazioneLista c = null;
					if (sonoPrimo){
						c = new CombinazioneLista();
						mappa.put(indiceMappa, c);
					}
					else{
						c = mappa.get(indiceMappa);
					}
					if (numElementiDaCombinare != livAlQualeSono){
						int it = partenza;
						while (it <= dubbi.size()){
							c = mappa.get(indiceMappa);
							c.addSrlElemento(dubbi.get(num));
//							System.out.println(" c "+ c + " "+ dubbi.get(num).stampaElemento()+ " num "+num+ " partenza "+partenza+ " calcolo "+((dubbi.size() - numElementiDaCombinare +1 - (livAlQualeSono -1) + partenza)+ " liv "+livAlQualeSono+ " num combinare "+numElementiDaCombinare));
							creaRicorsivamenteCombinazioni(dubbi, indiceMappa, mappa, numElementiDaCombinare, it, false, (livAlQualeSono+1));
							indiceMappa++;
							c = new CombinazioneLista();
							mappa.put(indiceMappa, c);
							it ++;
							
						}
					}
					else{
//						System.out.println(" c "+ c + " "+ dubbi.get(num).stampaElemento()+ " num "+num+ " partenza "+partenza+ " calcolo "+((dubbi.size() - numElementiDaCombinare +1 - (livAlQualeSono -1) + partenza)+ " liv "+livAlQualeSono+ " num combinare "+numElementiDaCombinare));
						c.addSrlElemento(dubbi.get(num));
						return;
					}
					num ++;
				}
			}
			else{
				for (int i = partenza; i < (dubbi.size() - (numElementiDaCombinare - livAlQualeSono)); i++){
					CombinazioneLista c = null;
					c = new CombinazioneLista();
					mappa.put(indiceMappa, c);
					indiceMappa ++;

					c.addSrlElemento(dubbi.get(i));
//					System.out.println(" c "+ c + " "+ dubbi.get(i).stampaElemento());
				}
			}
				
//		}
		
		
		
		
	}

	private static Map<Integer, CombinazioneLista> ripulisciMappaCombinazioni(Map<Integer, CombinazioneLista> mappa, int numElementiDaCombinare){
		List<Integer> daRimuovere = new LinkedList<Integer>();
		for (Integer i : mappa.keySet()){
			if (mappa.get(i).listaSrlElemento.size() < numElementiDaCombinare){
				daRimuovere.add(i);
			}
			List<SrlElemento> listat = new LinkedList<SrlElemento>();
			for (SrlElemento e : mappa.get(i).listaSrlElemento){
				if (!listat.contains(e)){
					listat.add(e);
				}
				else
					daRimuovere.add(i);
			}
		}
//		System.out.println(daRimuovere);
		List<Integer> valori = new LinkedList<Integer>(mappa.keySet());
		for (Integer i : valori)
			if (daRimuovere.contains(i))
				mappa.remove(i);

		List<Integer> valori2 = new LinkedList<Integer>(mappa.keySet());
		
		Map<Integer, CombinazioneLista> mappaCopiata = new HashMap<Integer, CombinazioneLista>();
		mappaCopiata.putAll(mappa);
//		for (Integer i : valori2){
//			if (!mappaContieneGiaElementi (mappa, i))
//				mappaCopiata.put(i, mappa.get(i));
//		}
		return mappaCopiata;
	}
	
	private static boolean mappaContieneGiaElementi (Map<Integer, CombinazioneLista> mappa, Integer i){
		List<SrlElemento> lista = new LinkedList<SrlElemento>(); 
		lista.addAll(mappa.get(i).listaSrlElemento);
		
		for (Integer ii : mappa.keySet()){
			boolean trovato = true;
			if (ii != i){
				for (SrlElemento e : mappa.get(ii).listaSrlElemento){
					if (!lista.contains(e))
						trovato = false;
				}
			}
			if (trovato && ii != i)
				return true;
		}
		return false;
	}
	
    private static void controllaSeParoleSonoCollegateNellAlbero(List<SrlElemento> elementi, List<SrlElemento> elementiVisti, SrlElemento head){
    	if (head == null && elementi.size() > 0){
    		head = trovaHeadSeFraseParziale (elementi, elementi.get(0));
			elementiVisti.add(head);
//    		System.out.println("head "+head.stampaElemento());
    	}
    	for (SrlElemento e : elementi){
    		if (e.tree == head.order && !elementiVisti.contains(e)){ // lo devo ancora processare
    			elementiVisti.add(e);
//    			System.out.println("- "+e.stampaElemento());
    			controllaSeParoleSonoCollegateNellAlbero(elementi, elementiVisti, e);
    		}
    	}
    }
    
    private static void scriviCoppieSuFile(List<SrlElemento> ltemp, String nomeFileAzioni){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(nomeFileAzioni));
			BufferedReader readera = new BufferedReader(new FileReader("Elephant_actions.txt"));
			BufferedReader readerb = new BufferedReader(new FileReader("Elephant_gestures.txt"));
			Map<String, String> mappa = new TreeMap<String, String>();
			List<String> listaa = new LinkedList<String>();
			List<String> listab = new LinkedList<String>();

			String line;
			while ((line = readera.readLine()) != null) {
				listaa.add(line);
			}
			
			String lineb;
			while ((lineb = readerb.readLine()) != null) {
				listab.add(lineb);
			}

			for (SrlElemento e : ltemp){
				for (String a : listaa)
					mappa.put(e.word+":::::"+a, "");

				for (String b : listab)
					mappa.put(e.word+":::::"+b, "");
				
				if (e.srlElementoAggiunto != null){
					for (String a : listaa)
						mappa.put(e.srlElementoAggiunto.word+":::::"+a, "");

					for (String b : listab)
						mappa.put(e.srlElementoAggiunto.word+":::::"+b, "");
				}
					
			}
			
			for (String a : listaa)
				for (String b : listab){
					mappa.put(a+":::::"+b, "");
				}
			
			for (String s : mappa.keySet()){
				String ar [] = s.split(":::::");
				writer.write(ar[0]+" "+ar[1]+"\n");
				writer.flush();
				System.out.println(ar[0]+" "+ar[1]);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

    	
    	
    }

    private static void caricaAzioni(String nomeFileAzioni){
		try {
			BufferedReader readera = new BufferedReader(new FileReader(nomeFileAzioni));
			String line;

			while ((line = readera.readLine()) != null) {
				String [] splittato = line.split("#");
				if (splittato.length > 1)
					mappaAzioni.put(line, new Azione(splittato[0], splittato[1]));
				else{
					String tipo = tipoParolaSelezionato(line);
					mappaAzioni.put(line, new Azione(line, tipo));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    private static void caricaGesti(String nomeFileGesti){
		try {
			BufferedReader readera = new BufferedReader(new FileReader(nomeFileGesti));
			String line;
			
			while ((line = readera.readLine()) != null) {
				String [] splittato = line.split("#");
				if (splittato.length > 1)
					mappaGesti.put(line, new Gesto(splittato[0], splittato[1]));
				else{
					String tipo = tipoParolaSelezionato(line);
					mappaGesti.put(line, new Gesto(line, tipo));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    
	private static void inserisciRisultatiSimilaritaInDb(Statement st, String similarity){
		try {
			BufferedWriter wri = new BufferedWriter(new FileWriter("Query/query"+similarity+".sql"));
			if (!app.usaWordPos)
				wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"similarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
			else
				wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
			BufferedReader a = new BufferedReader(new FileReader("WordNetSimilarity/fileCoppie-"+similarity+".txt"));
			
			String query = "";
			if (!app.usaWordPos)
				query = "SHOW TABLES LIKE '"+similarity+"similarity"+app.animale+"';";
			else
				query = "SHOW TABLES LIKE '"+similarity+"possimilarity"+app.animale+"';";

			ResultSet rs = st.executeQuery(query); 
			if (!rs.next()){
//				System.out.println("CREATE TABLE IF NOT EXISTS `"+similarity+"similarity` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL;");
				if (!app.usaWordPos)
					st.executeUpdate("CREATE TABLE IF NOT EXISTS `"+similarity+"similarity"+app.animale+"` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL);");
				else
					st.executeUpdate("CREATE TABLE IF NOT EXISTS `"+similarity+"possimilarity"+app.animale+"` (`word1` varchar(30) NOT NULL, `word1tipo` varchar(1) NOT NULL, `word2` varchar(30) NOT NULL, `word2tipo` varchar(1) NOT NULL, `similarity` double NOT NULL);");
			}
			else{
				if (!app.usaWordPos)
					st.executeUpdate("DELETE from `"+similarity+"similarity"+app.animale+"`");
				else
					st.executeUpdate("DELETE from `"+similarity+"possimilarity"+app.animale+"`");
			}
			rs.close();

			String line;
			int numquery = 0;
			while ((line = a.readLine()) != null) {
				line = line.replace("'", "''");
			    StringTokenizer stk = new StringTokenizer(line, "  ");
			    int count = StringUtils.countMatches(line, "#");
			    if (stk.hasMoreTokens() && count>=4){
				    String primoterminepiece = stk.nextToken().trim();
				    String secondoterminepiece = stk.nextToken().trim();
				    String valore = stk.nextToken().trim();
				    StringTokenizer primoterminetok = new StringTokenizer(primoterminepiece, "#");
				    StringTokenizer secondoterminetok = new StringTokenizer(secondoterminepiece, "#");
				    String primotermine = primoterminetok.nextToken();
				    String primoterminetipo = primoterminetok.nextToken();
				    String secondotermine = secondoterminetok.nextToken();
				    String secondoterminetipo = secondoterminetok.nextToken();
				    String sql = "";
				    if (!app.usaWordPos)
				    	sql="SELECT * FROM "+similarity+"similarity"+app.animale+" where (word1 = '"+primotermine+"' AND word2 = '"+secondotermine+"') OR (word2 = '"+primotermine+"' AND word1 = '"+secondotermine+"');";
				    else
				    	sql="SELECT * FROM "+similarity+"possimilarity"+app.animale+" where (word1 = '"+primotermine+"' AND word2 = '"+secondotermine+"') OR (word2 = '"+primotermine+"' AND word1 = '"+secondotermine+"');";
				    
					rs = st.executeQuery(sql);
					if (!rs.next()){
				    	if ((numquery%299)==0){
							wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"');\n");
							wri.flush();
							if (!app.usaWordPos)
								wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"similarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
							else
								wri.write("INSERT INTO  `"+app.db+"`.`"+similarity+"possimilarity"+app.animale+"` (`word1` , `word1tipo` , `word2` , `word2tipo` , `similarity`) VALUES\n");
					    }
					    else{
							wri.write("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
							wri.flush();
					    	
					    }
						System.out.println("('"+primotermine+"',  '"+primoterminetipo+"', '"+secondotermine+"',  '"+secondoterminetipo+"', '"+valore+"'),\n");
					    numquery++;
					}
			    }
			}
		} catch (Exception e) { e.printStackTrace();}
	}

	

    private static void scriviCoppieSuFileTipoParola(List<SrlElemento> ltemp, String nomeFileAzioni){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(nomeFileAzioni));
			String file1 = "";
			String file2 = "";
			
			if (app.animale.compareTo("elephant") == 0){
				file1 = "Elephant_actions.txt";
				file2 = "Elephant_gestures.txt";
			}
			if (app.animale.compareTo("elephantIS") == 0){
				file1 = "ElephantIS_actions.txt";
				file2 = "ElephantIS_gestures.txt";
			}
			else if (app.animale.compareTo("snail") == 0){
				file1 = "Snail_actions.txt";
				file2 = "Snail_gestures.txt";
			}
			else if (app.animale.compareTo("pocoyo08") == 0){
				file1 = "Pocoyo08_actions.txt";
				file2 = "Pocoyo08_gestures.txt";
			}

			BufferedReader readera = new BufferedReader(new FileReader(file1));
			BufferedReader readerb = new BufferedReader(new FileReader(file2));
			Map<String, String> mappa = new TreeMap<String, String>();
			List<String> listaa = new LinkedList<String>();
			List<String> listab = new LinkedList<String>();

			String line;
			while ((line = readera.readLine()) != null) {
				listaa.add(line);
			}
			
			String lineb;
			while ((lineb = readerb.readLine()) != null) {
				listab.add(lineb);
			}

			for (SrlElemento e : ltemp){
				String tipo =tipoParolaPerPath(e); 
				for (String a : listaa){
					String tipoa = tipoParolaSelezionato(a);
//					System.err.println("pippo " +a + " "+tipoParolaSelezionato(a));
					if (tipo != null)
						mappa.put(e.word+"°°°"+tipo+":::::"+a+"°°°"+tipoa, "");
					else
						mappa.put(e.word+":::::"+a+"°°°"+tipoa, "");
				}

				for (String b : listab){
					String tipob = tipoParolaSelezionato(b);
					if (tipo != null)
						mappa.put(e.word+"°°°"+tipo+":::::"+b+"°°°"+tipob, "");
//					mappa.put(e.word+"°°°"+tipo+":::::"+b+"°°°v", "");
					else
						mappa.put(e.word+":::::"+b+"°°°"+tipob, "");
//					mappa.put(e.word+":::::"+b+"°°°v", "");
				}
				
				// se ho delle parole composte
				if (e.srlElementoAggiunto != null){
					for (String a : listaa){
						String tipoa = tipoParolaSelezionato(a);
						mappa.put(e.srlElementoAggiunto.word+":::::"+a+"°°°"+tipoa, "");
					}

					for (String b : listab){
						String tipob = tipoParolaSelezionato(b);
						mappa.put(e.srlElementoAggiunto.word+":::::"+b+"°°°"+tipob, "");
//						mappa.put(e.srlElementoAggiunto.word+":::::"+b+"°°°v", "");
					}
				}
			}
			
			for (String a : listaa){
				String tipoa = tipoParolaSelezionato(a);
				for (String b : listab){
					String tipob = tipoParolaSelezionato(b);
					if (tipoa.compareTo(tipob) == 0)
						mappa.put(a+"°°°"+tipoa+":::::"+b+"°°°"+tipob, "");
					else{
						// qui devo:
						// 1) prendere la parola che ha il tipo grammaticale piu basso, nella gerarchia (quindi parto da avverbi, aggettivi, nomi e verbi)
						// 2) vedere se l'altra parola ha lo stesso tipo
						// 3) se si le scrivo entrambe, altrimenti scrivo come nel caso "identici"
//						vjsalgkjasl
						String potenzialePosComune = trovaPosMinore(tipoa, tipob);
						Boolean posConfrontabili = false;
						if (tipoa.compareTo(potenzialePosComune) == 0)
							posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(b, potenzialePosComune);
						else if (tipob.compareTo(potenzialePosComune) == 0)
							posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(a, potenzialePosComune);
						if (posConfrontabili)
							mappa.put(a+"°°°"+potenzialePosComune+":::::"+b+"°°°"+potenzialePosComune, "");
						else
							mappa.put(a+"°°°"+tipoa+":::::"+b+"°°°"+tipob, "");
					}
				}
			}
			
			for (String s : mappa.keySet()){
				String ar [] = s.split(":::::");
				String ss1 [] = ar[0].split("°°°");
				String ss2 [] = ar[1].split("°°°");
				String prima = "";
				String seconda = "";
				if (ss1.length > 1 )
					prima = ss1[0]+"#"+ss1[1];
				else
					prima = ss1[0];

				if (ss2.length > 1 )
					seconda = ss2[0]+"#"+ss2[1];
				else
					seconda = ss2[0];
				
//				writer.write(ar[0]+" "+ar[1]+"\n");
				writer.write(prima+" "+seconda+"\n");
				writer.flush();
				System.out.println(ar[0]+" "+ar[1]);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private static String tipoParolaPerPath(SrlElemento e){
    	if (e.pos != null){
        	if (e.pos.compareTo("NN") == 0 || e.pos.compareTo("NNS") == 0 || e.pos.compareTo("NNP") == 0 || e.pos.compareTo("NNPS") == 0)
        		return "n";
        	if (e.pos.compareTo("JJ") == 0 || e.pos.compareTo("JJR") == 0 || e.pos.compareTo("JJS") == 0)
        		return "a";
        	if (e.pos.compareTo("VB") == 0 || e.pos.compareTo("VBD") == 0 || e.pos.compareTo("VBG") == 0 || e.pos.compareTo("VBN") == 0 || e.pos.compareTo("VBP") == 0 || e.pos.compareTo("VBZ") == 0)
        		return "v";
        	if (e.pos.compareTo("RB") == 0 || e.pos.compareTo("RP") == 0)
        		return "r";
    	}
    	return null;
    }

    private static String tipoParolaPerPath(String s){
    	if (s.compareTo("NN") == 0 || s.compareTo("NNS") == 0 || s.compareTo("NNP") == 0 || s.compareTo("NNPS") == 0)
    		return "n";
    	if (s.compareTo("JJ") == 0 || s.compareTo("JJR") == 0 || s.compareTo("JJS") == 0)
    		return "a";
    	if (s.compareTo("VB") == 0 || s.compareTo("VBD") == 0 || s.compareTo("VBG") == 0 || s.compareTo("VBN") == 0 || s.compareTo("VBP") == 0 || s.compareTo("VBZ") == 0)
    		return "v";
    	if (s.compareTo("RB") == 0 || s.compareTo("RP") == 0)
    		return "r";
    	return null;
    }

    
    private static void clearAllAzioniGestiSrlElementi(List<SrlElemento> lista){
		for (Gesto g : app.mappaGesti.values()){
			g.listaSimAzione.clear();
			g.listaSimComplexGestoAzione.clear();
			g.listaSimSrlElem.clear();
			g.simFin.clear();
		}
		
		for (Azione a : app.mappaAzioni.values()){
			a.listaSimComplexGestoAzione.clear();
			a.listaSimGesto.clear();
			a.listaSimSrlElem.clear();
			a.listaSimContesto.clear();
			a.simFin.clear();
		}
		
		for (SrlElemento e : lista){
			e.listaSimAzione.clear();
			e.listaSimGesto.clear();
		}
	}
	
    private static Double calcolaSimilaritaConCombinazioni(String parola1, String parola2){
		double simMax = 0.0;
		double sim = 0.0;
		
		if (app.mappapath.containsKey(parola1+"°v___"+parola2+"°v"))
			sim = app.mappapath.get(parola1+"°v___"+parola2+"°v").path;
		else if (app.mappapath.containsKey(parola2+"°v___"+parola1+"°v"))
			sim = app.mappapath.get(parola2+"°v___"+parola1+"°v").path;
		if (sim > simMax)
			simMax = sim;

		if (app.mappapath.containsKey(parola1+"°n___"+parola2+"°n"))
			sim = app.mappapath.get(parola1+"°n___"+parola2+"°n").path;
		else if (app.mappapath.containsKey(parola2+"°n___"+parola1+"°n"))
			sim = app.mappapath.get(parola2+"°n___"+parola1+"°n").path;
		if (sim > simMax)
			simMax = sim;
		
		if (app.mappapath.containsKey(parola1+"°a___"+parola2+"°a"))
			sim = app.mappapath.get(parola1+"°a___"+parola2+"°a").path;
		else if (app.mappapath.containsKey(parola2+"°a___"+parola1+"°a"))
			sim = app.mappapath.get(parola2+"°a___"+parola1+"°a").path;
		if (sim > simMax)
			simMax = sim;
		
		if (app.mappapath.containsKey(parola1+"°v___"+parola2+"°n"))
			sim = app.mappapath.get(parola1+"°v___"+parola2+"°n").path;
		else if (app.mappapath.containsKey(parola2+"°v___"+parola1+"°n"))
			sim = app.mappapath.get(parola2+"°v___"+parola1+"°n").path;
		if (sim > simMax)
			simMax = sim;
		
		if (app.mappapath.containsKey(parola1+"°v___"+parola2+"°a"))
			sim = app.mappapath.get(parola1+"°v___"+parola2+"°a").path;
		else if (app.mappapath.containsKey(parola2+"°v___"+parola1+"°a"))
			sim = app.mappapath.get(parola2+"°v___"+parola1+"°a").path;
		if (sim > simMax)
			simMax = sim;

		if (app.mappapath.containsKey(parola1+"°n___"+parola2+"°v"))
			sim = app.mappapath.get(parola1+"°n___"+parola2+"°v").path;
		else if (app.mappapath.containsKey(parola2+"°n___"+parola1+"°v"))
			sim = app.mappapath.get(parola2+"°n___"+parola1+"°v").path;
		if (sim > simMax)
			simMax = sim;

		if (app.mappapath.containsKey(parola1+"°n___"+parola2+"°a"))
			sim = app.mappapath.get(parola1+"°n___"+parola2+"°a").path;
		if (app.mappapath.containsKey(parola2+"°n___"+parola1+"°a"))
			sim = app.mappapath.get(parola2+"°n___"+parola1+"°a").path;
		if (sim > simMax)
			simMax = sim;

		if (app.mappapath.containsKey(parola1+"°a___"+parola2+"°v"))
			sim = app.mappapath.get(parola1+"°a___"+parola2+"°v").path;
		if (app.mappapath.containsKey(parola2+"°a___"+parola1+"°v"))
			sim = app.mappapath.get(parola2+"°a___"+parola1+"°v").path;
		if (sim > simMax)
			simMax = sim;

		if (app.mappapath.containsKey(parola1+"°a___"+parola2+"°n"))
			sim = app.mappapath.get(parola1+"°a___"+parola2+"°n").path;
		if (app.mappapath.containsKey(parola2+"°a___"+parola1+"°n"))
			sim = app.mappapath.get(parola2+"°a___"+parola1+"°n").path;
		if (sim > simMax)
			simMax = sim;

		return simMax;
    }
    
    private static Double calcolaSimilaritaConCombinazioniResV(String parola1, String parola2){
		double simMax = 0.0;
		double sim = 0.0;
		
		if (app.mappares.containsKey(parola1+"°v___"+parola2+"°v"))
			sim = app.mappares.get(parola1+"°v___"+parola2+"°v").path;
		else if (app.mappares.containsKey(parola2+"°v___"+parola1+"°v"))
			sim = app.mappares.get(parola2+"°v___"+parola1+"°v").path;
		if (sim > simMax)
			simMax = sim;

		return simMax;
    }
    private static Double calcolaSimilaritaConCombinazioniResO(String parola1, String parola2){
  		double simMax = 0.0;
  		double sim = 0.0;
  		
  		if (app.mappares.containsKey(parola1+"°n___"+parola2+"°n"))
  			sim = app.mappares.get(parola1+"°n___"+parola2+"°n").path;
  		else if (app.mappares.containsKey(parola2+"°n___"+parola1+"°n"))
  			sim = app.mappares.get(parola2+"°n___"+parola1+"°n").path;
  		if (sim > simMax)
  			simMax = sim;

  		return simMax;
      }
    private static Double calcolaSimilaritaConCombinazioniResS(String parola1, String parola2){
  		double simMax = 0.0;
  		double sim = 0.0;
  		
  		if (app.mappares.containsKey(parola1+"°n___"+parola2+"°n"))
  			sim = app.mappares.get(parola1+"°n___"+parola2+"°n").path;
  		else if (app.mappares.containsKey(parola2+"°n___"+parola1+"°n"))
  			sim = app.mappares.get(parola2+"°n___"+parola1+"°n").path;
  		if (sim > simMax)
  			simMax = sim;

  		return simMax;
      }
    
	private static Double calcolaSimilaritaTraParole(String parola1, String parola2, String tipo1, String tipo2){
		if (parola1.compareTo(parola2) == 0)
			return 1.0;
		
		Double simMax = 0.0;
		Double sim = 0.0;
		
		if (tipo1 == null && tipo2 == null){
			
			if (app.mappapath.containsKey(parola1+"___"+parola2))				
				sim = app.mappapath.get(parola1+"___"+parola2).path;
			else if (app.mappapath.containsKey(parola2+"___"+parola1))
				sim = app.mappapath.get(parola2+"___"+parola1).path;
			if (sim > simMax)
				simMax = sim;

			sim = calcolaSimilaritaConCombinazioni(parola1, parola2);
			if (sim > simMax)
				simMax = sim;

		}
		else if (app.mappapath.containsKey(parola1+"°"+tipo1+"___"+parola2+"°"+tipo2))
			return app.mappapath.get(parola1+"°"+tipo1+"___"+parola2+"°"+tipo2).path;
		else if (app.mappapath.containsKey(parola2+"°"+tipo2+"___"+parola1+"°"+tipo1))
			return app.mappapath.get(parola2+"°"+tipo2+"___"+parola1+"°"+tipo1).path;
		return simMax;
	}
	
	private static Double calcolaSimilaritaTraParoleRes(String parola1, String parola2, String tipo1, String tipo2){
		
		
		Double simMax = 0.0;
		Double sim = 0.0;
		
		if (tipo1 == null && tipo2 == null){
			
			if (app.mappares.containsKey(parola1+"___"+parola2))				
				sim = app.mappares.get(parola1+"___"+parola2).path;
			else if (app.mappares.containsKey(parola2+"___"+parola1))
				sim = app.mappares.get(parola2+"___"+parola1).path;
			if (sim > simMax)
				simMax = sim;

			sim = calcolaSimilaritaConCombinazioni(parola1, parola2);
			if (sim > simMax)
				simMax = sim;

		}
		else if (app.mappares.containsKey(parola1+"°"+tipo1+"___"+parola2+"°"+tipo2))
			return app.mappares.get(parola1+"°"+tipo1+"___"+parola2+"°"+tipo2).path;
		else if (app.mappares.containsKey(parola2+"°"+tipo2+"___"+parola1+"°"+tipo1))
			return app.mappares.get(parola2+"°"+tipo2+"___"+parola1+"°"+tipo1).path;
		return simMax;
	}
    
	private static List<SimilaritaGestoAzione> calcolaSimilarita(List<SrlElemento> ltemp, Contesto contesto){
		List<SimilaritaGestoAzione> listaSimilarita = new LinkedList<SimilaritaGestoAzione>();
		Double simElemGesto = 0.0;
		Double simElemAzione = 0.0;
		for (SrlElemento e : ltemp){
			for (Azione a : mappaAzioni.values()){
				double sim = 0.0;
				if (app.mappapath.containsKey(e.word+"___"+a.nomeAzione))
					sim = app.mappapath.get(e.word+"___"+a.nomeAzione).path;
				else if (app.mappapath.containsKey(a.nomeAzione+"___"+e.word))
					sim = app.mappapath.get(a.nomeAzione+"___"+e.word).path;
				
				SimilaritaAzioneSrlElemento simAzSrlElem = new SimilaritaAzioneSrlElemento(a, e, sim);
				e.aggiungiSimilaritaAzione(simAzSrlElem);
				a.aggiungiSimilaritaSrlElemento(simAzSrlElem);
			}

			for (Gesto g : mappaGesti.values()){
				double sim = 0.0;
				if (app.mappapath.containsKey(e.word+"___"+g.nomeGesto))
					sim = app.mappapath.get(e.word+"___"+g.nomeGesto).path;
				else if (app.mappapath.containsKey(g.nomeGesto+"___"+e.word))
					sim = app.mappapath.get(g.nomeGesto+"___"+e.word).path;
				
				SimilaritaGestoSrlElemento simGeSrlElem = new SimilaritaGestoSrlElemento(g, e, sim);
				e.aggiungiSimilaritaGesto(simGeSrlElem);
				g.aggiungiSimilaritaSrlElemento(simGeSrlElem);
			}
		}
		
		for (Azione a : mappaAzioni.values()){
			for (Gesto g : mappaGesti.values()){
				double sim = 0.0;
				if (app.mappapath.containsKey(a.nomeAzione+"___"+g.nomeGesto))
					sim = app.mappapath.get(a.nomeAzione+"___"+g.nomeGesto).path;
				else if (app.mappapath.containsKey(g.nomeGesto+"___"+a.nomeAzione))
					sim = app.mappapath.get(g.nomeGesto+"___"+a.nomeAzione).path;
				
				SimilaritaGestoAzione simGeAz = new SimilaritaGestoAzione(g, a, sim);
				simGeAz.setContesto(contesto);
				g.aggiungiSimilaritaAzione(simGeAz);
				a.aggiungiSimilaritaGesto(simGeAz);
			}
		}
		
		// calcolo vero e proprio

		for (SrlElemento e : ltemp){
			for (Gesto g : mappaGesti.values()){
				double simGesto = g.getSimSrlElemento(e).similarita;
				for (Azione a : mappaAzioni.values()){
					double simAzione = a.getSimSrlElemento(e).similarita;
					double simGestoAzione = g.getSimAzione(a).similarita;
					double simComplex = (simGesto + simAzione + simGestoAzione) / 3;
//					double simComplex = (simGesto + simAzione ) / 2; // debug per vedere come cambiano le cose
//					double simComplex = (simAzione ); // debug per vedere come cambiano le cose
//					System.out.println("sim "+e.word+ " "+g.nomeGesto+ " "+a.nomeAzione+ " "+simComplex);
					SimilaritaComplexGestoAzione simCompl = new SimilaritaComplexGestoAzione(g, a, simComplex, contesto);
					g.aggiungiSimilaritaComplexGestoAzione(simCompl);
					a.aggiungiSimilaritaComplexGestoAzione(simCompl);
				}
			}
		}
		
		for (Gesto g : mappaGesti.values()){
			g.calcolaSimConOgniAzione();
		}
		
		for (Gesto g : mappaGesti.values()){
			for (SimilaritaGestoAzione s : g.simFin){
				s.setContesto(contesto);
				listaSimilarita.add(s);
			}
		}
		return listaSimilarita;
	}

	private static void calcolaSimilaritaAzioneGesto(List<SimilaritaContestoAzione> miglioriSimilaritaContestoAzione, List<Contesto> contesti){
		List<SimilaritaGestoAzione> listaSimilarita = new LinkedList<SimilaritaGestoAzione>();
		Double simElemGesto = 0.0;
		Double simElemAzione = 0.0;
		
		for (SimilaritaContestoAzione s : miglioriSimilaritaContestoAzione){
			// calcolo similarita' per ogni coppia gesto azione
			for (Gesto g : mappaGesti.values()){
				// devo controllare che azione e gesto abbiano stesso POS!
				String gestopos = "";
				String azionepos = "";
				
				if (s.azione.azionepos.compareTo(g.gestopos) == 0){
					gestopos = g.gestopos;
					azionepos = s.azione.azionepos;
				}
				else{
					String potenzialePosComune = trovaPosMinore(s.azione.azionepos, g.gestopos);
					Boolean posConfrontabili = false;
					if (s.azione.azionepos.compareTo(potenzialePosComune) == 0)
						posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(g.nomeGesto, potenzialePosComune);
					else if (g.gestopos.compareTo(potenzialePosComune) == 0)
						posConfrontabili = cercaSeParolaEDiUnDeterminatoPos(s.azione.nomeAzione, potenzialePosComune);
					if (posConfrontabili){
						gestopos = potenzialePosComune;
						azionepos = potenzialePosComune;
					}
					else{
						gestopos = g.gestopos;
						azionepos = s.azione.azionepos;
					}
				}
				
				double sim = 0.0;
				if (s.azione.azionepos != null && g.gestopos != null){
//					System.err.println(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos);
					if (app.mappapath.containsKey(s.azione.nomeAzione+"°"+azionepos+"___"+g.nomeGesto+"°"+gestopos))
						sim = app.mappapath.get(s.azione.nomeAzione+"°"+azionepos+"___"+g.nomeGesto+"°"+gestopos).path;
					else if (app.mappapath.containsKey(g.nomeGesto+"°"+gestopos+"___"+s.azione.nomeAzione+"°"+azionepos))
						sim = app.mappapath.get(g.nomeGesto+"°"+gestopos+"___"+s.azione.nomeAzione+"°"+azionepos).path;
				}
				else{
					sim = trovaMaxSimSeNonHoPosGesto(g, s.azione, g.gestopos, s.azione.azionepos);
					}
				
//				
//				if (app.mappapath.containsKey(s.azione.nomeAzione+"___"+g.nomeGesto))
//					sim = app.mappapath.get(s.azione.nomeAzione+"___"+g.nomeGesto).path;
//				else if (app.mappapath.containsKey(g.nomeGesto+"___"+s.azione.nomeAzione))
//					sim = app.mappapath.get(g.nomeGesto+"___"+s.azione.nomeAzione).path;
				
				SimilaritaGestoAzione simGeAz = new SimilaritaGestoAzione(g, s.azione, sim);
				simGeAz.setContesto(s.contesto);
				g.aggiungiSimilaritaAzione(simGeAz);
				s.azione.aggiungiSimilaritaGesto(simGeAz);
				System.err.println(" sim "+s.azione.nomeAzione + " "+g.nomeGesto+ " "+simGeAz.similarita);
			}
			
			// trovo il gesto migliore per una azione, a patto che in quel contesto non sia gia' presente un altro gesto
			s.contesto.listaSimGestoAzione.addAll(s.azione.listaSimGesto);
//			for (SimilaritaGestoAzione ga : s.azione.listaSimGesto)
//				System.out.println(ga.azione.nomeAzione+ " "+ga.gesto.nomeGesto+ " "+ga.contesto.elementiAmbientazione.get(0).word + " "+ga.similarita);
			
			
		}
		for (Contesto c : contesti){
			Collections.sort(c.listaSimGestoAzione, new SimilaritaGestoAzioneComparator());
			List<String> azioniContesto = new LinkedList<String>();
//			for (SimilaritaGestoAzione s : c.listaSimGestoAzione){
//				if (!azioniContesto.contains(s.azione.nomeAzione))
//					azioniContesto.add(s.azione.nomeAzione);
//			}
			
			for (SimilaritaGestoAzione s : c.listaSimGestoAzione){
				if (!c.mappaGestoAzione.containsKey(s.gesto.nomeGesto) && !azioniContesto.contains(s.azione.nomeAzione)){
					c.mappaGestoAzione.put(s.gesto.nomeGesto, s.azione.nomeAzione);
					c.mappaAzioneGesto.put(s.azione.nomeAzione, s.gesto.nomeGesto);
					azioniContesto.add(s.azione.nomeAzione);
					System.out.println("contesto "+c.elementiAmbientazione.get(0).word+ " "+s.azione.nomeAzione+ " "+s.gesto.nomeGesto+ " "+s.similarita);
				}
				
			}
		}
		
		
		
		
		
		
		/*
		for (SrlElemento e : ltemp){
			for (Gesto g : mappaGesti.values()){
				double simGesto = g.getSimSrlElemento(e).similarita;
				for (Azione a : mappaAzioni.values()){
					double simAzione = a.getSimSrlElemento(e).similarita;
					double simGestoAzione = g.getSimAzione(a).similarita;
					double simComplex = (simGesto + simAzione + simGestoAzione) / 3;
//					double simComplex = (simGesto + simAzione ) / 2; // debug per vedere come cambiano le cose
//					double simComplex = (simAzione ); // debug per vedere come cambiano le cose
//					System.out.println("sim "+e.word+ " "+g.nomeGesto+ " "+a.nomeAzione+ " "+simComplex);
					SimilaritaComplexGestoAzione simCompl = new SimilaritaComplexGestoAzione(g, a, simComplex, contesto);
					g.aggiungiSimilaritaComplexGestoAzione(simCompl);
					a.aggiungiSimilaritaComplexGestoAzione(simCompl);
				}
			}
		}
		
		for (Gesto g : mappaGesti.values()){
			g.calcolaSimConOgniAzione();
		}
		
		for (Gesto g : mappaGesti.values()){
			for (SimilaritaGestoAzione s : g.simFin){
				s.setContesto(contesto);
				listaSimilarita.add(s);
			}
		}
		return listaSimilarita;*/
	}


	private static List<SimilaritaContestoAzione> calcolaSimilaritaSoloConAzione(List<SrlElemento> ltemp, Contesto contesto, Statement st){
		List<SimilaritaContestoAzione> listaSimilarita = new LinkedList<SimilaritaContestoAzione>();
		Double simElemGesto = 0.0;
		Double simElemAzione = 0.0;
		for (SrlElemento e : ltemp){
			if (app.similarita.compareTo("bing") != 0){
				for (Azione a : mappaAzioni.values()){
					double sim = 0.0;
					if (!app.usaWordPos){
						if (app.mappapath.containsKey(e.word+"___"+a.nomeAzione))
							sim = app.mappapath.get(e.word+"___"+a.nomeAzione).path;
						else if (app.mappapath.containsKey(a.nomeAzione+"___"+e.word))
							sim = app.mappapath.get(a.nomeAzione+"___"+e.word).path;
					}
					else{
						if (e.posPath != null && a.azionepos != null){
//							System.err.println(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos);
							if (app.mappapath.containsKey(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos))
								sim = app.mappapath.get(e.word+"°"+e.posPath+"___"+a.nomeAzione+"°"+a.azionepos).path;
							else if (app.mappapath.containsKey(a.nomeAzione+"°"+a.azionepos+"___"+e.word+"°"+e.posPath))
								sim = app.mappapath.get(a.nomeAzione+"°"+a.azionepos+"___"+e.word+"°"+e.posPath).path;
						}
						else if (e.posPath == null && a.azionepos != null){
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
							
//								if (app.mappapath.containsKey(e.word+"___"+a.nomeAzione+"°"+a.azionepos))
//									sim = app.mappapath.get(e.word+"___"+a.nomeAzione+"°"+a.azionepos).path;
//								else if (app.mappapath.containsKey(a.nomeAzione+"°"+a.azionepos+"___"+e.word))
//									sim = app.mappapath.get(a.nomeAzione+"°"+a.azionepos+"___"+e.word).path;
							}
						else if (e.posPath != null && a.azionepos == null){
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
//							if (app.mappapath.containsKey(e.word+"°"+e.posPath+"___"+a.nomeAzione))
//								sim = app.mappapath.get(e.word+"°"+e.posPath+"___"+a.nomeAzione).path;
//							else if (app.mappapath.containsKey(a.nomeAzione+"___"+e.word+"°"+e.posPath))
//								sim = app.mappapath.get(a.nomeAzione+"___"+e.word+"°"+e.posPath).path;
						}
						else
							sim = trovaMaxSimSeNonHoPos(e, a, e.posPath, a.azionepos);
					}
//					System.out.println("+++++++++++++++++++++ "+a.nomeAzione+ " "+e.word+ " "+sim);
					
					if (sim > 1 && app.normalizzaLesk)
						sim = 1;
					
					SimilaritaAzioneSrlElemento simAzSrlElem = new SimilaritaAzioneSrlElemento(a, e, sim);
					e.aggiungiSimilaritaAzione(simAzSrlElem);
					a.aggiungiSimilaritaSrlElemento(simAzSrlElem);
					
					SimilaritaContestoAzione simCxAzione = new SimilaritaContestoAzione(contesto, a, sim);
					a.aggiungiSimilaritaContesto(simCxAzione);
//					System.out.println(a.nomeAzione+" "+e.word+ " "+sim);
				}
			}
			else{
				for (Azione a : mappaAzioni.values()){
					double sim = 0.0;
					String choice = a.nomeAzione;
					String problem = e.word;
					
					boolean trovato = false;
//					if (app.mappapath.containsKey(e.word+"___"+a.nomeAzione)){
//						trovato = true;
//						sim = app.mappapath.get(e.word+"___"+a.nomeAzione).path;
//					}
					if (app.mappapath.containsKey(choice+"___"+problem)){
						trovato = true;
						sim = app.mappapath.get(choice+"___"+problem).path;
					}
					
					if (!trovato){
						long occurrencesBoth = 0;
						long occurrencesChoice = 0;
						long occurrencesProblem = 0;
//						String prima = a.nomeAzione;
//						String seconda = e.word;

						boolean trovatoBoth = false;
						boolean trovatoChoice = false;
						boolean trovatoProblem = false;

						if (app.mappaoccurrences.containsKey(e.word+"___"+a.nomeAzione)){
							occurrencesBoth =mappaoccurrences.get(e.word+"___"+a.nomeAzione).occurrences;
							trovatoBoth = true;
						}
						if (app.mappaoccurrences.containsKey(a.nomeAzione+"___"+e.word)){
							occurrencesBoth =mappaoccurrences.get(a.nomeAzione+"___"+e.word).occurrences;
							trovatoBoth = true;
						}
						
						if (app.mappaoccurrences.containsKey(a.nomeAzione+"___")){
							occurrencesChoice =mappaoccurrences.get(a.nomeAzione+"___").occurrences;
							trovatoChoice = true;
						}
						
						if (app.mappaoccurrences.containsKey(e.word+"___")){
							occurrencesProblem =mappaoccurrences.get(e.word+"___").occurrences;
							trovatoProblem = true;
						}
						
						// calcolo il count per coppie di parole e singole parole
						if (!trovatoBoth){
						     BingSearchExample b = new BingSearchExample(choice, problem);
						     try {
								String response = b.getBing();
								occurrencesBoth = json(response);
								// inserirle in  db
								// inserirle in java
								String queryInsert = "INSERT INTO `bingcounts`(`word1`, `word2`, `count`) VALUES ('"+choice+"','"+problem+"',"+occurrencesBoth+")";
								st.executeUpdate(queryInsert);
								
								OccurrencesParoleString p = null;
								OccurrencesParoleString p1 = null;
								p = new OccurrencesParoleString(choice, problem, occurrencesBoth);
								p1 = new OccurrencesParoleString(choice, problem, occurrencesBoth);
								
								app.mappaoccurrences.put(choice+"___"+problem, p);
								app.mappaoccurrences.put(problem+"___"+choice, p1); 
								
							} catch (Exception ee) {ee.printStackTrace();}
						}
						
						if (!trovatoChoice){
						     BingSearchExample b = new BingSearchExample(choice, "");
						     try {
								String response = b.getBing();
								occurrencesChoice = json(response);
								// inserirle in  db
								// inserirle in java
								String queryInsert = "INSERT INTO `bingcounts`(`word1`, `word2`, `count`) VALUES ('"+choice+"','',"+occurrencesChoice+")";
								st.executeUpdate(queryInsert);
								
								OccurrencesParoleString p = null;
								OccurrencesParoleString p1 = null;
								p = new OccurrencesParoleString(choice, "", occurrencesChoice);
								p1 = new OccurrencesParoleString(choice, "", occurrencesChoice);
								
								app.mappaoccurrences.put(choice+"___", p);
								app.mappaoccurrences.put("___"+choice, p1); 
								
							} catch (Exception ee) {ee.printStackTrace();}
							
						}
							
						if (!trovatoProblem){
						     BingSearchExample b = new BingSearchExample("", problem);
						     try {
								String response = b.getBing();
								occurrencesProblem = json(response);
								// inserirle in  db
								// inserirle in java
								String queryInsert = "INSERT INTO `bingcounts`(`word1`, `word2`, `count`) VALUES ('','"+problem+"',"+occurrencesProblem+")";
								st.executeUpdate(queryInsert);
								
								OccurrencesParoleString p = null;
								OccurrencesParoleString p1 = null;
								p = new OccurrencesParoleString("", problem, occurrencesProblem);
								p1 = new OccurrencesParoleString("", problem, occurrencesProblem);
								
								app.mappaoccurrences.put(problem+"___", p);
								app.mappaoccurrences.put("___"+problem, p1); 
								
							} catch (Exception ee) {ee.printStackTrace();}
						}
						
						// calcolo PMI
						sim = (double)occurrencesBoth / (double)occurrencesChoice;
						
						PathParoleString p = null;
						PathParoleString p1 = null;
						
						p = new PathParoleString(choice, problem, sim);
						app.mappapath.put(choice+"___"+problem, p);
						
						String queryInsert = "INSERT INTO `bingsimilarity`(`choice`, `problem`, `similarity`) VALUES ('"+choice+"','"+problem+"',"+sim+")";
						try {
							st.executeUpdate(queryInsert);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}

					}

					SimilaritaAzioneSrlElemento simAzSrlElem = new SimilaritaAzioneSrlElemento(a, e, sim);
					e.aggiungiSimilaritaAzione(simAzSrlElem);
					a.aggiungiSimilaritaSrlElemento(simAzSrlElem);
					
					SimilaritaContestoAzione simCxAzione = new SimilaritaContestoAzione(contesto, a, sim);
					a.aggiungiSimilaritaContesto(simCxAzione);
//					System.out.println(a.nomeAzione+" "+e.word+ " "+sim);
				}
				
			}
			
			
			
			
			
		}
		
		// calcolo vero e proprio

//		for (SrlElemento e : ltemp){
//			for (Azione a : mappaAzioni.values()){
//				double simAzione = a.getSimSrlElemento(e).similarita;
//				double simComplex = simAzione;
//				
//			}
//		}
		
		for (Azione a : mappaAzioni.values()){
			a.calcolaMediaSimSrlElementi();
			for (SimilaritaContestoAzione s : a.simFin)
				listaSimilarita.add(s);
		}
		
		return listaSimilarita;
	}
	
	private static double trovaSimilaritaDaPath(SrlElemento e, Azione a, String elementoPosPath, String azionePosPath){
		Double sim = 0.0;
		if (app.mappapath.containsKey(e.word+"°"+elementoPosPath+"___"+a.nomeAzione+"°"+azionePosPath))
			sim = app.mappapath.get(e.word+"°"+elementoPosPath+"___"+a.nomeAzione+"°"+azionePosPath).path;
		else if (app.mappapath.containsKey(a.nomeAzione+"°"+azionePosPath+"___"+e.word+"°"+elementoPosPath))
			sim = app.mappapath.get(a.nomeAzione+"°"+azionePosPath+"___"+e.word+"°"+elementoPosPath).path;
		return sim;
	}

	private static double trovaSimilaritaDaPath(String parola1, String parola2, String parola1pos, String parola2pos){
		Double sim = 0.0;
		if (app.mappapath.containsKey(parola1+"°"+parola1pos+"___"+parola2+"°"+parola2pos))
			sim = app.mappapath.get(parola1+"°"+parola1pos+"___"+parola2+"°"+parola2pos).path;
		else if (app.mappapath.containsKey(parola2+"°"+parola2pos+"___"+parola1+"°"+parola1pos))
			sim = app.mappapath.get(parola2+"°"+parola2pos+"___"+parola1+"°"+parola1pos).path;
		return sim;
	}

	private static PathParoleString cercaPath(String parola1, String parola2, String parola1pos, String parola2pos){
		if (app.mappapath.containsKey(parola1+"°"+parola1pos+"___"+parola2+"°"+parola2pos))
			return app.mappapath.get(parola1+"°"+parola1pos+"___"+parola2+"°"+parola2pos);
		else if (app.mappapath.containsKey(parola2+"°"+parola2pos+"___"+parola1+"°"+parola1pos))
			return app.mappapath.get(parola2+"°"+parola2pos+"___"+parola1+"°"+parola1pos);
		return null;
	}

	private static PathParoleString cercaPath(String parola1, String parola2){
		if (app.mappapath.containsKey(parola1+"___"+parola2))
			return app.mappapath.get(parola1+"___"+parola2);
		else if (app.mappapath.containsKey(parola2+"___"+parola1))
			return app.mappapath.get(parola2+"___"+parola1);
		return null;
	}
	
	private static double trovaMaxSimSeNonHoPos(SrlElemento e, Azione a, String elementoPosPath, String azionePosPath){
		Double sim = 0.0;
		Double simMax = 0.0;
		List<String> wordnetPos = new LinkedList<String>();
		wordnetPos.add("n");
		wordnetPos.add("v");
		wordnetPos.add("a");
		wordnetPos.add("s");
		wordnetPos.add("r");
		
		if (elementoPosPath == null && azionePosPath != null){
			for (String s : wordnetPos){
				sim = trovaSimilaritaDaPath(e, a, s, azionePosPath);
				if (sim > simMax)
					simMax = sim;
			}
		}
		else if (elementoPosPath != null && azionePosPath == null){
			for (String s : wordnetPos){
				sim = trovaSimilaritaDaPath(e, a, elementoPosPath, s);
				if (sim > simMax)
					simMax = sim;
			}
		}
		else {
			for (String s1 : wordnetPos){
				for (String s2 : wordnetPos){
					sim = trovaSimilaritaDaPath(e, a, s1, s2);
					if (sim > simMax)
						simMax = sim;
				}
			}
		}
			
		
		return simMax;
		
	}

	private static double trovaSimilaritaDaPathGesto(Gesto g, Azione a, String gestoPosPath, String azionePosPath){
		Double sim = 0.0;
		if (app.mappapath.containsKey(g.nomeGesto+"°"+gestoPosPath+"___"+a.nomeAzione+"°"+azionePosPath))
			sim = app.mappapath.get(g.nomeGesto+"°"+gestoPosPath+"___"+a.nomeAzione+"°"+azionePosPath).path;
		else if (app.mappapath.containsKey(a.nomeAzione+"°"+azionePosPath+"___"+g.nomeGesto+"°"+gestoPosPath))
			sim = app.mappapath.get(a.nomeAzione+"°"+azionePosPath+"___"+g.nomeGesto+"°"+gestoPosPath).path;
		return sim;
	}

	
	private static double trovaMaxSimSeNonHoPosGesto(Gesto g, Azione a, String gestoPosPath, String azionePosPath){
		Double sim = 0.0;
		Double simMax = 0.0;
		List<String> wordnetPos = new LinkedList<String>();
		wordnetPos.add("n");
		wordnetPos.add("v");
		wordnetPos.add("a");
		wordnetPos.add("s");
		wordnetPos.add("r");
		
//		if (azionePosPath == null){
//			for (String s : wordnetPos){
//				sim = trovaSimilaritaDaPathGesto(g, a, gestoPosPath, s);
//				if (sim > simMax)
//					simMax = sim;
//			}
//		}
//		
//		
//		
		if (gestoPosPath == null && azionePosPath != null){
			for (String s : wordnetPos){
				sim = trovaSimilaritaDaPathGesto(g, a, s, azionePosPath);
				if (sim > simMax)
					simMax = sim;
			}
		}
		else if (gestoPosPath != null && azionePosPath == null){
			for (String s : wordnetPos){
				sim = trovaSimilaritaDaPathGesto(g, a, gestoPosPath, s);
				if (sim > simMax)
					simMax = sim;
			}
		}
		else {
			for (String s1 : wordnetPos){
				for (String s2 : wordnetPos){
					sim = trovaSimilaritaDaPathGesto(g, a, s1, s2);
					if (sim > simMax)
						simMax = sim;
				}
			}
		}
			

		return simMax;
		
	}

	
	private static List<SimilaritaGestoAzione> trovaMatchPerOgniAzione(List<SimilaritaGestoAzione> listSimilaritaGestoAzione){
		List<SimilaritaGestoAzione> simFin = new LinkedList<SimilaritaGestoAzione>();
		List<Azione> azioniGiaProcessate = new LinkedList<Azione>();
		for (SimilaritaGestoAzione s : listSimilaritaGestoAzione){
			if (!azioniGiaProcessate.contains(s.azione)){
				simFin.add(s);
				azioniGiaProcessate.add(s.azione);
			}
		}
		return simFin;
	}
	
	private static void preprocessaAlberoSrl(List<RelazioneSrl> listarelazionidbr1){
		// QUI DEVO CREARE L'ALBERO --> VEDO A CHE LIVELLO STA OGNI RELAZIONESRL
		// le ordino per numero di parole
		// guardo se sono contenute l'una nell'altra
		// se trovo relazioni orfane, le memorizzo e mi devo ricordare di trattarle

		List<String> oggetti = new LinkedList<String>();
		for (RelazioneSrl r1: listarelazionidbr1){
			oggetti.add(r1.object);
		}
		Collections.sort(oggetti, new SrlStringLenghtComparator()); // le ordino per numero di parole crescente
		
		List<RelazioneSrl> listarelazionidbr1ordinata = new LinkedList<RelazioneSrl>();
		for (String s : oggetti){
			for (RelazioneSrl r1 : listarelazionidbr1){
				if (r1.object.compareTo(s) == 0 && !listarelazionidbr1ordinata.contains(r1)){
					listarelazionidbr1ordinata.add(r1);
				}
			}
		}
		
		for (int i = 0; i < listarelazionidbr1ordinata.size(); i++){
			String parola = listarelazionidbr1ordinata.get(i).object;
			int parolaLenght = listarelazionidbr1ordinata.get(i).object.length();
			for (int j = (i+1); j < listarelazionidbr1ordinata.size(); j++){
				if (parolaLenght >3){
					if (listarelazionidbr1ordinata.get(j).object.contains(parola) && listarelazionidbr1ordinata.get(i).object.compareTo(listarelazionidbr1ordinata.get(j).object)!=0){ //  && listarelazionidbr1ordinata.get(j).predicate.compareTo(listarelazionidbr1ordinata.get(i).predicate) == 0
						listarelazionidbr1ordinata.get(i).setLivelloSuperiore(listarelazionidbr1ordinata.get(j));
						break;
					}
				}
				else
					if (listarelazionidbr1ordinata.get(j).object.contains(" "+parola+" ") && listarelazionidbr1ordinata.get(i).object.compareTo(listarelazionidbr1ordinata.get(j).object)!=0){ //   && listarelazionidbr1ordinata.get(j).predicate.compareTo(listarelazionidbr1ordinata.get(i).predicate) == 0
						listarelazionidbr1ordinata.get(i).setLivelloSuperiore(listarelazionidbr1ordinata.get(j));
						break;
					}
			}
		}
		
		for (RelazioneSrl r1 : listarelazionidbr1){
			if (r1.livelloSuperioreSrl==null){
				r1.livelloProfonditaSrl = 0;
				creaAlberoSrl(r1, listarelazionidbr1);
			}
		}
	}
	
	private static void creaAlberoSrl(RelazioneSrl questa, List<RelazioneSrl> listarelazionidbr1){
		for (RelazioneSrl r1 : listarelazionidbr1){
			if (r1.livelloSuperioreSrl!=null && r1.livelloSuperioreSrl.equals(questa)){
				r1.livelloProfonditaSrl = questa.livelloProfonditaSrl+1;
				creaAlberoSrl(r1, listarelazionidbr1);
			}
		}
	}

	private static Boolean haSopra (List<SrlElemento> listaelementifrase, SrlElemento elemento){
		for (SrlElemento e : listaelementifrase){
    		if (e.order == elemento.tree) // se ho sopra un altro elemento
    			return true;
		}
		return false;
	}

	private static SrlElemento trovaHeadSeFraseParziale (List<SrlElemento> listaelementifrase, SrlElemento elemento){
		for (SrlElemento e : listaelementifrase){
			if (!haSopra(listaelementifrase, e))
				return e;
		}
		return null;
	}
	
	private static SrlElemento restituisciSrlElementoDaTermEOrder(String term, Integer order, List<SrlElemento> ltemp){
		SrlElemento elemento = null;
		for (SrlElemento e : ltemp){
			if (e.term.compareTo(term) == 0 && e.order == order){
				elemento = e;
			}
		}
		return elemento;
	}
	
	private static SrlElemento restituisciSrlElementoDaWord(String originalword, List<SrlElemento> ltemp){
		SrlElemento elemento = null;
		for (SrlElemento e : ltemp){
			if (e.originalword.compareTo(originalword) == 0){
				elemento = e;
			}
		}
		return elemento;
	}

	private static void eliminaErroriSrl(List<SrlElemento> ltemp){
		// se original word Ã¨ plurale
		// e word Ã¨ plurale
		// e pos Ã¨ NNS --> controllo se word meno s finale (o es) Ã¨ contenuta in WordNet
		String parola = null;
		for (SrlElemento e : ltemp){
			if (e.pos != null && e.word != null){
				if (e.pos.compareTo("NNS") == 0 && e.word.compareTo(e.originalword) == 0){
					String lemma = e.originalword.substring(0, (e.originalword.length() - 1)); // tolgo s
					parola = app.parolaContenutaInWordNet(lemma);
					if (parola != null){
						e.word = parola;
					}
					else{
						lemma = e.originalword.substring(0, (e.originalword.length() - 2)); // tolgo es
						parola = app.parolaContenutaInWordNet(lemma);
						if (parola != null){
							e.word = parola;
						}
					}
				}			
			}

		}
	}
	
	private static void stampaTuttiSrlElementi(List<SrlElemento> ltemp){
		for (SrlElemento e : ltemp)
			System.out.println(" elemento "+e.stampaElemento());
	}
	
	private static void trovaParoleUniteWordNet(Statement st){
		System.out.println(" ________________________________________ ");
		String tabelladefinitionsrlmsr = "";
		tabelladefinitionsrlmsr = "definitionsrl"+app.animale;
		String nomeFile = "Storyboard-"+app.animale+"-tokenized.txt";
		BufferedReader a;
		Map<SrlElemento, SrlElemento> mappaPrecSucc = new HashMap<SrlElemento, SrlElemento>();
	    BufferedWriter out;
	    String nomeFileWriter = "queryParoleComposte.sql";

		try {
			a = new BufferedReader(new FileReader(nomeFile));
			out = new BufferedWriter(new FileWriter(nomeFileWriter));

			Map<String, SrlElemento> mappaelementi = new TreeMap<String, SrlElemento>();
			mappaelementi = caricoDefinitionsrl(st, tabelladefinitionsrlmsr);
			
			List<SrlElemento> ltemp = new LinkedList<SrlElemento>(mappaelementi.values());
			Collections.sort(ltemp, new SrlElementoComparator()); // le ordino per numero di parole crescente
			app.eliminaErroriSrl(ltemp);
//			stampaTuttiSrlElementi(ltemp);
			
			String line;
			SrlElemento elementoPrec = null;
			while ((line = a.readLine()) != null) {
			    StringTokenizer stk = new StringTokenizer(line, " ");
			    while (stk.hasMoreTokens()){
//				    System.out.println(" - riga");
				    String parola = stk.nextToken().trim();
				    SrlElemento e = restituisciSrlElementoDaWord(parola, ltemp); ///////////// verificare come mai steps non viene riconosciuto
				    ///////////////////// //////////////////////////////////////////////////// probabilmente e' perche' e' al plurale
				    
				    if (elementoPrec != null && e != null){
				    	mappaPrecSucc.put(elementoPrec, e);
						System.out.println("+ sono in " +elementoPrec.word+ " "+e.word);
				    }
				    elementoPrec = e;
			    }
			}
			for (SrlElemento e : mappaPrecSucc.keySet()){
				System.out.println("sono in " +e.word+ " "+mappaPrecSucc.get(e).word);
				String p = testWs4j(e.word, mappaPrecSucc.get(e).word);
				if (p != null){
					System.out.println(p);
					String query1 = "INSERT INTO `paroleunitewordnet"+app.animale+"`(`term`, `order`, `wordunita`) VALUES ('"+e.term+"',"+e.order+",'"+p+"'); \n";
					String query2 = "INSERT INTO `paroleunitewordnet"+app.animale+"`(`term`, `order`, `wordunita`) VALUES ('"+mappaPrecSucc.get(e).term+"',"+mappaPrecSucc.get(e).order+",'"+p+"'); \n";
					out.write(query1);
					out.flush();
					out.write(query2);
					out.flush();
					System.out.println(query1);
					System.out.println(query2);
				}
			}
	        out.close();

		}
		catch(Exception e){
			e.printStackTrace();
			
		}
	}

	private static String trovaPosMinore(String pos1, String pos2){
		int valore1 = 0;
		int valore2 = 0;
		if (pos1.compareTo("v") == 0)
			valore1 = 4;
		if (pos1.compareTo("n") == 0)
			valore1 = 3;
		if (pos1.compareTo("a") == 0)
			valore1 = 2;
		if (pos1.compareTo("r") == 0)
			valore1 = 1;

		if (pos2.compareTo("v") == 0)
			valore2 = 4;
		if (pos2.compareTo("n") == 0)
			valore2 = 3;
		if (pos2.compareTo("a") == 0)
			valore2 = 2;
		if (pos2.compareTo("r") == 0)
			valore2 = 1;
		
		if (valore1 > valore2)
			return pos2;
		else
			return pos1;
	}

	
	private static boolean cercaSeParolaEDiUnDeterminatoPos(String lemma, String pos){
		WordDAO w = new WordDAO();
		String parola = null;
		List<edu.cmu.lti.jawjaw.pobj.Word> lista = new LinkedList<edu.cmu.lti.jawjaw.pobj.Word>();
		lista = w.findWordsByLemma(lemma);
		int numN = 0;
		int numV = 0;
		int numA = 0;
		int numR = 0;
		for (edu.cmu.lti.jawjaw.pobj.Word word : lista){
			parola = word.getLemma()+ " "+word.getPos();
			if (word.getPos().toString().compareTo("n") == 0)
				numN ++;
			if (word.getPos().toString().compareTo("v") == 0)
				numV ++;
			if (word.getPos().toString().compareTo("a") == 0)
				numA ++;
			if (word.getPos().toString().compareTo("r") == 0)
				numR ++;
		}
		if (numV != 0 && pos.compareTo("v") == 0)
			return true;
		if (numN != 0 && pos.compareTo("n") == 0)
			return true;
		if (numA != 0 && pos.compareTo("a") == 0)
			return true;
		if (numR != 0 && pos.compareTo("r") == 0)
			return true;
		return false;
	}

	
	private static String tipoParolaSelezionato(String lemma){
		WordDAO w = new WordDAO();
		String parola = null;
		String append = "";
		List<edu.cmu.lti.jawjaw.pobj.Word> lista = new LinkedList<edu.cmu.lti.jawjaw.pobj.Word>();
		lista = w.findWordsByLemma(lemma);
		int numN = 0;
		int numV = 0;
		int numA = 0;
		int numR = 0;
		for (edu.cmu.lti.jawjaw.pobj.Word word : lista){
			parola = word.getLemma()+ " "+word.getPos();
			if (word.getPos().toString().compareTo("n") == 0)
				numN ++;
			if (word.getPos().toString().compareTo("v") == 0)
				numV ++;
			if (word.getPos().toString().compareTo("a") == 0)
				numA ++;
			if (word.getPos().toString().compareTo("r") == 0)
				numR ++;
		}
		if (numV != 0)
			append = "v";
		else if (numN != 0)
			append = "n";
		else if (numA != 0)
			append = "a";
		else if (numR != 0)
			append = "r";

		return append;
	}

	
	private static String parolaContenutaInWordNet(String lemma){
		WordDAO w = new WordDAO();
		String parola = null;
		List<edu.cmu.lti.jawjaw.pobj.Word> lista = new LinkedList<edu.cmu.lti.jawjaw.pobj.Word>();
		lista = w.findWordsByLemma(lemma);
		for (edu.cmu.lti.jawjaw.pobj.Word word : lista)
			parola = word.getLemma();
		return parola;
	}


	private static String Ws4jGetType(String lemma, String type){
		WordDAO w = new WordDAO();
		String parola = null;
		String pos = null;
		List<edu.cmu.lti.jawjaw.pobj.Word> lista = new LinkedList<edu.cmu.lti.jawjaw.pobj.Word>();
		lista = w.findWordsByLemma(lemma);
		for (edu.cmu.lti.jawjaw.pobj.Word word : lista){
			parola = word.getLemma();
			pos = word.getPos().toString();
			if (pos.compareTo(type) == 0)
				return pos;
		}
		return "";
	}

	
	private static String testWs4j(String prima, String seconda){
		WordDAO w = new WordDAO();
		String lemma = prima+" "+seconda;
		String parola = null;
		List<edu.cmu.lti.jawjaw.pobj.Word> lista = new LinkedList<edu.cmu.lti.jawjaw.pobj.Word>();
		lista = w.findWordsByLemma(lemma);
		for (edu.cmu.lti.jawjaw.pobj.Word word : lista)
			parola = word.getLemma();
		return parola;
	}
	
    private static Double runWs4j(String word1, String word2) {
//    	if (app.consideraSoloVerbiPerGesti)
        WS4JConfiguration.getInstance().setMFS(true);
        double s = 0.0;
        for ( RelatednessCalculator rc : rcs ) {
            s = rc.calcRelatednessOfWords(word1, word2);
//            System.out.println( word1+" "+word2 + " "+rc.getClass().getName()+"\t"+s );
        }
        return s;
    }
	
    private static void run( String word1, String word2 ) {
        WS4JConfiguration.getInstance().setMFS(true);
        for ( RelatednessCalculator rc : rcs ) {
            double s = rc.calcRelatednessOfWords(word1, word2);
            System.out.println( rc.getClass().getName()+"\t"+s );
        }
//        double s = Lesk.calcRelatednessOfWords(word1, word2);
//        System.out.println( rc.getClass().getName()+"\t"+s );

    }
    
    private static Integer json(String json){
//    	String json = "{\"d\":{\"results\":[{\"__metadata\":{\"uri\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=\u0027web\u0027&Query=\u0027moonwalk walk\u0027&$skip=0&$top=1\",\"type\":\"ExpandableSearchResult\"},\"ID\":\"4a2e27cc-1186-4ba8-8f1b-97641ee05fd3\",\"WebTotal\":\"898000\",\"WebOffset\":\"0\",\"ImageTotal\":\"\",\"ImageOffset\":\"\",\"VideoTotal\":\"\",\"VideoOffset\":\"\",\"NewsTotal\":\"\",\"NewsOffset\":\"\",\"SpellingSuggestionsTotal\":\"\",\"AlteredQuery\":\"\",\"AlterationOverrideQuery\":\"\",\"Web\":[{\"__metadata\":{\"uri\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/ExpandableSearchResultSet(guid\u00274a2e27cc-1186-4ba8-8f1b-97641ee05fd3\u0027)/Web?$skip=0&$top=1\",\"type\":\"WebResult\"},\"ID\":\"6d17b3d5-20b1-4555-b9a2-ec1b65716eaf\",\"Title\":\"Walk the Walk | Raising Money and Awareness | Uniting ...\",\"Description\":\"Breast cancer charity raising money and awareness through power walking challenges including midnight MoonWalk Marathons in London and Edinburgh\",\"DisplayUrl\":\"www.walkthewalk.org\",\"Url\":\"http://www.walkthewalk.org/\"}],\"Image\":[],\"Video\":[],\"News\":[],\"RelatedSearch\":[],\"SpellingSuggestions\":[]}],\"__next\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=\u0027web\u0027&Query=\u0027moonwalk%20walk\u0027&$skip=1&$top=1\"}}";
    	String parti[] = json.split("WebTotal\":\"");
    	String parti2[] = parti[1].split("\"");
//    	System.out.println(parti2[0]);
    	
    	return Integer.parseInt(parti2[0]);
//    	Gson gson = new Gson();
//    	DataObject obj = gson.fromJson(json, DataObject.class);
//    	
//    	System.out.println(obj);
    	
    	
//        String str = "{\"d\":{\"results\":[{\"__metadata\":{\"uri\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=\u0027web\u0027&Query=\u0027moonwalk walk\u0027&$skip=0&$top=1\",\"type\":\"ExpandableSearchResult\"},\"ID\":\"4a2e27cc-1186-4ba8-8f1b-97641ee05fd3\",\"WebTotal\":\"898000\",\"WebOffset\":\"0\",\"ImageTotal\":\"\",\"ImageOffset\":\"\",\"VideoTotal\":\"\",\"VideoOffset\":\"\",\"NewsTotal\":\"\",\"NewsOffset\":\"\",\"SpellingSuggestionsTotal\":\"\",\"AlteredQuery\":\"\",\"AlterationOverrideQuery\":\"\",\"Web\":[{\"__metadata\":{\"uri\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/ExpandableSearchResultSet(guid\u00274a2e27cc-1186-4ba8-8f1b-97641ee05fd3\u0027)/Web?$skip=0&$top=1\",\"type\":\"WebResult\"},\"ID\":\"6d17b3d5-20b1-4555-b9a2-ec1b65716eaf\",\"Title\":\"Walk the Walk | Raising Money and Awareness | Uniting ...\",\"Description\":\"Breast cancer charity raising money and awareness through power walking challenges including midnight MoonWalk Marathons in London and Edinburgh\",\"DisplayUrl\":\"www.walkthewalk.org\",\"Url\":\"http://www.walkthewalk.org/\"}],\"Image\":[],\"Video\":[],\"News\":[],\"RelatedSearch\":[],\"SpellingSuggestions\":[]}],\"__next\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=\u0027web\u0027&Query=\u0027moonwalk%20walk\u0027&$skip=1&$top=1\"}}";
//        JsonParserFactory factory=JsonParserFactory.getInstance();
//        JSONParser parser=factory.newJsonParser();
//        Map jsonData=parser.parseJson(str);
//
//        Map rootJson=(Map) jsonData.get("root");
//        List al=(List) rootJson.get("d");
//        String fName=(String) ((Map)al.get(0)).get("WebTotal");
//        System.out.println(fName);
    }
    public static boolean locIsPerifrasi(SrlElemento e){
    	boolean is = false;
    	List<String> loc = new ArrayList<String>();
    	loc.add("at");
    	loc.add("in");
//    	loc.add("out");
    
    	for(String s : loc){
    		if(s.compareToIgnoreCase(e.getWord())==0){
    			is = true;
    			return is;
    		}
    	}
    	return is;
    }
    public static SrlElemento getParolaCollegata(List<SrlElemento> lista,SrlElemento elm){
//    	Collections.sort(lista, new Comparator<SrlElemento>(){
//			@Override
//			public int compare(SrlElemento arg0, SrlElemento arg1) {
//				return -Integer.compare(arg0.getOrder(),arg1.getOrder());
//			}
//		});
    	int order = elm.getOrder();
    	for(SrlElemento srl :  lista){
    		if(order == srl.getTree())
//    			if(srl.getPos().equals("IN")==false)
    				return srl;
//    			else
//    				order = srl.getOrder();
    	}
    	return elm;
    }
    public static boolean isManieraCollegataVerbo(SrlElemento srl, SrlElemento verbo  ){
    	if(srl == null || verbo == null){
    		return false;
    	}
    	if(srl.equals(verbo))
    		return true;
    	else 
    		return false;
    }
}
