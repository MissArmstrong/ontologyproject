
import java.util.LinkedList;
import java.util.List;

public class RisultatoFinale {
	String numAzione;
	String testo;
	String azionerisultante;
	List<RisultatoFinaleParte> lista = new LinkedList<RisultatoFinaleParte>();
	
	public RisultatoFinale(String numAzione, String testo, String azionerisultante) {
		this.numAzione = numAzione;
		this.testo = testo;
		this.azionerisultante = azionerisultante;
	}
	
	public void addRisultato(RisultatoFinaleParte r){
		this.lista.add(r);
	}
}
