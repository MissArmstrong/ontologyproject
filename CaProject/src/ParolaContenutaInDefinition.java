
import java.util.Map;
import java.util.TreeMap;


public class ParolaContenutaInDefinition {
	String parola;
	Map<String, String> definizioni = new TreeMap<String, String>();
	
	public ParolaContenutaInDefinition(String parola) {
		this.parola = parola;
	}
	
	public void addParola(String parola, String definizione){
		definizioni.put(parola, definizione);
	}

	public Map<String, String> getDefinizioni(){
		return definizioni;
	}
}
