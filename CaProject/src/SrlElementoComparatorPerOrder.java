
import java.util.Comparator;


public class SrlElementoComparatorPerOrder implements Comparator<SrlElemento>{

	@Override
	public int compare(SrlElemento e1, SrlElemento e2) {
		return e1.order - e2.order;
	}

}
