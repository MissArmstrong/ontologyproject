package model;

import java.util.ArrayList;
import java.util.List;

/*
 * Modello dell'oggetto su cui si possono eseguire azioni e pu�
 * essere manovrato da diversi strumenti
 */
public class OggettoModel {

	private String nome="";
	private String nomeSpecifico ="";
	private List<AzioneModel> azioni;
	private List<StrumentoModel> strumenti;
	
	// Add for complete the information into the ontology
	private String nomeAlternativo = "";
	private LivelloModel padre = null;
	
	public OggettoModel(String nome) {
		super();
		this.nome = nome;
		this.strumenti = new ArrayList<StrumentoModel>();
		this.azioni = new ArrayList<AzioneModel>();
		
	}
	
	public OggettoModel(String nomeGenerico,String nomeSpecifico) {
		this.nome = nomeGenerico;
		this.nomeSpecifico = nomeSpecifico;
		this.strumenti = new ArrayList<StrumentoModel>();
		this.azioni = new ArrayList<AzioneModel>();
		
	}
	
	// Add for complete the information into the ontology
	public OggettoModel(String nomeGenerico,String nomeSpecifico, String nomeAlternativo) {
		this.nome = nomeGenerico;
		this.nomeSpecifico = nomeSpecifico;
		this.nomeSpecifico = nomeSpecifico;
		this.strumenti = new ArrayList<StrumentoModel>();
		this.azioni = new ArrayList<AzioneModel>();
		
	}
	
	// Add for complete the information into the ontology
	public void setPadre(LivelloModel padre){
		this.padre = padre;
	}
	
	public LivelloModel getPadre(){
		return this.padre;
	}
	
	public String getNome(){
		return this.nome;
	}
	public String getNomeSpecifico(){
		if(nomeSpecifico.equals("")){
			return nome;
		}else
		return nomeSpecifico;
	}
	
	// Add for complete the information into the ontology
	public String getNomeAlternativo() {
		if(nomeAlternativo.equals("")){
			return nomeSpecifico;
		}else
		return  nomeAlternativo;
	}
	
	public void AggiungiAzioniUtilizzabili(List<AzioneModel> azione){
		for(AzioneModel am : azione){
			this.azioni.add(am);
		}
		
	}
	public void AggiungiStrumentiUtilizzabili(List<StrumentoModel> strum){
		for(StrumentoModel sm : strum){
			this.strumenti.add(sm);
		}
		
	}

	public List<AzioneModel> getAzioni() {
		return azioni;
	}

	public List<StrumentoModel> getStrumenti() {
		return strumenti;
	}
	
	
	public String toString() {
		return  nome ;
	}
}
