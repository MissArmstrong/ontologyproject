package model;

public class TriplaModel {
	
	private AzioneModel azione;
	private OggettoModel oggetto;
	private StrumentoModel strumento;
	public TriplaModel(AzioneModel azione, OggettoModel oggetto,
			StrumentoModel strumento) {
		super();
		this.azione = azione;
		this.oggetto = oggetto;
		this.strumento = strumento;
	}
	public AzioneModel getAzione() {
		return azione;
	}
	public OggettoModel getOggetto() {
		return oggetto;
	}
	public StrumentoModel getStrumento() {
		return strumento;
	}
	@Override
	public String toString() {
		String s="";
		
		if(azione!=null){
			s += azione.getNome();
		}else{
			s += "NO_VERB";
		}
		if(oggetto!=null){
			s +=" "+oggetto.getNomeSpecifico();
		}else{
			s += " NO_OBJ";
		}
		if(strumento!= null){
			s +=" "+ strumento.getNomeSpecifico();
		}else{
			s += " NO_STRUM";
		}
		return s;
//		return  azione.getNome() + " " +oggetto.getNomeSpecifico()+ " " + strumento.getNomeSpecifico();
	}
	
	

}
