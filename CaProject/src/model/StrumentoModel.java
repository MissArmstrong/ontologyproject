package model;

import java.util.ArrayList;
import java.util.List;

public class StrumentoModel {
	
	private String nome="";
	private String nomeSpecifico=""; 
	
	// Add for complete the information into the ontology
	private String nomeAlternativo= "";
	
	private List<AzioneModel> azioni = null ;
	private List<OggettoModel> oggetti = null;
	
	
	public StrumentoModel(String nome) {
		super();
		this.nome = nome;
		azioni = new ArrayList<AzioneModel>();
		oggetti = new ArrayList<OggettoModel>();
	}
	
	public StrumentoModel(String nomeGenerico, String nomeSpecifico) {
		this.nome = nomeGenerico;
		this.nomeSpecifico = nomeSpecifico;
		
		azioni = new ArrayList<AzioneModel>();
		oggetti = new ArrayList<OggettoModel>();
	}

	// Add for complete the information into the ontology
	public StrumentoModel(String nomeGenerico, String nomeSpecifico, String nomeAlternativo) {
		this.nome = nomeGenerico;
		this.nomeSpecifico = nomeSpecifico;
		this.nomeAlternativo = nomeAlternativo;
		azioni = new ArrayList<AzioneModel>();
		oggetti = new ArrayList<OggettoModel>();
	}

	
	public String getNome(){
		return this.nome;
	}
	
	public void AggiungiAzioneUtilizzabile(AzioneModel azione){
		this.azioni.add(azione);
		
	}
	public void AggiungiOggettoUtilizzabile(OggettoModel ogg){
		this.oggetti.add(ogg);
	}
	
	public void AggiungiAzioniUtilizzabili(List<AzioneModel> azione){
		for(AzioneModel am : azione){
			this.azioni.add(am);
		}
		
	}
	public void AggiungiOggettiUtilizzabili(List<OggettoModel> Ogg){
		for(OggettoModel om : Ogg){
			this.oggetti.add(om);
		}
		
	}

	public List<AzioneModel> getAzioni() {
		return azioni;
	}

	public List<OggettoModel> getOggetti() {
		return oggetti;
	}

	@Override
	public String toString() {
		return  nome;
	}
	
	public String getNomeGenerico() {
		return  nome ;
	}
	
	public String getNomeSpecifico() {
		if(nomeSpecifico.equals("")){
			return nome;
		}else
		return  nomeSpecifico ;
	}

	// Add for complete the information into the ontology
	public String getNomeAlternativo() {
		if(nomeAlternativo.equals("")){
			return nomeSpecifico;
		}else
		return  nomeAlternativo;
	}
	
}
