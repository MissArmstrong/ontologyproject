package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LivelloModel {
	
	Map<Object,Integer> componenti;
	private int nElementi = 0;
	private int nElementiSmontati = 0;
	private int nElementiRimontati = 0;
	private int livelloBOM ;
	private int lvMaxBOM ;
	private int StatoLivello = 1;
	private String idName= "";
	private List<LivelloModel> listaTotaleLivelli;
	
	// Added variable for integration with the ontology
	private LivelloModel padre = null;
	
	public LivelloModel(int livelloBOM) {
		this.livelloBOM = livelloBOM;
		componenti = new LinkedHashMap<Object, Integer>();
	}
	public LivelloModel(int livelloBOM,int lvMaxBOM,String idName,Collection<LivelloModel> collection ) {
		this.livelloBOM = livelloBOM;
		this.lvMaxBOM = lvMaxBOM;
		this.idName = idName;
		componenti = new LinkedHashMap<Object, Integer>();
		this.listaTotaleLivelli = new ArrayList<LivelloModel>(collection);
		
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Each level can have a father level, if father is null the level is the root level that contains all the levels */
	public void setPadre(LivelloModel padre){
		this.padre = padre;
	}
	
	public LivelloModel getPadre(){
		return this.padre;
	}
	
	public List<LivelloModel> getListaTotaleLivelli() {
		return listaTotaleLivelli;
	}
	
	public int getLvMaxBOM(){
		return this.lvMaxBOM;
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Added for give to each level an id name, not only the root */
	public void setIdName(String idName){
		this.idName = idName;
	}
	
	public String getIdName(){
		return this.idName;
	}
	public int getLivelloBOM(){
		return this.livelloBOM;
	}
	public int getNElementi(){
		return this.nElementi;
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Added for setting the system into a default stage */
	public void setNElementiSmontati(int value){
		this.nElementiSmontati = value;
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Added for setting the system into a default stage */
	public void setNElementiRimontati(int value){
		this.nElementiRimontati = value;
	}
	
	public int getNElementiSmontati(){
		return this.nElementiSmontati;
	}
	public void smonta(OggettoModel om ,AzioneModel am){
		try{
			int BOMEffect = am.getBOMEffect();
//			if(am.getNome().equals("clean")){
//				System.out.println("yeah");
//			}
			if(BOMEffect==2){
				componenti.put(om, 2);
				this.nElementiSmontati++;
				this.nElementiRimontati++;
			}else if(BOMEffect==1){
				return;
			}
			else if(BOMEffect==0){
			componenti.put(om, 0);
			this.nElementiSmontati++;
			}
			
			controllaStatoLivello();
		}catch(NullPointerException e){
			System.err.println("NullPointerException");
		}
	}
	public void rimonta(OggettoModel om,AzioneModel am) {
		if(am==null){
			return;
		}
		int BOMEffect = am.getBOMEffect();
		if(BOMEffect==1){
				return;
			}
		componenti.put(om, 2);
		this.nElementiRimontati++;
		controllaStatoLivello();
		
	}
	
	/* Method has been added to be integrated with the ontology.
	 * The method is identical to the old except for "controllaStatoLivelloOntologia" method */
	public void smontaOntologia(OggettoModel om ,AzioneModel am){
		try{
			int BOMEffect = am.getBOMEffect();
//			if(am.getNome().equals("clean")){
//				System.out.println("yeah");
//			}
			if(BOMEffect==2){
				componenti.put(om, 2);
				this.nElementiSmontati++;
				this.nElementiRimontati++;
			}else if(BOMEffect==1){
				return;
			}
			else if(BOMEffect==0){
			componenti.put(om, 0);
			this.nElementiSmontati++;
			}
			
			controllaStatoLivelloOntologia();
		}catch(NullPointerException e){
			System.err.println("NullPointerException");
		}
	}
	
	/* Method has been added to be integrated with the ontology.
	 * The method is identical to the old except for "controllaStatoLivelloOntologia" method */
	public void rimontaOntologia(OggettoModel om,AzioneModel am) {
		if(am==null){
			return;
		}
		int BOMEffect = am.getBOMEffect();
		if(BOMEffect==1){
				return;
			}
		componenti.put(om, 2);
		this.nElementiRimontati++;
		controllaStatoLivelloOntologia();
		
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Added for the hint search */
	public LivelloModel ricercaLivello(String suggerimento)
	{
		// First search only into the object
		for (Map.Entry<Object, Integer> entry : componenti.entrySet())
		{
			if(entry.getKey() instanceof OggettoModel)
			{
				String nameObject =  ((OggettoModel) entry.getKey()).getNome();
				String[] partName = nameObject.split("_");
				for(String part: partName)
				{
					if(suggerimento.contains(part))
					{
						return this;
					}
				}
			}
		}
		
		// Then search into the level in a recursive way
		for (Map.Entry<Object, Integer> entry : componenti.entrySet())
		{
			if(entry.getKey() instanceof LivelloModel)
			{
				String nameLevel =  ((LivelloModel) entry.getKey()).getIdName();
				String[] partName = nameLevel.split("_");
				for(String part: partName)
				{
					if(suggerimento.contains(part))
					{
						return this;
					}
				}
				LivelloModel currentLevel = (LivelloModel) entry.getKey();
				currentLevel = currentLevel.ricercaLivello(suggerimento);
				if(currentLevel != null)
				{
					return currentLevel;
				}
			}
		}
		return null;
	}
	
	
	
	public int getStatoLivello(){
		return this.StatoLivello;
	}

	public void setStatoLivello(int livello){
		this.StatoLivello = livello;
	}
	
	public void controllaStatoLivello(){
		if(this.nElementi==nElementiSmontati){
			this.StatoLivello=0;
		}
		if(this.nElementi==nElementiRimontati){
			this.StatoLivello=2;
		}
	}
	
	/* Method has been added to be integrated with the ontology.
	 * Same method as "controllaStatoLivello" but the level status is also transmitted to the father */	
	public void controllaStatoLivelloOntologia(){
		if(this.nElementi==nElementiSmontati){
			this.StatoLivello=0;
			this.getPadre().getComponenti().put(this, this.StatoLivello);
		}
		if(this.nElementi==nElementiRimontati){
			this.StatoLivello=2;
			this.getPadre().getComponenti().put(this, this.StatoLivello);
		}
	}
//	public void aggiungiComponenti(List<OggettoModel> listaOm){
//		for(OggettoModel om : listaOm){
//			this.componenti.put(om, 1);
//			this.nElementi++;
//		}
//	}
	
//	public void aggiungiLivelli(List<Object> list){
//		for(Object lm : list){
//			this.componenti.put(lm, 1);
//			this.nElementi++;
//		}
//	}
	

	
	
	public void aggiungiComponenti(List<Object> list){
		for(Object lm : list){
			this.componenti.put(lm, 1);
			this.nElementi++;
		}
	}

	public Map<Object, Integer> getComponenti() {
		return componenti;
	}
	public void finito(){
		this.StatoLivello =2;
	}
	
}
