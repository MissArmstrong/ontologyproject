package model;

import java.util.ArrayList;
import java.util.List;

public class AzioneModel {

	private String nome ="";
	private List<OggettoModel> oggetti;
	private List<StrumentoModel> strumenti;
	private int BOMEffect;  //1-nessun effetto;0-smonta o rimonta,2-smonta e rimonta
	
	public AzioneModel(String nome) {
		super();
		this.nome = nome;
		this.oggetti = new ArrayList<OggettoModel>();
		this.strumenti = new ArrayList<StrumentoModel>();
		this.BOMEffect = 0;
	}
	
	public AzioneModel(String nome, int BOMEffect) {
		super();
		this.nome = nome;
		this.BOMEffect = BOMEffect;
		this.oggetti = new ArrayList<OggettoModel>();
		this.strumenti = new ArrayList<StrumentoModel>();
	}
	public int getBOMEffect(){
		return this.BOMEffect;
	}
	public String getNome(){
		return this.nome;
	}
	public void AggiungiStrumentoUtilizzabile(StrumentoModel strum){
		this.strumenti.add(strum);
		
	}
	public void AggiungiOggettoUtilizzabile(OggettoModel ogg){
		this.oggetti.add(ogg);
	}
	
	public void AggiungiStrumentiUtilizzabili(List<StrumentoModel> strum){
		for(StrumentoModel sm : strum){
			this.strumenti.add(sm);
		}
		
	}
	public void AggiungiOggettiUtilizzabili(List<OggettoModel> Ogg){
		for(OggettoModel om : Ogg){
			this.oggetti.add(om);
		}
		
	}

	public List<OggettoModel> getOggetti() {
		return oggetti;
	}

	public List<StrumentoModel> getStrumenti() {
		return strumenti;
	}

	@Override
	public String toString() {
		return  nome ;
	}
	

}
