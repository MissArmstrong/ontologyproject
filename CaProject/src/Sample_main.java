import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.stream.Stream;

import com.mysql.cj.jdbc.MysqlDataSource;

import OWLOntologies.SystemReader;

public class Sample_main {

	public static void main(String[] args) throws IOException
	{
		SystemReader systemReader = new SystemReader();
		String systemName = "";
		
		// Ritorna il nome del file senza estensione, no path o altro
		systemName = systemReader.createOntologySystemFromFile("CSVFrom3DModels/Scooter.csv");//TMS_Laser.csv");
		
		// metodi chiara
		
		// Prima dell'analisi controllare se effetivamente il sistema esiste
		if(!systemName.equals(""))
		{
			System.out.println("*** START ANALYSIS OF THE MANUAL INSTRUCTIONS FOR SYSTEM '" + systemName + "' ***"); 
			app_integrazione app1 = new app_integrazione();

			app1.analysysMethodPart1(systemName, "Path");
			app1.analysysMethodPart2(systemName, "Path");
			app1.analysysMethodPart3(systemName, "Path");
			app1.analysysMethodPart4(systemName, "Path");
			app1.analysysMethodPart5(systemName, "Path");
		}
	}

}
