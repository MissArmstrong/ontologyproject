
import java.util.Comparator;


public class SrlElementoComparatorProfonditaAlbero implements Comparator<SrlElemento>{

	@Override
	public int compare(SrlElemento e1, SrlElemento e2) {
		if (e1.livello > e2.livello)
			return -1;
		return 1;
	}
}
