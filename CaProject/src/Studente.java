

import java.util.Map;
import java.util.TreeMap;


public class Studente {
	private Map<Integer, Situazione> situazioni;
	private int numeroStudente;
	
	public Studente(int numeroStudente) {
		this.situazioni = new TreeMap<Integer, Situazione>();
		this.numeroStudente = numeroStudente;
	}
	
	public void addSituazione(Situazione s){
		situazioni.put(s.getNumero(), s);
	}
	
	public Map<Integer, Situazione> getSituazioni(){
		return situazioni;
	}
	
	public int getNumeroStudente(){
		return this.numeroStudente;
	}
	
}
