

public class SimilaritaGestoAzione {
	Gesto gesto;
	Azione azione;
	Double similarita;
	Contesto contesto;

	public SimilaritaGestoAzione(Gesto gesto, Azione azione, Double similarita) {
		this.gesto = gesto;
		this.azione = azione;
		this.similarita = similarita;
	}

	public void setContesto(Contesto contesto) {
		this.contesto = contesto;
	}
	
	

}
