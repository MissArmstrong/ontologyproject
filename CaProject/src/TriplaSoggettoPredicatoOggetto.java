


public class TriplaSoggettoPredicatoOggetto extends Tripla{
	StanfordParser soggetto;
	StanfordParser predicato;
	StanfordParser oggetto;
	StanfordParser aCompPredicato;
	StanfordParser aModSoggetto;
	StanfordParser aModOggetto;
	StanfordParser soggettoDiPronome;

	public TriplaSoggettoPredicatoOggetto(StanfordParser soggetto, StanfordParser predicato, StanfordParser oggetto) {
		super("soggettoPredicatoOggetto");
		this.soggetto = soggetto;
		this.predicato = predicato;
		this.oggetto = oggetto;
	}
	
	public String stampa(){
		String stampa = "soggettoPredicatoOggetto ";
		if (soggettoDiPronome != null)
			stampa += "("+soggettoDiPronome.word+")";
		if (aModSoggetto != null)
			stampa += " "+aModSoggetto.stampa();
		stampa += " "+soggetto.stampa() ;
		if (aCompPredicato != null)
			stampa += " "+aCompPredicato.stampa();
		stampa += predicato.stampa();
		if (aModOggetto != null)
			stampa += " "+aModOggetto.stampa();
		stampa += oggetto.stampa()+ " "+super.relazione;
		return stampa;
//
//		
//		
//		if (aCompPredicato != null && aModOggetto != null)
//			return "soggettoPredicatoOggetto"+soggetto.stampa()+ " "+predicato.stampa()+ " "+aModOggetto.stampa()+" "+oggetto.stampa()+ " "+aCompPredicato.stampa()+" "+super.relazione;
//		if (aCompPredicato != null)
//			return "soggettoPredicatoOggetto"+soggetto.stampa()+ " "+predicato.stampa()+ " "+oggetto.stampa()+ " "+aCompPredicato.stampa()+" "+super.relazione;
//		if (aModOggetto != null)
//			return "soggettoPredicatoOggetto"+soggetto.stampa()+ " "+predicato.stampa()+ " "+aModOggetto.stampa()+" "+oggetto.stampa()+ " "+super.relazione;
//
//		return "soggettoPredicatoOggetto"+soggetto.stampa()+ " "+predicato.stampa()+ " "+oggetto.stampa()+ " "+super.relazione;
	}

	public void setACompPredicato(StanfordParser aCompPredicato){
		this.aCompPredicato = aCompPredicato;
	}

	public void setAModSoggetto(StanfordParser aModSoggetto){
		this.aModSoggetto = aModSoggetto;
	}

	public void setAModOggetto(StanfordParser aModOggetto){
		this.aModOggetto = aModOggetto;
	}
	
	public void setSoggettoDiPronome(StanfordParser soggettoDiPronome){
		this.soggettoDiPronome = soggettoDiPronome;
	}
}
