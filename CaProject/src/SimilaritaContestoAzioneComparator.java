
import java.util.Comparator;


public class SimilaritaContestoAzioneComparator implements Comparator<SimilaritaContestoAzione>{

	@Override
	public int compare(SimilaritaContestoAzione s1, SimilaritaContestoAzione s2) {
		return (int) - (s1.similarita * 1000 - s2.similarita * 1000);
	}

}
