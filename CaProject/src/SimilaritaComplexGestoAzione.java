

public class SimilaritaComplexGestoAzione {
	Gesto gesto;
	Azione azione;
	Double similarita;
	Contesto contesto;

	public SimilaritaComplexGestoAzione(Gesto gesto, Azione azione, Double similarita, Contesto contesto) {
		this.gesto = gesto;
		this.azione = azione;
		this.similarita = similarita;
		this.contesto = contesto;
	}
}
