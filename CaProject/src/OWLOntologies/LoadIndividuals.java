package OWLOntologies;

/**
 * Class that tries to load an ontology and add some individuals linked with a property
 */
import java.io.File;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class LoadIndividuals {
	
//    private static final IRI ONTOLOGY_IRI = IRI.create("http://www.co-ode.org/ontologies/pizza/pizza.owl");
    private static final File ontologyFile = new File(".\\ontologies\\ToolsOntology.owl");

    @SuppressWarnings("javadoc")
    public static void main(String[] args) {
        try {
        	
            // Load the ontology inside the "ontologie" folder, it could be loaded even by its IRI
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyFile);
//          OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ONTOLOGY_IRI);
                       
            System.out.println("Loaded ontology: " + ontology.getOntologyID());
            
            // Get our ontology IRI in order to perform actions 
            IRI ontologyIRI = ontology.getOntologyID().getOntologyIRI();
            
            // Retrieve OWLDataFactory
            OWLDataFactory factory = manager.getOWLDataFactory();
            
            // Create the individuals that will be saved inside the ontology
            OWLIndividual pentagon = factory.getOWLNamedIndividual(IRI.create(ontologyIRI + "#Pentagon"));
            OWLIndividual pentagonScrew = factory.getOWLNamedIndividual(IRI.create(ontologyIRI + "#PentagonScrew"));
            
            // Create the property that will link the individuals
            OWLObjectProperty screws = factory.getOWLObjectProperty(IRI.create(ontologyIRI + "#screws"));
            
            // Create the axiom, the triplete that merge individuals and property
            OWLObjectPropertyAssertionAxiom axiom1 = factory.getOWLObjectPropertyAssertionAxiom(screws, pentagon, pentagonScrew);
            
            // Add the axiom to the ontology
            AddAxiom addAxiom1 = new AddAxiom(ontology, axiom1);
            manager.applyChange(addAxiom1);
            
            // Save the ontology
            try {
				manager.saveOntology(ontology);
			} catch (OWLOntologyStorageException e) {
				System.out.println("Error while saving the ontology");
				e.printStackTrace();
			}
            
            IRI iri = IRI.create("http://www.semanticweb.org/owlapi/ontologies/ontology#Pentagon");
            // Now we create the class
            OWLClass clsAMethodA = factory.getOWLClass(iri);
            
            
        } catch (OWLOntologyCreationException e) {
            System.out.println("Could not load ontology: " + e.getMessage());
        }
    }
}