package OWLOntologies;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import uk.ac.manchester.cs.owl.owlapi.OWLObjectPropertyImpl;

import org.semanticweb.HermiT.Reasoner.ReasonerFactory;

public class OntologyReaderInitial
{
	
	public static void main(String[] args)
	{
		// init
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		File toolsFile = new File("ontologies/ToolsOntology.owl");
		OWLOntology toolsOntology;
		
		// load
		try
		{
			toolsOntology = manager.loadOntologyFromOntologyDocument(toolsFile);
			System.out.println("Loaded ontology: " + toolsOntology.getOntologyID());
			
			IRI location = manager.getOntologyDocumentIRI(toolsOntology);
			System.out.println("\tfrom: " + location); 
			
			// get and configure HermiT reasoner
			OWLReasonerFactory reasonerFactory = new ReasonerFactory();
			ConsoleProgressMonitor progessMonitor = new ConsoleProgressMonitor();
			OWLReasonerConfiguration config = new SimpleConfiguration(progessMonitor);
		
			// create the reasoner instance, classfy and compute inferences
			OWLReasoner reasoner = reasonerFactory.createReasoner(toolsOntology, config);
			reasoner.precomputeInferences(InferenceType.values());
			
			DefaultPrefixManager prefixManager = new DefaultPrefixManager("http://www.semanticweb.org/chiara/ontologies/2016/11/toolsontology#");
			
//			"http://www.semanticweb.org/chiara/ontologies/2016/11/toolsontology#"
//			new DefaultPrefixManager(null, null, "http://www.semanticweb.org/chiara/ontologies/2016/11/toolsontology#");
			
			// get all the objects
			OWLDataFactory dataFactory = manager.getOWLDataFactory();
			OWLClass objects = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), "Object"));
			NodeSet<OWLNamedIndividual> individualsNodeSet = reasoner.getInstances(objects, false);
			
			Set<OWLNamedIndividual> individualsSet = new HashSet<OWLNamedIndividual>();
			for(Node<OWLNamedIndividual> node : individualsNodeSet)
			{
				OWLNamedIndividual individual = node.getRepresentativeElement();
				individualsSet.add(individual);
			}
			
			for(OWLNamedIndividual object : individualsSet)
			{
				System.out.println("Individual name: " + prefixManager.getShortForm(object));
			
				//getScrewedBy
				OWLObjectPropertyImpl objectProperty = new OWLObjectPropertyImpl(IRI.create(prefixManager.getDefaultPrefix() + "screws"));
//				EntitySearcher.get
			}
		}
		catch(OWLOntologyCreationException e)
		{
			e.printStackTrace();
		}
	}

}
