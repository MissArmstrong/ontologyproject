package OWLOntologies;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

public class OntologyManualInsertion {
	
	private OWLOntologyManager owlOntologyManager;
	private OWLOntology owlOntology;
	private OWLDataFactory owlDataFactory;
	private IRI ontologyIRI;
	private DefaultPrefixManager prefixManager;
	private OWLReasoner owlReasoner;
	
	private Boolean isOntologyURI;
	private String ontologyPath;
	private URI ontologyURI;
	private File ontologyFile;
	
	
	public OntologyManualInsertion(String ontologyPath, Boolean isOntologyURI) {
		super();
		this.ontologyPath = ontologyPath;
		this.isOntologyURI = isOntologyURI;
		
		// Check if the ontologyPath leads to a local file or to an ontology referenced by a URI
		if(isOntologyURI)
		{
			try 
			{
				this.ontologyURI = new URI(ontologyPath);
			} 
			catch (URISyntaxException e) 
			{
				System.out.println("URISyntaxException " + e.getMessage());
			}
			
			this.ontologyFile = new File(this.ontologyURI);
		}
		else
		{
			this.ontologyFile = new File(ontologyPath);
		}
			
		// Create the OWLManager
		owlOntologyManager = OWLManager.createOWLOntologyManager();
		
		// Load the ontology from the file passed by its path or URI
		try 
		{
			owlOntology = owlOntologyManager.loadOntologyFromOntologyDocument(ontologyFile);
		} 
		catch (OWLOntologyCreationException e) 
		{
			System.out.println("OWLOntologyCreationException " + e.getMessage());
		}
		
		// Take the IRI, the identifier of the ontology, part of the URI
		ontologyIRI = owlOntology.getOntologyID().getOntologyIRI();	
		
		// Starting from the IRI, create the DefaultPrefixManager, used to query the ontology
		prefixManager = new DefaultPrefixManager(ontologyIRI.toString() + "#");
		
		// Retrieve the OWLDataFactory used in the methods to create the axioms and the object used to query the ontology
		owlDataFactory = owlOntologyManager.getOWLDataFactory();
		
		// Get and configure HermiT reasoner
		OWLReasonerFactory reasonerFactory = new ReasonerFactory();
		ConsoleProgressMonitor progessMonitor = new ConsoleProgressMonitor();
		OWLReasonerConfiguration configuration = new SimpleConfiguration(progessMonitor);
		
		// Create the reasoner instance, classify and compute inferences
		owlReasoner = reasonerFactory.createReasoner(owlOntology, configuration);
		owlReasoner.precomputeInferences(InferenceType.values());
	}
	
	/*
	 * GENERAL INFORMATION ABOUT DATA AND OBJECT PROPERTY OF THE ONTOLOGY
	 */

	/**
	 *  Gives back the list of individuals and for each data property, if any is present, 
	 *  the name and the values are listed
	 */
	public void showOntologyDataProperties() {
		
		System.out.println("Data properties");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			// Take the Data Properties of the individual from the ontology
			Map<OWLDataPropertyExpression, Set<OWLLiteral>> dataProperties = individual.getDataPropertyValues(owlOntology);
			
			// Print each Data Property of the current individual and its corresponding value
			for(Entry<OWLDataPropertyExpression, Set<OWLLiteral>> entry : dataProperties.entrySet())
			{
				System.out.println("  - " + prefixManager.getShortForm(individual.getIRI()).substring(1));
				
				System.out.println("\t- " + prefixManager.getShortForm(entry.getKey().asOWLDataProperty().getIRI()).substring(1));
				
				for(OWLLiteral literal : entry.getValue()){
					System.out.println("\t  - " + literal.getLiteral());
				}
			}
		}
	}

	/**
	 * Gives back the list of individuals and for each object property, if any is present, 
	 * the name and the values are listed
	 */
	public void showOntologyObjectProperties() {
		
		System.out.println("Object properties");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			// Take the Object Properties of the individual from the ontology
			Map<OWLObjectPropertyExpression, Set<OWLIndividual>> objectProperties = individual.getObjectPropertyValues(owlOntology);
			
			// Print each Object Property of the current individual and its corresponding value
			for(Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet())
			{
				// Print the starting element of the Object Property
				System.out.println("  - " + prefixManager.getShortForm(individual.asOWLNamedIndividual().getIRI()).substring(1));
				
				// Print the Object Property
				System.out.println("    - " + prefixManager.getShortForm(entry.getKey().asOWLObjectProperty().getIRI()).substring(1));
				
				// Print also the individual on which the property is applied
				for(OWLIndividual individualProperty : entry.getValue()){
					System.out.println("      - " + prefixManager.getShortForm(individualProperty.asOWLNamedIndividual().getIRI()).substring(1));
				}
			}
		}
	}
	
	/*
	 * METHODS TO INSERT DATA PROPERTIES
	 */

	/**
	 * Insert the component's name given the element name, composing the URI, the name you want assign 
	 * and language in which is written
	 * 
	 * @param elementName component's name, composing the URI
	 * @param name the name to assign to the component
	 * @param language the language in which the name is written
	 * @return Boolean
	 */
	public Boolean insertComponentName(String elementName, String name, String language) {
		
		System.out.println("Inserting component name");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");
				
				// Retrieve the Name Data Property
				OWLDataProperty nameDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "name"));
				
				System.out.println("Adding property " + nameDataProperty.getIRI().toString());

				// Get the literal, in order to add the language, and put it in an Axiom to apply on the ontology
				OWLLiteral nameLiteral = owlDataFactory.getOWLLiteral(name, language);
				OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(nameDataProperty, individual, nameLiteral);
				
				AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
				owlOntologyManager.applyChange(addAxiom);
							
	            try 
	            {
					owlOntologyManager.saveOntology(owlOntology);
				} 
	            catch (OWLOntologyStorageException e) 
	            {
					System.out.println("Error while saving the ontology");
					e.printStackTrace();
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Insert the component's size given the element name, composing the URI, and its size
	 * 
	 * @param elementName component's name, composing the URI
	 * @param size the component size
	 * @return Boolean
	 */
	public Boolean insertComponentSize(String elementName, Double size) {
		
		System.out.println("Insertion component size");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);

		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{			
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");

				// Retrieve the Size Data Property
				OWLDataProperty sizeDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "size"));
				
				System.out.println("Adding property " + sizeDataProperty.getIRI().toString());
				
				// Create the axiom to and add it to the ontology to be applied
				OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(sizeDataProperty, individual, size);
	           
				AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
				owlOntologyManager.applyChange(addAxiom);
							
	            try 
	            {
					owlOntologyManager.saveOntology(owlOntology);
				} 
	            catch (OWLOntologyStorageException e) 
	            {
					System.out.println("Error while saving the ontology");
					e.printStackTrace();
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Insert the component's level given the element name, composing the URI, and its level
	 * 
	 * 
	 * @param elementName component's name, composing the URI
	 * @param level of the component in the system's hierarchy
	 * @return Boolean
	 */
	public Boolean insertComponentLevel(String elementName, Integer level) {
		
		System.out.println("Insertion component level");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");

				// Retrieve the Size Data Property
				OWLDataProperty levelDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "level"));
				
				System.out.println("Adding property " + levelDataProperty.getIRI().toString());
				
				// Create the axiom to and add it to the ontology to be applied
				OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(levelDataProperty, individual, level);
	           
				AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
				owlOntologyManager.applyChange(addAxiom);
							
	            try {
					owlOntologyManager.saveOntology(owlOntology);
				} catch (OWLOntologyStorageException e) {
					System.out.println("Error while saving the ontology");
					e.printStackTrace();
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Insert the component's position given the element name, composing the URI, and its position as an adjective
	 * 
	 * 
	 * @param component's name, composing the URI
	 * @param adjective indicating the position
	 * @return Boolean
	 */
	public Boolean insertAdjectivePosition(String elementName, String adjective) {
		
		System.out.println("Insertion component adjective position");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");
				
				// Retrieve the Adjective Position Data Property
				OWLDataProperty adjectivePosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "adjectivePosition"));
				
				// Check if the Data Property can be applied to the individual entered as parameter
				Boolean canBeApplied = false;
				
				// Retrieve the domains of the Data Property
				NodeSet<OWLClass> domains = owlReasoner.getDataPropertyDomains(adjectivePosDataProperty, true);
				
				// Retrieve the classes to which the individual belongs
				NodeSet<OWLClass> types = owlReasoner.getTypes(individual, false);
				
				// Check the domains of the Data Property
				for(OWLClass domain : domains.getFlattened())
				{
					if(types.getFlattened().contains(domain))
					{
						canBeApplied = true;
					}
				}
				
				if(canBeApplied)
				{
					System.out.println("Adding property " + adjectivePosDataProperty.getIRI().toString());
					
					// Create the axiom to and add it to the ontology to be applied
					OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(adjectivePosDataProperty, individual, adjective);
		           
					AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
					owlOntologyManager.applyChange(addAxiom);
								
		            try {
						owlOntologyManager.saveOntology(owlOntology);
					} catch (OWLOntologyStorageException e) {
						System.out.println("Error while saving the ontology");
						e.printStackTrace();
					}
		            
		            return true;
				}
				else
				{
					System.out.println("Data property can't be applied to this individual");
					return false;
				}
				
			}
		}
		
		return false;
	}
	
	/**
	 * Insert the component's position given the element name, composing the URI, and its vertical position
	 * 
	 * 
	 * @param component's name, composing the URI
	 * @param value indicating the vertical position
	 * @return Boolean
	 */	
	public Boolean insertVerticalPosition(String elementName, Double position) {
		
		System.out.println("Insertion component vertical position");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");
				
				// Retrieve the Vertical Position Data Property
				OWLDataProperty verticalPosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "verticalPosition"));
				
				// Check if the Data Property can be applied to the individual entered as parameter
				Boolean canBeApplied = false;
				
				// Retrieve the domains of the Data Property
				NodeSet<OWLClass> domains = owlReasoner.getDataPropertyDomains(verticalPosDataProperty, false);
				
				// Retrieve the classes to which the individual belongs
				NodeSet<OWLClass> types = owlReasoner.getTypes(individual, false);
				
				// Check the domains of the Data Property
				for(OWLClass domain : domains.getFlattened())
				{
					if(types.getFlattened().contains(domain))
					{
						canBeApplied = true;
					}
				}
				
				if(canBeApplied)
				{
					System.out.println("Adding property " + verticalPosDataProperty.getIRI().toString());
					
					// Create the axiom to and add it to the ontology to be applied
					OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(verticalPosDataProperty, individual, position);
		           
					AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
					owlOntologyManager.applyChange(addAxiom);
								
		            try {
						owlOntologyManager.saveOntology(owlOntology);
					} catch (OWLOntologyStorageException e) {
						System.out.println("Error while saving the ontology");
						e.printStackTrace();
					}
					
					return true;
				}
				else
				{
					System.out.println("Data property can't be applied to this individual");
					return false;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Insert the component's position given the element name, composing the URI, and its horizontal position
	 * 
	 * @param elementName component's name, composing the URI
	 * @param position value indicating the horizontal position
	 * @return Boolean
	 */	
	public Boolean insertHorizontalPosition(String elementName, Double position) {
		
		System.out.println("Insertion component horizontal position");
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{
				System.out.println("Found individual");
				
				// Retrieve the Horizontal Data Property
				OWLDataProperty horizontalPosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "horizontalPosition"));
				
				// Check if the Data Property can be applied to the individual entered as parameter
				Boolean canBeApplied = false;
				
				// Retrieve the domains of the Data Property
				NodeSet<OWLClass> domains = owlReasoner.getDataPropertyDomains(horizontalPosDataProperty, false);
				
				// Retrieve the classes to which the individual belongs
				NodeSet<OWLClass> types = owlReasoner.getTypes(individual, false);
				
				// Check the domains of the Data Property
				for(OWLClass domain : domains.getFlattened())
				{
					if(types.getFlattened().contains(domain))
					{
						canBeApplied = true;
					}
				}
				
				if(canBeApplied)
				{
					System.out.println("Adding property " + horizontalPosDataProperty.getIRI().toString());
					
					// Create the axiom to and add it to the ontology to be applied
					OWLDataPropertyAssertionAxiom axiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(horizontalPosDataProperty, individual, position);
		           
					AddAxiom addAxiom = new AddAxiom(owlOntology, axiom);
					owlOntologyManager.applyChange(addAxiom);
								
		            try {
						owlOntologyManager.saveOntology(owlOntology);
					} catch (OWLOntologyStorageException e) {
						System.out.println("Error while saving the ontology");
						e.printStackTrace();
					}
					
					return true;
				}
				else
				{
					System.out.println("Data property can't be applied to this individual");
					return false;
				}
			}
		}
		
		return false;
	}
	
	/*
	 * METHODS TO LIST DATA PROPERTIES
	 */
	
	/**
	 * Retrieve the component's name, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getComponentName(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{		
				// Retrieve the Name Data Property
				OWLDataProperty nameDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "name"));	
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = owlReasoner.getDataPropertyValues(individual, nameDataProperty);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral() + ", language: " + value.getLang());
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Retrieve the component's size, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getComponentSize(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{			
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{	
				// Retrieve the Size Data Property
				OWLDataProperty sizeDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "size"));
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = individual.getDataPropertyValues(sizeDataProperty, owlOntology);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral());
				}
				
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Retrieve the component's level in the system hierarchy, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getComponentLevel(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{			
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{	
				// Retrieve the Level Data Property
				OWLDataProperty levelDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "level"));
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = individual.getDataPropertyValues(levelDataProperty, owlOntology);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral());
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Retrieve the component's adjective position, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getAdjectivePosition(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{			
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{	
				// Retrieve the Adjective Position Data Property
				OWLDataProperty adjectivePosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "adjectivePosition"));
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = individual.getDataPropertyValues(adjectivePosDataProperty, owlOntology);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral());
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Retrieve the component's vertical position, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getVerticalPosition(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{		
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{	
				// Retrieve the Vertical Position Data Property
				OWLDataProperty verticalPosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "verticalPosition"));
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = individual.getDataPropertyValues(verticalPosDataProperty, owlOntology);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral());
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Retrieve the component's horizontal position, giving the element name, the one used to compose the URI
	 * 
	 * @param elementName component's name, composing the URI
	 * @return Boolean
	 */
	public Boolean getHorizontalPosition(String elementName) {
		
		// Retrieve the all the individuals in the ontology
		NodeSet<OWLNamedIndividual> nodeSet =  owlReasoner.getInstances(owlDataFactory.getOWLThing(), false);
		
		// For each individual retrieved
		for(OWLNamedIndividual individual : nodeSet.getFlattened())
		{		
			// If the individual is the one entered as parameter
			if(prefixManager.getShortForm(individual.getIRI()).substring(1).equals(elementName))
			{	
				// Retrieve the Horizontal Position Data Property
				OWLDataProperty horizontalPosDataProperty = owlDataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "horizontalPosition"));
				
				// Get the values of the Data Property of the individual
				Set<OWLLiteral> values = individual.getDataPropertyValues(horizontalPosDataProperty, owlOntology);
				
				// Print all the values
				for(OWLLiteral value : values)
				{
					System.out.println(value.getLiteral());
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	
	/*
	 * METHODS TO INSERT OBJECT PROPERTIES
	 */
	
	/**
	 * Insert a connection between two components, a start and an end ones
	 * 
	 * @param startElement component's name, composing the URI, of the element from which starts the 
	 * unidirectional connection
	 * @param endElement component's name, composing the URI, of the element to which ends the 
	 * unidirectional connection
	 * @return Boolean
	 */
	public Boolean insertConnection(String startElement, String endElement) {
		
		System.out.println("Insertion of connection between components");
		
		// Check if the start element belongs to the domain and if the end element belongs
		// to the range of the Object Property, both for the direct and the inverse
		Boolean canBeAddedStartDirect = false;
		Boolean canBeAddedEndDirect = false;
		Boolean canBeAddedStartInverse = false;
		Boolean canBeAddedEndInverse = false;
		NodeSet<OWLClass> domains;
		NodeSet<OWLClass> ranges;
		
		// Get the classes to which the start element belongs
		OWLNamedIndividual startIndividual = owlDataFactory.getOWLNamedIndividual(startElement, prefixManager);
		NodeSet<OWLClass> startTypes = owlReasoner.getTypes(startIndividual, false);
		
		// Get the classes to which the end element belongs
		OWLNamedIndividual endIndividual = owlDataFactory.getOWLNamedIndividual(endElement, prefixManager);
		NodeSet<OWLClass> endTypes = owlReasoner.getTypes(endIndividual, false);

		// Retrieve the connectsTo Object Property, the direct one
		OWLObjectProperty connectsTo = owlDataFactory.getOWLObjectProperty("connectsTo", prefixManager);
		
		System.out.println("Checking direct property: " + prefixManager.getShortForm(connectsTo.getIRI()).substring(1));
		
		// Retrieve the domains of the Object Property and check if the start element's domain is 
		// listed inside of them
		domains = owlReasoner.getObjectPropertyDomains(connectsTo, true);
		for(OWLClass domain : domains.getFlattened())
		{
			System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
			if(startTypes.getFlattened().contains(domain))
			{
				canBeAddedStartDirect = true;
			}
		}

		// Retrieve the ranges of the Object Property and check if the end element's range is 
		// listed inside of them
		ranges = owlReasoner.getObjectPropertyRanges(connectsTo, true);
		for(OWLClass range : ranges.getFlattened())
		{
			System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
			if(endTypes.getFlattened().contains(range))
			{
				canBeAddedEndDirect = true;
			}
		}
		
		// Retrieve the isConnectedTo Object Property, the inverse one
		OWLObjectProperty isConnectedTo = owlDataFactory.getOWLObjectProperty("isConnectedTo", prefixManager);
		
		System.out.println("Checking inverse property: " + prefixManager.getShortForm(isConnectedTo.getIRI()).substring(1));
		
		// Retrieve the domains of the Object Property and check if the start element's domain is 
		// listed inside of them
		domains = owlReasoner.getObjectPropertyDomains(isConnectedTo, true);
		for(OWLClass domain : domains.getFlattened())
		{
			System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
			if(endTypes.getFlattened().contains(domain))
			{
				canBeAddedStartInverse = true;
			}
		}

		// Retrieve the ranges of the Object Property and check if the end element's range is 
		// listed inside of them
		ranges = owlReasoner.getObjectPropertyRanges(isConnectedTo, true);
		for(OWLClass range : ranges.getFlattened())
		{
			System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
			if(startTypes.getFlattened().contains(range))
			{
				canBeAddedEndInverse = true;
			}
		}
		
		// If both the start and end element have the classes, then add the Object Property
		if(canBeAddedStartDirect && canBeAddedEndDirect && canBeAddedStartInverse && canBeAddedEndInverse)
		{
			System.out.println("Adding direct property: " + prefixManager.getShortForm(connectsTo.getIRI()).substring(1));
			
			// Create the axiom of the direct Object Property and add it to the ontology to be applied
			OWLObjectPropertyAssertionAxiom directAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(connectsTo, startIndividual, endIndividual);
	       
			AddAxiom addDirectAxiom = new AddAxiom(owlOntology, directAxiom);
			owlOntologyManager.applyChange(addDirectAxiom);
					
			System.out.println("Adding inverse property: " + prefixManager.getShortForm(isConnectedTo.getIRI()).substring(1));
			
			// Create the axiom of the inverse Object Property and add it to the ontology to be applied
			OWLObjectPropertyAssertionAxiom inverseAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(isConnectedTo, endIndividual, startIndividual);
	       
			AddAxiom addInverseAxiom = new AddAxiom(owlOntology, inverseAxiom);
			owlOntologyManager.applyChange(addInverseAxiom);
			
	        try 
	        {
				owlOntologyManager.saveOntology(owlOntology);
			} 
	        catch (OWLOntologyStorageException e) 
	        {
				System.out.println("Error while saving the ontology");
				e.printStackTrace();
			}
		
	        return true;
	        
		}
		else
		{
			System.out.println("Object property can't be applied to these individuals");
			return false;
		}
	}
	
	/**
	 * Insert a parent-son relationship between two components of the system's hierarchy 
	 * 
	 * @param parentElement component's name, composing the URI, of the parent element
	 * @param childElement component's name, composing the URI, of the child element
	 * @return Boolean
	 */
	public Boolean insertInclusion(String parentElement, String childElement) {
		
		System.out.println("Insertion of inclusion between components");
		
		// Check if the parent element belongs to the domain and if the child element belongs
		// to the range of the Object Property, both for the direct and the inverse
		Boolean canBeAddedParentDirect = false;
		Boolean canBeAddedChildDirect = false;
		Boolean canBeAddedParentInverse = false;
		Boolean canBeAddedChildInverse = false;
		NodeSet<OWLClass> domains;
		NodeSet<OWLClass> ranges;
		
		// Get the classes to which the parent element belongs
		OWLNamedIndividual parentIndividual = owlDataFactory.getOWLNamedIndividual(parentElement, prefixManager);
		NodeSet<OWLClass> parentTypes = owlReasoner.getTypes(parentIndividual, false);
		
		// Get the classes to which the child element belongs
		OWLNamedIndividual childIndividual = owlDataFactory.getOWLNamedIndividual(childElement, prefixManager);
		NodeSet<OWLClass> childTypes = owlReasoner.getTypes(childIndividual, false);

		// Retrieve the includes Object Property, the direct one
		OWLObjectProperty includes = owlDataFactory.getOWLObjectProperty("includes", prefixManager);
		
		System.out.println("Checking direct property: " + prefixManager.getShortForm(includes.getIRI()).substring(1));
		
		// Retrieve the domains of the Object Property and check if the parent element's domain is 
		// listed inside of them
		domains = owlReasoner.getObjectPropertyDomains(includes, true);
		for(OWLClass domain : domains.getFlattened())
		{
			System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
			if(parentTypes.getFlattened().contains(domain))
			{
				canBeAddedParentDirect = true;
			}
		}

		// Retrieve the ranges of the Object Property and check if the child element's range is 
		// listed inside of them
		ranges = owlReasoner.getObjectPropertyRanges(includes, true);
		for(OWLClass range : ranges.getFlattened())
		{
			System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
			if(childTypes.getFlattened().contains(range))
			{
				canBeAddedChildDirect = true;
			}
		}
		
		// Retrieve the isIncluded Object Property, the inverse one
		OWLObjectProperty isIncluded = owlDataFactory.getOWLObjectProperty("isIncluded", prefixManager);
		
		System.out.println("Checking inverse property: " + prefixManager.getShortForm(isIncluded.getIRI()).substring(1));
		
		// Retrieve the domains of the Object Property and check if the start element's domain is 
		// listed inside of them
		domains = owlReasoner.getObjectPropertyDomains(isIncluded, true);
		for(OWLClass domain : domains.getFlattened())
		{
			System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
			if(childTypes.getFlattened().contains(domain))
			{
				canBeAddedParentInverse = true;
			}
		}

		// Retrieve the ranges of the Object Property and check if the end element's range is 
		// listed inside of them
		ranges = owlReasoner.getObjectPropertyRanges(isIncluded, true);
		for(OWLClass range : ranges.getFlattened())
		{
			System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
			if(parentTypes.getFlattened().contains(range))
			{
				canBeAddedChildInverse = true;
			}
		}
		
		// If both the parent and child element have the classes, then add the Object Property
		if(canBeAddedParentDirect && canBeAddedChildDirect && canBeAddedParentInverse && canBeAddedChildInverse)
		{
			System.out.println("Adding direct property: " + prefixManager.getShortForm(includes.getIRI()).substring(1));
			
			// Create the axiom of the direct Object Property and add it to the ontology to be applied
			OWLObjectPropertyAssertionAxiom directAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(includes, parentIndividual, childIndividual);
	       
			AddAxiom addDirectAxiom = new AddAxiom(owlOntology, directAxiom);
			owlOntologyManager.applyChange(addDirectAxiom);
					
			System.out.println("Adding inverse property: " + prefixManager.getShortForm(isIncluded.getIRI()).substring(1));
			
			// Create the axiom of the inverse Object Property and add it to the ontology to be applied
			OWLObjectPropertyAssertionAxiom inverseAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(isIncluded, childIndividual, parentIndividual);
	       
			AddAxiom addInverseAxiom = new AddAxiom(owlOntology, inverseAxiom);
			owlOntologyManager.applyChange(addInverseAxiom);
			
	        try 
	        {
				owlOntologyManager.saveOntology(owlOntology);
			} 
	        catch (OWLOntologyStorageException e) 
	        {
				System.out.println("Error while saving the ontology");
				e.printStackTrace();
			}
		
	        return true;
	        
		}
		else
		{
			System.out.println("Object property can't be applied to these individuals");
			return false;
		}
	}
	
	/**
	 * Insertion of a generic Object Property describing an operation, needed between a start and an end element inside the ontology
	 * 
	 * @param startElement component's name, composing the URI, of the element from which starts the 
	 * unidirectional connection
	 * @param endElement component's name, composing the URI, of the element to which ends the 
	 * unidirectional connection
	 * @param operationName name of the operation to insert
	 * @return
	 */
	public Boolean insertOperation(String startElement, String endElement, String operationName) {
		
		System.out.println("Insertion of operation between components");
		
		// Check if the start element belongs to the domain and if the end element belongs
		// to the range of the Object Property, both for the direct and the inverse
		Boolean canBeAddedStartDirect = false;
		Boolean canBeAddedEndDirect = false;
		Boolean canBeAddedStartInverse = false;
		Boolean canBeAddedEndInverse = false;
		NodeSet<OWLClass> domains;
		NodeSet<OWLClass> ranges;
		
		// Retrieve the generic Object Property from the ontology and its inverse property
		OWLObjectProperty operation = owlDataFactory.getOWLObjectProperty(operationName, prefixManager);
		
		// Retrieve the parent Object Property, the one containing all the others
		OWLObjectProperty doOperation = owlDataFactory.getOWLObjectProperty("doOperation", prefixManager);
		
		// Check if the Object Property really exists inside the ontology, only if it exists the 
		// property can be added to the ontology
		Boolean found = false;
		
		if(operation == null)
		{
			System.out.println("Object property not found in this ontology");
			return false;
		}
		
		// Take the parent property of the direct Object Property
		if(operation.getSuperProperties(owlOntology) != null)
		{
			for(OWLObjectPropertyExpression propertyExpression : operation.getSuperProperties(owlOntology))
			{
				// If the parent property is the doOperation one, then the property can be added
				if(propertyExpression.getNamedProperty().getIRI().equals(doOperation.getIRI()))
				{
					found = true;
				}
			}
		}
		else
		{
			System.out.println("Parent object property not found in this ontology");
			return false;
		}
		
		if(found)
		{
			System.out.println("Object property correctly belonging to doOperation parent object property");
			
			// Get the classes to which the start element belongs
			OWLNamedIndividual startIndividual = owlDataFactory.getOWLNamedIndividual(startElement, prefixManager);
			NodeSet<OWLClass> startTypes = owlReasoner.getTypes(startIndividual, false);
			
			// Get the classes to which the end element belongs
			OWLNamedIndividual endIndividual = owlDataFactory.getOWLNamedIndividual(endElement, prefixManager);
			NodeSet<OWLClass> endTypes = owlReasoner.getTypes(endIndividual, false);
	
			System.out.println("Checking direct property: " + prefixManager.getShortForm(operation.getIRI()).substring(1));
			
			// Retrieve the domains of the Object Property and check if the start element's domain is 
			// listed inside of them
			domains = owlReasoner.getObjectPropertyDomains(operation, true);
			for(OWLClass domain : domains.getFlattened())
			{
				System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
				if(startTypes.getFlattened().contains(domain))
				{
					canBeAddedStartDirect = true;
				}
			}
	
			// Retrieve the ranges of the Object Property and check if the end element's range is 
			// listed inside of them
			ranges = owlReasoner.getObjectPropertyRanges(operation, true);
			for(OWLClass range : ranges.getFlattened())
			{
				System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
				if(endTypes.getFlattened().contains(range))
				{
					canBeAddedEndDirect = true;
				}
			}
			
			// Retrieve the inverse Object Property
			OWLObjectProperty inverseOperation = operation.getInverses(owlOntology).iterator().next().asOWLObjectProperty();
			
			System.out.println("Checking inverse property: " + prefixManager.getShortForm(inverseOperation.getIRI()).substring(1));
			
			// Retrieve the domains of the Object Property and check if the start element's domain is 
			// listed inside of them
			domains = owlReasoner.getObjectPropertyDomains(inverseOperation, true);
			for(OWLClass domain : domains.getFlattened())
			{
				System.out.println(" - domain: " + prefixManager.getShortForm(domain.getIRI()).substring(1));
				if(endTypes.getFlattened().contains(domain))
				{
					canBeAddedStartInverse = true;
				}
			}
	
			// Retrieve the ranges of the Object Property and check if the end element's range is 
			// listed inside of them
			ranges = owlReasoner.getObjectPropertyRanges(inverseOperation, true);
			for(OWLClass range : ranges.getFlattened())
			{
				System.out.println(" - range: " + prefixManager.getShortForm(range.getIRI()).substring(1));
				if(startTypes.getFlattened().contains(range))
				{
					canBeAddedEndInverse = true;
				}
			}
			
			// If both the parent and child element have the classes, then add the Object Property
			if(canBeAddedStartDirect && canBeAddedEndDirect && canBeAddedStartInverse && canBeAddedEndInverse)
			{
				System.out.println("Adding direct property: " + prefixManager.getShortForm(operation.getIRI()).substring(1));
				
				// Create the axiom of the direct Object Property and add it to the ontology to be applied
				OWLObjectPropertyAssertionAxiom directAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(operation, startIndividual, endIndividual);
		       
				AddAxiom addDirectAxiom = new AddAxiom(owlOntology, directAxiom);
				owlOntologyManager.applyChange(addDirectAxiom);
						
				System.out.println("Adding inverse property: " + prefixManager.getShortForm(inverseOperation.getIRI()).substring(1));
				
				// Create the axiom of the inverse Object Property and add it to the ontology to be applied
				OWLObjectPropertyAssertionAxiom inverseAxiom = owlDataFactory.getOWLObjectPropertyAssertionAxiom(inverseOperation, endIndividual, startIndividual);
		       
				AddAxiom addInverseAxiom = new AddAxiom(owlOntology, inverseAxiom);
				owlOntologyManager.applyChange(addInverseAxiom);
				
		        try 
		        {
					owlOntologyManager.saveOntology(owlOntology);
				} 
		        catch (OWLOntologyStorageException e) 
		        {
					System.out.println("Error while saving the ontology");
					e.printStackTrace();
				}
			
		        return true;
		        
			}
			else
			{
				System.out.println("Object property can't be applied to these individuals");
				return false;
			}
		}
		else
		{
			System.out.println("Object property don't belong to doOperation parent object property");
			return false;
		}
	}
	
	
	/*
	 * METHODS TO LIST OBJECT PROPERTIES
	 */
	
	/**
	 * Retrieve the list of all the elements with the Object Property of Connection, both direct and inverse ones
	 * @return Boolean
	 */
	public Boolean getIndividualWithConnectionProperty() {
		
		System.out.println("Retrieving all the individuals with\n - property: connectsTo\n - inverse property: isConnectedTo\n");
		
		// Retrieve the connectsTo Object Property from the ontology
		OWLObjectProperty connectsToObjectProperty = owlDataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "connectsTo"));

		// For each axiom linked to the Object Property
		for(OWLAxiom axiom : connectsToObjectProperty.getReferencingAxioms(owlOntology))
		{
			// If the axiom is linked to some individuals
			if(!axiom.getIndividualsInSignature().isEmpty())
			{
				System.out.println("  - Axiom found");
				
				// Print the individuals linked to the Object Property
				for(OWLNamedIndividual individual : axiom.getIndividualsInSignature())
				{
					
					System.out.println("   - " + prefixManager.getShortForm(individual.getIRI()).substring(1));
				}
				
				System.out.println();
			}
			else
			{
				System.out.println("Object property is not linked to any individual");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Retrieve the list of all the elements with the Object Property of Inclusion, both direct and inverse ones
	 * @return Boolean
	 */
	public Boolean getIndividualWithInclusionProperty() 
	{
		
		System.out.println("Retrieving all the individuals with\n - property: includes\n - inverse property: isIncluded\n");
		
		// Retrieve the connectsTo Object Property from the ontology
		OWLObjectProperty includesToObjectProperty = owlDataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "includes"));

		// For each axiom linked to the Object Property
		for(OWLAxiom axiom : includesToObjectProperty.getReferencingAxioms(owlOntology))
		{
			// If the axiom is linked to some individuals
			if(!axiom.getIndividualsInSignature().isEmpty())
			{
				System.out.println("  - Axiom found");
				
				// Print the individuals linked to the Object Property
				for(OWLNamedIndividual individual : axiom.getIndividualsInSignature())
				{
					
					System.out.println("   - " + prefixManager.getShortForm(individual.getIRI()).substring(1));
				}
				
				System.out.println();
			}
			else
			{
				System.out.println("Object property is not linked to any individual");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Retrieve the list of all the elements with a generic operation Object Property, both direct and inverse ones
	 * @return Boolean
	 */
	public Boolean getIndividualWithOperationProperty(String operationName) 
	{
		
		System.out.println("Retrieving all the individuals with\n - property: " + operationName);
		
		// Retrieve the connectsTo Object Property from the ontology
		OWLObjectProperty operationObjectProperty = owlDataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + operationName));
		
		// Retrieve the parent Object Property, the one containing all the others
		OWLObjectProperty doOperation = owlDataFactory.getOWLObjectProperty("doOperation", prefixManager);
		
		// Check if the Object Property really exists inside the ontology, only if it exists the 
		// property can be added to the ontology
		Boolean found = false;
		
		if(operationObjectProperty == null)
		{
			System.out.println("Object property not found in this ontology");
			return false;
		}
		
		// Take the parent property of the direct Object Property
		if(operationObjectProperty.getSuperProperties(owlOntology) != null)
		{
			for(OWLObjectPropertyExpression propertyExpression : operationObjectProperty.getSuperProperties(owlOntology))
			{
				// If the parent property is the doOperation one, then the property can be added
				if(propertyExpression.getNamedProperty().getIRI().equals(doOperation.getIRI()))
				{
					found = true;
				}
			}
		}
		else
		{
			System.out.println("Parent object property not found in this ontology");
			return false;
		}
		
		if(found)
		{
			// For each axiom linked to the Object Property
			for(OWLAxiom axiom : operationObjectProperty.getReferencingAxioms(owlOntology))
			{
				// If the axiom is linked to some individuals
				if(!axiom.getIndividualsInSignature().isEmpty())
				{
					System.out.println("  - Axiom found");
					
					// Print the individuals linked to the Object Property
					for(OWLNamedIndividual individual : axiom.getIndividualsInSignature())
					{
						System.out.println("   - " + prefixManager.getShortForm(individual.getIRI()).substring(1));
					}
					
					System.out.println();
				}
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	

	// SIMPLE MAIN EXAMPLE
	public static void main(String[] args)
	{
		OntologyManualInsertion prova = new OntologyManualInsertion(".\\ontologies\\ToolsOntology.owl", false);
//		Boolean result = prova.insertComponentLevel("Bristol", 1);
		
//		prova.insertComponentName("Robertson", "punta quadrata", "it");
//		prova.insertComponentLevel("Bristol", 0);
//		prova.insertAdjectivePosition("Bristol", "down");
//		prova.insertHorizontalPosition("Bristol", 32.2);
//		prova.insertVerticalPosition("Bristol", 23.1);
//		prova.insertComponentSize("Bristol", 2.1);
		
//		prova.insertConnection("ComponentProva", "ConnectionProva");	
//		prova.insertInclusion("ComponentProva", "ChildProva");
		prova.insertOperation("Line", "ComponentProva", "lifts");
		
//		prova.getIndividualWithConnectionProperty();
//		prova.getIndividualWithInclusionProperty();
		
//		prova.showOntologyDataProperties();
		prova.showOntologyObjectProperties();
		
		prova.getIndividualWithOperationProperty("lifts");
//		prova.getComponentName("Bristol");
//		prova.getComponentLevel("Bristol");
//		prova.getAdjectivePosition("Bristol");
//		prova.getHorizontalPosition("Bristol");
//		prova.getVerticalPosition("Bristol");
//		prova.getComponentSize("Bristol");
	}

}
