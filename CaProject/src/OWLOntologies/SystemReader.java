package OWLOntologies;

// Java standard library import
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// OWL API library import
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

/**
* The class analyzes the system's file saved as a ".csv":
 * -> FIRST  : check the extension file, 1 ^ reading of the file to check the existence of multiple root
 *			   * CHECK IF THE VALUE OF THE ELEMENT NUMBER OF THE ROOT IN THE BOM, OBTAINED BY THE 3D INVENTOR'S MODEL 
 *				 IS THE SAME FOR ALL THE MATERIALS *
 * -> SECOND : if the FIRST phase is positive, make an copy of the ontology and save it with the name of the root
 * -> THIRD  : create a tree structure during the reading of the file
 * 		       * FOR EACH NODE CHECK THE TYPE (AtomicPart, AtomicComponent, CompositeComponent) *
 * -> FOURTH : populate the ontology with the system components and parts in a hierarchical way
 * 			   * BASED ON THE NODE TYPE, ESTABILISH WHO IS THE FATHER AND WHO IS THE CHILD *
 * -> FIFTH  : in case the "screw" parts appear, put them in the correct class based on the screw type
 * 			   * CHECK THE NAME OF THE PART, IF IT CONTAINS THE TERM "SCREW" THE PART IS A SCREW *
 * 			   * CHECK IF IN THE PART'S NAME CONTAIS THE SCREW'S TYPE AND CHECK IF IT EXISTS INTO THE ONTOLOGY *
 * 		       * IF THE SCREW TYPE DOES NOT EXIST INTO THE ONTOLOGY THE SCREW IS COSIDER AS GENERIC * 

 * Serve per analizzare il file del sistema salvato come csv, 
 * -> prima cosa : controllo estensione file + 1^ lettura della testata + 1^ lettura file;( salvare nome sistema) OK
 * -> seconda cosa : se prima fase positiva copia dell'ontologia e salvarla su un file col nome del sistema (nome in 1)
 * -> terza cosa: creare la struttura ad albero leggendo il file in csv
 * 				 * Per ogni nodo verificarne il tipo (AtomicPart, CompositeComponet, AtomicComponent)
 * -> quarta cosa: popolare l'ontologia con il sistema facendo i giusti collegamenti
 * 				 * Verificare il tipo di nodo e da li fare i collegamenti col padre (chiedere per il collegamento con Strumento Mano )
 * -> quinta cosa: controllo elementi vite e ,se non specificato il tipo metterlo nelle generiche viti (smontabili da un cacciavite generico)
 * 				 * Controllare dal nome del tipo di nodo se è una vite e verificare se nel nome sia compreso il tipo di vite tra quelli nell'ontologias
 * 			     * Se si prendo quella vite e la definisco parte del componente e la inserisco nel tipo di vita considerata
 * 				 * Se no lo considero come vite generica (o non specificata)
 */


public class SystemReader {
	
	// General useful instances
	private String nameFile;
	private String pathFileSystem;
	private String nameSystem;
	private Node<String> systemTree;
	private File sysFile;
	
	// OWL API instances
	private OWLOntologyManager manager;
	private OWLOntology sysOntology = null;
	private OWLReasoner reasoner = null;
	private OWLReasonerFactory reasonerFactory = null;
	private OWLReasonerConfiguration config = null;
	private OWLDataFactory dataFactory = null;
	private OWLClass atomicComponent, compositeComponent, atomicPart;
	private OWLObjectPropertyExpression partOf;
	private ConsoleProgressMonitor progessMonitor = null;
	private DefaultPrefixManager prefixManager = null;

	public SystemReader()
	{
		super();
		nameFile = "";
		nameSystem = "";
		pathFileSystem = "";
	}
	
	/** 
	 * The method defines the file name received as input as default  and checks the file correctness.
	 * If the extension is wrong or the object defined into the file does not follow the basic requirement, 
	 * the method returns false.
	 * In the opposite case, It returns true and set the file name as default.
	 * 
	 * Definisce come default il nome del file inserito e controlla la correttezza del file. 
	 * Nel caso l'estensione sia sbagliata o l'oggetto descritto non rispetti il requisito base per il suo 
	 * funzionamento allora il metodo ritorna false
	 * In caso contrario ritorna true e setta internamente il nome del file
	 */
	private boolean addAndCheckFile(String fileName)
	{
		Boolean flag = false;
		String[] piece = fileName.split("\\.");
		
		// Check the file's extension, in this case is the string after the last "." in case there are more than one "."
		if(!piece[piece.length-1].equals("csv"))
		{
			System.out.println("Error - file extension not correct!");
			return false;
		}
		
		// Before the first file's reading check if the system has more than one root 
		BufferedReader reader;
		try
		{
			reader = new BufferedReader(new FileReader(fileName));
			
			// Support variables
			int i = 0;
			int rootNumber = 0;
			
			// Start file's reading
		    String line;
		    while ((line = reader.readLine()) != null)
		    {
		    	// Jump the header line
		    	if (i == 0)
		    	{
		    		i++;
		    		continue;
		    	}
		    		
		    	// The file is a ".csv" one
		    	piece = line.split(";");
		    	
		    	// The system name is saved, is in the first line after the header
		    	if(i == 1 && piece[0].length() == 1)
		    	{
		    		rootNumber = Integer.parseInt(piece[0]);
		    		this.nameSystem = piece[1];
		    		this.nameSystem = this.nameSystem.replaceAll(" ", "_");
		    		i++;
		    		continue;
		    	}
		    	
		    	String[] numberPiece = piece[0].split("\\.");
		    	
		    	// Check if exists more than one root in the system
		    	if(Integer.parseInt(numberPiece[0]) > rootNumber)
		    	{
		    		flag = true;
		    		break;
		    	}
		    }
		    reader.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// If the file does not respect the requirements, the method returns false
		if(flag)
		{
			System.out.println("Error - the object hierarchy is not as the stardand requires: More than one root system found");
			nameSystem = "";
			return false;
		}
		this.nameFile = fileName;
		
		System.out.println("Correct file format insert! System name: "+ this.nameSystem);
		return true;
	}
	
	
	/**
	 * The method creates a new ontology copied from the base model. There is no system elements inside it.
	 * If a file with the same name already exists the methods does not overwrite it and returns false.
	 * 
	 * Crea una nuova ontologia copia partendo dal modello base senza elementi al suo interno
	 * Se non trova il file, il nome non è settato o ci sono problemi nella lettura ritorna false
	 */
	private boolean newOntologySystem()
	{
		// Take the source file and create a copy with a different name
		File source = new File("ontologies/ToolsOntology_2.owl");
		this.pathFileSystem = "ontologies/"+ this.nameSystem + ".owl";
	    File dest = new File(pathFileSystem);
	
	    // Check all the possible problems with the system name or the existence of the file for avoid overwrite it
	    if(this.nameSystem.equals("") || this.nameSystem == null)
	    {
			System.out.println("Problem with the name of the system, it could be null or not defined before");
			return false;
		}
	    if(dest.exists())
	    {
	    	System.out.println("File already exists");
	    	return false;
	    }
	    
	    // Simple copy-cycle
	    InputStream is = null;
	    OutputStream os = null;
	    try
	    {
	    	is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        
	        byte[] buffer = new byte[1024];
	        int length;
	        
	        while ((length = is.read(buffer)) > 0)
	        {
	            os.write(buffer, 0, length);
	        }

	        is.close();
	        os.close();
	    }
	    catch (IOException e)
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dest.delete();
		
			return false;
		}
		return true;
	}

	/**
	 * The method inserts the system components and parts into the new ontology just created
	 */
	private void addSystemToOntology()
	{
	
		// Initialize the class 
		manager = OWLManager.createOWLOntologyManager();
		sysFile = new File(pathFileSystem);
		
		// Load all the information necessary to proceed 
		try
		{
			sysOntology  = manager.loadOntologyFromOntologyDocument(sysFile);
			IRI location = manager.getOntologyDocumentIRI(sysOntology);

			System.out.println("Loaded ontology: " + sysOntology.getOntologyID());
			System.out.println("\tfrom: " + location); 
			
			// Get and Configure HermiT reasoner + creation of the data factory
			reasonerFactory = new ReasonerFactory();
			progessMonitor  = new ConsoleProgressMonitor();
			config          = new SimpleConfiguration(progessMonitor);
		
			prefixManager = new DefaultPrefixManager("http://www.semanticweb.org/chiara/ontologies/2016/11/toolsontology#");

			dataFactory = manager.getOWLDataFactory();
			
			reasoner = reasonerFactory.createReasoner(sysOntology, config);
			reasoner.precomputeInferences(InferenceType.values());
	        

			// Get all the owl class that needed for the algorithm
			compositeComponent = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), "CompositeComponent"));
			atomicComponent    = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), "AtomicComponent"));
			atomicPart         = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), "AtomicPart"));
			partOf  	       = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "partOf"));
			
			// Call the insertSystemInTheOntology method
			insertSystemInTheOntology(this.systemTree, 0);
		
		}
		catch(OWLOntologyCreationException | OWLOntologyStorageException e)
		{
			e.printStackTrace();
		}

	}
	
	/**
	 * This method find the first screw type that satisfied the component into the system and insert it.
	 */
	private OWLIndividual findScrewType(String screw)
	{
		// For do this operation is needed the base ontology for avoid error
		OntologyReader toolOntologyReader = new OntologyReader("ontologies\\ToolsOntology_2.owl");
		List<String> listScrew = toolOntologyReader.getAllIstanceInClass("Screw");
		
		String lastScrew = "";
		OWLClass superClass = null;
		OWLNamedIndividual elementScrew = null;
		
		// Main cycle for all the screw into the "toolsOntology"
		for(String s : listScrew)
		{
			// Retrieve the individual and the list of names (if they exists) for that screw into the "toolsOntology"
			elementScrew = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(), s));
			List<String> nameScrew = toolOntologyReader.getListOfDataPropertyOfIndividual("name", s);
			
			// Obtain the name of the screw (used for retrieve the screwdriver)
			lastScrew = s.replace("Screw", "");
			String genericName = screw;
			
			// Use a regex to find duplicated elements
			Pattern p = Pattern.compile("_\\d+$");
			Matcher m = p.matcher(genericName);
			if (m.find())
			{
				genericName = genericName.replaceFirst("_\\d+$", "");
			}
			
			// In case the screw of the system has more than one word as name...
			String[] partsScrewName = genericName.split("_");
			boolean findScrew = false;
	
			for(String partName : partsScrewName)
			{
				// If only one word makes the match... set the "findScrew" flag as true
				if(lastScrew.toLowerCase().contains(partName.toLowerCase()))
				{
					findScrew = true;
				}
			}
			
			// Check if a match with the official screw name is found
			if(findScrew)
			{
				String nameClass = toolOntologyReader.getSuperClassOfIndividual(s);
				superClass = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(),nameClass));
				break;
			}
			else
			{
				// Check if the non-official screw name are contained into the screw received in input
				for(String l: nameScrew)
				{
					// In case of matching...
					if(genericName.toLowerCase().contains(l.toLowerCase()))
					{
						// Obtain the superclass of the individual if it matches
						String nameClass = toolOntologyReader.getSuperClassOfIndividual(s);
						superClass = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(),nameClass));
						break;
					}
				}
				
				// Needed for exit from the second cycle
				if(superClass != null)
				{
					break;
				}
			}
		}
		
		// All the operation to add all the information into the sysOntology
		OWLObjectPropertyExpression doOperation = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "doOperation"));
		OWLObjectPropertyExpression unscrews    = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "unscrews"));
		OWLObjectPropertyExpression screws      = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "screws"));
		OWLNamedIndividual newScrew = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),screw));

		OWLNamedIndividual screwDriver = null;

		// Check if the screw has a known type or not, in the case if it is not found use a generic screwdriver and 
		// insert the screw into the GenericScrew class
		if(superClass != null)
		{
			// Particular case if the screw is a Thumb/Finger, in this case the "screwdriver" is an hand
			if(lastScrew.equals("Thumb"))
			{
				screwDriver = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),"Hand"));
			}
			else
			{
				screwDriver = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),lastScrew));
			}
		}
		else
		{
			// Case of a generic screw
			superClass = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(),"GenericScrew"));
			screwDriver = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),"GenericScrewDriver"));
		}
		
		// Define all the axiom and add them to the ontology
		OWLAxiom classAxiom = dataFactory.getOWLClassAssertionAxiom(superClass, newScrew);
		OWLAxiom screwAxiom = dataFactory.getOWLObjectPropertyAssertionAxiom(screws, screwDriver, newScrew);
		OWLAxiom unscrewAxiom = dataFactory.getOWLObjectPropertyAssertionAxiom(unscrews, screwDriver, newScrew);
		OWLAxiom doOperationAxiom = dataFactory.getOWLObjectPropertyAssertionAxiom(doOperation, screwDriver, newScrew);
		
		// Add them into the ontology
		manager.addAxiom(sysOntology, screwAxiom);
		manager.addAxiom(sysOntology, unscrewAxiom);
		manager.addAxiom(sysOntology, doOperationAxiom);
		manager.addAxiom(sysOntology, classAxiom);
		
		// Save them into the sysOntology
        try
        {
        	manager.saveOntology(sysOntology);
			return newScrew;
		}
        catch (OWLOntologyStorageException e)
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        // In case of error or a throw exception return null
		return null;
	}
	
	
	/* Last check after the reading of the system hierarchy for the composite components.
	 * The operation is recursive 
	 */
	private void checkTree(Node<String> node)
	{
		// If a child of a child of the current node has children, the current node is a composite component
		for(Node<String> child : node.getChildren())
		{
			// Check if the child has children
			if(child.hasChildren())
			{
				// If yes, check if one of his children has again... children
				for(Node<String> childOfChild : node.getChildren())
				{
					if(childOfChild.hasChildren())
					{
						// If yes, change the node type
						node.setNodeType("CompositeComponent");
					}
				}
				checkTree(child);
			}		
		}
	}
	
	
	/* Recursively, add the components into the ontology*/
	
	private void insertSystemInTheOntology(Node<String> prova, int i) throws OWLOntologyStorageException
	{
		// Retrieve all the useful information from the sysOntology and from the current node
		OWLDataProperty level    = dataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix(),"level"));
		OWLDataProperty quantity = dataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix(),"quantity"));
		OWLIndividual fatherComponent = null;
		
		// Obtain from the node some useful information
		String type = prova.getNodeType();
		Integer qnt = prova.getQuantity();
		String id = prova.getData();
		
		// In case of root node, add it first... then the children
		if(i == 0)
		{
			// Insert the father and save it
			OWLIndividual component = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),id));
			fatherComponent = component;
			
			// Set the axioms
			OWLAxiom axiomComponent = dataFactory.getOWLClassAssertionAxiom(compositeComponent, component);
			OWLAxiom levelAxiom = dataFactory.getOWLDataPropertyAssertionAxiom(level, component, i);
			OWLAxiom quantityAxiom = dataFactory.getOWLDataPropertyAssertionAxiom(quantity, component, qnt);
			
			// Add and save them into the ontology
			manager.addAxiom(sysOntology, levelAxiom);
			manager.addAxiom(sysOntology, quantityAxiom);
			manager.addAxiom(sysOntology, axiomComponent);
	        manager.saveOntology(sysOntology);
	        
	        // Set the next depth level for the children
	        i++;
		}
		else
		{
			// In case a generic node, obtain the father
			fatherComponent = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),id));
		}
		
		// Cycle for all the child of the current node
		for(Node<String> child : prova.getChildren())
		{
			// In case exists more than one component with the same name
			for(int k = 0; k < child.getQuantity(); k++)
			{
				id = child.getData();				
				
				// Rename the name in case the quantity is bigger than 1
				if(child.getQuantity() > 1)
				{
					// Use a regex for add "_NUMBER" to the component id
					Pattern p = Pattern.compile("_\\d+$");
					Matcher m = p.matcher(id);
					if (!m.find())
					{
						// In case of the first element 
						id = id.concat("_"+(k+1));
					}
					else
					{
						// For the other ones
						id = id.replaceFirst("_\\d+$", "_" +(k+1)); 				
					}
					child.setData(id);
				}
				
				// For the child, obtain useful information
				type = child.getNodeType();
				qnt = child.getQuantity();
				OWLIndividual component  = null;
				
				// Check if the component is a screw or not
				if(type.equals("Screw"))
				{
					// If it is, go to the findScrewType subroutine
					component = this.findScrewType(id);
				}
				else
				{
					// If not, check the correct component class and add it into the ontology as an axiom
					component = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),id));
					
					// Obtain the axiom
					OWLAxiom componentAxiom = null;
					if(type.equals("AtomicPart"))
					{
						componentAxiom = dataFactory.getOWLClassAssertionAxiom(atomicPart, component);
					}
					if(type.equals("AtomicComponent"))
					{
						componentAxiom = dataFactory.getOWLClassAssertionAxiom(atomicComponent, component);
					}
					if(type.equals("CompositeComponent"))
					{
						componentAxiom = dataFactory.getOWLClassAssertionAxiom(compositeComponent, component);
					}
					
					// Add it to the ontology
					manager.addAxiom(sysOntology, componentAxiom);
				}
				
				// Add the rest of axioms into the ontology and save it
				OWLAxiom levelAxiom = dataFactory.getOWLDataPropertyAssertionAxiom(level, component, i);
				OWLAxiom quantityAxiom = dataFactory.getOWLDataPropertyAssertionAxiom(quantity, component, qnt);
				OWLObjectPropertyAssertionAxiom axiomFather = dataFactory.getOWLObjectPropertyAssertionAxiom(partOf, component, fatherComponent);
				
				// Add them to the ontology...
				manager.addAxiom(sysOntology, levelAxiom);
				manager.addAxiom(sysOntology, quantityAxiom);
				manager.addAxiom(sysOntology, axiomFather);
		       
				// ... and save them
				manager.saveOntology(sysOntology);
				
		        // In case the child has children.... SIGH recall the method... recursive is bad in java :(
				if(child.hasChildren())
				{
					insertSystemInTheOntology(child,i+1);
				}
			}
		}
	}
	
	/**
	 * The method create a tree based on the file structure received in input
	 */
	private void createTreeSystem()
	{
		BufferedReader reader;
		String[] piece;
		String[] position;
		String nameObject;
		Integer currentDim = 0, previousDim = 0;
		Node<String> currentNode = new Node<String>();
		
		try
		{
			reader = new BufferedReader(new FileReader(this.nameFile));
			
			// Read the file
		    String line;
		    Integer count = 0;
		    while ((line = reader.readLine()) != null)
		    {
		    	// Jump the header line
		    	if(count == 0)
		    	{
		    		count ++;
		    		continue;
		    	}
		    	
		    	// Split the line in strings separates by ";"
		    	piece = line.split(";");
		    	
		    	// Check if the name components has spaces and replace them with "_"
		    	nameObject = piece[1];
		    	nameObject = nameObject.replaceAll(" ", "_");
		    	
		    	// Split the component position into the system and save how many number there are
		    	position = piece[0].split("\\.");
		    	currentDim = position.length;
		    	
		    	// In case there is only one number, the component is the root
		    	if(currentDim == 1)
		    	{
		    		// Create the first node and save it as currentNode
		    		this.systemTree = new Node<String>(nameObject);
		    		currentNode = this.systemTree;
		    		
		    		// Add type and quantity to the root
		    		currentNode.setNodeType("CompositeComponent");
		    		currentNode.setQuantity(Integer.parseInt(piece[5]));
		    		
		    		// Increment the dimensions, all the children of the root start with a dimension bigger than 1
		    		previousDim = currentDim+1;
		    		continue;
		    	}
		    	
		    	// For all the components that isn't the root
		    	if(currentDim > 1)
		    	{
		    		// In this case we return on the father node, the list of elements is finished for that component 
		    		if(currentDim < previousDim)
		    		{
		    			// In case the node need to go up more than one node...
		    			int upTo = previousDim - currentDim;
		    			Boolean childFlag = false;

		    			// In case the node need to go up more than one node...
		    			while(upTo > 0)
		    			{
		    				currentNode = currentNode.getParent();
		    				
		    				// Check for the current node if at least one child has children...
		    				for(Node<String> child : currentNode.getChildren())
			    			{
			    				if(child.hasChildren())
			    				{
			    					childFlag = true;
			    					break;
			    				}
			    			}
		    				
		    				// If yes, set the node type as composite component
		    				if(childFlag)
			    			{
			    				currentNode.setNodeType("CompositeComponent");
			    			}
		    				
		    				upTo--;
		    			}
		    			
		    			// Another check for be sure everything is correct
		    			for(Node<String> child : currentNode.getChildren())
		    			{
		    				if(child.hasChildren())
		    				{
		    					childFlag = true;
		    					break;
		    				}
		    			}
		    			if(childFlag)
		    			{
		    				currentNode.setNodeType("CompositeComponent");
		    			}
		    		}
		    		
		    		// In this case the current node is a component (as default it is set as an atomic one)
		    		if(currentDim > previousDim)
		    		{
		    			// Find the correct child index
			    		String stringIndex = position[currentDim-2];
			    		Integer index = Integer.parseInt(stringIndex);
			    		index--;
			    		
			    		// Retrieve the child and set the type
			    		currentNode = currentNode.getChildAt(index);
			    		currentNode.setNodeType("AtomicComponent");
			    	}
		    		
		    		// Create a new node, check if it is a screw or not
		    		Node<String> nodeObject = new Node<String>(nameObject);
		    		if(nameObject.toLowerCase().contains("screw"))
		    		{
		    			nodeObject.setNodeType("Screw");
		    		}
		    		else
		    		{
		    			nodeObject.setNodeType("AtomicPart");
		    		}
		    		
		    		// Add the quantity information and add it as child of the current node
		    		nodeObject.setQuantity(Integer.parseInt(piece[5]));
		    		currentNode.addChild(nodeObject);
		    	}
		    	
		    	previousDim = currentDim;
		    }
		    
		    // Last check of the system
			checkTree(this.systemTree);
			
		    reader.close();
		
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    private void print(Node<String> currentNode, String prefix, boolean isTail, String duplicateString ) {
    	System.out.println(prefix + (isTail ? "'-- " : "|-- ") + currentNode.getData() + duplicateString);
        
        int i = 0;
        for(Node<String> child : currentNode.getChildren())
        {
	    	for(int j=0; j < child.getQuantity(); j++)
	    	{
	    		String duplicate = "";
	    		if(child.getQuantity()>1)
	    			duplicate = "_"+(j+1);
				
        		if(i != currentNode.getChildren().size() - 1 )
    				print( child ,prefix + (isTail ? "    " : "|   "), false,duplicate);
        	}
        	i++;
        }
        if(currentNode.getChildren().size()>0){
        	Node<String> lastChild = currentNode.getChildAt(currentNode.getChildren().size()-1);
        
        	for(int j=0; j < lastChild.getQuantity(); j++)
	    	{
	    		String duplicate = "";
	    		if(lastChild.getQuantity()>1)
	    			duplicate = "_"+(j+1);
				
	    		if(j != lastChild.getQuantity() -1)
	    			print(currentNode.getChildAt(currentNode.getChildren().size()-1),prefix + (isTail ?"    " : "|   "), false,duplicate);
	    		else
	    			print(currentNode.getChildAt(currentNode.getChildren().size()-1),prefix + (isTail ?"    " : "|   "), true,duplicate);
	    	}
        }

    }
	
	
	/**
	 * Only public method to execute the system creation operation from file
	 */
	public String createOntologySystemFromFile(String pathFile)
	{
		// Check if the file is correct
		if(this.addAndCheckFile(pathFile))
		{
			// For be secure, fist we create the tree system
			this.createTreeSystem();
			
			// Then if the file already exists, no need to create it for avoid the overwrite of useful information
			if(this.newOntologySystem())
			{
				// Create the system's tree and add it to the ontology
				this.addSystemToOntology();
				System.out.println("System '" + this.nameSystem + "' added into the new ontology \".owl\" file '" + this.pathFileSystem + "'" );	
			}
			else
			{
				System.out.println("Sorry the file already exists or it not respect the basic requirement");
			}
			
			// Print of the system as a tree
			System.out.println("\nPrint of the system tree '" + this.nameSystem + "'");
			print(this.systemTree,"",true,"");
			System.out.println("");
			return this.nameSystem;
		}
		else
		{
			System.out.println("Sorry the file extension is not corret or it not respect the basic requirement");
		}
		return null;
	}
	
	public static void main(String[] args)
	{
		SystemReader prova = new SystemReader();
		prova.createOntologySystemFromFile("D:\\Utenti\\GMoody\\Desktop\\ScooterTry.csv");//TMS_Laser.csv");//TMS_Laser.csv"); //"provaDrone.csv");
	}
	
	 
}
