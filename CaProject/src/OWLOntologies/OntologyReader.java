package OWLOntologies;

// Java standard library import
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// OWL API library import
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

// OWL API library import for the HermiT Reasoner
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;

/**
 * Set of methods used to read the ontology and other for integration with the app.java code
 */

public class OntologyReader
{
	// OWLAPI class used to open and read an ontology (in this case the Tools one)
	private File toolsFile;

	// Variables from the OWL API library
	private DefaultPrefixManager prefixManager = null;
	private ConsoleProgressMonitor progessMonitor = null;

	private OWLOntologyManager manager;
	private OWLOntology toolsOntology = null;
	private OWLReasonerFactory reasonerFactory = null;
	private OWLReasonerConfiguration config = null;
	private OWLReasoner reasoner = null;
	private OWLDataFactory dataFactory = null;
	
	// Used to initialize the class
	public OntologyReader(String pathFile)
	{
		// Initialize the class 
		manager = OWLManager.createOWLOntologyManager();
		toolsFile = new File(pathFile);
		
		// Load all the information necessary to proceed 
		try
		{
			toolsOntology = manager.loadOntologyFromOntologyDocument(toolsFile);
			System.out.println("Loaded ontology: " + toolsOntology.getOntologyID());
			
			IRI location = manager.getOntologyDocumentIRI(toolsOntology);
			System.out.println("\tfrom: " + location); 
			
			// get and configure HermiT reasoner
			reasonerFactory = new ReasonerFactory();
			progessMonitor = new ConsoleProgressMonitor();
			config = new SimpleConfiguration(progessMonitor);
		
			prefixManager = new DefaultPrefixManager("http://www.semanticweb.org/chiara/ontologies/2016/11/toolsontology#");
			
			// dataFactory creation from the owl manager
			dataFactory = manager.getOWLDataFactory();
			
			// create the reasoner instance, classify and compute inferences
			reasoner = reasonerFactory.createReasoner(toolsOntology, config);
			reasoner.precomputeInferences(InferenceType.values());		// get all the objects
		}
		catch(OWLOntologyCreationException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Return the list of all the classes into an ontology
	 */
	public List<String> getClassList()
	{
		// Get all the objects
		Set<OWLClass> classes = toolsOntology.getClassesInSignature();
		
		List<String> oWLClasses = new ArrayList<String>();
		for(OWLClass c : classes)
		{
			oWLClasses.add(prefixManager.getShortForm(c).substring(1, prefixManager.getShortForm(c).length()));
		}
		return oWLClasses;
	}

	/**
	 * Return the list of all the action into the ontology
	 */
	public List<String> getAllObjectProperty()
	{
		Set<OWLObjectProperty> obProperty = toolsOntology.getObjectPropertiesInSignature();
		
		List<String> oWLObjectProperty = new ArrayList<String>();
		for(OWLObjectProperty op : obProperty)
		{
			oWLObjectProperty.add(prefixManager.getShortForm(op).substring(1, prefixManager.getShortForm(op).length()));
		}
		return oWLObjectProperty;
	}
	
	/**
	 * For a given dataProperty and individual, the method return a list of values for that particular data property
	 */
	public List<String> getListOfDataPropertyOfIndividual(String dataProperty, String nameIndividual)
	{
		List<String> listValueReturned = new ArrayList<String>();
		
		OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),nameIndividual));
		OWLDataProperty name = dataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix(),"name"));
		Set<OWLLiteral> listValuesDataproperty = reasoner.getDataPropertyValues(individual, name);
		
		for(OWLLiteral ol: listValuesDataproperty)
		{
			listValueReturned.add(ol.getLiteral());
		}
		
		return listValueReturned;
	}
	
	/**
	 * Get vale of the "Level" Data property for a given object, if it exists
	 */
	public Integer getComponentLevel(String component)
	{
		OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),component));
		OWLDataProperty level = dataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix(),"level"));
		
		OWLLiteral value = reasoner.getDataPropertyValues(individual, level).iterator().next();

		return Integer.parseInt(value.getLiteral());
	}
	
	/**
	 * For a given NamedIndividual, the method return the corresponding superclass
	 */
	public String getSuperClassOfIndividual(String nameIndividual)
	{
		String returnedClass = "";
		
		OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),nameIndividual));
		OWLClass superClass = reasoner.getTypes(individual, true).getFlattened().iterator().next();

		returnedClass =  prefixManager.getShortForm((OWLEntity)superClass);
		return returnedClass.substring(1);
	}
	
	/**
	 * Return the list of all the instances into a class
	 */
	public List<String> getAllIstanceInClass(String className)
	{
		
		OWLClass objects = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), className));
		
		NodeSet<OWLNamedIndividual> individualsNodeSet = reasoner.getInstances(objects, false);
		Set<OWLNamedIndividual> individualsSet = individualsNodeSet.getFlattened();
		List<String> returnedList = new ArrayList<String>();
		
		for(OWLNamedIndividual object : individualsSet)
		{
			String shortForm = prefixManager.getShortForm((OWLEntity)object);
			if(shortForm.equals(object.toString())){
				shortForm = shortForm.replaceAll(prefixManager.getDefaultPrefix(), ":");
				shortForm = shortForm.replace("<","");
				shortForm = shortForm.replace(">","");
			}
			//System.out.println(shortForm + " " + prefixManager.getDefaultPrefix());
			returnedList.add(shortForm.substring(1, shortForm.length()));
			
			//System.out.println("Individual name: " + prefixManager.getShortForm((OWLEntity)object));
		}
		return returnedList;
	}
	
	
	// Per azione, su quale classe ha range, su quali istanze
	/**
	 * Return the list of all the instances connected to an individual by an action(object property)
	 */
	public List<String> getIndividualValueforObjectProperty(String individual, String action)
	{
		OWLNamedIndividual individuals = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix() + individual));

		OWLObjectPropertyExpression actions = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + action));
	 	List<String> returnedList = new ArrayList<String>();
	 	
	 	// Versione con EntitySearcher (funziona solo con la versione 4.2.8 di OWL API)
		
//		OWLObjectPropertyImpl op = new OWLObjectPropertyImpl(IRI.create(prefixManager.getDefaultPrefix() + action ));
//		Set<OWLIndividual> prova = new HashSet<>(EntitySearcher.getObjectPropertyValues(individuals, op, toolsOntology));
//		for(OWLIndividual p : prova)
//		{
//			System.out.println("Individual name: " + prefixManager.getShortForm((OWLEntity)p));
//		}
		 
	 	// Versione col reasoner, in questo caso si ottengono anche relazioni non dirette
	 	Set<OWLNamedIndividual> individualValues = reasoner.getObjectPropertyValues(individuals, actions).getFlattened();

		for(OWLNamedIndividual p : individualValues)
		{
			String shortForm = prefixManager.getShortForm((OWLEntity)p);
			if(shortForm.equals(p.toString())){
				shortForm = shortForm.replaceAll(prefixManager.getDefaultPrefix(), ":");
				shortForm = shortForm.replace("<","");
				shortForm = shortForm.replace(">","");
			}
			returnedList.add(shortForm.substring(1, shortForm.length()));//prefixManager.getShortForm(p).substring(1, prefixManager.getShortForm(p).length()));
			//System.out.println("Individual name: " + prefixManager.getShortForm((OWLEntity)p));
		}

		return returnedList;
	}
	
	/**
	 * For a given class, return the list of subclasses.
	 */
	public List<String> getSubClasses(String superClass){
		OWLClass objects = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), superClass));

		List<String> returnedList = new ArrayList<String>();
		
		Set<OWLClass> subClasses = reasoner.getSubClasses(objects, true).getFlattened();
		for(OWLClass object : subClasses)
		{
				returnedList.add(prefixManager.getShortForm(object).substring(1, prefixManager.getShortForm(object).length()));
				//System.out.println("Class name: " + prefixManager.getShortForm(object));
		}
		
		return returnedList;
	}
	
	/**
	 * Method for the integration with the previous Fabio project
	 */
	public Integer getBOMEffectValueOfAction(String action)
	{
		OWLObjectPropertyExpression objectProperty = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + action));
		OWLAnnotationProperty BOMEffectAnnotation = dataFactory.getOWLAnnotationProperty(IRI.create(prefixManager.getDefaultPrefix() + "BOMEffect"));
				
		Set<OWLAnnotation> list = ((OWLEntity) objectProperty).getAnnotations(toolsOntology);
		for(OWLAnnotation annotation:list)
		{
			if(annotation.getProperty().equals(BOMEffectAnnotation))
			{
				return Integer.parseInt(((OWLLiteral) annotation.getValue()).getLiteral());
			}
		}
		
		return 0;
	}
	
	/**
	 * Method for the integration with the previous Fabio project
	 */
	public String getEnglishName(String individual){
		OWLNamedIndividual object = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix() + individual));
		OWLDataProperty nameProperty = dataFactory.getOWLDataProperty(IRI.create(prefixManager.getDefaultPrefix() + "name"));
		
		
		Set<OWLLiteral> values = reasoner.getDataPropertyValues(object, nameProperty);
		for(OWLLiteral value: values){
			if(value.getLang().equals("en"))
				return value.getLiteral();
		}
		
		return "";
	}
	
	/**
	 * Return the list of range classes of an action
	 */
	public List<String> getRangeClassforObjectProperty(String action)
	{
		OWLObjectPropertyExpression actions = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + action));
		
		List<String> returnedList = new ArrayList<String>();
		Set<OWLClass> individualsSet = reasoner.getObjectPropertyRanges(actions, false).getFlattened();//);
		
		for(OWLClass object : individualsSet)
		{
			// Controlla se � la radice
			if(!reasoner.getTopClassNode().contains(object))
			{
				// se NON al � aggiunge
				returnedList.add(prefixManager.getShortForm(object).substring(1, prefixManager.getShortForm(object).length()));
				System.out.println("Class name: " + prefixManager.getShortForm(object));
			}
		}
		return returnedList;
	}
	
	/**
	 * This method is created only for integrate the new project with the Fabio one.
	 * The method return true if the class with name "className" is into the ontology, false otherwise
	 */
	public boolean isClassIntoOntology(String className){
		return toolsOntology.containsClassInSignature(IRI.create(prefixManager.getDefaultPrefix(), className));
	}
	
	/*
	 * This method is created only for integrate the new project with the Fabio one.
	 * The method return true if the individual  with name "individualName" is into the ontology, false otherwise
	 **/
	public boolean isIndividualIntoOntology(String individualName){
		return toolsOntology.containsIndividualInSignature(IRI.create(prefixManager.getDefaultPrefix(), individualName));
	}
	
	
	/*	VECCHIA INTESTAZIONE 
	   
	   Il metodo funziona ma solo per le propriet� che riguardano la classe attuale
	   Per esempio per Hand non ritorna nessuna propriet� anche se dovrebbe ereditare le propriet�
	   della superclasse 
	
	   Sono riuscita ad ottenere un risultato decente MA non so se funziona se chiedi che per esempio
	   "Hand" possa usare "replaces" su "Object"
	
		// Vecchia intestazione
		//public List<String> getAllObjectPropertyInClass(String className)
	
	*/
	
	/**
	 * The method returns all the actions from axioms of a class or the ones that have that class as domain
	 * It also returns the list of range classes for action 
	 * 
	 * NUOVA INTESTAZIONE 
	 * Il metodo ritorna tutte le azioni che fanno parte degli assiomi della classe o di cui
	 * sono direttamente il dominio
	 * Inoltre per azione vengono anche ritornate le classi di range per quella particolare azione 
	 */
	public Map<String,Set<String>> getAllObjectPropertyInClass(String className)
	{
		OWLClass objects = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(), className));
		
		Map<String, Set<String>> returnedMap = new HashMap<String, Set<String>>();
		
		// Retrieve the axioms for that class
		Set<OWLClassAxiom> classAxiom = toolsOntology.getAxioms(objects);
		for(OWLClassAxiom axiom: classAxiom){

			// Select only the axioms that contains a class and it not contains the exact cardinality
	    	for(OWLClassExpression classExpression:axiom.getNestedClassExpressions())
	    	{
	            if(classExpression.getClassExpressionType()!=ClassExpressionType.OWL_CLASS && 
	               classExpression.getClassExpressionType()!=ClassExpressionType.OBJECT_EXACT_CARDINALITY)
	            {
	               OWLObjectProperty objectProperty = axiom.getObjectPropertiesInSignature().iterator().next();
	               
	               // Check if the object property has sub property, if yes add all of them
	               Set<OWLObjectPropertyExpression> opList = objectProperty.getSubProperties(toolsOntology);
	               if(opList.size() > 0)
	               {
	            	   for(OWLObjectPropertyExpression oProperty : opList)
	            	   {
	            		   String objectPropertyName = prefixManager.getShortForm((OWLEntity) oProperty);
			               objectPropertyName = objectPropertyName.substring(1, objectPropertyName.length());
			               String domainClass =  prefixManager.getShortForm(classExpression.getClassesInSignature().iterator().next());
		            	   domainClass = domainClass.substring(1, domainClass.length());
		
			               if(returnedMap.containsKey(objectPropertyName))
			               {
			            	   returnedMap.get(objectPropertyName).add(domainClass);
			               }
			               else
			               {
			            	   returnedMap.put(objectPropertyName, new HashSet<String>());
				               returnedMap.get(objectPropertyName).add(domainClass);
			               }
	            	   }
	               }
	               else
	               {
		               String objectPropertyName = prefixManager.getShortForm(objectProperty);
		               objectPropertyName = objectPropertyName.substring(1, objectPropertyName.length());
		               String domainClass =  prefixManager.getShortForm(classExpression.getClassesInSignature().iterator().next());
	            	   domainClass = domainClass.substring(1, domainClass.length());
	
		               if(returnedMap.containsKey(objectPropertyName))
		               {
		            	   returnedMap.get(objectPropertyName).add(domainClass);
		               }
		               else
		               {
		            	   returnedMap.put(objectPropertyName, new HashSet<String>());
			               returnedMap.get(objectPropertyName).add(domainClass);
		               }
		           }
	            }
	    	}
		}
		
		// Non avendo trovato un modo diretto per richiamare le propriet� dell'oggetto, ho preso la lista di
		// tutte quelle esistenti nell'ontologia
		Set<OWLObjectProperty> listProperty = toolsOntology.getObjectPropertiesInSignature();
		for(OWLObjectProperty property : listProperty){
			Set<OWLClass> rangeClasses = reasoner.getObjectPropertyRanges(property, true).getFlattened();
			Set<String> classRanges = new HashSet<String>();
			for(OWLClass rangeClass: rangeClasses){
				classRanges.add(prefixManager.getShortForm(rangeClass).substring(1, prefixManager.getShortForm(rangeClass).length()));
			}
			Set<OWLClass> strictDomainClasses = reasoner.getObjectPropertyDomains(property, true).getFlattened();

			if(strictDomainClasses.contains(objects)){
				String objectPropertyName = prefixManager.getShortForm(property);
	            objectPropertyName = objectPropertyName.substring(1, objectPropertyName.length());
	           
				//System.out.println("\t\t" + prefixManager.getShortForm(property));
				if(returnedMap.containsKey(objectPropertyName)){
	           	   returnedMap.get(objectPropertyName).addAll(classRanges);
	            }
	            else{
	        	   returnedMap.put(objectPropertyName, new HashSet<String>());
	               returnedMap.get(objectPropertyName).addAll(classRanges);
		        }
			}
			
			
			// Codice vecchio, lasciato nel caso potesse essere utile, in questo caso venivano prese le azioni che erano direttamente collegate
			// alla classe e quelle delle sue superclassi ristrette (quelle subito sopra quindi)
			/*// Qui creo la lista
			
			// Siccome non ho trovato un modo per farlo diversamente, ho preso due set di classi che hanno come domain
			// le classi su cui agiscono.
			//
			//   largeDomainClasses -> ritorna sia classi che sottoclassi (NON penso ritorni le superclassi)
			//   strictDomainClasses -> ritorna SOLO le classi dirette
			Set<OWLClass> strictDomainClasses = reasoner.getObjectPropertyDomains(property, true).getFlattened();
			Set<OWLClass> largeDomainClasses = reasoner.getObjectPropertyDomains(property, false).getFlattened();
			
			if(largeDomainClasses.contains(objects))
			{
				// Se sono direttamente collegate vanno aggiunte senza problemi alla lista
				returnedList.add(prefixManager.getShortForm(objects).substring(1, prefixManager.getShortForm(objects).length()));
				System.out.println("\t\t->Diretti: "+ prefixManager.getShortForm(property));
					
			}
			else
			{
				// Se no devo fare un po' di giri...
				// Prima prendo le superclassi della classe objects
				Set<OWLClass> superClassObject = reasoner.getSuperClasses(objects, false).getFlattened();
				
				// Stream per verificare l'esistenza della superclasse all'interno di largeDomainClasses
				for(OWLClass superClass : superClassObject)
				{
					// Se la superclasse � contenuta nelle classi ristette del dominio della propriet� e NON � la radice
					// viene aggiunto alla lista
					if(strictDomainClasses.contains(superClass) && !reasoner.getTopClassNode().contains(superClass))
					{
						returnedList.add(prefixManager.getShortForm(property).substring(1, prefixManager.getShortForm(property).length()));
						System.out.println("\t\t" + prefixManager.getShortForm(property));

					}
				}
				
			}
		*/
		}

		/*for (Map.Entry<String, Set<String>> entry : returnedMap.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}*/
		return returnedMap; 
	}
	
	public static void main(String[] args)
	{
		OntologyReader prova = new OntologyReader("ontologies\\ToolsOntology_2.owl");	
		//OntologyManualInsertion prova2 = new OntologyManualInsertion(".\\ontologies\\ToolsOntology_2.owl", false);
		//System.out.println(prova.getClassList());
		//System.out.println(prova.getAllIstanceInClass(""));
		/*System.out.println(prova.getAllObjectPropertyInClass("Component"));
		System.out.println(prova.getAllObjectPropertyInClass("Hand"));
		System.out.println(prova.getAllObjectPropertyInClass("Tool"));
		System.out.println(prova.getAllObjectPropertyInClass("Object"));
		System.out.println(prova.getAllObjectPropertyInClass("StructuralThing"));
		System.out.println(prova.getAllObjectPropertyInClass("Screw"));
		System.out.println(prova.getAllObjectPropertyInClass("ScrewDriver"));
		System.out.println(prova.getAllObjectPropertyInClass("Cloth"));
		System.out.println(prova.getAllObjectPropertyInClass("Air"));
		*/
	}
	
	
	
	/**************************************************************************************
	 *								!!!!!OLD CODE!!!!!									  *
	 * PROVA INSERIMENTO INDIVIDUI NELL'ONTOLOGIA ED INSERIMENTO DI RELAZIONI TRA DI ESSI *
	 **************************************************************************************/
//	public void cose() throws OWLOntologyStorageException{
//		OWLReasoner reasoner = reasonerFactory.createReasoner(toolsOntology, config);
//		reasoner.precomputeInferences(InferenceType.values());
//		OWLDataFactory dataFactory = manager.getOWLDataFactory();
//
//		//List<String>  listExternalTamperScrewdriver = this.getAllIstanceInClass("TamperResistant");
//		OWLObjectPropertyExpression doOperation = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "doOperation"));
//		OWLObjectPropertyExpression screws = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "screws"));
//		OWLObjectPropertyExpression unscrews = dataFactory.getOWLObjectProperty(IRI.create(prefixManager.getDefaultPrefix() + "unscrews"));
//
//		OWLClass ExternalTamperClass = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(),"TamperResistant"));
//		//OWLClass ExternalTamperScrewClass = dataFactory.getOWLClass(IRI.create(prefixManager.getDefaultPrefix(),"TamperResistantScrew"));
//
//		
//		for(OWLIndividual owlI: ExternalTamperClass.getIndividuals(toolsOntology)){
//			String onlyName = prefixManager.getShortForm((OWLEntity)owlI).substring(1, prefixManager.getShortForm((OWLEntity)owlI).length());
//			System.out.println(onlyName);
//			if(onlyName.equals("BreakawaHead"))
//				onlyName.replaceAll("BreakawaHead", "BreakawayHead");
//			OWLIndividual ind = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(), onlyName+"Screw"));
//			OWLObjectPropertyAssertionAxiom axiom1 = dataFactory.getOWLObjectPropertyAssertionAxiom(doOperation, owlI, ind);
//			OWLObjectPropertyAssertionAxiom axiom2 = dataFactory.getOWLObjectPropertyAssertionAxiom(screws, owlI, ind);
//			OWLObjectPropertyAssertionAxiom axiom3 = dataFactory.getOWLObjectPropertyAssertionAxiom(unscrews, owlI, ind);
//			AddAxiom addAxiom1 = new AddAxiom(toolsOntology, axiom1);
//            manager.applyChange(addAxiom1);
//            AddAxiom addAxiom2 = new AddAxiom(toolsOntology, axiom2);
//            manager.applyChange(addAxiom2);
//            AddAxiom addAxiom3 = new AddAxiom(toolsOntology, axiom3);
//            manager.applyChange(addAxiom3);
//            
//            manager.saveOntology(toolsOntology);
//
//		}
//		/*for(String s:listExternalTamperScrewdriver){
//			OWLIndividual screw = dataFactory.getOWLNamedIndividual(IRI.create(prefixManager.getDefaultPrefix(),s+"Screw"));
//            OWLAxiom axiom = dataFactory.getOWLClassAssertionAxiom(ExternalTamperScrewClass, screw);
//            manager.addAxiom(toolsOntology, axiom);
//            manager.saveOntology(toolsOntology);
//
//		}*/
//		
//	}
	
}
