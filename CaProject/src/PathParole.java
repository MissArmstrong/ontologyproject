

public class PathParole {
	SrlElemento parola1;
	SrlElemento parola2; 
	double path;
	boolean view; 
	
	public PathParole(SrlElemento parola1, SrlElemento parola2) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		path = 0.0;
		view = false;
	}

	public PathParole(SrlElemento parola1, SrlElemento parola2, double path) {
		this.parola1 = parola1;
		this.parola2 = parola2;
		this.path = path;
		view = false;
	}
	
	
}
