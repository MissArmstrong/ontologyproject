
import java.util.Comparator;


public class SrlElementoComparatorPerTermIS implements Comparator<SrlElemento>{

	@Override
	public int compare(SrlElemento e1, SrlElemento e2) {
		if (e1.numeroAzione != e2.numeroAzione)
			return e1.numeroAzione - e2.numeroAzione;
		if (e1.numeroVersioneFrase != e2.numeroVersioneFrase)
			return e1.numeroVersioneFrase - e2.numeroVersioneFrase;
		
		if (e1.tipo.compareTo("contesto") == 0 && e2.tipo.compareTo("azione") == 0)
			return -1;

		if (e2.tipo.compareTo("contesto") == 0 && e1.tipo.compareTo("azione") == 0)
			return 1;
		
		int i1 = Integer.parseInt(e1.term);
		int i2 = Integer.parseInt(e2.term);

		if (i1 != i2)
			return i1 - i2;
		return e1.order - e2.order;
	}

}
