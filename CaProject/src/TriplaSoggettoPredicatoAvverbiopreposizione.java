


public class TriplaSoggettoPredicatoAvverbiopreposizione extends Tripla{
	StanfordParser soggetto;
	StanfordParser predicato;
	StanfordParser avverbiopreposizione;
	StanfordParser aCompPredicato;
	StanfordParser aModSoggetto;
	StanfordParser soggettoDiPronome;

	public TriplaSoggettoPredicatoAvverbiopreposizione(StanfordParser soggetto, StanfordParser predicato, StanfordParser avverbiopreposizione) {
		super("soggettoPredicatoAvverbiopreposizione");
		this.soggetto = soggetto;
		this.predicato = predicato;
		this.avverbiopreposizione = avverbiopreposizione;
	}
	
	public String stampa(){
		String stampa = "soggettoPredicatoAvverbiopreposizione ";
		if (soggettoDiPronome != null)
			stampa += "("+soggettoDiPronome.word+")";
		if (aModSoggetto != null)
			stampa += " "+aModSoggetto.stampa();
		stampa += " "+soggetto.stampa() ;
		if (aCompPredicato != null)
			stampa += " "+aCompPredicato.stampa();
		stampa += predicato.stampa()+ " "+avverbiopreposizione.stampa()+ " "+super.relazione;
		return stampa;
		
//		
//		
//		if (aCompPredicato != null && aModSoggetto != null)
//			return "soggettoPredicatoAvverbiopreposizione"+aModSoggetto.stampa()+" "+soggetto.stampa()+ " "+predicato.stampa()+ " "+avverbiopreposizione.stampa()+ " "+aCompPredicato.stampa()+" "+super.relazione;
//		if (aCompPredicato != null)
//			return "soggettoPredicatoAvverbiopreposizione"+soggetto.stampa()+ " "+predicato.stampa()+ " "+avverbiopreposizione.stampa()+ " "+aCompPredicato.stampa()+" "+super.relazione;
//		if (aModSoggetto != null)
//			return "soggettoPredicatoAvverbiopreposizione"+aModSoggetto.stampa()+" "+soggetto.stampa()+ " "+predicato.stampa()+ " "+avverbiopreposizione.stampa()+ " "+super.relazione;
//
//		return "soggettoPredicatoAvverbiopreposizione"+soggetto.stampa()+ " "+predicato.stampa()+ " "+avverbiopreposizione.stampa()+ " "+super.relazione;
	}

	public void setACompPredicato(StanfordParser aCompPredicato){
		this.aCompPredicato = aCompPredicato;
	}

	public void setAModSoggetto(StanfordParser aModSoggetto){
		this.aModSoggetto = aModSoggetto;
	}

	public void setSoggettoDiPronome(StanfordParser soggettoDiPronome){
		this.soggettoDiPronome = soggettoDiPronome;
	}

}

