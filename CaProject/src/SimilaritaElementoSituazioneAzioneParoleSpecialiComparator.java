
import java.util.Comparator;


public class SimilaritaElementoSituazioneAzioneParoleSpecialiComparator implements Comparator<SimilaritaElementoSituazioneAzione>{

	@Override
	public int compare(SimilaritaElementoSituazioneAzione s1, SimilaritaElementoSituazioneAzione s2) {
		return (int) - (s1.similaritaPiuParoleSpeciali * 1000 - s2.similaritaPiuParoleSpeciali * 1000);
	}

}
