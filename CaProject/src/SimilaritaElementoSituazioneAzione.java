

public class SimilaritaElementoSituazioneAzione {
	ElementoSituazioneAzione elemento;
	Azione azione; 
	Double similarita;
	Double similaritaPiuTakeMake;
	Double similaritaPiuParoleSpeciali;
	Double similaritaPiuTakeMakeEParoleSpeciali;

	public SimilaritaElementoSituazioneAzione(ElementoSituazioneAzione elemento, Azione azione, Double similarita, Double similaritaPiuTakeMake, 
			Double similaritaPiuParoleSpeciali, Double similaritaPiuTakeMakeEParoleSpeciali) {
		this.elemento = elemento;
		this.azione = azione;
		this.similarita = similarita;
		this.similaritaPiuTakeMake = similaritaPiuTakeMake;
		this.similaritaPiuParoleSpeciali = similaritaPiuParoleSpeciali;
		this.similaritaPiuTakeMakeEParoleSpeciali = similaritaPiuTakeMakeEParoleSpeciali;
	}

	public SimilaritaElementoSituazioneAzione(ElementoSituazioneAzione elemento, Azione azione, Double similarita) {
		this.elemento = elemento;
		this.azione = azione;
		this.similarita = similarita;
	}

}
