Le frasi sono memorizzate all'interno della cartella "frasi"


All'interno della cartella "xFileCoppieDivise" sono memorizzati i file necessari
per la creazione delle tripleMacchina;
	nei file: 
	- Azioni.txt
	- Oggetti.txt
	- Strumenti.txt ed opzionali (- Maniere.txt , - Posizioni.txt)
	vengono definite le azioni, gli oggetti e gli strumenti permessi dal nostro sistema
	nei file:
	fileCoppieDivise(xxx).txt    dove (xxx) sta per verbo o oggetto o strumento
	vengono memorizzate le coppie di (xxx)Originale-(xxx)Macchina per ogni (xxx)Originale e (xxx)Macchina,
	vengono successivamente riuniti nel file fileCoppieDiviseUnico.txt
	questo viene processato da WordNnet creando il file fileCoppieDiviseSimilarity.txt, che contiene le misure
	di similarit� tra termini con l'algoritmo scelto in fase di processamento con WordNet.
	
La cartella xBOMandModel contiene i file (tutti i termini sono separati da una virgola ","):
	- BOM.txt: scrivere qui le BOM che si vuole il programma utilizzi
			   per scrivere una BOM : 
			   	- scrivere su di una riga per primo il nome del livello specificando sempre "lv" seguito dal numero
			   	  che lo caratterizza (es. "1"), seguito dal nome per lo specifico livello.
			   	  N.B. il primo livello e' il livello 0, il primo che verr�' creato sara' quindi lv1(...)
			   	- riempire per primi i livelli piu' prfondi e mano amano i pi� alti fino a definire il livello zero
			   	- il livello zero si apre con il nome del livello che dovra' essere definito come
			   	  "***" seguito dal nome per quella specifica BOM
	- Model.txt: scrivere qui le istanze delle classi (xxx)Model che si vuole il sistema utilizzi
				  - il file e' diviso in due  
				  	    dalla stringa ("***"): - la prima sezione crea le istanze delle classi
				  							   - la seconda popola le liste all'interno delle specifiche istanze 
				  							    creando le relazioni tra le stesse che permettono il completamento
				  							    automatico
				  - per creare un'istanza di AzioneModel:
				  		- scrivere come primo termine "a" seguito dal nome dell'istanza che si vuole creare
				  		- scrivere come secondo termine la stringa a cui dovr� fare riferimento (presa da una
				  		  tra le azioni in Azioni.txt)
				  		- scrivere come terzo terminie il tipo di verbo:
				  														- 0 opzionale, verbo normale (es. remove)
				  														- 1 verbo che non fa avanzare la BOM (es. clean)
				  														- 2 verbo che compie due azioni sulla BOM (es. replace)
				  - per creare un'istanza di OggettoModel:
				  		- scrivere come primo termine "o" seguito dal nome dell'istanza che si vuole creare
				  		- scrivere come secondo termine la stringa a cui dovr� fare riferimento (presa da una
				  		  tra gli oggetti in Oggetti.txt) (es. screw)
				  		- (opzionale) scrivere come terzo termine il nomeSpecifico del particolare oggetto (es. Phillips_screw)
				   - per creare un'istanza di StrumentoModel:
				  		- scrivere come primo termine "s" seguito dal nome dell'istanza che si vuole creare
				  		- scrivere come secondo termine la stringa a cui dovr� fare riferimento (presa da una
				  		  tra gli strumenti in Strumenti.txt) (es. screwdriver)
				  		- (opzionale) scrivere come terzo termine il nomeSpecifico del particolare Strumento (es. Phillips_screwdriver)
				   - per creare una relazione:
				        - scrivere come primo termine il nome dell'istanza di StrumentoModel scegliendola tra una di quelle che si e'
				          creata nella sezione precedente
				        - negli altri termini scrivere i nomi delle istanze di:
				        			- AzioneModel: azioni che possono essere compiute con lo specifico strumento
				        			- OggettoModel: oggetti su cui lo specifico strumento puo' agire
La cartella xRiultatiManuale contiene i file:
	- risultatiManuale.csv: tripleModel ottenute dall'elaborazione
	- risultatiCorretti: risultati corretti per come ce li aspetteremmo
	- risultatiCorrettiBOM: risultati corretti per come ce li aspetteremmo tra le frasi processate con BOM
	- risultatiCorrettiNoBOM: risultati corretti per come ce li aspetteremmo tra le frasi processate senza BOM


Iter completo di funzionamento del programma:
		- salvare una frase all' interno della cartella frasi e lamciare il programma.
		  questa verra' processata dall'analisi NLP (se ci si accorge che sta anallizando una stringo vuota 
		  interrompere e rilanciare)
		a questo punto combiare:
					app.scritturaSuFileCoppieDivise = false; in
					app.scritturaSuFileCoppieDivise = true;  e rilanciare
		il sistema scrive tutte coppie tra le parole selezionate dalle tripleOriginali calcolate al passo precedente con
		quelle presenti nei file (xxx).txt 
		- risettare app.scritturaSuFileCoppieDivise = false; e calcolare da riga di comando la similarita'
			per cygwin path  --> similarity.pl --type WordNet::Similarity::path --file C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseUnico.txt > C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseSimilarity.txt
	        per cygwin res   --> similarity.pl --type WordNet::Similarity::res --file C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseUnico.txt > C:/Users/Sofia/workspace/Loccisano_per_IS_per_tesista/xFileCoppieDivise/fileCoppieDiviseSimilarityRes.txt
		- decommentare la riga: 
			inserisciRisultatiSimilaritaCoppieDiviseInDbIS("x");  dove in x indicare il metodo
			con cui si vuole che vengano scritti ifile sql:
															- "Path"
															- "Res"
			e lanciare il programma
		- ricommentare la riga
		 	inserisciRisultatiSimilaritaCoppieDiviseInDbIS("x")
		  caricare nel database il file: querypathCoppieDivise.sql per PATH
								 oppure  querypathCoppieDiviseRes.sql per RES
		  e lanciare il programma.

Se si posseggono entrambi i file di similarit� calcolati tramite PATH e RES si puo' comandare do utilizzare solo PATH	  
	private static final boolean onlyPath = true;	
	o utilizzare entrambe le misure nella creazione delle tripleMacchina
	private static final boolean onlyPath = false;
	  
Alinterno del package model sono presenti le classi relative all'implementazione delle classi (xxx)Model
e della BOM (livelloModel)		   				        			