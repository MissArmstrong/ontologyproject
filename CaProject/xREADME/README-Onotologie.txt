﻿ README - Versione integrazione con ontologie
 Le classi contententi i metodi, che eseguono operazioni di lettura e scrittura sulle ontologie, sono contenuti nella cartella 'src/OWLOntologies'.
 L'unica classe che non esegue operazioni dirette è SystemReader che, partendo dalla BOM di un sistema, crea una nuova ontologia.
 
 La classe OntologyReader contiene una serie di metodi addetti a leggere il contenuto di un ontologia.
 Di seguito la lista dei metodi ed una breve descrizione:
 -getClassList: il metodo ritorna una lista di tutte le classi contenute nell'ontologia.
 -getAllObjectProperty: il metodo ritorna una lista di tutte le object property contenuti nell'ontologia.
 -getListOfDataPropertyOfIndividual: per una data property ed individuo, il metodo ritorna una lista di valori per quella particolare data property associata a quell'individuo.
 -getComponentLevel: metodo aggiunto come integrazione del vecchio codice con le ontologie; il metodo ritorna il livello di gerarchia a cui si trova un dato componente del sistema.
 -getSuperClassOfIndividual: per un dato individo, viene detto a quale classe appartiene.
 -getAllIstanceInClass: il metodo ritorna una lista con tutti gli individui contenuti in una classe.
 -getIndividualValueforObjectProperty: il metodo ritorna una lista di individui associati ad un altro attraverso una data object property.
 -getSubClasses: per una data classe, il metodo ritorna una lista delle sue sottoclassi.
 -getBOMEffectValueOfAction: metodo aggiunto come integrazione del vecchio codice con le ontologie; per una data object property il metodo ritorna il valore della variabile BOMEffect, se esiste.
 -getEnglishName: metodo aggiunto come integrazione del vecchio codice con le ontologie; per la data property 'name' di un individuo il metodo ritorna il valore in inglese, se esiste.
 -getRangeClassforObjectProperty: il metodo ritorna le classi su cui una data object property agisce.
 -isClassIntoOntology: il metodo controlla se una data classe esista all'interno dell'ontologia.
 -isIndividualIntoOntology: il metodo controlla se un dato individo esista all'interno dell'ontologia.
 -getAllObjectPropertyInClass: il metodo ritorna tutte le azioni che fanno parte degli assiomi della classe o di cui sono direttamente il dominio. Inoltre per azione vengono anche ritornate le classi di range per quella particolare azione.
  
 Il main per testare il programma è contenuto nel file java Simple_main.java.
 Questo tenta prima di creare un ontologia partendo dalla BOM di un sistema se non esiste, e poi esegue nella sua completezza app_integrazione.java che corrisponde all'integrazione del codice di Fabio Bellotti con le ontologie.
 app_integrazione.java ha mantenuto lo stesso main del codice originale sviluppato da Fabio ma esegue solo la "fase" iniziale del programma da come si legge sul ReadMe anch'esso scritto da Fabio, contenuto nella cartella xREADME.
 Per semplificare l'esecuzione di queste fasi, senza la necessiatà di modificare continuamente il codice del main, è possibile utilizzare i metodi analysysMethodPart (da 1 a 5). 
 Ogni metodo esegue in maniera indipendente una fase delle fasi descritta nel ReadMe originale:
 -analysysMethodPart1: esegue il programma con la variabile "scritturaSuFileCoppieDivise" a falso,
 -analysysMethodPart2: esegue il programma con la variabile "scritturaSuFileCoppieDivise" a vero,
 -analysysMethodPart3: esegue lo script perl similarity.pl
 -analysysMethodPart4: crea uno script sql ed esegue tale script sul database
 -analysysMethodPart5: riesegue il programma come al punto uno mostrando i risultati finali di tutto il processo.
 
 In app_integrazione.java tutti i metodi aggiunti per l'integrazione con le ontologie, oltre alle 5 parti del main, sono:
 -reset: inizializza tutte le variabili utilizzate ad ogni iterazione.
 -doOperationInSameLevel: durante l'operazione di montaggio/smontaggio ricerca livelli che contengono parti simili a quella trovata su cui eseguire la stessa operazione.
 -doOperationInSameObject: durante l'operazione di montaggio/smontaggio ricerca nello stesso livello parti simili a quella trovata su cui eseguire la stessa operazione (ex mettere/togliere le gambe ad un tavolo).
 -isLevelRemoved: controlla se tutte le parti di un componente(livello) sono smontate.
 -resetSystem: setta tutti i componenti e parti del sistema come montati.
 -setComponentsAsDefault: metodo ricorsivo richiamato inizialmente da resetSystem per settare tutti i componenti/parti come montati.
 -isOggettoIntoTheModel: il metodo ricerca l'oggetto con nome contenuto nella variabile "oggettoMacchina" nella gerarchia del sistema.
 -isOggettoModelOntologia: metodo ricorsivo richiamato da isOggettoIntoTheModel per eseguire la ricercaa.
 -checkIfLevelHasSubLevel: controlla se il livello corrente abbia sottolivelli (se un componente sia composito o atomico).
 Tutti questi sono metodi interni non richiamabili fuori dalla classe.
 
In aggiunta alle classi e i metodi sopra riportati, la classe OntologyManualInsertion intende offrire alcuni metodi aggiuntivi utili al completamento della descrizione dell'ontologia, inserendo ulteriori informazioni
al suo interno. I metodi sono divisi per proprieta' inseribile e ad ognuno che ne permette l'inserimento vi e' il corrispettivo per la sua lettura.
- Data Properties
  -  showOntologyDataProperties: permette di visualizzare l'elenco delle Data properties attribuite ad ogni individuo dell'ontologia
  -  insertComponentName: da' la possibilita' di inserire il nome di un individuo dell'ontologia, scegliendo anche la lingua in cui e' inserito
  -  insertComponentSize: permette di definire la grandezza dell'individuo all'interno dell'ontologia, utile nel caso di viti o cacciaviti
  -  insertComponentLevel: definisce il livello di profondita' dell'individuo all'interno della gerarchia dell'ontologia
  -  insertAdjectivePosition: si puo' definire la posizione dell'individuo nell'ontologia tramite aggettivi, quali "in basso", "in alto", ecc.
  -  insertVerticalPosition: con questo metodo si puo' esprimere la posizione verticale di un individuo all'interno del sistema, dando un riferimento spaziale, quali coordinate rispetto ad un punto di riferimento
  -  insertHorizontalPosition: con questo metodo, invece, si puo' esprimere la posizione orizzontale di un individuo all'interno del sistema, dando un riferimento spaziale, quali coordinate rispetto ad un punto di riferimento
-Object Properties
  -  showOntologyObjectProperties: permette di visualizzare l'elenco delle Object properties che collegano due individui
  -  insertConnection: da' la possibilita' di definire un legame di "connessione" tra due individui all'interno dell'ontologia
  -  insertInclusion: definisce una relazione di inclusione tra due elementi, uno padre e l'altro figlio
  -  insertOperation: con questo metodo si puo' inserire una nuova operazione nell'ontologia, andando a definire quali sono le classi coinvolte 
